<ul class="nav nav-tabs mb-4 timeline-tab">
    <li class="nav-item dropdown">
        <a href="javascript:;" class="nav-link {{ controllerAction() == 'Automation2Controller@contacts' ? 'active' : '' }}" onclick="timelinePopup.load('{{ action([\App\Http\Controllers\Automation2Controller::class, 'contacts'], [
            'uid' => $automation->uid,
        ]) }}')">
            {{ trans('messages.automation.audience') }}
        </a>
    </li>
    <li class="nav-item dropdown">
        <a href="javascript:;" class="nav-link {{ controllerAction() == 'Automation2Controller@timeline' ? 'active' : '' }}" onclick="timelinePopup.load('{{ action([\App\Http\Controllers\Automation2Controller::class, 'timeline'], $automation->uid) }}')">
            {{ trans('messages.automation.timeline') }}
        </a>
    </li>
</ul>

<script>
    @if (isset($tab))
        $('.timeline-tab .nav-link.{{ $tab }}').addClass('active');
    @endif
</script>
