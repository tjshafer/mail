<ul class="nav nav-tabs mc-nav mb-3" id="pills-tab" role="tablist">
    @foreach (App\Model\TemplateCategory::all() as $cat)
        <li class="nav-item {{ isset($category) && $category->uid == $cat->uid ? 'active' : '' }}">
            <a class="choose-template-tab" href="{{ action([\App\Http\Controllers\CampaignController::class, 'templateLayout'], [
                'uid' => $campaign->uid,
                'category_uid' => $cat->uid,
            ]) }}">
                {{ $cat->name }}
            </a>
        </li>
    @endforeach
    <li class="nav-item {{ actionName() == 'templateUpload' ? 'active' : '' }}"><a class="choose-template-tab nav-link" href="{{ action([\App\Http\Controllers\CampaignController::class, 'templateUpload'], $campaign->uid) }}">Upload</a></li>
</ul>
