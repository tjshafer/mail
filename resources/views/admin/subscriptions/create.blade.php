@extends('layouts.popup.medium')

@section('content')
	<div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h2 class="mt-0">{{ trans('messages.subscription.create_new_subsctiption') }}</h2>
            <p>{{ trans('messages.subscription.create_new_subsctiption.intro') }}</p>

            <form enctype="multipart/form-data" action="{{ action([\App\Http\Controllers\Admin\SubscriptionController::class, 'create']) }}" method="POST" class="subscription-form form-validate-jqueryx">
                @csrf

                <div class="row">
                    <div class="col-md-6">
                        @include('helpers.form_control', [
                            'type' => 'select_ajax',
                            'class' => '',
                            'name' => 'customer_uid',
                            'label' => trans('messages.select_customer'),
                            'help_class' => 'subscription',
                            'rules' => $rules,
                            'url' => action([\App\Http\Controllers\Admin\CustomerController::class, 'select2']),
                            'placeholder' => trans('messages.select_customer')
                        ])

                        @include('helpers.form_control', [
                            'type' => 'select_ajax',
                            'class' => '',
                            'name' => 'plan_uid',
                            'label' => trans('messages.select_plan'),
                            'help_class' => 'subscription',
                            'rules' => $rules,
                            'url' => action([\App\Http\Controllers\Admin\PlanController::class, 'select2']),
                            'placeholder' => trans('messages.select_plan')
                        ])
                    </div>
                </div>

                <button class="btn btn-primary bg-grey mt-3">{{ trans('messages.subscription.create') }}</button>
            </form>
        </div>
    </div>

    <script>
        $('.subscription-form').submit(function(e) {
            e.preventDefault();

            var url = $(this).attr('action');
            var data = $(this).serialize();

            $.ajax({
                url: url,
                method: 'POST',
                data: data,
                statusCode: {
                    // validate error
                    400: function (res) {
                        newSubscription.loadHtml(res.responseText);
                    }
                },
                success: function (response) {
                    newSubscription.hide();

                    // notify
                    notify('success', '{{ trans('messages.notify.success') }}', response.message);

                    tableFilterAll();
                }
            });
        });
    </script>
@endsection
