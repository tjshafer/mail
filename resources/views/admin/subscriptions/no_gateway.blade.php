<div class="alert alert-danger">
    {!! trans('messages.admin.no_primary_payment', [
        'link' => action([\App\Http\Controllers\Admin\PaymentController::class, 'index']),
    ]) !!}
</div>
