<ul class="nav nav-tabs nav-tabs-top">
	@if (Auth::user()->admin->getPermission("setting_general") == 'yes')
		<li class="{{ $action == "general" ? "active" : "" }} text-semibold">
			<a href="{{ action([\App\Http\Controllers\Admin\SettingController::class, 'general']) }}">
			<i class="icon-equalizer2"></i> {{ trans('messages.general') }}</a></li>
	@endif
	@if (Auth::user()->admin->getPermission("setting_general") == 'yes')
		<li class="{{ $action == "mailer" ? "active" : "" }} text-semibold">
			<a href="{{ action([\App\Http\Controllers\Admin\SettingController::class, 'mailer']) }}">
			<i class="icon-envelop"></i> {{ trans('messages.system_email') }}</a></li>
	@endif
	@if (Auth::user()->admin->getPermission("setting_sending") == 'yes' && false)
		<li class="{{ $action == "sending" ? "active" : "" }} text-semibold">
			<a href="{{ action([\App\Http\Controllers\Admin\SettingController::class, 'sending']) }}">
			<i class="icon-paperplane"></i> {{ trans('messages.sending') }}</a></li>
	@endif
	@if (Auth::user()->admin->getPermission("setting_system_urls") == 'yes')
		<li class="{{ $action == "urls" ? "active" : "" }} text-semibold">
			<a href="{{ action([\App\Http\Controllers\Admin\SettingController::class, 'urls']) }}">
			<i class="icon-link"></i> {{ trans('messages.system_urls') }}</a></li>
	@endif
	@if (Auth::user()->admin->getPermission("setting_background_job") == 'yes')
		<li class="{{ $action == "cronjob" ? "active" : "" }} text-semibold">
			<a href="{{ action([\App\Http\Controllers\Admin\SettingController::class, 'cronjob']) }}">
			<i class="icon-alarm"></i> {{ trans('messages.background_job') }}</a></li>
	@endif

</ul>
