<div class="row">
    <div class="col-md-12">
        <div class="tabbable">
            <ul class="nav nav-tabs nav-tabs-top page-second-nav">
                <li rel0="AccountController/profile">
                    <a href="{{ action([\App\Http\Controllers\Admin\AccountController::class, 'profile']) }}" class="level-1">
                        <i class="icon-user position-left"></i> {{ trans('messages.my_profile') }}
                    </a>
                </li>
                <li rel0="AccountController/contact">
                    <a href="{{ action([\App\Http\Controllers\Admin\AccountController::class, 'contact']) }}" class="level-1">
                        <i class="icon-office position-left"></i> {{ trans('messages.contact_information') }}
                    </a>
                </li>
                <li rel0="AccountController/api">
                    <a href="{{ action([\App\Http\Controllers\Admin\AccountController::class, 'api']) }}" class="level-1">
                        <i class="icon-key position-left"></i> {{ trans('messages.api_token') }}
                    </a>
                </li>
                <li rel0="NotificationController">
                    <a href="{{ action([\App\Http\Controllers\Admin\NotificationController::class, 'index']) }}" class="level-1">
                        <i class="icon-history position-left"></i> {{ trans('messages.notifications') }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
