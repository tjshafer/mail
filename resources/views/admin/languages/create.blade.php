@extends('layouts.backend')

@section('title', trans('messages.create_language'))

@section('page_script')
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/validate.js') }}"></script>
@endsection

@section('page_header')

			<div class="page-title">
				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="{{ action([\App\Http\Controllers\Admin\HomeController::class, 'index']) }}">{{ trans('messages.home') }}</a></li>
				</ul>
				<h1>
					<span class="text-semibold"><i class="icon-plus-circle2"></i> {{ trans('messages.create_language') }}</span>
				</h1>
			</div>

@endsection

@section('content')
                <form action="{{ action([\App\Http\Controllers\Admin\LanguageController::class, 'store']) }}" method="POST" class="form-validate-jqueryz">
					@csrf

					@include('admin.languages._form')
				<form>

@endsection
