@extends('layouts.backend')

@section('title', $language->name)

@section('page_script')
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/validate.js') }}"></script>
@endsection

@section('page_header')

			<div class="page-title">
				<ul class="breadcrumb breadcrumb-caret position-right">
					<li><a href="{{ action([\App\Http\Controllers\Admin\HomeController::class, 'index']) }}">{{ trans('messages.home') }}</a></li>
				</ul>
				<h1>
					<span class="text-semibold"><i class="icon-pencil"></i> {{ $language->name }}</span>
				</h1>
			</div>

@endsection

@section('content')

				<form enctype="multipart/form-data" action="{{ action([\App\Http\Controllers\Admin\LanguageController::class, 'update'], $language->uid) }}" method="POST" class="form-validate-jquery">
					@csrf
					@method('PATCH')

					@include('admin.languages._form')

				<form>

@endsection
