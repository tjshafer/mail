@extends('layouts.backend')

@section('title', $customer->user->displayName())

@section('page_script')
    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/validate.js') }}"></script>
@endsection

@section('page_header')

    <div class="page-title">
        <ul class="breadcrumb breadcrumb-caret position-right">
            <li><a href="{{ action([\App\Http\Controllers\HomeController::class, 'index']) }}">{{ trans('messages.home') }}</a></li>
            <li><a href="{{ action([\App\Http\Controllers\Admin\CustomerController::class, 'index']) }}">{{ trans('messages.customers') }}</a></li>
            <li class="active">{{ trans('messages.update') }}</li>
        </ul>
        <h1>
            <span class="text-semibold"><i class="icon-profile"></i> {{ $customer->user->displayName() }}</span>
        </h1>
    </div>

@endsection

@section('content')
    @include('admin.customers._tabs')

    <form enctype="multipart/form-data" action="{{ action([\App\Http\Controllers\Admin\CustomerController::class, 'update'], $customer->uid) }}" method="POST" class="form-validate-jquery">
        @csrf
        @method('PATCH')

        @include('admin.customers._form')

    <form>
@endsection
