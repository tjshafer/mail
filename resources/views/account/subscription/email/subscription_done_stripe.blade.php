<p>{!! trans('messages.subscription_done.stripe.email', [
    'customer' => $customerName,
    'plan' => $planName,
    'link' => $link,
]) !!}</p>

--<br>
{{ \App\Model\Setting::get('site_name') }}
