<?php

use App\Http\Controllers\Api;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware('auth:api')->group(function () {
    //
    Route::get('', fn() => response()->json(\Auth::guard('api')->user()));

    // Simple authentication
    Route::get('me', fn() => response()->json(\Auth::guard('api')->user()));

    // List
    Route::post('lists/{uid}/add-field', [Api\MailListController::class, 'addField']);
    Route::resource('lists', Api\MailListController::class);

    // Campaign
    Route::post('campaigns/{uid}/pause', [Api\CampaignController::class, 'pause']);
    Route::resource('campaigns', Api\CampaignController::class);

    // Subscriber
    Route::post('subscribers/{uid}/add-tag', [Api\SubscriberController::class, 'addTag']);
    Route::get('subscribers/email/{email}', [Api\SubscriberController::class, 'showByEmail']);
    Route::patch('subscribers/{uid}/subscribe', [Api\SubscriberController::class, 'subscribe']);
    Route::patch('subscribers/{uid}/unsubscribe', [Api\SubscriberController::class, 'unsubscribe']);
    Route::delete('subscribers/{uid}', [Api\SubscriberController::class, 'delete']);

    Route::resource('subscribers', Api\SubscriberController::class);

    // Automation
    Route::post('automations/{uid}/api/call', [Api\AutomationController::class, 'apiCall']);

    // Sending server
    Route::resource('sending_servers', Api\SendingServerController::class);

    // Plan
    Route::resource('plans', Api\PlanController::class);

    // Customer
    Route::match(['get', 'post'], 'login-token', [Api\CustomerController::class, 'loginToken']);
    Route::post('customers/{uid}/assign-plan/{plan_uid}', [Api\CustomerController::class, 'assignPlan']);
    Route::patch('customers/{uid}/disable', [Api\CustomerController::class, 'disable']);
    Route::patch('customers/{uid}/enable', [Api\CustomerController::class, 'enable']);
    Route::resource('customers', Api\CustomerController::class);

    // Subscription
    Route::resource('subscriptions', Api\SubscriptionController::class);

    // File
    Route::post('file/upload', [Api\FileController::class, 'upload']);

    // File
    Route::post('automations/{uid}/execute', [Api\AutomationController::class, 'execute'])->name('automation_execute');

    // Subscription
    Route::post('notification', [Api\NotificationController::class, 'index']);
});
