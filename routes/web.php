<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AccountSubscriptionController;
use App\Http\Controllers\Admin;

use App\Http\Controllers\AutoTrigger;
use App\Http\Controllers\Automation2Controller;
use App\Http\Controllers\BlacklistController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\EmailVerificationServerController;
use App\Http\Controllers\FieldController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MailListController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SamplesController;
use App\Http\Controllers\SegmentController;
use App\Http\Controllers\SenderController;
use App\Http\Controllers\SendingDomainController;
use App\Http\Controllers\SendingServerController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SourceController;
use App\Http\Controllers\SubscriberController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\TrackingDomainController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 * The following routes
 * + customer_files
 * + customer_thumbs
 *
 * are used to retrieved files through filemanager
 *
 */
Route::get('/files/{uid}/{name?}', [function ($uid, $name) {
    $path = storage_path('app/users/'.$uid.'/home/files/'.$name);
    $mime_type =  \App\Library\File::getFileType($path);
    if (\File::exists($path)) {
        return response()->file($path, ['Content-Type' => $mime_type]);
    } else {
        abort(404);
    }
}])->where('name', '.+')->name('user_files');

// assets path for customer thumbs
Route::get('/thumbs/{uid}/{name?}', [function ($uid, $name) {
    $path = storage_path('app/users/'.$uid.'/home/thumbs/'.$name);
    if (\File::exists($path)) {
        $mime_type =  \App\Library\File::getFileType($path);

        return response()->file($path, ['Content-Type' => $mime_type]);
    } else {
        abort(404);
    }
}])->where('name', '.+')->name('user_thumbs');

// assets path for email
Route::get('/p/assets/{path}', [function ($token) {
    // Notice $path should be relative only for Appmail/storage/ folder
    // For example, with a real path of /home/deploy/Appmail/storage/app/sub/example.png => $path = "app/sub/example.png"
    $decodedPath =  \App\Library\StringHelper::base64UrlDecode($token);
    $absPath = storage_path($decodedPath);

    if (\File::exists($absPath)) {
        $mime_type =  \App\Library\File::getFileType($absPath);

        return response()->file($absPath, ['Content-Type' => $mime_type]);
    } else {
        abort(404);
    }
}])->name('public_assets');

// Setting upload path
Route::get('setting/{filename}', [SettingController::class, 'file']);
Route::get('/no-plan', [AccountSubscriptionController::class, 'noPlan']);

// Translation data
Route::get('/datatable_locale', [Controller::class, 'datatable_locale']);
Route::get('/jquery_validate_locale', [Controller::class, 'jquery_validate_locale']);

// For visitor with Web UI, loading the right app language
Route::middleware('not_logged_in')->group(function () {
    // Helper method to generate other routes for authentication
    Auth::routes();

    Route::get('/login/token/{token}', [Controller::class, 'tokenLogin']);

    Route::get('user/activate/{token}', [UserController::class, 'activate']);
    Route::get('/disabled', [Controller::class, 'userDisabled']);
    Route::get('/offline', [Controller::class, 'offline']);
    Route::get('/not-authorized', [Controller::class, 'notAuthorized']);
    Route::get('/demo', [Controller::class, 'demo']);
    Route::get('/demo/go/{view}', [Controller::class, 'demoGo']);
    Route::get('/autologin/{api_token}', [Controller::class, 'autoLogin']);
    Route::get('/reload/cache', [Controller::class, 'reloadCache']);
    Route::get('/migrate/run', [Controller::class, 'runMigration']);
    Route::post('/remote-job/{remote_job_token}', [Controller::class, 'remoteJob']);

    //Subscriber avatar
    Route::get('assets/images/avatar/subscriber-{uid?}.jpg', [SubscriberController::class, 'avatar']);

    // User resend activation email
    Route::get('users/resend-activation-email', [UserController::class, 'resendActivationEmail']);

    // Plan
    Route::get('plans/select2', [PlanController::class, 'select2']);

    // Customer registration
    Route::post('users/register', [UserController::class, 'register']);
    Route::get('users/register', [UserController::class, 'register']);
});

// Without authentication


Route::middleware('auth')->group(function () {
    // get files from download
    Route::get('/download/{name?}', [function ($name) {
        $path = storage_path('app/download/'.$name);
        if (\File::exists($path)) {
            return response()->download($path);
        } else {
            abort(404);
        }
    }])->where('name', '.+')->name('download');
});

Route::middleware('auth', 'frontend')->group(function () {
    // Account subscription
    Route::post('subscription/invoice/{invoice_uid}/cancel', [AccountSubscriptionController::class, 'cancelInvoice']);
    Route::match(['get', 'post'], 'subscription/change-plan', [AccountSubscriptionController::class, 'changePlan']);
    Route::post('subscription/init', [AccountSubscriptionController::class, 'init']);
    Route::get('subscription', [AccountSubscriptionController::class, 'index']);
    Route::post('subscription/checkout', [AccountSubscriptionController::class, 'checkout']);
    Route::get('subscription/payment', [AccountSubscriptionController::class, 'payment']);
    Route::post('subscription/cancel-now', [AccountSubscriptionController::class, 'cancelNow']);
    Route::post('subscription/resume', [AccountSubscriptionController::class, 'resume']);
    Route::post('subscription/cancel', [AccountSubscriptionController::class, 'cancel']);

    // Customer login back
    Route::get('customers/login-back', [CustomerController::class, 'loginBack']);
    Route::get('customers/admin-area', [CustomerController::class, 'adminArea']);

    // Customer profile/information
    Route::post('account/payment/remove', [AccountController::class, 'removePaymentMethod']);
    Route::match(['get', 'post'], 'account/payment/edit', [AccountController::class, 'editPaymentMethod']);
    Route::get('account/profile', [AccountController::class, 'profile']);
    Route::post('account/profile', [AccountController::class, 'profile']);
    Route::get('account/contact', [AccountController::class, 'contact']);
    Route::post('account/contact', [AccountController::class, 'contact']);
    Route::get('account/logs', [AccountController::class, 'logs']);
    Route::get('account/logs/listing', [AccountController::class, 'logsListing']);
    Route::get('account/quota_log_2', [AccountController::class, 'quotaLog2']);
    Route::get('account/quota_log', [AccountController::class, 'quotaLog']);
    Route::get('account/billing', [AccountController::class, 'billing']);
    Route::match(['get', 'post'], 'account/billing/edit', [AccountController::class, 'editBillingAddress']);
});

Route::middleware('auth', 'frontend', 'subscription')->group(function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('frontend/docs/api/v1', [Controller::class, 'docsApiV1']);

    Route::get('account/api/renew', [AccountController::class, 'renewToken']);
    Route::get('account/api', [AccountController::class, 'api']);

    // Mail list
    Route::get('lists/{uid}/clone-to-customers/choose', [MailListController::class, 'cloneForCustomersChoose']);
    Route::post('lists/{uid}/clone-to-customers', [MailListController::class, 'cloneForCustomers']);

    Route::get('lists/{uid}/verification/{job_uid}/progress', [MailListController::class, 'verificationProgress']);
    Route::get('lists/{uid}/verification', [MailListController::class, 'verification']);
    Route::post('lists/{uid}/verification/start', [MailListController::class, 'startVerification']);
    Route::post('lists/{uid}/verification/{job_uid}/stop', [MailListController::class, 'stopVerification']);
    Route::post('lists/{uid}/verification/reset', [MailListController::class, 'resetVerification']);
    Route::post('lists/copy', [MailListController::class, 'copy']);
    Route::get('lists/quick-view', [MailListController::class, 'quickView']);
    Route::get('lists/{uid}/list-growth', [MailListController::class, 'listGrowthChart']);
    Route::get('lists/{uid}/list-statistics-chart', [MailListController::class, 'statisticsChart']);
    Route::get('lists/sort', [MailListController::class, 'sort']);
    Route::get('lists/listing/{page?}', [MailListController::class, 'listing']);
    Route::get('lists/delete', [MailListController::class, 'delete']);
    Route::get('lists/delete/confirm', [MailListController::class, 'deleteConfirm']);
    Route::get('lists/{uid}/overview', [MailListController::class, 'overview'])->name('mail_list');
    Route::resource('lists', MailListController::class);
    Route::get('lists/{uid}/edit', [MailListController::class, 'edit']);
    Route::patch('lists/{uid}/update', [MailListController::class, 'update']);
    Route::get('lists/{uid}/embedded-form', [MailListController::class, 'embeddedForm']);
    Route::get('lists/{uid}/embedded-form-frame', [MailListController::class, 'embeddedFormFrame']);

    // Field
    Route::get('lists/{list_uid}/fields', [FieldController::class, 'index']);
    Route::get('lists/{list_uid}/fields/sort', [FieldController::class, 'sort']);
    Route::post('lists/{list_uid}/fields/store', [FieldController::class, 'store']);
    Route::get('lists/{list_uid}/fields/sample/{type}', [FieldController::class, 'sample']);
    Route::get('lists/{list_uid}/fields/{uid}/delete', [FieldController::class, 'delete']);

    // Subscriber
    Route::match(['get', 'post'], 'lists/{list_uid}/subscribers/assign-values', [SubscriberController::class, 'assignValues']);
    Route::match(['get', 'post'], 'lists/{list_uid}/subscribers/bulk-delete', [SubscriberController::class, 'bulkDelete']);

    Route::post('lists/{list_uid}/subscriber/{uid}/remove-tag', [SubscriberController::class, 'removeTag']);
    Route::match(['get', 'post'], 'lists/{list_uid}/subscriber/{uid}/update-tags', [SubscriberController::class, 'updateTags']);

    Route::post('lists/{list_uid}/subscribers/resend/confirmation-email/{uids?}', [SubscriberController::class, 'resendConfirmationEmail']);
    Route::post('subscriber/{uid}/verification/start', [SubscriberController::class, 'startVerification']);
    Route::post('subscriber/{uid}/verification/reset', [SubscriberController::class, 'resetVerification']);
    Route::get('lists/{from_uid}/copy-move-from/{action}', [SubscriberController::class, 'copyMoveForm']);
    Route::post('subscribers/move', [SubscriberController::class, 'move']);
    Route::post('subscribers/copy', [SubscriberController::class, 'copy']);
    Route::get('lists/{list_uid}/subscribers', [SubscriberController::class, 'index']);
    Route::get('lists/{list_uid}/subscribers/create', [SubscriberController::class, 'create']);
    Route::get('lists/{list_uid}/subscribers/listing', [SubscriberController::class, 'listing']);
    Route::post('lists/{list_uid}/subscribers/store', [SubscriberController::class, 'store']);
    Route::get('lists/{list_uid}/subscribers/{uid}/edit', [SubscriberController::class, 'edit']);
    Route::patch('lists/{list_uid}/subscribers/{uid}/update', [SubscriberController::class, 'update']);
    Route::post('lists/{list_uid}/subscribers/delete', [SubscriberController::class, 'delete']);
    Route::get('lists/{list_uid}/subscribers/delete', [SubscriberController::class, 'delete']);
    Route::get('lists/{list_uid}/subscribers/subscribe', [SubscriberController::class, 'subscribe']);
    Route::get('lists/{list_uid}/subscribers/unsubscribe', [SubscriberController::class, 'unsubscribe']);

    Route::get('lists/{list_uid}/subscribers/import', [SubscriberController::class, 'import']);
    Route::post('lists/{list_uid}/subscribers/import/dispatch', [SubscriberController::class, 'dispatchImportJob']);
    Route::get('lists/{list_uid}/import/{job_uid}/progress', [SubscriberController::class, 'importProgress']);
    Route::get('lists/import/{job_uid}/log/download', [SubscriberController::class, 'downloadImportLog']);
    Route::post('lists/import/{job_uid}/cancel', [SubscriberController::class, 'cancelImport']);

    Route::get('lists/{list_uid}/subscribers/export', [SubscriberController::class, 'export']);
    Route::post('lists/{list_uid}/subscribers/export/dispatch', [SubscriberController::class, 'dispatchExportJob']);
    Route::get('lists/export/{job_uid}/progress', [SubscriberController::class, 'exportProgress']);
    Route::get('lists/export/{job_uid}/log/download', [SubscriberController::class, 'downloadExportedFile']);
    Route::post('lists/export/{job_uid}/cancel', [SubscriberController::class, 'cancelExport']);

    // Segment
    Route::get('segments/condition-value-control', [SegmentController::class, 'conditionValueControl']);
    Route::get('segments/select_box', [SegmentController::class, 'selectBox']);
    Route::get('lists/{list_uid}/segments', [SegmentController::class, 'index']);
    Route::get('lists/{list_uid}/segments/{uid}/subscribers', [SegmentController::class, 'subscribers']);
    Route::get('lists/{list_uid}/segments/{uid}/listing_subscribers', [SegmentController::class, 'listing_subscribers']);
    Route::get('lists/{list_uid}/segments/create', [SegmentController::class, 'create']);
    Route::get('lists/{list_uid}/segments/listing', [SegmentController::class, 'listing']);
    Route::post('lists/{list_uid}/segments/store', [SegmentController::class, 'store']);
    Route::get('lists/{list_uid}/segments/{uid}/edit', [SegmentController::class, 'edit']);
    Route::patch('lists/{list_uid}/segments/{uid}/update', [SegmentController::class, 'update']);
    Route::get('lists/{list_uid}/segments/delete', [SegmentController::class, 'delete']);
    Route::get('lists/{list_uid}/segments/sample_condition', [SegmentController::class, 'sample_condition']);

    // Page
    Route::get('lists/{list_uid}/pages/{alias}/update', [PageController::class, 'update']);
    Route::post('lists/{list_uid}/pages/{alias}/update', [PageController::class, 'update']);
    Route::post('lists/{list_uid}/pages/{alias}/preview', [PageController::class, 'preview']);

    // Template
    Route::match(['get', 'post'], 'templates/{uid}/categories', [TemplateController::class, 'categories']);

    Route::match(['get', 'post'], 'templates/{uid}/update-thumb-url', [TemplateController::class, 'updateThumbUrl']);
    Route::match(['get', 'post'], 'templates/{uid}/update-thumb', [TemplateController::class, 'updateThumb']);

    Route::get('templates/{uid}/builder/change-template/{change_uid}', [TemplateController::class, 'builderChangeTemplate']);
    Route::get('templates/builder/templates/{category_uid?}', [TemplateController::class, 'builderTemplates']);
    Route::post('templates/builder/create', [TemplateController::class, 'builderCreate']);
    Route::get('templates/builder/create', [TemplateController::class, 'builderCreate']);
    Route::post('templates/{uid}/builder/edit/asset', [TemplateController::class, 'uploadTemplateAssets']);
    Route::get('templates/{uid}/builder/edit/content', [TemplateController::class, 'builderEditContent']);
    Route::post('templates/{uid}/builder/edit', [TemplateController::class, 'builderEdit']);
    Route::get('templates/{uid}/builder/edit', [TemplateController::class, 'builderEdit']);

    Route::post('templates/{uid}/copy', [TemplateController::class, 'copy']);
    Route::get('templates/{uid}/copy', [TemplateController::class, 'copy']);
    Route::get('templates/listing/{page?}', [TemplateController::class, 'listing']);
    Route::get('templates/choosing/{campaign_uid}/{page?}', [TemplateController::class, 'choosing']);
    Route::get('templates/upload', [TemplateController::class, 'uploadTemplate']);
    Route::post('templates/upload', [TemplateController::class, 'uploadTemplate']);
    Route::get('templates/{uid}/preview', [TemplateController::class, 'preview']);
    Route::get('templates/delete', [TemplateController::class, 'delete']);
    Route::resource('templates', TemplateController::class);
    Route::get('templates/{uid}/edit', [TemplateController::class, 'edit']);
    Route::patch('templates/{uid}/update', [TemplateController::class, 'update']);

    // Campaign
    Route::get('campaigns/{uid}/chart2', [CampaignController::class, 'chart2']);
    Route::post('campaigns/{uid}/remove-attachment', [CampaignController::class, 'removeAttachment']);
    Route::get('campaigns/{uid}/download-attachment', [CampaignController::class, 'downloadAttachment']);
    Route::post('campaigns/{uid}/upload-attachment', [CampaignController::class, 'uploadAttachment']);
    Route::get('campaigns/{uid}/template/builder-select', [CampaignController::class, 'templateBuilderSelect']);

    Route::match(['get', 'post'], 'campaigns/{uid}/template/builder-plain', [CampaignController::class, 'builderPlainEdit']);
    Route::match(['get', 'post'], 'campaigns/{uid}/template/builder-classic', [CampaignController::class, 'builderClassic']);
    Route::match(['get', 'post'], 'campaigns/{uid}/plain', [CampaignController::class, 'plain']);
    Route::get('campaigns/{uid}/template/change/{template_uid}', [CampaignController::class, 'templateChangeTemplate']);

    Route::get('campaigns/{uid}/template/content', [CampaignController::class, 'templateContent']);
    Route::match(['get', 'post'], 'campaigns/{uid}/template/edit', [CampaignController::class, 'templateEdit']);
    Route::match(['get', 'post'], 'campaigns/{uid}/template/upload', [CampaignController::class, 'templateUpload']);
    Route::get('campaigns/{uid}/template/layout/list', [CampaignController::class, 'templateLayoutList']);
    Route::match(['get', 'post'], 'campaigns/{uid}/template/layout', [CampaignController::class, 'templateLayout']);
    Route::get('campaigns/{uid}/template/create', [CampaignController::class, 'templateCreate']);

    Route::get('campaigns/{uid}/spam-score', [CampaignController::class, 'spamScore']);
    Route::get('campaigns/{from_uid}/copy-move-from/{action}', [CampaignController::class, 'copyMoveForm']);
    Route::match(['get', 'post'], 'campaigns/{uid}/resend', [CampaignController::class, 'resend']);
    Route::get('campaigns/{uid}/tracking-log/download', [CampaignController::class, 'trackingLogDownload']);
    Route::get('campaigns/job/{uid}/progress', [CampaignController::class, 'trackingLogExportProgress']);
    Route::get('campaigns/job/{uid}/download', [CampaignController::class, 'download']);

    Route::get('campaigns/{uid}/template/review-iframe', [CampaignController::class, 'templateReviewIframe']);
    Route::get('campaigns/{uid}/template/review', [CampaignController::class, 'templateReview']);
    Route::get('campaigns/select-type', [CampaignController::class, 'selectType']);
    Route::get('campaigns/{uid}/list-segment-form', [CampaignController::class, 'listSegmentForm']);
    Route::get('campaigns/{uid}/preview/content/{subscriber_uid?}', [CampaignController::class, 'previewContent']);
    Route::get('campaigns/{uid}/preview', [CampaignController::class, 'preview']);
    Route::post('campaigns/send-test-email', [CampaignController::class, 'sendTestEmail']);
    Route::get('campaigns/delete/confirm', [CampaignController::class, 'deleteConfirm']);
    Route::post('campaigns/copy', [CampaignController::class, 'copy']);
    Route::get('campaigns/{uid}/subscribers', [CampaignController::class, 'subscribers']);
    Route::get('campaigns/{uid}/subscribers/listing', [CampaignController::class, 'subscribersListing']);
    Route::get('campaigns/{uid}/open-map', [CampaignController::class, 'openMap']);
    Route::get('campaigns/{uid}/tracking-log', [CampaignController::class, 'trackingLog']);
    Route::get('campaigns/{uid}/tracking-log/listing', [CampaignController::class, 'trackingLogListing']);
    Route::get('campaigns/{uid}/bounce-log', [CampaignController::class, 'bounceLog']);
    Route::get('campaigns/{uid}/bounce-log/listing', [CampaignController::class, 'bounceLogListing']);
    Route::get('campaigns/{uid}/feedback-log', [CampaignController::class, 'feedbackLog']);
    Route::get('campaigns/{uid}/feedback-log/listing', [CampaignController::class, 'feedbackLogListing']);
    Route::get('campaigns/{uid}/open-log', [CampaignController::class, 'openLog']);
    Route::get('campaigns/{uid}/open-log/listing', [CampaignController::class, 'openLogListing']);
    Route::get('campaigns/{uid}/click-log', [CampaignController::class, 'clickLog']);
    Route::get('campaigns/{uid}/click-log/listing', [CampaignController::class, 'clickLogListing']);
    Route::get('campaigns/{uid}/unsubscribe-log', [CampaignController::class, 'unsubscribeLog']);
    Route::get('campaigns/{uid}/unsubscribe-log/listing', [CampaignController::class, 'unsubscribeLogListing']);

    Route::get('campaigns/quick-view', [CampaignController::class, 'quickView']);
    Route::get('campaigns/{uid}/chart24h', [CampaignController::class, 'chart24h']);
    Route::get('campaigns/{uid}/chart', [CampaignController::class, 'chart']);
    Route::get('campaigns/{uid}/chart/countries/open', [CampaignController::class, 'chartCountry']);
    Route::get('campaigns/{uid}/chart/countries/click', [CampaignController::class, 'chartClickCountry']);
    Route::get('campaigns/{uid}/overview', [CampaignController::class, 'overview']);
    Route::get('campaigns/{uid}/links', [CampaignController::class, 'links']);
    Route::get('campaigns/listing/{page?}', [CampaignController::class, 'listing']);
    Route::get('campaigns/{uid}/recipients', [CampaignController::class, 'recipients']);
    Route::post('campaigns/{uid}/recipients', [CampaignController::class, 'recipients']);
    Route::get('campaigns/{uid}/setup', [CampaignController::class, 'setup']);
    Route::post('campaigns/{uid}/setup', [CampaignController::class, 'setup']);
    Route::get('campaigns/{uid}/template', [CampaignController::class, 'template']);
    Route::post('campaigns/{uid}/template', [CampaignController::class, 'template']);
    Route::get('campaigns/{uid}/template/select', [CampaignController::class, 'templateSelect']);
    Route::get('campaigns/{uid}/template/choose/{template_uid}', [CampaignController::class, 'templateChoose']);
    Route::get('campaigns/{uid}/template/preview', [CampaignController::class, 'templatePreview']);
    Route::get('campaigns/{uid}/template/iframe', [CampaignController::class, 'templateIframe']);
    Route::get('campaigns/{uid}/template/build/{style}', [CampaignController::class, 'templateBuild']);
    Route::get('campaigns/{uid}/template/rebuild', [CampaignController::class, 'templateRebuild']);
    Route::get('campaigns/{uid}/schedule', [CampaignController::class, 'schedule']);
    Route::post('campaigns/{uid}/schedule', [CampaignController::class, 'schedule']);
    Route::get('campaigns/{uid}/confirm', [CampaignController::class, 'confirm']);
    Route::post('campaigns/{uid}/confirm', [CampaignController::class, 'confirm']);
    Route::get('campaigns/delete', [CampaignController::class, 'delete']);
    Route::get('campaigns/select2', [CampaignController::class, 'select2']);
    Route::get('campaigns/pause', [CampaignController::class, 'pause']);
    Route::get('campaigns/restart', [CampaignController::class, 'restart']);
    Route::resource('campaigns', CampaignController::class);
    Route::get('campaigns/{uid}/edit', [CampaignController::class, 'edit']);
    Route::patch('campaigns/{uid}/update', [CampaignController::class, 'update']);
    Route::get('campaigns/{uid}/run', [CampaignController::class, 'run']);
    Route::get('campaigns/{uid}/update-stats', [CampaignController::class, 'updateStats']);

    Route::get('users/login-back', [UserController::class, 'loginBack']);

    // Sending servers
    Route::post('sending_servers/{uid}/test-connection', [SendingServerController::class, 'testConnection']);
    Route::post('sending_servers/{uid}/test', [SendingServerController::class, 'test']);
    Route::get('sending_servers/{uid}/test', [SendingServerController::class, 'test']);
    Route::get('sending_servers/select', [SendingServerController::class, 'select']);
    Route::get('sending_servers/listing/{page?}', [SendingServerController::class, 'listing']);
    Route::get('sending_servers/sort', [SendingServerController::class, 'sort']);
    Route::get('sending_servers/delete', [SendingServerController::class, 'delete']);
    Route::get('sending_servers/disable', [SendingServerController::class, 'disable']);
    Route::get('sending_servers/enable', [SendingServerController::class, 'enable']);
    Route::resource('sending_servers', SendingServerController::class);
    Route::get('sending_servers/create/{type}', [SendingServerController::class, 'create']);
    Route::post('sending_servers/create/{type}', [SendingServerController::class, 'store']);
    Route::get('sending_servers/{id}/edit/{type}', [SendingServerController::class, 'edit']);
    Route::patch('sending_servers/{id}/update/{type}', [SendingServerController::class, 'update']);

    // Sending domain
    Route::post('sending_domains/{id}/updateVerificationTxtName', [SendingDomainController::class, 'updateVerificationTxtName']);
    Route::post('sending_domains/{id}/updateDkimSelector', [SendingDomainController::class, 'updateDkimSelector']);
    Route::get('sending_domains/{id}/records', [SendingDomainController::class, 'records']);
    Route::post('sending_domains/{id}/verify', [SendingDomainController::class, 'verify']);
    Route::get('sending_domains/listing/{page?}', [SendingDomainController::class, 'listing']);
    Route::get('sending_domains/sort', [SendingDomainController::class, 'sort']);
    Route::get('sending_domains/delete', [SendingDomainController::class, 'delete']);
    Route::resource('sending_domains', SendingDomainController::class);

    // Tracking domain
    Route::get('tracking_domains/listing/{page?}', [TrackingDomainController::class, 'listing']);
    Route::get('tracking_domains/delete', [TrackingDomainController::class, 'delete']);
    Route::get('tracking_domains/{uid}/verify', [TrackingDomainController::class, 'verify']);
    Route::resource('tracking_domains', TrackingDomainController::class);

    // Email verification servers
    Route::get('email_verification_servers/options', [EmailVerificationServerController::class, 'options']);
    Route::get('email_verification_servers/listing/{page?}', [EmailVerificationServerController::class, 'listing']);
    Route::get('email_verification_servers/sort', [EmailVerificationServerController::class, 'sort']);
    Route::get('email_verification_servers/delete', [EmailVerificationServerController::class, 'delete']);
    Route::get('email_verification_servers/disable', [EmailVerificationServerController::class, 'disable']);
    Route::get('email_verification_servers/enable', [EmailVerificationServerController::class, 'enable']);
    Route::resource('email_verification_servers', EmailVerificationServerController::class);

    // Blacklists
    Route::get('blacklists/import', [BlacklistController::class, 'import']);
    Route::post('blacklists/import/start', [BlacklistController::class, 'startImport']);
    Route::get('blacklists/import/{job_uid}/progress', [BlacklistController::class, 'importProgress']);
    Route::post('blacklists/import/{job_uid}/cancel', [BlacklistController::class, 'cancelImport']);

    Route::get('blacklists/listing/{page?}', [BlacklistController::class, 'listing']);
    Route::get('blacklists/delete', [BlacklistController::class, 'delete']);
    Route::resource('blacklists', BlacklistController::class);

    // Sender
    Route::get('senders/dropbox', [SenderController::class, 'dropbox']);
    Route::get('senders/listing/{page?}', [SenderController::class, 'listing']);
    Route::get('senders/sort', [SenderController::class, 'sort']);
    Route::get('senders/delete', [SenderController::class, 'delete']);
    Route::resource('senders', SenderController::class);

    // Notifications
    Route::resource('notifications', NotificationController::class);
    Route::post('notifications/{id}/hide', [NotificationController::class, 'hide']);

    // Automation2
    Route::match(['get', 'post'], 'automation2/{email_uid}/send-test-email', [Automation2Controller::class, 'sendTestEmail']);
    Route::get('automation2/{uid}/cart/items', [Automation2Controller::class, 'cartItems']);
    Route::get('automation2/{uid}/cart/list', [Automation2Controller::class, 'cartList']);
    Route::get('automation2/{uid}/cart/stats', [Automation2Controller::class, 'cartStats']);
    Route::match(['get', 'post'], 'automation2/{uid}/car/change-store', [Automation2Controller::class, 'cartChangeStore']);
    Route::match(['get', 'post'], 'automation2/{uid}/car/wait', [Automation2Controller::class, 'cartWait']);
    Route::match(['get', 'post'], 'automation2/{uid}/car/change-list', [Automation2Controller::class, 'cartChangeList']);
    Route::get('automation2/{uid}/condition/setting', [Automation2Controller::class, 'conditionSetting']);
    Route::get('automation2/{uid}/operation/show', [Automation2Controller::class, 'operationShow']);
    Route::match(['get', 'post'], 'automation2/{uid}/operation/edit', [Automation2Controller::class, 'operationEdit']);
    Route::match(['get', 'post'], 'automation2/{uid}/operation/create', [Automation2Controller::class, 'operationCreate']);
    Route::get('automation2/{uid}/operation/select', [Automation2Controller::class, 'operationSelect']);
    Route::post('automation2/{uid}/wait-time', [Automation2Controller::class, 'waitTime']);
    Route::get('automation2/{uid}/wait-time', [Automation2Controller::class, 'waitTime']);
    Route::get('automation2/{uid}/last-saved', [Automation2Controller::class, 'lastSaved']);
    Route::post('automation2/{uid}/subscribers/{subscriber_uid}/restart', [Automation2Controller::class, 'subscribersRestart']);
    Route::post('automation2/{uid}/subscribers/{subscriber_uid}/remove', [Automation2Controller::class, 'subscribersRemove']);
    Route::get('automation2/{uid}/subscribers/{subscriber_uid}/show', [Automation2Controller::class, 'subscribersShow']);
    Route::get('automation2/{uid}/subscribers/list', [Automation2Controller::class, 'subscribersList']);
    Route::get('automation2/{uid}/subscribers/list', [Automation2Controller::class, 'subscribersList']);
    Route::get('automation2/{uid}/subscribers', [Automation2Controller::class, 'subscribers']);
    Route::get('automation2/{uid}/template/{email_uid}/preview/content', [Automation2Controller::class, 'templatePreviewContent']);
    Route::get('automation2/{uid}/template/{email_uid}/preview', [Automation2Controller::class, 'templatePreview']);
    Route::match(['get', 'post'], 'automation2/{uid}/template/{email_uid}/plain-edit', [Automation2Controller::class, 'templateEditPlain']);
    Route::get('automation2/{uid}/template/{email_uid}/builder-select', [Automation2Controller::class, 'templateBuilderSelect']);
    Route::get('automation2/segment-select', [Automation2Controller::class, 'segmentSelect']);

    Route::match(['get', 'post'], 'automation2/{uid}/template/{email_uid}/edit-classic', [Automation2Controller::class, 'templateEditClassic']);
    Route::match(['get', 'post'], 'automation2/{uid}/contacts/copy-to-new-list', [Automation2Controller::class, 'copyToNewList']);
    Route::post('automation2/{uid}/contacts/export', [Automation2Controller::class, 'exportContacts']);

    Route::get('automation2/{uid}/contacts/{contact_uid}/retry', [Automation2Controller::class, 'contactRetry']);
    Route::post('automation2/{uid}/contacts/{contact_uid}/tag/remove', [Automation2Controller::class, 'removeTag']);
    Route::match(['get', 'post'], 'automation2/{uid}/contacts/tag', [Automation2Controller::class, 'tagContacts']);
    Route::match(['get', 'post'], 'automation2/{uid}/contact/{contact_uid}/tag', [Automation2Controller::class, 'tagContact']);
    Route::post('automation2/{uid}/contact/{contact_uid}/remove', [Automation2Controller::class, 'removeContact']);
    Route::get('automation2/{uid}/contact/{contact_uid}/profile', [Automation2Controller::class, 'profile']);
    Route::get('automation2/{uid}/timeline/list', [Automation2Controller::class, 'timelineList']);
    Route::get('automation2/{uid}/timeline', [Automation2Controller::class, 'timeline']);
    Route::post('automation2/{uid}/contacts/list', [Automation2Controller::class, 'contactsList']);
    Route::get('automation2/{uid}/contacts/list', [Automation2Controller::class, 'contactsList']);
    Route::get('automation2/{uid}/contacts', [Automation2Controller::class, 'contacts']);

    Route::get('automation2/{uid}/insight', [Automation2Controller::class, 'insight']);
    Route::post('automation2/{uid}/data/save', [Automation2Controller::class, 'saveData']);
    Route::post('automation2/{uid}/update', [Automation2Controller::class, 'update']);
    Route::get('automation2/{uid}/settings', [Automation2Controller::class, 'settings']);

    Route::post('automation2/{uid}/template/{email_uid}/attachment/{attachment_uid}/remove', [Automation2Controller::class, 'emailAttachmentRemove']);
    Route::get('automation2/{uid}/template/{email_uid}/attachment/{attachment_uid}', [Automation2Controller::class, 'emailAttachmentDownload']);

    Route::post('automation2/{uid}/template/{email_uid}/attachment', [Automation2Controller::class, 'emailAttachmentUpload']);
    Route::post('automation2/{uid}/template/{email_uid}/remove-template', [Automation2Controller::class, 'templateRemove']);
    Route::get('automation2/{uid}/template/{email_uid}/content', [Automation2Controller::class, 'templateContent']);
    Route::match(['get', 'post'], 'automation2/{uid}/template/{email_uid}/edit', [Automation2Controller::class, 'templateEdit']);
    Route::match(['get', 'post'], 'automation2/{uid}/template/{email_uid}/upload', [Automation2Controller::class, 'templateUpload']);
    Route::match(['get', 'post'], 'automation2/{uid}/template/{email_uid}/layout', [Automation2Controller::class, 'templateLayout']);
    Route::post('automation2/{uid}/template/{email_uid}/layout/list', [Automation2Controller::class, 'templateLayoutList']);
    Route::get('automation2/{uid}/template/{email_uid}/create', [Automation2Controller::class, 'templateCreate']);

    Route::post('automation2/{uid}/email/{email_uid}/delete', [Automation2Controller::class, 'emailDelete']);
    Route::match(['get', 'post'], 'automation2/{uid}/email/setup', [Automation2Controller::class, 'emailSetup']);
    Route::match(['get', 'post'], 'automation2/{uid}/email/{email_uid}/confirm', [Automation2Controller::class, 'emailConfirm']);
    Route::match(['get', 'post'], 'automation2/{uid}/email/{email_uid?}', [Automation2Controller::class, 'email']);
    Route::match(['get', 'post'], 'automation2/{uid}/email/{email_uid}/template', [Automation2Controller::class, 'emailTemplate']);
    Route::match(['get', 'post'], 'automation2/{uid}/action/edit', [Automation2Controller::class, 'actionEdit']);
    Route::match(['get', 'post'], 'automation2/{uid}/trigger/edit', [Automation2Controller::class, 'triggerEdit']);
    Route::post('automation2/{uid}/action/select', [Automation2Controller::class, 'actionSelect']);
    Route::get('automation2/{uid}/action/select/confirm', [Automation2Controller::class, 'actionSelectConfirm']);
    Route::get('automation2/{uid}/action/select', [Automation2Controller::class, 'actionSelectPupop']);
    Route::post('automation2/{uid}/trigger/select', [Automation2Controller::class, 'triggerSelect']);
    Route::get('automation2/{uid}/trigger/select', [Automation2Controller::class, 'triggerSelectPupop']);
    Route::get('automation2/{uid}/trigger/select/confirm', [Automation2Controller::class, 'triggerSelectConfirm']);
    Route::match(['get'], 'automation2/{uid}/edit', [Automation2Controller::class, 'edit']);
    Route::match(['get', 'post'], 'automation2/create', [Automation2Controller::class, 'create']);
    Route::patch('automation2/disable', [Automation2Controller::class, 'disable']);
    Route::patch('automation2/enable', [Automation2Controller::class, 'enable']);
    Route::delete('automation2/delete', [Automation2Controller::class, 'delete']);
    Route::get('automation2/listing', [Automation2Controller::class, 'listing']);
    Route::get('automation2', [Automation2Controller::class, 'index']);
    Route::get('automation2/{uid}/debug', [Automation2Controller::class, 'debug']);
    Route::get('automation2/{automation}/{subscriber}/trigger', [Automation2Controller::class, 'triggerNow']);
    Route::get('trigger/{id}', [AutoTrigger::class, 'show']);
    Route::get('trigger/{id}/check', [AutoTrigger::class, 'check']);
    Route::get('automation2/{automation}/run', [Automation2Controller::class, 'run']);

    Route::get('email/preview/{uid}/{subscriber_uid}', [EmailController::class, 'preview']);

    // Sample UI/UX
    Route::get('sample', [SamplesController::class, 'index']);

    // Products
    Route::get('/products/json', [ProductController::class, 'json']);
    Route::get('/products/image/{uid}', [ProductController::class, 'image']);
    Route::get('products/listing', [ProductController::class, 'listing']);
    Route::resource('products', ProductController::class);

    // Source
    Route::get('sources/{uid}/detail', [SourceController::class, 'show']);
    Route::match(['get', 'post'], 'sources/woo-connect', [SourceController::class, 'wooConnect']);
    Route::post('sources/{uid}/sync', [SourceController::class, 'sync']);
    Route::post('sources/delete', [SourceController::class, 'delete']);
    Route::get('sources/connect', [SourceController::class, 'connect']);
    Route::get('sources/listing', [SourceController::class, 'listing']);
    Route::resource('sources', SourceController::class);
});

// ADMIN AREA
Route::middleware('auth', 'backend')->group(function () {
    Route::get('admin', [Admin\HomeController::class, 'index']);
    Route::get('admin/docs/api/v1', [Admin\ApiController::class, 'doc']);

    // Notification
    Route::get('admin/notifications/delete', [Admin\NotificationController::class, 'delete']);
    Route::get('admin/notifications/listing', [Admin\NotificationController::class, 'listing']);
    Route::get('admin/notifications', [Admin\NotificationController::class, 'index']);

    // User
    Route::get('admin/users/switch/{uid}', [Admin\UserController::class, 'switch_user']);
    Route::get('admin/users/listing/{page?}', [Admin\UserController::class, 'listing']);
    Route::get('admin/users/sort', [Admin\UserController::class, 'sort']);
    Route::get('admin/users/delete', [Admin\UserController::class, 'delete']);
    Route::resource('admin/users', Admin\UserController::class);

    // Template
    Route::match(['get', 'post'], 'admin/templates/{uid}/categories', [Admin\TemplateController::class, 'categories']);

    Route::match(['get', 'post'], 'admin/templates/{uid}/update-thumb-url', [Admin\TemplateController::class, 'updateThumbUrl']);
    Route::match(['get', 'post'], 'admin/templates/{uid}/update-thumb', [Admin\TemplateController::class, 'updateThumb']);

    Route::get('admin/templates/{uid}/builder/change-template/{change_uid}', [Admin\TemplateController::class, 'builderChangeTemplate']);
    Route::get('admin/templates/builder/templates/{category_uid}', [Admin\TemplateController::class, 'builderTemplates']);
    Route::post('admin/templates/builder/create', [Admin\TemplateController::class, 'builderCreate']);
    Route::get('admin/templates/builder/create', [Admin\TemplateController::class, 'builderCreate']);
    Route::post('admin/templates/{uid}/builder/edit/asset', [Admin\TemplateController::class, 'uploadTemplateAssets']);
    Route::get('admin/templates/{uid}/builder/edit/content', [Admin\TemplateController::class, 'builderEditContent']);
    Route::post('admin/templates/{uid}/builder/edit', [Admin\TemplateController::class, 'builderEdit']);
    Route::get('admin/templates/{uid}/builder/edit', [Admin\TemplateController::class, 'builderEdit']);

    Route::post('admin/templates/{uid}/copy', [Admin\TemplateController::class, 'copy']);
    Route::get('admin/templates/{uid}/copy', [Admin\TemplateController::class, 'copy']);
    Route::get('admin/templates/{uid}/preview', [Admin\TemplateController::class, 'preview']);
    Route::get('admin/templates/listing/{page?}', [Admin\TemplateController::class, 'listing']);
    Route::get('admin/templates/upload', [Admin\TemplateController::class, 'uploadTemplate']);
    Route::post('admin/templates/upload', [Admin\TemplateController::class, 'uploadTemplate']);
    Route::get('admin/templates/delete', [Admin\TemplateController::class, 'delete']);
    Route::resource('admin/templates', Admin\TemplateController::class);
    Route::get('admin/templates/{uid}/edit', [Admin\TemplateController::class, 'edit']);
    Route::patch('admin/templates/{uid}/update', [Admin\TemplateController::class, 'update']);

    // Layout
    Route::get('admin/layouts/listing/{page?}', [Admin\LayoutController::class, 'listing']);
    Route::get('admin/layouts/sort', [Admin\LayoutController::class, 'sort']);
    Route::resource('admin/layouts', Admin\LayoutController::class);

    // Sending servers
    Route::get('admin/sending_servers/{uid}/senders/dropbox', [Admin\SendingServerController::class, 'fromDropbox']);
    Route::post('admin/sending_servers/{uid}/remove-domain/{domain}', [Admin\SendingServerController::class, 'removeDomain']);
    Route::post('admin/sending_servers/{uid}/add-domain', [Admin\SendingServerController::class, 'addDomain']);
    Route::get('admin/sending_servers/{uid}/add-domain', [Admin\SendingServerController::class, 'addDomain']);
    Route::get('admin/sending_servers/aws-region-host', [Admin\SendingServerController::class, 'awsRegionHost']);

    Route::post('admin/sending_servers/{uid}/test-connection', [Admin\SendingServerController::class, 'testConnection']);

    Route::post('admin/sending_servers/{uid}/config', [Admin\SendingServerController::class, 'config']);
    Route::post('admin/sending_servers/sending-limit', [Admin\SendingServerController::class, 'sendingLimit']);
    Route::get('admin/sending_servers/sending-limit', [Admin\SendingServerController::class, 'sendingLimit']);

    Route::get('admin/sending_servers/select2', [Admin\SendingServerController::class, 'select2']);
    Route::post('admin/sending_servers/{uid}/test', [Admin\SendingServerController::class, 'test']);
    Route::get('admin/sending_servers/{uid}/test', [Admin\SendingServerController::class, 'test']);
    Route::get('admin/sending_servers/select', [Admin\SendingServerController::class, 'select']);
    Route::get('admin/sending_servers/listing/{page?}', [Admin\SendingServerController::class, 'listing']);
    Route::get('admin/sending_servers/sort', [Admin\SendingServerController::class, 'sort']);
    Route::get('admin/sending_servers/delete', [Admin\SendingServerController::class, 'delete']);
    Route::get('admin/sending_servers/disable', [Admin\SendingServerController::class, 'disable']);
    Route::get('admin/sending_servers/enable', [Admin\SendingServerController::class, 'enable']);
    Route::resource('admin/sending_servers', Admin\SendingServerController::class);
    Route::get('admin/sending_servers/create/{type}', [Admin\SendingServerController::class, 'create']);
    Route::post('admin/sending_servers/create/{type}', [Admin\SendingServerController::class, 'store']);
    Route::get('admin/sending_servers/{id}/edit/{type}', [Admin\SendingServerController::class, 'edit']);
    Route::patch('admin/sending_servers/{id}/update/{type}', [Admin\SendingServerController::class, 'update']);

    // Bounce handlers
    Route::post('admin/bounce_handlers/{uid}/test', [Admin\BounceHandlerController::class, 'test']);
    Route::get('admin/bounce_handlers/listing/{page?}', [Admin\BounceHandlerController::class, 'listing']);
    Route::get('admin/bounce_handlers/sort', [Admin\BounceHandlerController::class, 'sort']);
    Route::get('admin/bounce_handlers/delete', [Admin\BounceHandlerController::class, 'delete']);
    Route::resource('admin/bounce_handlers', Admin\BounceHandlerController::class);

    // Feedback Loop handlers
    Route::post('admin/feedback_loop_handlers/{uid}/test', [Admin\FeedbackLoopHandlerController::class, 'test']);
    Route::get('admin/feedback_loop_handlers/listing/{page?}', [Admin\FeedbackLoopHandlerController::class, 'listing']);
    Route::get('admin/feedback_loop_handlers/sort', [Admin\FeedbackLoopHandlerController::class, 'sort']);
    Route::get('admin/feedback_loop_handlers/delete', [Admin\FeedbackLoopHandlerController::class, 'delete']);
    Route::resource('admin/feedback_loop_handlers', Admin\FeedbackLoopHandlerController::class);

    // Language
    Route::get('admin/languages/delete/confirm', [Admin\LanguageController::class, 'deleteConfirm']);
    Route::get('admin/languages/listing/{page?}', [Admin\LanguageController::class, 'listing']);
    Route::get('admin/languages/delete', [Admin\LanguageController::class, 'delete']);
    Route::get('admin/languages/{id}/translate', [Admin\LanguageController::class, 'translate']);
    Route::post('admin/languages/{id}/translate', [Admin\LanguageController::class, 'translate']);
    Route::get('admin/languages/disable', [Admin\LanguageController::class, 'disable']);
    Route::get('admin/languages/enable', [Admin\LanguageController::class, 'enable']);
    Route::get('admin/languages/{id}/download', [Admin\LanguageController::class, 'download']);
    Route::get('admin/languages/{id}/upload', [Admin\LanguageController::class, 'upload']);
    Route::post('admin/languages/{id}/upload', [Admin\LanguageController::class, 'upload']);
    Route::resource('admin/languages', Admin\LanguageController::class);

    // Settings
    Route::post('admin/settings/payment', [Admin\SettingController::class, 'payment']);
    Route::post('admin/settings/advanced/{name}/update', [Admin\SettingController::class, 'advancedUpdate']);
    Route::get('admin/settings/advanced', [Admin\SettingController::class, 'advanced']);
    Route::post('admin/settings/upgrade/cancel', [Admin\SettingController::class, 'cancelUpgrade']);
    Route::post('admin/settings/upgrade', [Admin\SettingController::class, 'doUpgrade']);
    Route::post('admin/settings/upgrade/upload', [Admin\SettingController::class, 'uploadApplicationPatch']);
    Route::get('admin/settings/upgrade', [Admin\SettingController::class, 'upgrade']);
    Route::get('u', [Admin\SettingController::class, 'upgrade']); // shortcut to upgrade page
    Route::post('admin/settings/license', [Admin\SettingController::class, 'license']);
    Route::get('admin/settings/license', [Admin\SettingController::class, 'license']);
    Route::match(['get', 'post'], 'admin/settings/mailer/test', [Admin\SettingController::class, 'mailerTest']);
    Route::get('admin/settings/mailer', [Admin\SettingController::class, 'mailer']);
    Route::post('admin/settings/mailer', [Admin\SettingController::class, 'mailer']);
    Route::get('admin/settings/cronjob', [Admin\SettingController::class, 'cronjob']);
    Route::post('admin/settings/cronjob', [Admin\SettingController::class, 'cronjob']);
    Route::get('admin/settings/urls', [Admin\SettingController::class, 'urls']);
    Route::get('admin/settings/sending', [Admin\SettingController::class, 'sending']);
    Route::post('admin/settings/sending', [Admin\SettingController::class, 'sending']);
    Route::get('admin/settings/general', [Admin\SettingController::class, 'general']);
    Route::post('admin/settings/general', [Admin\SettingController::class, 'general']);
    Route::get('admin/settings/logs', [Admin\SettingController::class, 'logs']);
    Route::get('log', [Admin\SettingController::class, 'download_log']);
    Route::get('admin/settings/{tab?}', [Admin\SettingController::class, 'index']);
    Route::post('admin/settings', [Admin\SettingController::class, 'index']);
    Route::get('admin/update-urls', [Admin\SettingController::class, 'updateUrls']);

    // Tracking log
    Route::get('admin/tracking_log', [Admin\TrackingLogController::class, 'index']);
    Route::get('admin/tracking_log/listing', [Admin\TrackingLogController::class, 'listing']);

    // Feedback log
    Route::get('admin/bounce_log', [Admin\BounceLogController::class, 'index']);
    Route::get('admin/bounce_log/listing', [Admin\BounceLogController::class, 'listing']);

    // Open log
    Route::get('admin/open_log', [Admin\OpenLogController::class, 'index']);
    Route::get('admin/open_log/listing', [Admin\OpenLogController::class, 'listing']);

    // Click log
    Route::get('admin/click_log', [Admin\ClickLogController::class, 'index']);
    Route::get('admin/click_log/listing', [Admin\ClickLogController::class, 'listing']);

    // Feedback log
    Route::get('admin/feedback_log', [Admin\FeedbackLogController::class, 'index']);
    Route::get('admin/feedback_log/listing', [Admin\FeedbackLogController::class, 'listing']);

    // Unsubscribe log
    Route::get('admin/unsubscribe_log', [Admin\UnsubscribeLogController::class, 'index']);
    Route::get('admin/unsubscribe_log/listing', [Admin\UnsubscribeLogController::class, 'listing']);

    // Blacklist
    Route::get('admin/blacklists/import', [Admin\BlacklistController::class, 'import']);
    Route::get('admin/blacklists/import/{job_uid}/progress', [Admin\BlacklistController::class, 'importProgress']);
    Route::post('admin/blacklists/import/{job_uid}/cancel', [Admin\BlacklistController::class, 'cancelImport']);
    Route::post('admin/blacklists/import/start', [Admin\BlacklistController::class, 'startImport']);

    Route::get('admin/blacklist', [Admin\BlacklistController::class, 'index']);
    Route::get('admin/blacklist/listing', [Admin\BlacklistController::class, 'listing']);
    Route::get('admin/blacklist/delete', [Admin\BlacklistController::class, 'delete']);
    Route::get('admin/blacklists/{id}/reason', [Admin\BlacklistController::class, 'reason']);

    // Customer Group
    Route::get('admin/customer_groups/listing/{page?}', [Admin\CustomerGroupController::class, 'listing']);
    Route::get('admin/customer_groups/sort', [Admin\CustomerGroupController::class, 'sort']);
    Route::get('admin/customer_groups/delete', [Admin\CustomerGroupController::class, 'delete']);
    Route::resource('admin/customer_groups', Admin\CustomerGroupController::class);

    // Customer
    Route::match(['get', 'post'], 'admin/customers/{uid}/assign-plan', [Admin\CustomerController::class, 'assignPlan']);
    Route::get('admin/customers/{uid}/su-account', [Admin\CustomerController::class, 'subAccount']);
    Route::post('admin/customers/{uid}/contact', [Admin\CustomerController::class, 'contact']);
    Route::get('admin/customers/{id}/contact', [Admin\CustomerController::class, 'contact']);
    Route::get('admin/customers/growthChart', [Admin\CustomerController::class, 'growthChart']);
    Route::get('admin/customers/{id}/subscriptions', [Admin\CustomerController::class, 'subscriptions']);
    Route::get('admin/customers/select2', [Admin\CustomerController::class, 'select2']);
    Route::get('admin/customers/login-as/{uid}', [Admin\CustomerController::class, 'loginAs']);
    Route::get('admin/customers/listing/{page?}', [Admin\CustomerController::class, 'listing']);
    Route::get('admin/customers/sort', [Admin\CustomerController::class, 'sort']);
    Route::get('admin/customers/delete', [Admin\CustomerController::class, 'delete']);
    Route::get('admin/customers/disable', [Admin\CustomerController::class, 'disable']);
    Route::get('admin/customers/enable', [Admin\CustomerController::class, 'enable']);
    Route::resource('admin/customers', Admin\CustomerController::class);

    // Admin Group
    Route::get('admin/admin_groups/listing/{page?}', [Admin\AdminGroupController::class, 'listing']);
    Route::get('admin/admin_groups/sort', [Admin\AdminGroupController::class, 'sort']);
    Route::get('admin/admin_groups/delete', [Admin\AdminGroupController::class, 'delete']);
    Route::resource('admin/admin_groups', Admin\AdminGroupController::class);

    // Admin
    Route::get('admin/admins/login-as/{uid}', [Admin\AdminController::class, 'loginAs']);
    Route::get('admin/admins/listing/{page?}', [Admin\AdminController::class, 'listing']);
    Route::get('admin/admins/sort', [Admin\AdminController::class, 'sort']);
    Route::get('admin/admins/delete', [Admin\AdminController::class, 'delete']);
    Route::get('admin/admins/disable', [Admin\AdminController::class, 'disable']);
    Route::get('admin/admins/enable', [Admin\AdminController::class, 'enable']);
    Route::get('admin/admins/login-back', [Admin\AdminController::class, 'loginBack']);
    Route::resource('admin/admins', Admin\AdminController::class);

    // Account
    Route::get('admin/account/api/renew', [Admin\AccountController::class, 'renewToken']);
    Route::get('admin/account/api', [Admin\AccountController::class, 'api']);
    Route::get('admin/account/profile', [Admin\AccountController::class, 'profile']);
    Route::post('admin/account/profile', [Admin\AccountController::class, 'profile']);
    Route::get('admin/account/contact', [Admin\AccountController::class, 'contact']);
    Route::post('admin/account/contact', [Admin\AccountController::class, 'contact']);

    // Plan
    Route::post('admin/plans/{uid}/visible/on', [Admin\PlanController::class, 'visibleOn']);
    Route::post('admin/plans/{uid}/visible/off', [Admin\PlanController::class, 'visibleOff']);

    Route::match(['get', 'post'], 'admin/plans/{uid}/sending-server/subaccount', [Admin\PlanController::class, 'sendingServerSubaccount']);
    Route::match(['get', 'post'], 'admin/plans/{uid}/sending-server/own', [Admin\PlanController::class, 'sendingServerOwn']);
    Route::match(['get', 'post'], 'admin/plans/{uid}/sending-server/option', [Admin\PlanController::class, 'sendingServerOption']);
    Route::match(['get', 'post'], 'admin/plans/{uid}/wizard/sending-server', [Admin\PlanController::class, 'wizardSendingServer']);
    Route::match(['get', 'post'], 'admin/plans/wizard', [Admin\PlanController::class, 'wizard']);
    Route::get('admin/plans/{uid}/sending-server', [Admin\PlanController::class, 'sendingServer']);
    Route::match(['get', 'post'], 'admin/plans/{uid}/billing-cycle', [Admin\PlanController::class, 'billingCycle']);
    Route::match(['get', 'post'], 'admin/plans/{uid}/sending-limit', [Admin\PlanController::class, 'sendingLimit']);
    Route::get('admin/plans/{uid}/email-verification', [Admin\PlanController::class, 'emailVerification']);
    Route::get('admin/plans/{uid}/general', [Admin\PlanController::class, 'general']);
    Route::get('admin/plans/{uid}/quota', [Admin\PlanController::class, 'quota']);
    Route::get('admin/plans/{uid}/security', [Admin\PlanController::class, 'security']);
    Route::get('admin/plans/{uid}/email-footer', [Admin\PlanController::class, 'emailFooter']);
    Route::get('admin/plans/{uid}/payment', [Admin\PlanController::class, 'payment']);
    Route::get('admin/plans/{uid}/billing-history', [Admin\PlanController::class, 'billingHistory']);
    Route::get('admin/plans/{uid}/general', [Admin\PlanController::class, 'general']);
    Route::post('admin/plans/{uid}/save', [Admin\PlanController::class, 'save']);

    Route::post('admin/plans/{id}/sending_servers/{sending_server_uid}/set-primary', [Admin\PlanController::class, 'setPrimarySendingServer']);
    Route::match(['get', 'post'], 'admin/plans/{id}/sending_servers/fitness', [Admin\PlanController::class, 'fitness']);
    Route::post('admin/plans/{id}/sending_servers/{sending_server_uid}/remove', [Admin\PlanController::class, 'removeSendingServer']);
    Route::post('admin/plans/{id}/sending_servers/add', [Admin\PlanController::class, 'addSendingServer']);
    Route::get('admin/plans/{id}/sending_servers/add', [Admin\PlanController::class, 'addSendingServer']);
    Route::get('admin/plans/{id}/sending_servers', [Admin\PlanController::class, 'sendingServers']);
    Route::get('admin/plans/pieChart', [Admin\PlanController::class, 'pieChart']);
    Route::get('admin/plans/delete/confirm', [Admin\PlanController::class, 'deleteConfirm']);
    Route::get('admin/plans/select2', [Admin\PlanController::class, 'select2']);
    Route::get('admin/plans/listing/{page?}', [Admin\PlanController::class, 'listing']);
    Route::get('admin/plans/sort', [Admin\PlanController::class, 'sort']);
    Route::get('admin/plans/delete', [Admin\PlanController::class, 'delete']);
    Route::get('admin/plans/disable', [Admin\PlanController::class, 'disable']);
    Route::post('admin/plans/enable', [Admin\PlanController::class, 'enable']);
    Route::post('admin/plans/copy', [Admin\PlanController::class, 'copy']);
    Route::resource('admin/plans', Admin\PlanController::class);

    // Currency
    Route::get('admin/currencies/select2', [Admin\CurrencyController::class, 'select2']);
    Route::get('admin/currencies/listing/{page?}', [Admin\CurrencyController::class, 'listing']);
    Route::get('admin/currencies/sort', [Admin\CurrencyController::class, 'sort']);
    Route::get('admin/currencies/delete', [Admin\CurrencyController::class, 'delete']);
    Route::get('admin/currencies/disable', [Admin\CurrencyController::class, 'disable']);
    Route::get('admin/currencies/enable', [Admin\CurrencyController::class, 'enable']);
    Route::resource('admin/currencies', Admin\CurrencyController::class);

    // Subscription
    Route::post('admin/subscriptions/{id}/approve', [Admin\SubscriptionController::class, 'approve']);
    // Route::match(['get','post'], 'admin/subscriptions/create', [Admin\SubscriptionController::class, 'create']);
    // Route::post('admin/subscriptions/{id}/approve-pending', [Admin\SubscriptionController::class, 'approvePending']);
    // Route::post('admin/subscriptions/{id}/renew', [Admin\SubscriptionController::class, 'renew']);
    Route::match(['get', 'post'], 'admin/subscriptions/{id}/reject-pending', [Admin\SubscriptionController::class, 'rejectPending']);
    Route::get('admin/subscriptions/{id}/invoices', [Admin\SubscriptionController::class, 'invoices']);
    // Route::post('admin/subscriptions/{id}/change-plan', [Admin\SubscriptionController::class, 'changePlan']);
    // Route::get('admin/subscriptions/{id}/change-plan', [Admin\SubscriptionController::class, 'changePlan']);
    Route::post('admin/subscriptions/{id}/cancel-now', [Admin\SubscriptionController::class, 'cancelNow']);
    Route::post('admin/subscriptions/{id}/resume', [Admin\SubscriptionController::class, 'resume']);
    Route::post('admin/subscriptions/{id}/cancel', [Admin\SubscriptionController::class, 'cancel']);
    Route::get('admin/subscriptions/listing/{page?}', [Admin\SubscriptionController::class, 'listing']);
    Route::get('admin/subscriptions/sort', [Admin\SubscriptionController::class, 'sort']);
    Route::delete('admin/subscriptions/delete', [Admin\SubscriptionController::class, 'delete']);
    Route::resource('admin/subscriptions', Admin\SubscriptionController::class);

    // Email verification servers
    Route::get('admin/email_verification_servers/options', [Admin\EmailVerificationServerController::class, 'options']);
    Route::get('admin/email_verification_servers/listing/{page?}', [Admin\EmailVerificationServerController::class, 'listing']);
    Route::get('admin/email_verification_servers/sort', [Admin\EmailVerificationServerController::class, 'sort']);
    Route::get('admin/email_verification_servers/delete', [Admin\EmailVerificationServerController::class, 'delete']);
    Route::get('admin/email_verification_servers/disable', [Admin\EmailVerificationServerController::class, 'disable']);
    Route::get('admin/email_verification_servers/enable', [Admin\EmailVerificationServerController::class, 'enable']);
    Route::resource('admin/email_verification_servers', Admin\EmailVerificationServerController::class);

    // Sub account
    Route::get('admin/sub_accounts/{uid}/delete/confirm', [Admin\SubAccountController::class, 'deleteConfirm']);
    Route::delete('admin/sub_accounts/{uid}/delete', [Admin\SubAccountController::class, 'delete']);
    Route::get('admin/sub_accounts/listing/{page?}', [Admin\SubAccountController::class, 'listing']);
    Route::resource('admin/sub_accounts', Admin\SubAccountController::class);

    // Payment gateways
    Route::post('admin/payment/gateways/{name}/disable', [Admin\PaymentController::class, 'disable']);
    Route::post('admin/payment/gateways/{name}/enable', [Admin\PaymentController::class, 'enable']);
    Route::post('admin/payment/gateways/{name}/set-primary', [Admin\PaymentController::class, 'setPrimary']);
    Route::post('admin/payment/gateways/update/{name}', [Admin\PaymentController::class, 'update']);
    Route::get('admin/payment/gateways/edit/{name}', [Admin\PaymentController::class, 'edit']);
    Route::get('admin/payment/gateways/index', [Admin\PaymentController::class, 'index']);

    // Plugin managements
    Route::match(['get', 'post'], 'admin/plugins/install', [Admin\PluginController::class, 'install']);
    Route::get('admin/plugins/listing/{page?}', [Admin\PluginController::class, 'listing']);
    Route::get('admin/plugins/sort', [Admin\PluginController::class, 'sort']);
    Route::get('admin/plugins/delete', [Admin\PluginController::class, 'delete']);
    Route::get('admin/plugins/disable', [Admin\PluginController::class, 'disable']);
    Route::get('admin/plugins/enable', [Admin\PluginController::class, 'enable']);
    Route::resource('admin/plugins', Admin\PluginController::class);

    // GeoIP Management
    Route::get('admin/geoip', [Admin\GeoIpController::class, 'index']);
    Route::match(['get', 'post'], 'admin/geoip/setting', [Admin\GeoIpController::class, 'setting']);
    Route::post('admin/geoip/reset', [Admin\GeoIpController::class, 'reset']);
});
