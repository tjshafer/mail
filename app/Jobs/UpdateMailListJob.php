<?php

namespace App\Jobs;

use App\Model\Blacklist;
use App\Model\MailList;

class UpdateMailListJob extends Base
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected MailList $list)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->list->updateCachedInfo();
        // blacklist new emails (if any)
        Blacklist::doBlacklist($this->list->customer);
    }
}
