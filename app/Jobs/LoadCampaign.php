<?php

namespace App\Jobs;

use Acelle\Library\Traits\Trackable;
use App\Model\Campaign;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LoadCampaign implements ShouldQueue
{
    use Trackable, Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected Campaign $campaign)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        $this->campaign->setSending();

        $this->campaign->prepare(function ($campaign, $subscriber, $server) {
            $this->batch()->add(new SendMessage($campaign, $subscriber, $server));
        });
    }
}
