<?php

namespace App\Jobs;

use Acelle\Library\Traits\Trackable;

class ExportCampaignLog extends Base
{
    use Trackable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $campaign, protected $logtype)
    {
        // Set the initial value for progress check
        $this->afterDispatched(function ($thisJob, $monitor) {
            $monitor->setJsonData([
                'percentage' => 0,
            ]);
        });
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->campaign->generateTrackingLogCsv($this->logtype, function ($percentage, $path) {
            $this->monitor->updateJsonData([
                'percentage' => $percentage,
                'path' => $path,
            ]);
        });
    }
}
