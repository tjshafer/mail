<?php

namespace App\Jobs;

class UpdateAutomation extends Base
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $automation)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->automation->updateCache();
    }
}
