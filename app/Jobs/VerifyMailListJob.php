<?php

namespace App\Jobs;

use Acelle\Library\Traits\Trackable;
use Illuminate\Bus\Batchable;

class VerifyMailListJob extends Base
{
    use Batchable, Trackable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $mailList, protected $server)
    {
        $this->afterDispatched(function ($thisJob, $monitor) {
            $monitor->setJsonData([
                'percentage' => 0,
                'total' => 0,
                'processed' => 0,
                'failed' => 0,
                'message' => 'Verification process is being queued...',
            ]);
        });
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        $this->monitor->updateJsonData([
            'message' => 'Verification is in progress...',
        ]);

        // Get subscribers that are not verified
        $query = $this->mailList->subscribers()->unverified();

        // Query batches of 1000 records each, dispatch the verification job
        // Add job to batch
        cursorIterate($query, 'subscribers.id', $size = 1000, function ($subscribers, $page) {
            foreach ($subscribers as $subscriber) {
                $this->batch()->add(new VerifySubscriber($subscriber, $this->server));
            }
        });
    }
}
