<?php

namespace App\Jobs;

use App\Model\Subscriber;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMessage implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $campaign, protected Subscriber $subscriber, protected $server)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        // Check customer quota
        while ($this->campaign->customer->overQuota()) {
            // throw new \Exception('Customer has reached sending limit');
            sleep(60);
        }

        // Check sending server quota
        while ($this->server->overQuota()) {
            sleep(60);
        }

        $logger = $this->campaign->logger();

        // Prepare the email message to send
        [$message, $msgId] = $this->campaign->prepareEmail($this->subscriber, $this->server);

        // Actually send!
        $logger->info(sprintf('Sending to %s [Server "%s"]', $this->subscriber->email, $this->server->name));
        $sent = $this->server->send($message);

        // Log
        $this->campaign->trackMessage($sent, $this->subscriber, $this->server, $msgId);
        $logger->info(sprintf('Sent to %s [Server "%s"]', $this->subscriber->email, $this->server->name));
    }
}
