<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Batchable;

class VerifyAndCreateSubscriber extends Base
{
    use Batchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $list, protected $attributes, protected $logger)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        try {
            // Create subscriber RECORD
            // Perform simple email address validation
            $subscriber = $this->list->addSubscriberFromArray($this->attributes);
        } catch (Exception $e) {
            // Email is always present
            // Do not throw exception here, in case of invalid email address
            $this->logger->error('['.$this->attributes['email'].'] failed to import. '.$e->getMessage());

            return;
        }

        // Verify email address against remote service
        $verifier = $this->list->customer->getEmailVerificationServers()->first();

        throw_if(is_null($verifier), new Exception('No email verification service found'));

        $isDeliverable = $subscriber->verify($verifier);
        if (! $isDeliverable) {
            // In case of failure, delete the newly created contact
            // Throw exception to log
            $subscriber->delete();
            $this->logger->error(sprintf('[%s] failed to import. Undeliverable email [checked by %s]', $subscriber->email, $verifier->name));
        } else {
            $this->logger->error(sprintf('[%s] successfully imported [checked by %s]', $subscriber->email, $verifier->name));
        }
    }
}
