<?php

namespace App\Jobs;

class UpdateUserJob extends Base
{
    public function __construct(protected $customer)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->customer->updateCache();
    }
}
