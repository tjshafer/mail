<?php

namespace App\Jobs;

use Illuminate\Bus\Batchable;

class VerifySubscriber extends Base
{
    use Batchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $subscriber, protected $server)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        $this->subscriber->verify($this->server);
    }
}
