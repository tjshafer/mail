<?php

namespace App\Jobs;

use Acelle\Library\Traits\Trackable;

class ExportSubscribersJob extends Base
{
    use Trackable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $mailList)
    {
        // Set the initial value for progress check
        $this->afterDispatched(function ($thisJob, $monitor) {
            $monitor->setJsonData([
                'percentage' => 0,
                'total' => 0,
                'processed' => 0,
                'failed' => 0,
                'message' => 'Export is being queued for processing...',
                'filepath' => $this->mailList->getExportFilePath(),
            ]);
        });
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->mailList->export(function ($processed, $total, $failed, $message) {
            $percentage = ($total && $processed) ? (int) ($processed * 100 / $total) : 0;

            $this->monitor->updateJsonData([
                'percentage' => $percentage,
                'total' => $total,
                'processed' => $processed,
                'failed' => $failed,
                'message' => $message,
            ]);
        });
    }
}
