<?php

namespace App\Jobs;

class DeliverEmail extends Base
{
    // one email may be delivered to a given subscriber more than once (weekly recurring, birthday... for example)

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $email, protected $subscriber, protected $triggerId)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->email->deliverTo($this->subscriber, $this->triggerId);
    }
}
