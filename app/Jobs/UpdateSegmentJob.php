<?php

namespace App\Jobs;

class UpdateSegmentJob extends Base
{
    public function __construct(protected $segment)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->segment->updateCache();
    }
}
