<?php

namespace App\Jobs;

class UpdateCampaignJob extends Base
{
    public function __construct(protected $campaign)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->campaign->updateCache();
    }
}
