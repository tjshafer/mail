<?php

namespace App\Jobs;

use Exception;

class RunAutomation extends Base
{
    public function __construct(protected $automation)
    {
        throw_if(! $this->automation->allowApiCall(), new Exception(sprintf('Automation "%s" is not set up to be triggered via API. Cannot start!', $this->automation->name)));
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (! $this->automation->allowApiCall()) {
            $this->automation->logger()->info(sprintf('Automation "%s" is not set up to be triggered via API (job handle)', $this->automation->name));
            throw new Exception('Automation is not set up to be triggered via API (job handle)');
        }
        $this->automation->logger()->info(sprintf('Actually run automation "%s" in response to API call', $this->automation->name));
        $this->automation->execute();
    }
}
