<?php

namespace App\Jobs;

use Acelle\Library\Traits\Trackable;
use App\Model\Blacklist;

class ImportBlacklistJob extends Base
{
    use Trackable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $filepath, protected $customer = null)
    {
        $this->afterDispatched(function ($thisJob, $monitor) {
            $monitor->setJsonData([
                'percentage' => 0,
                'total' => 0,
                'processed' => 0,
                'failed' => 0,
                'message' => 'Import is being queued for processing...',
            ]);
        });
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->monitor->updateJsonData([
            'message' => 'Import is in progress...',
        ]);

        Blacklist::import($this->filepath, $this->customer, function ($processed, $total, $failed, $message) {
            $percentage = ($total && $processed) ? (int) ($processed * 100 / $total) : 0;

            $this->monitor->updateJsonData([
                'percentage' => $percentage,
                'total' => $total,
                'processed' => $processed,
                'failed' => $failed,
                'message' => $message,
            ]);
        });
    }
}
