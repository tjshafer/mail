<?php

namespace App\Jobs;

use Acelle\Library\Log as MailLog;

class SendConfirmationEmailJob extends Base
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $subscribers, protected $mailList)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        MailLog::info(sprintf('Start re-sending confirmation email to %s contacts', count($this->subscribers)));
        foreach ($this->subscribers as $subscriber) {
            try {
                MailLog::info(sprintf('Re-sending confirmation email to %s (%s)', $subscriber->email, $subscriber->id));
                $this->mailList->sendSubscriptionConfirmationEmail($subscriber);
            } catch (\Exception $e) {
                MailLog::error(sprintf('Something went wrong when re-sending confirmation email for mail list %s. Error: %s', $this->mailList->name, $e->getMessage()));
                break;
            }
        }
        MailLog::info('Finish re-sending confirmation email');
    }
}
