<?php

namespace App\Library\Automation;

class Trigger extends Action
{
    protected function doExecute(): bool
    {
        return true;
    }

    public function getActionDescription(): string
    {
        $nameOrEmail = $this->autoTrigger->subscriber->getFullNameOrEmail();

        return sprintf('User %s subscribes to mail list, automation triggered!', $nameOrEmail);
    }

    public function getProgressDescription()
    {
        if (is_null($this->getLastExecuted())) {
            return '* Triggering automation';
        } else {
            return 'Automation triggered';
        }
    }
}
