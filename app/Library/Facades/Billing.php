<?php

namespace App\Library\Facades;

use Acelle\Library\BillingManager;
use Illuminate\Support\Facades\Facade;

class Billing extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return BillingManager::class;
    }
}
