<?php

namespace App\Library\Storage;

use Acelle\Library\Storage\Contracts\Storable;
use Acelle\Library\Storage\Contracts\StorageService;
use Aws\Credentials\Credentials;
use Aws\S3\S3Client;

class S3 implements StorageService
{
    public function __construct(protected $apiKey, protected $apiSecret, protected $region, protected $bucket)
    {
    }

    public function store(Storable $object): void
    {
        // Upload an object by streaming the contents of a file
        // $pathToFile should be absolute path to a file on disk
        $result = $this->getClient()->putObject([
            'Bucket' => $this->bucket,
            'Key' => $object->getArchivePath(),
            'SourceFile' => $object->toZip(),
            'Metadata' => [
                'Foo' => 'abc',
                'Baz' => '123',
            ],
        ]);
    }

    public function getClient(): \Aws\S3\S3Client
    {
        $client = new S3Client([
            'version' => 'latest',
            'region' => $this->region,
            'credentials' => new Credentials($this->apiKey, $this->apiSecret),
        ]);

        return $client;
    }
}
