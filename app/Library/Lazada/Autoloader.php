<?php

use Illuminate\Support\Str;

class Autoloader
{
    /**
     * Autoloade Class in SDK.
     * PS: only load SDK class
     *
     * @param  string  $class class name
     */
    public static function autoload(string $class): void
    {
        $name = $class;
        if (Str::of($name)->contains('\\')) {
            $name = strstr($class, '\\', true);
        }

        $filename = LAZOP_AUTOLOADER_PATH.'/lazop/'.$name.'.php';
        if (is_file($filename)) {
            include $filename;

            return;
        }
    }
}

spl_autoload_register('Autoloader::autoload');
