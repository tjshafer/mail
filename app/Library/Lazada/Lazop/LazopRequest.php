<?php

namespace App\Library\Lazada\Lazop;

class LazopRequest
{
    public $headerParams = [];

    public $udfParams = [];

    public $fileParams = [];

    public function __construct(public $apiName, public $httpMethod = 'POST')
    {
        throw_if($this->startWith($apiName, '//'), new Exception('api name is invalid. It should be start with /'));
    }

    public function addApiParam($key, $value): void
    {
        throw_if(! is_string($key), new Exception('api param key should be string'));

        if (is_object($value)) {
            $this->udfParams[$key] = json_decode($value);
        } else {
            $this->udfParams[$key] = $value;
        }
    }

    public function addFileParam($key, $content, $mimeType = 'application/octet-stream'): void
    {
        throw_if(! is_string($key), new Exception('api file param key should be string'));

        $file = [
            'type' => $mimeType,
            'content' => $content,
            'name' => $key,
        ];
        $this->fileParams[$key] = $file;
    }

    public function addHttpHeaderParam($key, $value): void
    {
        throw_if(! is_string($key), new Exception('http header param key should be string'));

        throw_if(! is_string($value), new Exception('http header param value should be string'));

        $this->headerParams[$key] = $value;
    }

    public function startWith($str, $needle): bool
    {
        return str_starts_with($str, $needle);
    }
}
