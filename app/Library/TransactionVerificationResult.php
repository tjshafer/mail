<?php

namespace App\Library;

class TransactionVerificationResult
{
    const RESULT_DONE = 'done';

    const RESULT_FAILED = 'failed';

    const RESULT_STILL_PENDING = 'still-pending';

    // For Stripe / PayPal / Braintree only when the service will start and finish the transaction immediately
    const RESULT_VERIFICATION_NOT_NEEDED = 'not-needed';

    public function __construct(public $result, public $error = null)
    {
    }

    public function isDone(): bool
    {
        // normally run invoice.fulfill() after that
        return $this->result == self::RESULT_DONE;
    }

    public function isFailed(): bool
    {
        // run invoice.payfailed() after that
        return $this->result == self::RESULT_FAILED;
    }

    public function isStillPending(): bool
    {
        // Normally for services that immediately returns a result already
        //
        return $this->result == self::RESULT_STILL_PENDING;
    }

    public function isVerificationNotNeeded(): string
    {
        return $this->result = self::RESULT_VERIFICATION_NOT_NEEDED;
    }
}
