<?php

namespace App\Library;

use Acelle\Library\Contracts\PaymentGatewayInterface;
use Acelle\Library\Facades\Billing;
use Exception;

class AutoBillingData
{
    public function __construct(protected PaymentGatewayInterface $gateway, protected $data = [])
    {
    }

    public function toJson(): string|bool
    {
        return json_encode([
            'type' => $this->gateway->getType(),
            'data' => $this->data,
        ]);
    }

    public function getGateway():  \App\Library\Contracts\PaymentGatewayInterface
    {
        return $this->gateway;
    }

    public function getData()
    {
        return $this->data['data'];
    }

    public static function fromJson($json): ? \App\Library\AutoBillingData
    {
        $data = json_decode($json, true);

        throw_if(! isset($data['type']), new Exception('Missing type from auto billing data json'));

        // service is not registered -> no valid AutoBillingData -> return null
        if (! Billing::isGatewayRegistered($data['type'])) {
            return null;
        }

        $gateway = Billing::getGateway($data['type']);

        unset($data['type']);

        return new self($gateway, $data);
    }
}
