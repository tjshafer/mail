<?php

namespace App\Http\Middleware;

use App\Model\User;
use Closure;

class Frontend
{
    /**
     * Handle an incoming request.
     *
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next, $guard = null)
    {
        $user = $request->user();

        // If user have no frontend access but has backend access
        if (isset($user) && ! $user->can('customer_access', User::class) && $user->can('admin_access', User::class)) {
            return redirect()->action([\App\Http\Controllers\Admin\HomeController::class, 'index']);
        }

        // check if user not authorized for customer access
        if (! $user->can('customer_access', User::class)) {
            return redirect()->action([\App\Http\Controllers\Controller::class, 'notAuthorized']);
        }

        // Site offline
        if ( \App\Model\Setting::get('site_online') == 'false' &&
            (isset($user) && $user->customer->getOption('access_when_offline') != 'yes')
        ) {
            return redirect()->action([\App\Http\Controllers\Controller::class, 'offline']);
        }

        // If user is disabled
        if (
            (isset($user) && is_object($user->customer) && ! $user->customer->isActive() && is_null($user->admin))
        ) {
            return redirect()->action([\App\Http\Controllers\Controller::class, 'userDisabled']);
        }

        // Language
        if (is_object($user->customer->language)) {
            \App::setLocale($user->customer->language->code);
            \Illuminate\Support\Carbon::setLocale($user->customer->language->code);
        }

        return $next($request);
    }
}
