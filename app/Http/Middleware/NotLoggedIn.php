<?php

namespace App\Http\Middleware;

use Closure;

class NotLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        $default_language = \App\Model\Language::find( \App\Model\Setting::get('default_language'));
        if (isset($_COOKIE['last_language_code'])) {
            $language_code = $_COOKIE['last_language_code'];
        } elseif (is_object($default_language)) {
            $language_code = $default_language->code;
        } else {
            $language_code = 'en';
        }

        // Language
        try {
            if ($language_code) {
                \App::setLocale($language_code);
                \Illuminate\Support\Carbon::setLocale($language_code);
            }
        } catch (\Exception $e) {
        }

        return $next($request);
    }
}
