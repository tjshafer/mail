<?php

namespace App\Http\Controllers;

class SamplesController extends Controller
{
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('samples.index');
    }
}
