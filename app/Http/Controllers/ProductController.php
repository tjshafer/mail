<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('products.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $products = Product::search($request)->paginate($request->per_page);
        $view = $request->view ? $request->view : 'grid';

        return view('products._list_'.$view, [
            'products' => $products,
        ]);
    }

    public function image(Request $request, $uid)
    {
        $product = Product::findByUid($uid);

        if ($product->getImagePath()) {
            return response()->file($product->getImagePath());
        } else {
            return response()->file(public_path('image/no-product-image.png'));
        }
    }

    /**
     * Display a listing of the resource.
     */
    public function json(Request $request): \Illuminate\Http\JsonResponse|string
    {
        // listing product for
        if ($request->action == 'list') {
            return response()->json(
                Product::search($request)->paginate($request->per_page)
                    ->map(fn($product, $key) => [
                        'id' => $product->uid,
                        'name' => $product->title,
                        'price' => format_price($product->price),
                        'image' => action([\App\Http\Controllers\ProductController::class, 'image'], $product->uid),
                        'description' => substr(strip_tags($product->description), 0, 100),
                        'link' => action([\App\Http\Controllers\ProductController::class, 'index']),
                    ])->toArray()
            );
        }

        // return a product info
        if ($request->product_id) {
            $product = Product::findByUid($request->product_id);

            return response()->json([
                'id' => $product->uid,
                'name' => $product->title,
                'price' => format_price($product->price),
                'image' => action([\App\Http\Controllers\ProductController::class, 'image'], $product->uid),
                'description' => substr(strip_tags($product->description), 0, 100),
                'link' => action([\App\Http\Controllers\ProductController::class, 'index']),
            ]);
        }

        $results = Product::search($request)->paginate($request->per_page)
        ->map(fn($item, $key) => ['text' => $item->title, 'id' => $item->uid])->toArray();

        $json = '{
            "items": '.json_encode($results).',
            "more": '.(empty($results) ? 'false' : 'true').'
        }';

        return $json;
    }
}
