<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmailVerificationServerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new \App\Model\EmailVerificationServer())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);
        $servers = \App\Model\EmailVerificationServer::search($request);

        return view('email_verification_servers.index', [
            'servers' => $servers,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new \App\Model\EmailVerificationServer())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);
        $servers = \App\Model\EmailVerificationServer::search($request)->paginate($request->per_page);

        return view('email_verification_servers._list', [
            'servers' => $servers,
        ]);
    }

    /**
     * Select sending server type.
     */
    public function select(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('email_verification_servers.select');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = new \App\Model\EmailVerificationServer();
        $server->status = \App\Model\EmailVerificationServer::STATUS_ACTIVE;
        $server->uid = '0';
        $server->fill($request->old());

        // authorize
        if (! $request->user()->customer->can('create', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        $options = [];
        if (! empty($request->old()['options'])) {
            $options = $request->old()['options'];
        }

        return view('email_verification_servers.create', [
            'server' => $server,
            'options' => $options,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $server = new \App\Model\EmailVerificationServer();

        // authorize
        if (! $request->user()->customer->can('create', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $server->fill($request->all());

            $this->validate($request, $server->rules());

            // Save current user info
            $server->customer_id = $request->user()->customer->id;
            $server->status = \App\Model\EmailVerificationServer::STATUS_ACTIVE;
            $server->options = json_encode($request->options);

            if ($server->save()) {
                // Log
                $server->log('created', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.email_verification_server.created'));

                return redirect()->action([\App\Http\Controllers\EmailVerificationServerController::class, 'index']);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\EmailVerificationServer::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        $options = $server->getOptions();
        if (! empty($request->old()['options'])) {
            $options = $request->old()['options'];
        }

        return view('email_verification_servers.edit', [
            'server' => $server,
            'options' => $options,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $server = \App\Model\EmailVerificationServer::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $server->fill($request->all());

            $this->validate($request, $server->rules());

            // Save current user info
            $server->options = json_encode($request->options);

            if ($server->save()) {
                // Log
                $server->log('updated', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.email_verification_server.updated'));

                return redirect()->action([\App\Http\Controllers\EmailVerificationServerController::class, 'index']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = \App\Model\EmailVerificationServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('delete', $item)) {
                // Log
                $item->log('deleted', $request->user()->customer);

                $item->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.email_verification_servers.deleted');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function disable(Request $request): void
    {
        $items = \App\Model\EmailVerificationServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('disable', $item)) {
                $item->disable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.email_verification_servers.disabled');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function enable(Request $request): void
    {
        $items = \App\Model\EmailVerificationServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('enable', $item)) {
                $item->enable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.email_verification_servers.enabled');
    }

    /**
     * Email verification server display options form.
     */
    public function options(Request $request, $uid = null): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($uid) {
            $server = \App\Model\EmailVerificationServer::findByUid($uid);
        } else {
            $server = new \App\Model\EmailVerificationServer($request->all());
            $options = $server->getOptions();
        }

        return view('email_verification_servers._options', [
            'server' => $server,
            'options' => $options,
        ]);
    }
}
