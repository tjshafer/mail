<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlanController extends Controller
{
    /**
     * Select2 plan.
     */
    public function select2(Request $request): void
    {
        echo \App\Model\Plan::select2($request);
    }
}
