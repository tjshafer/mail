<?php

namespace App\Http\Controllers;

use App\Model\Subscription;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Log in back user.
     */
    public function loginBack(Request $request): \Illuminate\Http\RedirectResponse
    {
        if ($request->user()->admin) {
            return redirect()->action([\App\Http\Controllers\Admin\HomeController::class, 'index']);
        }

        $id = \Session::pull('orig_customer_id');
        $orig_user = \App\Model\User::findByUid($id);

        \Auth::login($orig_user);

        return redirect()->action([\App\Http\Controllers\Admin\CustomerController::class, 'index']);
    }

    /**
     * Admin area.
     */
    public function adminArea(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $id = \Session::get('orig_customer_id');
        $orig_user = \App\Model\User::findByUid($id);

        // Get current subscription
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;

        $next_billing_date = null;
        if (is_object($subscription)) {
            $next_billing_date = $subscription->ends_at;
            if ($subscription->isActive()) {
                $next_billing_date = $subscription->current_period_ends_at;
            }
        }

        return view('customers.admin_area', [
            'customer' => $customer,
            'subscription' => $subscription,
            'next_billing_date' => $next_billing_date,
        ]);
    }
}
