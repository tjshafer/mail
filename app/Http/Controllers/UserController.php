<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use App\Model\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Log in back user.
     */
    public function loginBack(Request $request): \Illuminate\Http\RedirectResponse
    {
        $id = \Session::pull('orig_user_id');
        $orig_user = User::findByUid($id);

        \Auth::login($orig_user);

        return redirect()->action([\App\Http\Controllers\Admin\UserController::class, 'index']);
    }

    /**
     * Activate user account.
     *
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $token)
    {
        $userActivation = \App\Model\UserActivation::where('token', '=', $token)->first();

        if (! $userActivation) {
            return view('notAuthorized');
        } else {
            $userActivation->user->setActivated();

            $request->session()->put('user-activated', trans('messages.user.activated'));

            if (isset($request->redirect)) {
                return redirect()->away(urldecode($request->redirect));
            } else {
                return redirect()->action([\App\Http\Controllers\HomeController::class, 'index']);
            }
        }
    }

    /**
     * Resen activation confirmation email.
     */
    public function resendActivationEmail(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $user = User::findByUid($request->uid);

        try {
            $user->sendActivationMail($user->email, action([\App\Http\Controllers\HomeController::class, 'index']));
        } catch (\Exception $e) {
            return view('somethingWentWrong', ['message' => trans('messages.something_went_wrong_with_email_service').': '.$e->getMessage()]);
        }

        return view('users.registration_confirmation_sent');
    }

    /**
     * User registration.
     */
    public function register(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ( \App\Model\Setting::get('enable_user_registration') == 'no') {
            return $this->notAuthorized();
        }

        // If already logged in
        if (! is_null($request->user())) {
            return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
        }

        // Initiate customer object for filling the form
        $customer = new Customer();
        $user = new User();
        if (! empty($request->old())) {
            $customer->fill($request->old());
            $user->fill($request->old());
        }

        // save posted data
        if ($request->isMethod('post')) {
            $user->fill($request->all());
            $rules = $user->registerRules();

            // Captcha check
            if ( \App\Model\Setting::get('registration_recaptcha') == 'yes') {
                $success =  \App\Library\Tool::checkReCaptcha($request);
                if (! $success) {
                    $rules['recaptcha_invalid'] = 'required';
                }
            }

            $this->validate($request, $rules);

            // Okay, create it
            $user = $customer->createAccountAndUser($request);

            // Send registration confirmation email
            try {
                $user->sendActivationMail($user->displayName());
            } catch (\Exception $e) {
                return view('somethingWentWrong', ['message' => trans('messages.something_went_wrong_with_email_service').': '.$e->getMessage()]);
            }

            return view('users.register_confirmation_notice');
        }

        return view('users.register', [
            'customer' => $customer,
            'user' => $user,
        ]);
    }
}
