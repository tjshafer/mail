<?php

namespace App\Http\Controllers;

use App\Model\Lazada;
use App\Model\Product;
use App\Model\Source;
use App\Model\WooCommerce;
use Illuminate\Http\Request;

class SourceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('sources.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $sources = Source::search($request)->paginate($request->per_page);

        return view('sources._list', [
            'sources' => $sources,
        ]);
    }

    /**
     * Show source details.
     */
    public function show(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $source = Source::findByUid($uid);
        $automation = $request->user()->customer->getAbandonedEmailAutomation($source);

        return view('sources.show', [
            'source' => $source,
            'automation' => $automation,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $customer = $request->user()->customer;

        // Get lazada connection
        $lazadaSource = $request->user()->customer->newProductSource('Lazada');

        // authorize
        if (\Gate::denies('create', Source::class)) {
            return $this->notAuthorized();
        }

        return view('sources.create', [
            'lazadaConnectLink' => $lazadaSource->service()->getConnectLink(),
        ]);
    }

    /**
     * Receive code and generate token.
     */
    public function connect(Request $request): \Illuminate\Http\RedirectResponse
    {
        // connection
        $lazadaSource = $request->user()->customer->newProductSource('Lazada');
        $lazadaSource->init($request->code);

        // redirect
        $request->session()->flash('success', 'Connected to Lazada!');

        return redirect()->action([\App\Http\Controllers\SourceController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if (isSiteDemo()) {
            echo trans('messages.operation_not_allowed_in_demo');

            return;
        }

        $sources = \App\Model\Source::whereIn('uid', explode(',', $request->uids));

        foreach ($sources->get() as $source) {
            // authorize
            if (! \Gate::allows('delete', $source)) {
                // Redirect to my lists page
                return response()->json([
                    'status' => 'error',
                    'message' => trans('messages.source.delete.can_not'),
                ]);
            }

            // do delete
            $source->delete();
        }

        // Redirect to my lists page
        return response()->json([
            'status' => 'success',
            'message' => trans('messages.source.deleted'),
        ]);
    }

    /**
     * Import product from source.
     */
    public function sync(Request $request, $uid): \Illuminate\Http\RedirectResponse
    {
        // connection
        $source = Source::findByUid($uid);
        $source = $source->classMapping();

        // import products
        $source->sync();

        // redirect
        $request->session()->flash('alert-success', 'Products were imported!');

        return redirect()->action([\App\Http\Controllers\SourceController::class, 'index']);
    }

    /**
     * Connect to WooCommerce.
     */
    public function wooConnect(Request $request): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // saving
        if ($request->isMethod('post')) {
            [$source, $validator] = WooCommerce::init($request->connect_url, $request->user()->customer);

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('sources.wooConnect', [
                    'errors' => $validator->errors(),
                ], 400);
            }

            // success
            return response()->json([
                'status' => 'success',
                'redirect' => action([\App\Http\Controllers\SourceController::class, 'index']),
            ]);
        }

        return view('sources.wooConnect');
    }
}
