<?php

namespace App\Http\Controllers;

use App\Model\Attachment;
use App\Model\Automation2;
use App\Model\Email;
use App\Model\MailList;
use App\Model\Subscriber;
use App\Model\Template;
use App\Model\TemplateCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Automation2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('automation2.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automations = $request->user()->customer->automation2s()
            ->search($request->keyword)
            ->orderBy($request->sort_order, $request->sort_direction)
            ->paginate($request->per_page);

        return view('automation2._list', [
            'automations' => $automations,
        ]);
    }

    /**
     * Creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = $request->user()->customer;

        // init automation
        $automation = new Automation2([
            'name' => trans('messages.automation.untitled'),
        ]);
        $automation->status = Automation2::STATUS_INACTIVE;

        // authorize
        if (\Gate::denies('create', $automation)) {
            return $this->noMoreItem();
        }

        // saving
        if ($request->isMethod('post')) {
            // fill before save
            $automation->fillRequest($request);

            // make validator
            $validator = Validator::make($request->all(), $automation->rules());

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.create', [
                    'automation' => $automation,
                    'errors' => $validator->errors(),
                ], 400);
            }

            // pass validation and save
            $automation->mail_list_id = MailList::findByUid($request->mail_list_uid)->id;
            $automation->customer_id = $customer->id;
            $automation->data = '[{"title":"Click to choose a trigger","id":"trigger","type":"ElementTrigger","options":{"init":"false", "key": ""}}]';
            $automation->save();

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.created.redirecting'),
                'url' => action([\App\Http\Controllers\Automation2Controller::class, 'edit'], ['uid' => $automation->uid]),
            ], 201);
        }

        return view('automation2.create', [
            'automation' => $automation,
        ]);
    }

    /**
     * Update automation.
     */
    public function update(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $customer = $request->user()->customer;

        // find automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // fill before save
        $automation->fillRequest($request);

        // make validator
        $validator = Validator::make($request->all(), $automation->rules());

        // redirect if fails
        if ($validator->fails()) {
            return response()->view('automation2.settings', [
                'automation' => $automation,
                'errors' => $validator->errors(),
            ], 400);
        }

        // pass validation and save
        $automation->updateMailList(MailList::findByUid($request->mail_list_uid));

        // save
        $automation->save();

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.updated'),
        ], 201);
    }

    /**
     * Update automation.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveData(Request $request, $uid)
    {
        // find automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        if ($request->resetTrigger) {
            $automation->saveDataAndResetTriggers($request->data);
        } else {
            $automation->saveData($request->data);
        }
    }

    /**
     * Creating a new resource.
     */
    public function edit(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $automation->updateCacheInBackground();

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.edit', [
            'automation' => $automation,
        ]);
    }

    /**
     * Automation settings in sidebar.
     */
    public function settings(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.settings', [
            'automation' => $automation,
        ]);
    }

    /**
     * Select trigger type popup.
     */
    public function triggerSelectPupop(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $types = [
            'welcome-new-subscriber',
            'say-happy-birthday',
            'subscriber-added-date',
            'specific-date',
            'say-goodbye-subscriber',
            'api-3-0',
            'weekly-recurring',
            'monthly-recurring',
        ];

        if (config('custom.woo')) {
            $types[] = 'woo-abandoned-cart';
        }

        return view('automation2.triggerSelectPupop', [
            'types' => $types,
            'automation' => $automation,
            'trigger' => $automation->getTrigger(),
        ]);
    }

    /**
     * Select trigger type confirm.
     */
    public function triggerSelectConfirm(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $rules = $this->triggerRules()[$request->key];

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.triggerSelectConfirm', [
            'key' => $request->key,
            'automation' => $automation,
            'trigger' => $automation->getTrigger(),
            'rules' => $rules,
        ]);
    }

    /**
     * Select trigger type.
     */
    public function triggerRules(): array
    {
        return [
            'welcome-new-subscriber' => [],
            'say-happy-birthday' => [
                'options.before' => 'required',
                'options.at' => 'required',
                'options.field' => 'required',
            ],
            'specific-date' => [
                'options.date' => 'required',
                'options.at' => 'required',
            ],
            'say-goodbye-subscriber' => [],
            'api-3-0' => [],
            'subscriber-added-date' => [
                'options.delay' => 'required',
                'options.at' => 'required',
            ],
            'weekly-recurring' => [
                'options.days_of_week' => 'required',
                'options.at' => 'required',
            ],
            'monthly-recurring' => [
                'options.days_of_month' => 'required|array|min:1',
                'options.at' => 'required',
            ],
            'woo-abandoned-cart' => [
                'options.source_uid' => 'required',
            ],
        ];
    }

    /**
     * Validate trigger.
     */
    public function vaidateTrigger($request, $type): array
    {
        $valid = true;

        $rules = $this->triggerRules()[$type];

        // make validator
        $validator = Validator::make($request->all(), $rules);

        $valid = $valid && ! $validator->fails();

        return [$validator,  $valid];
    }

    /**
     * Select trigger type.
     */
    public function triggerSelect(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        [$validator, $result] = $this->vaidateTrigger($request, $request->options['key']);

        // redirect if fails
        if (! $result) {
            return response()->view('automation2.triggerSelectConfirm', [
                'key' => $request->options['key'],
                'automation' => $automation,
                'trigger' => $automation->getTrigger(),
                'rules' => $this->triggerRules()[$request->options['key']],
                'errors' => $validator->errors(),
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.trigger.added'),
            'title' => trans('messages.automation.trigger.title', [
                'title' => trans('messages.automation.trigger.tree.'.$request->options['key']),
            ]),
            'options' => $request->options,
            'rules' => $this->triggerRules()[$request->options['key']],
        ]);
    }

    /**
     * Select action type popup.
     */
    public function actionSelectPupop(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $types = [
            'send-an-email',
            'wait',
            'condition',
        ];

        if (config('custom.woo')) {
            $types[] = 'operation';
        }

        return view('automation2.actionSelectPupop', [
            'types' => $types,
            'automation' => $automation,
            'hasChildren' => $request->hasChildren,
        ]);
    }

    /**
     * Select action type confirm.
     */
    public function actionSelectConfirm(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.actionSelectConfirm', [
            'key' => $request->key,
            'automation' => $automation,
            'element' => $automation->getElement(),
        ]);
    }

    /**
     * Select action type confirm.
     */
    public function conditionSetting(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.condition.setting', [
            'automation' => $automation,
            'element' => $automation->getElement($request->element_id),
        ]);
    }

    /**
     * Select trigger type.
     *
     * @return \Illuminate\Http\Response
     */
    public function actionSelect(Request $request, $uid)
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        if ($request->key == 'wait') {
            $delayOptions = $automation->getDelayOptions();
            $parts = explode(' ', $request->time);
            $title = trans('messages.time.wait_for').' '.$parts[0].' '.trans_choice('messages.time.'.$parts[1], $parts[0]);

            foreach ($delayOptions as $deplayOption) {
                if ($deplayOption['value'] == $request->time) {
                    $title = trans('messages.automation.wait.delay.'.$request->time);
                }
            }

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.action.added'),
                'title' => $title,
                'options' => [
                    'key' => $request->key,
                    'time' => $request->time,
                ],
            ]);
        } elseif ($request->key == 'condition') {
            if ($request->type == 'open') {
                return response()->json([
                    'status' => 'success',
                    'message' => trans('messages.automation.action.added'),
                    'title' => trans('messages.automation.action.condition.read_email.title'),
                    'options' => [
                        'key' => $request->key,
                        'type' => $request->type,
                        'email' => empty($request->email) ? null : $request->email,
                    ],
                ]);
            } elseif ($request->type == 'click') {
                return response()->json([
                    'status' => 'success',
                    'message' => trans('messages.automation.action.added'),
                    'title' => trans('messages.automation.action.condition.click_link.title'),
                    'options' => [
                        'key' => $request->key,
                        'type' => $request->type,
                        'email_link' => empty($request->email_link) ? null : $request->email_link,
                    ],
                ]);
            } elseif ($request->type == 'cart_buy_anything') {
                return response()->json([
                    'status' => 'success',
                    'message' => trans('messages.automation.action.updated'),
                    'title' => trans('messages.automation.action.condition.cart_buy_anything.title'),
                    'options' => [
                        'key' => $request->key,
                        'type' => $request->type,
                    ],
                ]);
            } elseif ($request->type == 'cart_buy_item') {
                return response()->json([
                    'status' => 'success',
                    'message' => trans('messages.automation.action.updated'),
                    'title' => trans('messages.automation.action.condition.cart_buy_item.title', [
                        'item' => $request->item_title,
                    ]),
                    'options' => [
                        'key' => $request->key,
                        'type' => $request->type,
                        'item_id' => $request->item_id,
                        'item_title' => $request->item_title,
                    ],
                ]);
            }
        } else {
            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.action.added'),
                'title' => trans('messages.automation.action.title', [
                    'title' => trans('messages.automation.action.'.$request->key),
                ]),
                'options' => [
                    'key' => $request->key,
                    'after' => $request->after,
                ],
            ]);
        }
    }

    /**
     * Edit trigger.
     */
    public function triggerEdit(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $rules = $this->triggerRules()[$request->key];

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        if ($request->isMethod('post')) {
            [$validator, $result] = $this->vaidateTrigger($request, $request->options['key']);

            // redirect if fails
            if (! $result) {
                return response()->view('automation2.triggerEdit', [
                    'key' => $request->options['key'],
                    'automation' => $automation,
                    'trigger' => $automation->getTrigger(),
                    'rules' => $this->triggerRules()[$request->options['key']],
                    'errors' => $validator->errors(),
                ], 400);
            }

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.trigger.updated'),
                'title' => trans('messages.automation.trigger.title', [
                    'title' => trans('messages.automation.trigger.tree.'.$request->options['key']),
                ]),
                'options' => $request->options,
            ]);
        }

        return view('automation2.triggerEdit', [
            'key' => $request->key,
            'automation' => $automation,
            'trigger' => $automation->getTrigger(),
            'rules' => $rules,
        ]);
    }

    /**
     * Edit action.
     */
    public function actionEdit(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
            if ($request->key == 'wait') {
                $delayOptions = $automation->getDelayOptions();
                $parts = explode(' ', $request->time);
                $title = trans('messages.time.wait_for').' '.$parts[0].' '.trans_choice('messages.time.'.$parts[1], $parts[0]);

                foreach ($delayOptions as $deplayOption) {
                    if ($deplayOption['value'] == $request->time) {
                        $title = trans('messages.automation.wait.delay.'.$request->time);
                    }
                }

                return response()->json([
                    'status' => 'success',
                    'message' => trans('messages.automation.action.updated'),
                    'title' => $title,
                    'options' => [
                        'key' => $request->key,
                        'time' => $request->time,
                    ],
                ]);
            } elseif ($request->key == 'condition') {
                if ($request->type == 'open') {
                    return response()->json([
                        'status' => 'success',
                        'message' => trans('messages.automation.action.updated'),
                        'title' => trans('messages.automation.action.condition.read_email.title'),
                        'options' => [
                            'key' => $request->key,
                            'type' => $request->type,
                            'email' => empty($request->email) ? null : $request->email,
                        ],
                    ]);
                } elseif ($request->type == 'click') {
                    return response()->json([
                        'status' => 'success',
                        'message' => trans('messages.automation.action.updated'),
                        'title' => trans('messages.automation.action.condition.click_link.title'),
                        'options' => [
                            'key' => $request->key,
                            'type' => $request->type,
                            'email_link' => empty($request->email_link) ? null : $request->email_link,
                        ],
                    ]);
                } elseif ($request->type == 'cart_buy_anything') {
                    return response()->json([
                        'status' => 'success',
                        'message' => trans('messages.automation.action.updated'),
                        'title' => trans('messages.automation.action.condition.cart_buy_anything.title'),
                        'options' => [
                            'key' => $request->key,
                            'type' => $request->type,
                        ],
                    ]);
                } elseif ($request->type == 'cart_buy_item') {
                    return response()->json([
                        'status' => 'success',
                        'message' => trans('messages.automation.action.updated'),
                        'title' => trans('messages.automation.action.condition.cart_buy_item.title', [
                            'item' => $request->item_title,
                        ]),
                        'options' => [
                            'key' => $request->key,
                            'type' => $request->type,
                            'item_id' => $request->item_id,
                            'item_title' => $request->item_title,
                        ],
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 'success',
                    'message' => trans('messages.automation.action.updated'),
                    'title' => trans('messages.automation.action.title', [
                        'title' => trans('messages.automation.action.'.$request->key),
                    ]),
                    'options' => [
                        'key' => $request->key,
                        'after' => $request->after,
                    ],
                ]);
            }
        }

        return view('automation2.actionEdit', [
            'key' => $request->key,
            'automation' => $automation,
            'element' => $automation->getElement($request->id),
        ]);
    }

    /**
     * Email setup.
     */
    public function emailSetup(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        if ($request->email_uid) {
            $email = Email::findByUid($request->email_uid);
        } else {
            $email = new Email([
                'sign_dkim' => true,
                'track_open' => true,
                'track_click' => true,
                'action_id' => $request->action_id,
            ]);
        }

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
            // fill before save
            $email->fillAttributes($request->all());

            // Tacking domain
            if (isset($params['custom_tracking_domain']) && $params['custom_tracking_domain'] && isset($params['tracking_domain_uid'])) {
                $tracking_domain = \App\Model\TrackingDomain::findByUid($params['tracking_domain_uid']);
                if (is_object($tracking_domain)) {
                    $this->tracking_domain_id = $tracking_domain->id;
                } else {
                    $this->tracking_domain_id = null;
                }
            } else {
                $this->tracking_domain_id = null;
            }

            // make validator
            $validator = Validator::make($request->all(), $email->rules($request));

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.email.setup', [
                    'automation' => $automation,
                    'email' => $email,
                    'errors' => $validator->errors(),
                ], 400);
            }

            // pass validation and save
            $email->automation2_id = $automation->id;
            $email->customer_id = $automation->customer_id;
            $email->save();

            return response()->json([
                'status' => 'success',
                'title' => trans('messages.automation.send_a_email', ['title' => $email->subject]),
                'message' => trans('messages.automation.email.set_up.success'),
                'url' => action([\App\Http\Controllers\Automation2Controller::class, 'emailTemplate'], [
                    'uid' => $automation->uid,
                    'email_uid' => $email->uid,
                ]),
                'options' => [
                    'email_uid' => $email->uid,
                ],
            ], 201);
        }

        return view('automation2.email.setup', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Delete automation email.
     */
    public function emailDelete(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // delete email
        $email->deleteAndCleanup();

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.email.deteled'),
        ], 201);
    }

    /**
     * Email template.
     */
    public function emailTemplate(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        if (! $email->hasTemplate()) {
            return redirect()->action([\App\Http\Controllers\Automation2Controller::class, 'templateCreate'], [
                'uid' => $automation->uid,
                'email_uid' => $email->uid,
            ]);
        }

        return view('automation2.email.template', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Email show.
     */
    public function email(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.email.index', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Email confirm.
     */
    public function emailConfirm(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
        }

        return view('automation2.email.confirm', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Create template.
     */
    public function templateCreate(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.email.template.create', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Create template from layout.
     */
    public function templateLayout(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        if ($request->isMethod('post')) {
            $template = Template::findByUid($request->template_uid);
            $email->setTemplate($template);

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.email.template.theme.selected'),
            ], 201);
        }

        // GET goes here
        if ($request->category_uid) {
            $category = TemplateCategory::findByUid($request->category_uid);
        } else {
            $category = TemplateCategory::first();
        }

        return view('automation2.email.template.layout', [
            'automation' => $automation,
            'email' => $email,
            'category' => $category,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function templateLayoutList(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($request->uid);
        $email = Email::findByUid($request->email_uid);

        // from
        if ($request->from == 'mine') {
            $templates = $request->user()->customer->templates();
        } elseif ($request->from == 'gallery') {
            $templates = Template::shared();
        } else {
            $templates = Template::shared()
                ->orWhere('customer_id', '=', $request->user()->customer->id);
        }

        $templates = $templates->search($request->keyword)
            ->categoryUid($request->category_uid)
            ->orderBy($request->sort_order, $request->sort_direction)
            ->paginate($request->per_page);

        return view('automation2.email.template.layoutList', [
            'automation' => $automation,
            'email' => $email,
            'templates' => $templates,
        ]);
    }

    /**
     * Select builder for editing template.
     */
    public function templateBuilderSelect(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.email.template.templateBuilderSelect', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Edit campaign template.
     */
    public function templateEdit(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // save campaign html
        if ($request->isMethod('post')) {
            $rules = [
                'content' => 'required',
            ];

            $this->validate($request, $rules);

            $email->setTemplateContent($request->content);
            $email->save();

            return response()->json([
                'status' => 'success',
            ]);
        }

        return view('automation2.email.template.edit', [
            'automation' => $automation,
            'list' => $automation->mailList,
            'email' => $email,
            'templates' => $request->user()->customer->getBuilderTemplates(),
        ]);
    }

    /**
     * Campaign html content.
     */
    public function templateContent(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.email.template.content', [
            'content' => $email->getTemplateContent(),
        ]);
    }

    /**
     * Upload template.
     */
    public function templateUpload(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            $email->uploadTemplate($request);

            // return redirect()->action([\App\Http\Controllers\CampaignController::class, 'template'], $campaign->uid);
            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.email.template.uploaded'),
            ]);

            // throw a validation error otherwise
        }

        return view('automation2.email.template.upload', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Remove exist template.
     */
    public function templateRemove(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $email->removeTemplate();

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.email.template.removed'),
        ], 201);
    }

    /**
     * Template preview.
     */
    public function templatePreview(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.email.template.preview', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Template preview.
     *
     * @return \Illuminate\Http\Response
     */
    public function templatePreviewContent(Request $request, $uid, $email_uid)
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        echo $email->getHtmlContent();
    }

    /**
     * Attachment upload.
     *
     * @return \Illuminate\Http\Response
     */
    public function emailAttachmentUpload(Request $request, $uid, $email_uid)
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        foreach ($request->file as $file) {
            $email->uploadAttachment($file);
        }
    }

    /**
     * Attachment remove.
     */
    public function emailAttachmentRemove(Request $request, $uid, $email_uid, $attachment_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $automation = Automation2::findByUid($uid);
        $attachment = Attachment::findByUid($request->attachment_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $attachment->remove();

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.email.attachment.removed'),
        ], 201);
    }

    /**
     * Attachment download.
     */
    public function emailAttachmentDownload(Request $request, $uid, $email_uid, $attachment_uid): \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($request->email_uid);
        $attachment = Attachment::findByUid($request->attachment_uid);

        // authorize
        if (\Gate::denies('read', $automation)) {
            return $this->notAuthorized();
        }

        return response()->download(storage_path('app/'.$attachment->file), $attachment->name);
    }

    /**
     * Enable automation.
     *
     * @param  int  $id
     */
    public function enable(Request $request): \Illuminate\Http\JsonResponse
    {
        $automations = Automation2::whereIn('uid', explode(',', $request->uids));

        foreach ($automations->get() as $automation) {
            // authorize
            if (\Gate::allows('enable', $automation)) {
                $automation->enable();
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => trans_choice('messages.automation.enabled', $automations->count()),
        ]);
    }

    /**
     * Disable event.
     *
     * @param  int  $id
     */
    public function disable(Request $request): \Illuminate\Http\JsonResponse
    {
        $automations = Automation2::whereIn('uid', explode(',', $request->uids));

        foreach ($automations->get() as $automation) {
            // authorize
            if (\Gate::allows('disable', $automation)) {
                $automation->disable();
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => trans_choice('messages.automation.disabled', $automations->count()),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(Request $request): \Illuminate\Http\JsonResponse
    {
        if (isSiteDemo()) {
            return response()->json([
                'status' => 'notice',
                'message' => trans('messages.operation_not_allowed_in_demo'),
            ], 403);
        }

        $automations = Automation2::whereIn('uid', explode(',', $request->uids));

        foreach ($automations->get() as $automation) {
            // authorize
            if (\Gate::allows('delete', $automation)) {
                $automation->delete();
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => trans_choice('messages.automation.deleted', $automations->count()),
        ]);
    }

    /**
     * Automation insight page.
     */
    public function insight(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.insight', [
            'automation' => $automation,
            'stats' => $automation->readCache('SummaryStats'),
            'insight' => $automation->getInsight(),
        ]);
    }

    /**
     * Automation contacts list.
     */
    public function contacts(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        $subscribers = $automation->subscribers();
        $count = $subscribers->count();

        return view('automation2.contacts.index', [
            'automation' => $automation,
            'count' => $count,
            'stats' => $automation->getSummaryStats(),
            'subscribers' => $subscribers,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function contactsList(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        $sortBy = $request->sortBy ?: 'subscribers.id';
        $sortOrder = $request->sortOrder ?: 'DESC';

        // list by type
        $subscribers = $automation->getSubscribersWithTriggerInfo()
                                  ->simpleSearch($request->keyword)
                                  ->addSelect('subscribers.created_at')
                                  ->addSelect('auto_triggers.updated_at')
                                  ->orderBy($sortBy, $sortOrder);
        $contacts = $subscribers->paginate($request->per_page);

        return view('automation2.contacts.list', [
            'automation' => $automation,
            'contacts' => $contacts,
        ]);
    }

    /**
     * Automation timeline.
     */
    public function timeline(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.timeline.index', [
            'automation' => $automation,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function timelineList(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        $timelines = $automation->timelines()->paginate($request->per_page);

        return view('automation2.timeline.list', [
            'automation' => $automation,
            'timelines' => $timelines,
        ]);
    }

    /**
     * Automation contact profile.
     */
    public function profile(Request $request, $uid, $contact_uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $contact = Subscriber::findByUid($contact_uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.profile', [
            'automation' => $automation,
            'contact' => $contact,
        ]);
    }

    /**
     * Automation remove contact.
     */
    public function removeContact(Request $request, $uid, $contact_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $automation = Automation2::findByUid($uid);
        $contact = Subscriber::findByUid($contact_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.contact.deleted'),
        ], 201);
    }

    /**
     * Automation tag contact.
     */
    public function tagContact(Request $request, $uid, $contact_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $contact = Subscriber::findByUid($contact_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
            $contact->updateTags($request->tags);

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.contact.tagged', [
                    'contact' => $contact->getFullName(),
                ]),
            ], 201);
        }

        return view('automation2.contacts.tagContact', [
            'automation' => $automation,
            'contact' => $contact,
        ]);
    }

    /**
     * Automation tag contacts.
     */
    public function tagContacts(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $subscribers = $automation->subscribers();

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
            // make validator
            $validator = Validator::make($request->all(), [
                'tags' => 'required',
            ]);

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.contacts.tagContacts', [
                    'automation' => $automation,
                    'subscribers' => $subscribers,
                    'errors' => $validator->errors(),
                ], 400);
            }

            // Copy to list
            foreach ($subscribers->get() as $subscriber) {
                $subscriber->addTags($request->tags);
            }

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.contacts.tagged', [
                    'count' => $subscribers->count(),
                ]),
            ], 201);
        }

        return view('automation2.contacts.tagContacts', [
            'automation' => $automation,
            'subscribers' => $subscribers,
        ]);
    }

    /**
     * Automation remove contact tag.
     */
    public function removeTag(Request $request, $uid, $contact_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $automation = Automation2::findByUid($uid);
        $contact = Subscriber::findByUid($contact_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $contact->removeTag($request->tag);

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.contact.tag.removed', [
                'tag' => $request->tag,
            ]),
        ], 201);
    }

    /**
     * Automation export contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportContacts(Request $request, $uid)
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $subscribers = $automation->subscribers();

        // saving
        if ($request->isMethod('post')) {
            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.contacts.exported'),
            ], 201);
        }
    }

    /**
     * Automation copy contacts to new list.
     */
    public function copyToNewList(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $subscribers = $subscribers = $automation->subscribers();

        // saving
        if ($request->isMethod('post')) {
            // make validator
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.contacts.copyToNewList', [
                    'automation' => $automation,
                    'subscribers' => $subscribers,
                    'errors' => $validator->errors(),
                ], 400);
            }

            // Crate new list
            $list = $automation->mailList->copy($request->name);

            // Copy to list
            foreach ($subscribers->get() as $subscriber) {
                $subscriber->copy($list);
            }

            // update cache
            $list->updateCache();

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.contacts.copied_to_new_list', [
                    'count' => $subscribers->count(),
                    'list' => $list->name,
                ]),
            ], 201);
        }

        return view('automation2.contacts.copyToNewList', [
            'automation' => $automation,
            'subscribers' => $subscribers,
        ]);
    }

    /**
     * Automation template classic builder.
     */
    public function templateEditClassic(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
            $rules = [
                'content' => 'required',
            ];

            $this->validate($request, $rules);

            $email->setTemplateContent($request->content);

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.email.content.updated'),
            ], 201);
        }

        return view('automation2.email.template.editClassic', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Automation template classic builder.
     */
    public function templateEditPlain(Request $request, $uid, $email_uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $email = Email::findByUid($email_uid);

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        // saving
        if ($request->isMethod('post')) {
            $rules = [
                'plain' => 'required',
            ];

            // make validator
            $validator = Validator::make($request->all(), $rules);

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.email.template.editPlain', [
                    'automation' => $automation,
                    'email' => $email,
                    'errors' => $validator->errors(),
                ], 400);
            }

            $email->plain = $request->plain;
            $email->save();

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.automation.email.plain.updated'),
            ], 201);
        }

        return view('automation2.email.template.editPlain', [
            'automation' => $automation,
            'email' => $email,
        ]);
    }

    /**
     * Segment select.
     */
    public function segmentSelect(Request $request): string|\Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->list_uid) {
            return '';
        }

        // init automation
        if ($request->uid) {
            $automation = Automation2::findByUid($request->uid);

            // authorize
            if (\Gate::denies('view', $automation)) {
                return $this->notAuthorized();
            }
        } else {
            $automation = new Automation2();

            // authorize
            if (\Gate::denies('create', $automation)) {
                return $this->notAuthorized();
            }
        }
        $list = MailList::findByUid($request->list_uid);

        return view('automation2.segmentSelect', [
            'automation' => $automation,
            'list' => $list,
        ]);
    }

    /**
     * Display a listing of subscribers.
     */
    public function subscribers(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init
        $automation = Automation2::findByUid($uid);
        $list = $automation->mailList;

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.subscribers.index', [
            'automation' => $automation,
            'list' => $list,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function subscribersList(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // init
        $automation = Automation2::findByUid($uid);
        $list = $automation->mailList;

        // authorize
        if (\Gate::denies('update', $automation)) {
            return $this->notAuthorized();
        }

        $subscribers = $automation->subscribers()->search($request)
            ->where('mail_list_id', '=', $list->id);

        // $total = distinctCount($subscribers);
        $total = $subscribers->count();
        $subscribers->with(['mailList', 'subscriberFields']);
        $subscribers = \optimized_paginate($subscribers, $request->per_page, null, null, null, $total);

        $fields = $list->getFields->whereIn('uid', explode(',', $request->columns));

        return view('automation2.subscribers._list', [
            'automation' => $automation,
            'subscribers' => $subscribers,
            'total' => $total,
            'list' => $list,
            'fields' => $fields,
        ]);
    }

    /**
     * Remove subscriber from automation.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscribersRemove(Request $request, $uid, $subscriber_uid)
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $subscriber = Subscriber::findByUid($subscriber_uid);

        // authorize
        if (\Gate::denies('update', $subscriber)) {
            return;
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.subscriber.removed'),
        ], 201);
    }

    /**
     * Restart subscriber for automation.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscribersRestart(Request $request, $uid, $subscriber_uid)
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $subscriber = Subscriber::findByUid($subscriber_uid);

        // authorize
        if (\Gate::denies('update', $subscriber)) {
            return;
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('messages.automation.subscriber.restarted'),
        ], 201);
    }

    /**
     * Display a listing of subscribers.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscribersShow(Request $request, $uid, $subscriber_uid)
    {
        // init automation
        $automation = Automation2::findByUid($uid);
        $subscriber = Subscriber::findByUid($subscriber_uid);

        // authorize
        if (\Gate::denies('read', $subscriber)) {
            return;
        }

        return view('automation2.subscribers.show', [
            'automation' => $automation,
            'subscriber' => $subscriber,
        ]);
    }

    /**
     * Get last saved time.
     *
     * @return \Illuminate\Http\Response
     */
    public function lastSaved(Request $request, $uid)
    {
        // init automation
        $automation = Automation2::findByUid($uid);

        // authorize
        if (\Gate::denies('view', $automation)) {
            return;
        }

        return trans('messages.automation.designer.last_saved', ['time' => $automation->updated_at->diffForHumans()]);
    }

    public function debug(Request $request, $uid)
    {
        $automation = Automation2::findByUid($uid);
        $type = $automation->getTriggerAction()->getOption('key');

        switch ($type) {
            case Automation2::TRIGGER_TYPE_WELCOME_NEW_SUBSCRIBER:

                $subscribers = $automation->getSubscribersWithTriggerInfo();

                if ($request->input('orderBy')) {
                    $subscribers = $subscribers->orderBy($request->input('orderBy'), $request->input('orderDir'));
                }

                $subscribers = $subscribers->simplePaginate(50);

                return view('automation2.debug_list_subscription', [
                    'automation' => $automation,
                    'subscribers' => $subscribers,
                ]);

                break;

            case Automation2::TRIGGER_TYPE_SAY_GOODBYE_TO_SUBSCRIBER:

                $subscribers = $automation->getSubscribersWithTriggerInfo()->where('subscribers.status', '=', 'unsubscribed');

                if ($request->input('orderBy')) {
                    $subscribers = $subscribers->orderBy($request->input('orderBy'), $request->input('orderDir'));
                }

                $subscribers = $subscribers->simplePaginate(50);

                return view('automation2.debug_list_unsubscription', [
                    'automation' => $automation,
                    'subscribers' => $subscribers,
                ]);

                break;

            case Automation2::TRIGGER_TYPE_SAY_HAPPY_BIRTHDAY:
                $subscribers = $automation->getSubscribersWithDateOfBirth();

                if ($request->input('email')) {
                    $subscribers = $subscribers->searchByEmail($request->input('email'));
                }

                if ($request->input('orderBy')) {
                    $subscribers = $subscribers->orderBy($request->input('orderBy'), $request->input('orderDir'));
                }

                $subscribers = $subscribers->simplePaginate(50);

                return view('automation2.debug', [
                    'automation' => $automation,
                    'subscribers' => $subscribers,
                ]);

                break;

            default:
                // code...
                break;
        }
    }

    public function debugTrigger(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $trigger = AutoTrigger::findByUid($uid);

        return view('automation2.debug', [
            'trigger' => $trigger,
        ]);
    }

    public function triggerNow(Request $request)
    {
        $automation = Automation2::findByUid($request->automation);
        $subscriber = Subscriber::findByUid($request->subscriber);

        $existingTrigger = $automation->getAutoTriggerFor($subscriber);

        if (! is_null($existingTrigger)) {
            echo sprintf("%s already triggered. Click <a href='%s'>here</a> for more details", $subscriber->email, action([\App\Http\Controllers\AutoTrigger::class, 'show'], ['id' => $existingTrigger->id]));

            return;
        }

        // Manually trigger, force!
        $automation->logger()->info(sprintf('Manually trigger contact %s', $subscriber->email));

        // Force trigger a contact
        // Even inactive contacts - in case of Say-Goodbye-Trigger for example
        $trigger = $automation->initTrigger($subscriber, $force = true);

        return redirect()->action([\App\Http\Controllers\AutoTrigger::class, 'show'], ['id' => $trigger->id]);
    }

    /**
     * Get last saved time.
     */
    public function contactRetry(Request $request, $uid, $contact_uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);
        $contact = Subscriber::findByUid($contact_uid);
        // authorize
        if (\Gate::denies('view', $automation)) {
            return $this->notAuthorized();
        }

        return view('automation2.contacts.list.error_row', [
            'automation' => $automation,
            'contact' => $contact,
        ]);
    }

    /**
     * Get wait time.
     */
    public function waitTime(Request $request, $uid): \Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            return response()->json([
                'status' => 'success',
                'amount' => $request->amount,
                'unit' => $request->unit,
            ]);
        }

        return view('automation2.waitTime', [
            'automation' => $automation,
        ]);
    }

    /**
     * Change cart automation2 list for auto adding byuer.
     */
    public function cartWait(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            // make validator
            $validator = Validator::make($request->all(), [
                'amount' => 'required',
                'unit' => 'required',
            ]);
            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.cartWait', [
                    'automation' => $automation,
                    'trigger' => $automation->getTrigger(),
                    'errors' => $validator->errors(),
                ], 400);
            }

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.cart.wait_updated'),
                'options' => [
                    'wait' => $request->amount.'_'.$request->unit,
                ],
            ]);
        }

        return view('automation2.cartWait', [
            'automation' => $automation,
            'trigger' => $automation->getTrigger(),
        ]);
    }

    /**
     * Change cart automation2 list for auto adding byuer.
     */
    public function cartChangeList(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            // make validator
            $validator = Validator::make($request->all(), [
                'options.list_uid' => 'required',
            ]);
            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.cartChangeList', [
                    'automation' => $automation,
                    'trigger' => $automation->getTrigger(),
                    'errors' => $validator->errors(),
                ], 400);
            }

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.cart.list_updated'),
                'options' => $request->options,
            ]);
        }

        return view('automation2.cartChangeList', [
            'automation' => $automation,
            'trigger' => $automation->getTrigger(),
        ]);
    }

    /**
     * Change cart automation2 list for auto adding byuer.
     */
    public function cartChangeStore(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            // make validator
            $validator = Validator::make($request->all(), [
                'options.source_uid' => 'required',
            ]);
            // redirect if fails
            if ($validator->fails()) {
                return response()->view('automation2.cartChangeSore', [
                    'automation' => $automation,
                    'trigger' => $automation->getTrigger(),
                    'errors' => $validator->errors(),
                ], 400);
            }

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.cart.store_updated'),
                'options' => $request->options,
            ]);
        }

        return view('automation2.cartChangeSore', [
            'automation' => $automation,
            'trigger' => $automation->getTrigger(),
        ]);
    }

    /**
     * Cart stats.
     */
    public function cartStats(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        return view('automation2.cart.stats', [
            'automation' => $automation,
        ]);
    }

    /**
     * Cart list.
     */
    public function cartList(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        return view('automation2.cart.list', [
            'automation' => $automation,
        ]);
    }

    /**
     * Cart list.
     */
    public function cartItems(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        return view('automation2.cart.items', [
            'automation' => $automation,
        ]);
    }

    /**
     * Operation select popup.
     */
    public function operationSelect(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            // return response()->json([
            //     'status' => 'success',
            //     'amount' => $request->amount,
            //     'unit' => $request->unit
            // ]);
        }

        return view('automation2.operationSelect', [
            'automation' => $automation,
            'types' => [
                'update_contact',
                'tag_contact',
                'copy_contact',
            ],
        ]);
    }

    /**
     * Operation edit popup.
     */
    public function operationCreate(Request $request, $uid): \Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            return response()->json([
                'status' => 'success',
                'title' => trans('messages.automation.operation.'.$request->options['operation_type']),
                'options' => $request->options,
                'message' => trans('messages.automation.operation.added'),
            ]);
        }

        return view('automation2.operationCreate', [
            'automation' => $automation,
            'types' => [
                'update_contact',
                'tag_contact',
                'copy_contact',
            ],
        ]);
    }

    /**
     * Operation edit popup.
     */
    public function operationShow(Request $request, $uid): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        return view('automation2.operationShow', [
            'automation' => $automation,
            'element' => $automation->getElement($request->id),
        ]);
    }

    /**
     * Operation edit popup.
     */
    public function operationEdit(Request $request, $uid): \Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $automation = Automation2::findByUid($uid);

        // saving
        if ($request->isMethod('post')) {
            return response()->json([
                'status' => 'success',
                'title' => trans('messages.automation.operation.'.$request->options['operation_type']),
                'options' => $request->options,
                'message' => trans('messages.automation.operation.updated'),
            ]);
        }

        return view('automation2.operationEdit', [
            'automation' => $automation,
            'element' => $automation->getElement($request->id),
        ]);
    }

    public function run(Request $request): void
    {
        $automation = Automation2::findByUid($request->automation);
        $automation->check();
        echo 'Done';
    }

    public function sendTestEmail(Request $request): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $email = Email::findByUid($request->email_uid);

        if ($request->isMethod('post')) {
            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
            ]);

            //
            if ($validator->fails()) {
                return response()->view('automation2.sendTestEmail', [
                    'email' => $email,
                    'errors' => $validator->errors(),
                ], 400);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'OK',
            ]);
        }

        return view('automation2.sendTestEmail', [
            'email' => $email,
        ]);
    }
}
