<?php

namespace App\Http\Controllers;

use App\Model\Plan;
use App\Model\Subscription;
use Illuminate\Http\Request;

class AccountSubscriptionController extends Controller
{
    /**
     * Customer subscription main page.
     **/
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
    {
        // init
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;

        // 1. HAVE NOT HAD SUBSCRIPTION YET OR SUBSCRIPTION IS ENDED
        if (! $subscription ||
            $subscription->isEnded()
        ) {
            return view('account.subscription.select_plan', [
                'plans' => Plan::getAvailablePlans(),
                'subscription' => $subscription,
            ]);
        }

        // @todo không để đây, chỉ test thôi, cần move qua cronjob
        // 1. 1 End luôn subscription nếu đã hết hạn
        //    2 Sinh ra RENEW invoice
        //    3 Xử lý thanh toán
        $subscription->check();

        // 2. IF PLAN NOT ACTIVE
        if (! $subscription->plan->isActive()) {
            return response()->view('errors.general', ['message' => __('messages.subscription.error.plan-not-active', ['name' => $subscription->plan->name])]);
        }

        // 3. SUBSCRIPTION IS NEW
        if ($subscription->isNew()) {
            $invoice = $subscription->getItsOnlyUnpaidInitInvoice();

            if ($invoice->isNew()) {
                if (! $invoice->getPendingTransaction()) {
                    return view('account.subscription.payment', [
                        'subscription' => $subscription,
                        'invoice' => $invoice,
                    ]);
                } else {
                    return view('account.subscription.pending', [
                        'subscription' => $subscription,
                        'invoice' => $invoice,
                    ]);
                }
            } else {
                throw new \Exception('There is no such case: new subscription must always have ONE unpaid invoice (which is either "new", and the getItsOnlyUnpaidInitInvoice() method above is supposed to throw an exception before reaching this point.');
            }
        }

        // 3. SUBSCRIPTION IS ACTIVE, SHOW DETAILS PAGE
        return view('account.subscription.index', [
            'subscription' => $subscription,
            'plan' => $subscription->plan,
        ]);
    }

    /**
     * Select plan.
     **/
    public function init(Request $request): \Illuminate\Http\RedirectResponse
    {
        // Get current customer
        $customer = $request->user()->customer;
        $plan = Plan::findByUid($request->plan_uid);

        // create new subscription
        $subscription = $customer->assignPlan($plan);

        // create init invoice
        if (! $subscription->invoices()->new()->count()) {
            $subscription->createInitInvoice();
        }

        // Check if subscriotion is new
        return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
    }

    /**
     * Checkout payment.
     **/
    public function checkout(Request $request): \Illuminate\Http\RedirectResponse
    {
        // Get current customer
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;
        $gateway = $customer->getPreferredPaymentGateway();
        $invoice = $subscription->getUnpaidInvoice();

        // redirect to service checkout
        return redirect()->away($gateway->getCheckoutUrl($invoice));
    }

    /**
     * Invoice payment.
     *
     * @return \Illuminate\Http\Response
     **/
    public function payment(Request $request)
    {
        // Get current customer
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;
        $invoice = $subscription->getUnpaidInvoice();

        if ($invoice->isNew()) {
            if ($invoice->getPendingTransaction()) {
                return view('account.subscription.pending', [
                    'subscription' => $subscription,
                    'invoice' => $invoice,
                ]);
            } else {
                return view('account.subscription.payment', [
                    'subscription' => $subscription,
                    'invoice' => $invoice,
                ]);
            }
        }

        // invoice was paid
        elseif ($invoice->isPaid()) {
            throw new \Exception('Paid invoice do not need payment screen!');
        }
    }

    /**
     * Change plan.
     **/
    public function changePlan(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;
        $gateway = $customer->getPreferredPaymentGateway();
        $plans = Plan::getAvailablePlans();

        // Authorization
        if (! $request->user()->customer->can('changePlan', $subscription)) {
            return $this->notAuthorized();
        }

        //
        if ($request->isMethod('post')) {
            $newPlan = Plan::findByUid($request->plan_uid);

            try {
                // set invoice as pending
                $changePlanInvoice = $subscription->createChangePlanInvoice($newPlan);
            } catch (\Exception $e) {
                $request->session()->flash('alert-error', $e->getMessage());

                return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
            }

            // return to subscription
            return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'payment']);
        }

        return view('account.subscription.change_plan', [
            'subscription' => $subscription,
            'gateway' => $gateway,
            'plans' => $plans,
        ]);
    }

    /**
     * Cancel subscription at the end of current period.
     */
    public function cancel(Request $request): \Illuminate\Http\RedirectResponse
    {
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;

        if ($request->user()->customer->can('cancel', $subscription)) {
            $subscription->cancel();
        }

        // Redirect to my subscription page
        $request->session()->flash('alert-success', trans('messages.subscription.cancelled'));

        return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
    }

    /**
     * Cancel subscription at the end of current period.
     */
    public function cancelInvoice(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        $invoice = \App\Model\Invoice::findByUid($uid);
        $subscription = $request->user()->customer->subscription;

        if (! $request->user()->customer->can('delete', $invoice)) {
            return $this->notAuthorized();
        }

        // if subscription is new -> cancel now subscription.
        // Make sure a new subscription must have a pending invoice
        if ($subscription->isNew()) {
            $subscription->abortNew();
        } else {
            $invoice->delete();
        }

        // Redirect to my subscription page
        $request->session()->flash('alert-success', trans('messages.invoice.cancelled'));

        return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
    }

    /**
     * Cancel subscription at the end of current period.
     */
    public function resume(Request $request): \Illuminate\Http\RedirectResponse
    {
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;

        if ($request->user()->customer->can('resume', $subscription)) {
            $subscription->resume();
        }

        // Redirect to my subscription page
        $request->session()->flash('alert-success', trans('messages.subscription.resumed'));

        return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
    }

    /**
     * Cancel now subscription at the end of current period.
     */
    public function cancelNow(Request $request): \Illuminate\Http\RedirectResponse
    {
        $customer = $request->user()->customer;
        $subscription = $customer->subscription;

        if ($request->user()->customer->can('cancelNow', $subscription)) {
            $subscription->cancelNow();
        }

        // Redirect to my subscription page
        $request->session()->flash('alert-success', trans('messages.subscription.cancelled_now'));

        return redirect()->action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']);
    }
}
