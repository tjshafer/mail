<?php

namespace App\Http\Controllers;

use Acelle\Library\StringHelper;
use App\Model\Email;
use App\Model\Subscriber;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): void
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): void
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): void
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id): void
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id): void
    {
        //
    }

    /**
     * Preview email for a given subscriber.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview($uid, $subscriber_uid)
    {
        $email = Email::findByUid($uid);
        $subscriber = Subscriber::findByUid($subscriber_uid);
        $logs = $email->trackingLogs()->where('subscriber_id', $subscriber->id)->get();

        if ($logs->count() == 0) {
            [$message, $msgId] = $email->prepare($subscriber);

            return response($message->toString())
                  ->header('Content-Type', 'text/plain');
        } else {
            $links = [];
            foreach ($logs as $log) {
                $path = route('openTrackingUrl', ['message_id' => StringHelper::base64UrlEncode($log->message_id)], false);
                $url = $email->buildTrackingUrl($path);
                $links[] = "{$log->email_id} <a href='{$url}'>{$url}</a>";
            }

            return response(implode('<br>', $links));
        }
    }
}
