<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Log in back user.
     */
    public function loginBack(Request $request): \Illuminate\Http\RedirectResponse
    {
        $id = \Session::pull('orig_user_id');
        $orig_user = \App\Model\User::findByUid($id);

        \Auth::login($orig_user);

        return redirect()->action([\App\Http\Controllers\Admin\UserController::class, 'index']);
    }
}
