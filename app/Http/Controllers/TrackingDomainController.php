<?php

namespace App\Http\Controllers;

use App\Model\TrackingDomain;
use Illuminate\Http\Request;

class TrackingDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new TrackingDomain())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);

        $trackingDomains = TrackingDomain::search($request);

        return view('tracking_domains.index', [
            'trackingDomains' => $trackingDomains,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new TrackingDomain())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);

        $trackingDomains = TrackingDomain::search($request)->paginate($request->per_page);

        return view('tracking_domains._list', [
            'trackingDomains' => $trackingDomains,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $domain = new TrackingDomain([
            'signing_enabled' => true,
        ]);
        $domain->status = TrackingDomain::STATUS_UNVERIFIED;
        $domain->uid = '0';
        $domain->fill($request->old());

        // authorize
        if (! $request->user()->customer->can('create', $domain)) {
            return $this->notAuthorized();
        }

        return view('tracking_domains.create', [
            'domain' => $domain,
            'readonly' => '0',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $domain = new TrackingDomain();

        // authorize
        if (! $request->user()->customer->can('create', $domain)) {
            return $this->notAuthorized();
        }

        // save posted data
        $this->validate($request, $domain->rules());

        // Save current user info
        $domain->fill($request->all());
        $domain->customer_id = $request->user()->customer->id;
        $domain->status = TrackingDomain::STATUS_UNVERIFIED;

        if ($domain->save()) {
            $request->session()->flash('alert-success', trans('messages.tracking_domain.created'));

            return redirect()->action([\App\Http\Controllers\TrackingDomainController::class, 'index']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, int $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $debug = is_null($request->input('tracking_domain_debug')) ? false : true;

        if ($debug) {
            session()->put('tracking_domain_debug', $debug);
        } else {
            session()->forget('tracking_domain_debug', $debug);
        }

        $domain = TrackingDomain::findByUid($id);
        $hostname = parse_url(url('/'), PHP_URL_HOST);

        return view('tracking_domains.show', [
            'domain' => $domain,
            'hostname' => $hostname,
        ]);
    }

    /**
     * Custom sort items.
     */
    public function sort(Request $request): void
    {
        echo trans('messages.tracking_domain._deleted_');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = TrackingDomain::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('delete', $item)) {
                $item->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.tracking_domains.deleted');
    }

    /**
     * Verify sending domain.
     *
     * @param  int  $id
     */
    public function verify(Request $request, $uid): \Illuminate\Http\RedirectResponse
    {
        $debug = session()->has('tracking_domain_debug');
        $domain = TrackingDomain::findByUid($uid);
        $domain->verify($debug);

        return redirect()->action([\App\Http\Controllers\TrackingDomainController::class, 'show'], ['tracking_domain' => $uid]);
    }

    /**
     * sending domain's records.
     *
     * @return \Illuminate\Http\Response
     */
    public function records(int $id)
    {
        $domain = TrackingDomain::findByUid($id);

        if ($domain->isAssociatedWithSendingServer()) {
            $options = $domain->getOptions()['verification'];
            $identity = $options['identity'];
            $dkims = $options['dkim'];
            $spf = array_key_exists('spf', $options) ? $options['spf'] : [];

            return view('tracking_domains._records_aws', [
                'domain' => $domain,
                'identity' => $identity,
                'dkims' => $dkims,
                'spf' => $spf,
            ]);
        } else {
            return view('tracking_domains._records', [
                'domain' => $domain,
            ]);
        }
    }
}
