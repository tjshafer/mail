<?php

namespace App\Http\Controllers;

use Acelle\Jobs\ImportBlacklistJob;
use App\Model\Blacklist;
use App\Model\JobMonitor;
use Exception;
use Illuminate\Http\Request;

class BlacklistController extends Controller
{
    /**
     * Search items.
     */
    public function search($request): \App\Model\collect
    {
        $request->merge(['customer_id' => $request->user()->customer->id]);
        $blacklists = \App\Model\Blacklist::search($request);

        return $blacklists;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = $request->user()->customer;

        if (! $customer->can('read', new \App\Model\Blacklist())) {
            return $this->notAuthorized();
        }

        $blacklists = $this->search($request);

        return view('blacklists.index', [
            'blacklists' => $blacklists,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new \App\Model\Blacklist())) {
            return $this->notAuthorized();
        }

        $blacklists = $this->search($request)->paginate($request->per_page);

        return view('blacklists._list', [
            'blacklists' => $blacklists,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $blacklist = new \App\Model\Blacklist([
            'signing_enabled' => true,
        ]);
        $blacklist->fill($request->old());

        // authorize
        if (! $request->user()->customer->can('create', $blacklist)) {
            return $this->notAuthorized();
        }

        return view('blacklists.create', [
            'blacklist' => $blacklist,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $blacklist = new \App\Model\Blacklist();

        // authorize
        if (! $request->user()->customer->can('create', $blacklist)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $this->validate($request, \App\Model\Blacklist::rules());

            // Save current user info
            $blacklist->fill($request->all());
            $blacklist->customer_id = $request->user()->customer->id;
            $blacklist->status = 'active';

            if ($blacklist->save()) {
                // Log
                $blacklist->log('created', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.blacklist.created'));

                return redirect()->action([\App\Http\Controllers\BlacklistController::class, 'index']);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $blacklist = \App\Model\Blacklist::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $blacklist)) {
            return $this->notAuthorized();
        }

        $blacklist->fill($request->old());

        return view('blacklists.edit', [
            'blacklist' => $blacklist,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $blacklist = \App\Model\Blacklist::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $blacklist)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $this->validate($request, \App\Model\Blacklist::rules());

            // Save current user info
            $blacklist->fill($request->all());

            if ($blacklist->save()) {
                // Log
                $blacklist->log('updated', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.blacklist.updated'));

                return redirect()->action([\App\Http\Controllers\BlacklistController::class, 'index']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        if ($request->select_tool == 'all_items') {
            $blacklists = $this->search($request);
        } else {
            $blacklists = \App\Model\Blacklist::whereIn('id', explode(',', $request->uids));
        }

        foreach ($blacklists->get() as $blacklist) {
            // authorize
            if ($request->user()->customer->can('delete', $blacklist)) {
                // Log
                $blacklist->delist($request->user()->customer);
                $blacklist->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.blacklists.deleted');
    }

    /**
     * Start import process.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $customer = $request->user()->customer;
        $job = $customer->importBlacklistJobs()->first();

        if ($job) {
            return view('blacklists.import', [
                'currentJobUid' => $job->uid,
                'cancelUrl' => action([\App\Http\Controllers\BlacklistController::class, 'cancelImport'], ['job_uid' => $job->uid]),
                'progressCheckUrl' => action([\App\Http\Controllers\BlacklistController::class, 'importProgress'], ['job_uid' => $job->uid]),
            ]);
        } else {
            return view('blacklists.import', [
                //
            ]);
        }
    }

    public function startImport(Request $request)
    {
        $customer = $request->user()->customer;

        if ($request->hasFile('file')) {
            $filepath = Blacklist::upload($request->file('file'));

            // Start system job
            $job = $customer->dispatchWithMonitor(new ImportBlacklistJob($filepath, $customer));

            return redirect()->action([\App\Http\Controllers\BlacklistController::class, 'import']);
        } else {
            throw new Exception('no upload file');
        }
    }

    /**
     * Check import proccessing.
     */
    public function importProgress(Request $request): \Illuminate\Http\JsonResponse
    {
        $job = JobMonitor::findByUid($request->job_uid);

        throw_if(is_null($job), new Exception(sprintf('Blacklist import job #%s does not exist', $request->job_uid)));

        $progress = $job->getJsonData();
        $progress['status'] = $job->status;
        $progress['error'] = $job->error;

        // Get progress updated by the import process and status of the final job monitor
        return response()->json($progress);
    }

    /**
     * Cancel importing job.
     */
    public function cancelImport(Request $request): \Illuminate\Http\JsonResponse
    {
        $job = JobMonitor::findByUid($request->job_uid);
        throw_if(is_null($job), new Exception(sprintf('Verification job #%s does not exist', $request->job_uid)));

        $job->cancel();

        return response()->json();
    }
}
