<?php

namespace App\Http\Controllers;

use App\Model\AutoTrigger as AutoTriggerModel;
use App\Model\DeliveryAttempt;
use App\Model\Email;
use Illuminate\Http\Request;

class AutoTrigger extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function show(Request $request): void
    {
        $trigger = AutoTriggerModel::find($request->id);
        $info = [];
        $info[] = sprintf('This is an auto trigger for automation {{ %s }}', $trigger->automation2->name);
        $info[] = sprintf('Subscriber {{ %s }}', $trigger->subscriber->email);

        $actions = [];
        $trigger->getActions(function ($a) use (&$actions, $trigger) {
            $description = '+ ['.(($a->getLastExecuted()) ? 'Executed' : 'Waiting').'] '.$a->getId().': '.$a->getTitle();
            if ($a->isCondition()) {
                $description .= ' ('.$a->getEvaluationResult().')';
            }

            if ($a->getType() == 'ElementAction' && $a->getLastExecuted()) {
                // Attempt
                $email = Email::findByUid($a->getOption('email_uid'));

                $attempt = DeliveryAttempt::where('email_id', $email->id)->where('auto_trigger_id', $trigger->id)->first();

                $id = $attempt->id;
                $description .= ' (Attempt: '.$id.', ';
            }

            $actions[] = $description;
        });

        $info[] = implode('<br>', $actions);

        echo implode('<br>', $info);
    }

    public function check(Request $request): void
    {
        $trigger = AutoTriggerModel::find($request->id);

        // Execute AutoTrigger#check
        // Notice that calling check() directly against AutoTrigger will not update automation's lastError
        $trigger->check();
        echo 'Done';
    }
}
