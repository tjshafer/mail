<?php

namespace App\Http\Controllers;

use App\Library\Facades\Hook;
use App\Model\SendingDomain;
use App\Model\Setting;
use Illuminate\Http\Request;

class SendingDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new SendingDomain())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);
        $plan = $request->user()->customer->activeSubscription()->plan;

        if ($plan->useSystemSendingServer()) {
            $server = $plan->primarySendingServer();
        } else {
            $server = null;
        }

        $items = SendingDomain::search($request, $server);

        return view('sending_domains.index', [
            'items' => $items,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new SendingDomain())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);
        $plan = $request->user()->customer->activeSubscription()->plan;

        if ($plan->useSystemSendingServer()) {
            $server = $plan->primarySendingServer();
        } else {
            $server = null;
        }

        $items = SendingDomain::search($request, $server)->paginate($request->per_page);

        return view('sending_domains._list', [
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = new SendingDomain([
            'signing_enabled' => true,
        ]);
        $server->status = 'active';
        $server->uid = '0';
        $server->fill($request->old());

        // authorize
        if (! $request->user()->customer->can('create', $server)) {
            return $this->notAuthorized();
        }

        return view('sending_domains.create', [
            'server' => $server,
            'readonly' => '0',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $domain = new SendingDomain();

        // authorize
        if (! $request->user()->customer->can('create', $domain)) {
            return $this->notAuthorized();
        }

        $subscription = $request->user()->customer->subscription;
        $plan = $subscription->plan;

        // save posted data
        $this->validate($request, SendingDomain::rules());

        // Save current user info
        $domain->fill($request->all());
        $domain->customer_id = $request->user()->customer->id;
        $domain->status = 'active';

        if ($domain->save()) {
            if ($plan->useSystemSendingServer()) {
                $server = $plan->primarySendingServer();
                if ($server->allowVerifyingOwnDomainsRemotely()) {
                    $domain->verifyWith($server);
                }
            }

            $domain->syncToMta();

            // Log
            $domain->log('created', $request->user()->customer);

            $request->session()->flash('alert-success', trans('messages.sending_domain.created'));

            return redirect()->action([\App\Http\Controllers\SendingDomainController::class, 'show'], $domain->uid);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $domain = SendingDomain::findByUid($id);

        return view('sending_domains.show', [
            'server' => $domain,
            'readonly' => 'readonly',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        return view('sending_domains.edit', [
            'server' => $server,
            'readonly' => 'readonly',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $server = SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $this->validate($request, SendingDomain::rules());

            // Save current user info
            $server->fill($request->all());

            if ($server->save()) {
                // Log
                $server->log('updated', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.sending_domain.updated'));

                return redirect()->action([\App\Http\Controllers\SendingDomainController::class, 'show'], $server->uid);
            }
        }
    }

    /**
     * Custom sort items.
     */
    public function sort(Request $request): void
    {
        echo trans('messages.sending_domain._deleted_');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = SendingDomain::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('delete', $item)) {
                // Log
                $item->log('deleted', $request->user()->customer);

                $item->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.sending_domains.deleted');
    }

    /**
     * Verify sending domain.
     */
    public function verify(int $id): void
    {
        $domain = SendingDomain::findByUid($id);
        $domain->verify();
    }

    /**
     * sending domain's records.
     *
     * @return \Illuminate\Http\Response
     */
    public function records(int $id)
    {
        $domain = SendingDomain::findByUid($id);

        if ($domain->isAssociatedWithSendingServer()) {
            $options = $domain->getOptions()['verification'];
            $identity = $options['identity'];
            $dkims = $options['dkim'];
            // Server spf will be replaced by Setting
            $spf = Setting::get('spf') ?: (array_key_exists('spf', $options) ? $options['spf'] : []);

            Hook::execute(
                'filter_aws_ses_dns_records',
                [&$identity, &$dkims, &$spf]
            );

            return view('sending_domains._records_aws', [
                'domain' => $domain,
                'identity' => $identity,
                'dkims' => $dkims,
                'spf' => $spf,
            ]);
        } else {
            return view('sending_domains._records', [
                'server' => $domain,
            ]);
        }
    }

    /**
     * update VerificationTxtName.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateVerificationTxtName(int $id, Request $request)
    {
        $server = SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        if (! $server->setVerificationTxtName($request->value)) {
            return response(trans('messages.sending_domain.verification_hostname.not_valid'), 404)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     * update VerificationTxtName.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateDkimSelector(int $id, Request $request)
    {
        $server = SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        if (! $server->setDkimSelector($request->value)) {
            return response(trans('messages.sending_domain.dkim_selector.not_valid'), 404)
                ->header('Content-Type', 'text/plain');
        }
    }
}
