<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SendingServerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new \App\Model\SendingServer())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);
        $items = \App\Model\SendingServer::search($request);

        return view('sending_servers.index', [
            'items' => $items,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->customer->can('read', new \App\Model\SendingServer())) {
            return $this->notAuthorized();
        }

        $request->merge(['customer_id' => $request->user()->customer->id]);
        $items = \App\Model\SendingServer::search($request)->paginate($request->per_page);

        return view('sending_servers._list', [
            'items' => $items,
        ]);
    }

    /**
     * Select sending server type.
     */
    public function select(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('sending_servers.select');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = new \App\Model\SendingServer();
        $server->status = 'active';
        $server->uid = '0';
        $server->quota_value = '1000';
        $server->quota_base = '1';
        $server->quota_unit = 'hour';
        $server->type = $request->type;
        $server->fill($request->old());

        return view('sending_servers.create', [
            'server' => $server,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $server = new \App\Model\SendingServer();
        $server->type = $request->type;

        // save posted data
        if ($request->isMethod('post')) {
            $this->validate($request, \App\Model\SendingServer::frontendRules($request->type));

            // Save current user info
            $server->fill($request->all());
            $server->customer_id = $request->user()->customer->id;
            $server->status = 'active';

            // bounce / feedback hanlder nullable
            if (empty($request->bounce_handler_id)) {
                $server->bounce_handler_id = null;
            }
            if (empty($request->feedback_loop_handler_id)) {
                $server->feedback_loop_handler_id = null;
            }

            if ($server->save()) {
                // Log
                $server->log('created', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.sending_server.created'));

                return redirect()->action([\App\Http\Controllers\SendingServerController::class, 'index']);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\SendingServer::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        // bounce / feedback hanlder nullable
        if ($request->old() && empty($request->old()['bounce_handler_id'])) {
            $server->bounce_handler_id = null;
        }
        if ($request->old() && empty($request->old()['feedback_loop_handler_id'])) {
            $server->feedback_loop_handler_id = null;
        }

        $server->fill($request->old());

        return view('sending_servers.edit', [
            'server' => $server,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $server = \App\Model\SendingServer::findByUid($id);

        // authorize
        if (! $request->user()->customer->can('update', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            // Save current user info
            $server->fill($request->all());

            // bounce / feedback hanlder nullable
            if (empty($request->bounce_handler_id)) {
                $server->bounce_handler_id = null;
            }

            if (empty($request->feedback_loop_handler_id)) {
                $server->feedback_loop_handler_id = null;
            }

            $this->validate($request, $server->getFrontendRules());

            if ($server->save()) {
                // Log
                $server->log('updated', $request->user()->customer);

                $request->session()->flash('alert-success', trans('messages.sending_server.updated'));

                return redirect()->action([\App\Http\Controllers\SendingServerController::class, 'index']);
            }
        }
    }

    /**
     * Custom sort items.
     *
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        $sort = json_decode($request->sort);
        foreach ($sort as $row) {
            $item = \App\Model\SendingServer::findByUid($row[0]);

            // authorize
            if (! $request->user()->customer->can('update', $item)) {
                return $this->notAuthorized();
            }

            $item->save();
        }

        echo trans('messages.sending_server._deleted_');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = \App\Model\SendingServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('delete', $item)) {
                // Log
                $item->log('deleted', $request->user()->customer);
                $item->plansSendingServers()->delete(); // workaround in case of MyISAM, no cascade
                $item->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.sending_servers.deleted');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function disable(Request $request): void
    {
        $items = \App\Model\SendingServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('disable', $item)) {
                $item->disable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.sending_servers.disabled');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function enable(Request $request): void
    {
        $items = \App\Model\SendingServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->customer->can('enable', $item)) {
                $item->enable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.sending_servers.enabled');
    }

    /**
     * Test Sending server.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request, $uid)
    {
        // Get current user
        $current_user = $request->user();

        // Fill new server info
        if ($uid) {
            $server = \App\Model\SendingServer::findByUid($uid);
        } else {
            $server = new \App\Model\SendingServer();
            $server->uid = 0;
        }

        $server->fill($request->all());
        $server->type = $request->type;

        // authorize
        if (! $current_user->customer->can('test', $server)) {
            return $this->notAuthorized();
        }

        if ($request->isMethod('post')) {
            // @todo testing method and return result here. Ex: echo json_encode($server->test())
            try {
                $server = $server->mapType();
                $server->sendTestEmail([
                    'from_email' => $request->from_email,
                    'to_email' => $request->to_email,
                    'subject' => $request->subject,
                    'plain' => $request->content,
                ]);
            } catch (\Exception $ex) {
                echo json_encode([
                    'status' => 'error', // or success
                    'message' => $ex->getMessage(),
                ]);

                return;
            }

            echo json_encode([
                'status' => 'success', // or success
                'message' => trans('messages.sending_server.test_success'),
            ]);

            return;
        }

        return view('sending_servers.test', [
            'server' => $server,
        ]);
    }
}
