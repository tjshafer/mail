<?php

namespace App\Http\Controllers;

use App\Model\Sender;
use Illuminate\Http\Request;

class SenderController extends Controller
{
    /**
     * Search items.
     */
    public function search($request): \App\Model\collect
    {
        $request->merge(['customer_id' => $request->user()->customer->id]);
        $senders = Sender::search($request);

        return $senders;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (\Gate::denies('listing', new Sender())) {
            return $this->notAuthorized();
        }

        $subscription = $request->user()->customer->subscription;
        $plan = $subscription->plan;

        if ($subscription->plan->useOwnSendingServer()) {
            $email = true;
            $domain = true;
        } else {
            $server = $plan->primarySendingServer();
            $email = $server->allowVerifyingOwnEmails() || $server->allowVerifyingOwnEmailsRemotely();
            $domain = $server->allowVerifyingOwnDomains() || $server->allowVerifyingOwnDomainsRemotely();
        }

        if (! $email && ! $domain) {
            return view('senders.available', [
                'identities' => $subscription->plan->getVerifiedIdentities(),
            ]);
        }

        if ($domain && ! $email) {
            return redirect()->to(url('sending_domains'));
        } else {
            return view('senders.index', [
                'senders' => $this->search($request),
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (\Gate::denies('listing', new Sender())) {
            return $this->notAuthorized();
        }

        return view('senders._list', [
            'senders' => $this->search($request)->paginate($request->per_page),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $sender = new Sender();

        $sender->fill($request->old());

        // authorize
        if (\Gate::denies('create', $sender)) {
            return $this->notAuthorized();
        }

        return view('senders.create', [
            'sender' => $sender,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        $sender = new Sender();

        // authorize
        if (\Gate::denies('create', $sender)) {
            return $this->notAuthorized();
        }

        $plan = $request->user()->customer->activeSubscription()->plan;

        $sender->fill($request->all());
        $sender->customer_id = $request->user()->customer->id;
        $sender->status = Sender::STATUS_PENDING;

        $this->validate($request, $sender->rules());

        try {
            $sender->save();

            if ($plan->useSystemSendingServer()) {
                $server = $plan->primarySendingServer();
            } else {
                $server = null;
            }

            $sender->verifyWith($server);

            return redirect()->action([\App\Http\Controllers\SenderController::class, 'show'], $sender->uid);
        } catch (\Exception $ex) {
            $sender->delete();
            $request->session()->flash('alert-error', $ex->getMessage());

            return redirect()->action([\App\Http\Controllers\SenderController::class, 'index']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function show(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $sender = Sender::findByUid($id);
        $sender->updateVerificationStatus();

        // authorize
        if (\Gate::denies('read', $sender)) {
            return $this->notAuthorized();
        }

        return view('senders.show', [
            'sender' => $sender,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $sender = Sender::findByUid($id);

        // authorize
        if (\Gate::denies('update', $sender)) {
            return $this->notAuthorized();
        }

        $sender->fill($request->old());

        return view('senders.edit', [
            'sender' => $sender,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $sender = Sender::findByUid($id);

        // authorize
        if (\Gate::denies('update', $sender)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $sender->name = $request->name;

            $this->validate($request, $sender->editRules());

            if ($sender->save()) {
                $request->session()->flash('alert-success', trans('messages.sender.updated'));

                return redirect()->action([\App\Http\Controllers\SenderController::class, 'show'], $sender->uid);
            }
        }
    }

    /**
     * Verify sender.
     *
     * @param  int  $id
     */
    public function verify(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        try {
            $sender = Sender::verifyToken($request->token);

            return view('senders.verified', [
                'sender' => $sender,
            ]);
        } catch (\Exception $ex) {
            return view('senders.failed', [
                'message' => $ex->getMessage(),
            ]);
        }
    }

    /**
     * Verify sender.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verifyResult(Request $request)
    {
        $sender = Sender::findByUid($request->uid);
        $sender->updateVerificationStatus();

        if ($sender->isVerified()) {
            return view('senders.verified', [
                'sender' => $sender,
            ]);
        } else {
            return view('senders.failed', [
                'message' => 'Failed to verify identity',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        if ($request->select_tool == 'all_items') {
            $senders = $this->search($request);
        } else {
            $senders = Sender::whereIn('uid', explode(',', $request->uids));
        }

        foreach ($senders->get() as $sender) {
            // authorize
            if ($request->user()->customer->can('delete', $sender)) {
                $sender->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.senders.deleted');
    }

    /**
     * Start import process.
     */
    public function import(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = $request->user()->customer;

        if ($request->isMethod('post')) {
            // authorize
            if (\Gate::denies('import', new Sender())) {
                return $this->notAuthorized();
            }

            if ($request->hasFile('file')) {
                // Start system job
                $job = new  \App\Jobs\ImportSenderJob($request->file('file')->path(), $request->user()->customer);
                $this->dispatch($job);
            } else {
                // @note: use try/catch instead
                echo 'max_file_upload';
            }
        }

        return view('senders.import', [

        ]);
    }

    /**
     * Dropbox list.
     */
    public function dropbox(Request $request): \Illuminate\Http\JsonResponse
    {
        $droplist = $request->user()->customer->verifiedIdentitiesDroplist(strtolower(trim($request->keyword)));

        return response()->json($droplist);
    }
}
