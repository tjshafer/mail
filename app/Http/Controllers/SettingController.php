<?php

namespace App\Http\Controllers;

use App\Model\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Render uploaded file.
     **/
    public function file(Request $request, $filename): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        //return \Image::make(Setting::getUploadFilePath($filename))->response();
        $path = Setting::getUploadFilePath($filename);
        $type = mime_content_type($path);
        if ($type == 'image/svg') {
            $type = 'image/svg+xml';
        }

        return response()->file($path, ['Content-Type' => $type]);
    }
}
