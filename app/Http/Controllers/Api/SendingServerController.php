<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

/**
 * /api/v1/sending_servers - API controller for managing sending servers.
 */
class SendingServerController extends Controller
{
    /**
     * Display all sending servers.
     *
     * GET /api/v1/sending_servers
     */
    public function index(): \Illuminate\Http\Response
    {
        $user = \Auth::guard('api')->user();

        // authorize
        if (! $user->admin->can('readAll', new \App\Model\SendingServer())) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $servers = \App\Model\SendingServer::getAll()
            ->select('uid', 'name', 'created_at', 'updated_at')
            ->get();

        return response()->json($servers, 200);
    }
}
