<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * /api/v1/plans - API controller for managing plans.
 */
class PlanController extends Controller
{
    /**
     * Display all plans.
     *
     * GET /api/v1/plans
     */
    public function index(): \Illuminate\Http\Response
    {
        $user = \Auth::guard('api')->user();

        // authorize
        if (! $user->can('read', new \App\Model\Plan())) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $rows = \App\Model\Plan::limit(100)->get();

        $plans = $rows->map(fn($plan) => [
            'uid' => $plan->uid,
            'name' => $plan->name,
            'price' => $plan->price,
            'currency_code' => $plan->currency->code,
            'frequency_amount' => $plan->frequency_amount,
            'frequency_unit' => $plan->frequency_unit,
            'options' => $plan->getOptions(),
            'status' => $plan->status,
            'quota' => $plan->quota,
            'created_at' => $plan->created_at,
            'updated_at' => $plan->updated_at,
        ]);

        return response()->json($plans, 200);
    }

    /**
     * Create a new plan.
     *
     * POST /api/v1/plans
     *
     * @param  \Illuminate\Http\Request  $request All plan information
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::guard('api')->user();

        $plan = new \App\Model\Plan();

        // authorize
        if (! $user->can('create', $plan)) {
            return response()->json(['status' => 0, 'message' => 'Unauthorized'], 401);
        }

        // save posted data
        if ($request->isMethod('post')) {
            $plan->fill($request->all());
            // $plan->options = json_encode($request->options);
            $plan->fillOptions($request->options);

            $validator = \Validator::make($request->all(), $plan->apiRules());
            if ($validator->fails()) {
                return response()->json($validator->messages(), 403);
            }

            $rules = [];
            if (isset($request->sending_servers)) {
                foreach ($request->sending_servers as $key => $param) {
                    if ($param['check']) {
                        $rules['sending_servers.'.$key.'.fitness'] = 'required';
                    }
                }
            }

            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 403);
            }

            $plan->admin_id = $user->admin->id;
            $plan->save();

            // check status
            $plan->checkStatus();

            // For sending servers
            if (isset($request->sending_servers)) {
                $plan->updateSendingServers($request->sending_servers);
            }

            // For email verification servers
            if (isset($request->email_verification_servers)) {
                $plan->updateEmailVerificationServers($request->email_verification_servers);
            }

            return response()->json([
                'status' => 1,
                'message' => trans('messages.plan.created'),
                'plan_uid' => $plan->uid,
            ], 200);
        }
    }
}
