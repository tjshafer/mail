<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

/**
 * /api/v1/plans - API controller for managing plans.
 */
class NotificationController extends Controller
{
    /**
     * Display all notifications.
     *
     * GET /api/v1/plans
     */
    public function index(): \Illuminate\Http\Response
    {
        $user = \Auth::guard('api')->user();

        return response()->json(['message' => 'Comming...'], 200);
    }
}
