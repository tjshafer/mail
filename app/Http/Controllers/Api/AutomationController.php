<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Acelle\Jobs\RunAutomation;
use App\Model\Automation2;

/**
 * /api/v1/campaigns - API controller for managing campaigns.
 */
class AutomationController extends Controller
{
    /**
     * Call api for automation api call type.
     *
     * GET /api/v1/campaigns
     */
    public function execute($uid): \Illuminate\Http\Response
    {
        try {
            $automation = Automation2::findByUid($uid);
            $automation->logger()->info(sprintf('Queuing automation "%s" in response to API call', $automation->name));
            dispatch(new RunAutomation($automation));

            return response()->json(['success' => true], 200);
        } catch (\Exception $ex) {
            return response()->json(['success' => false, 'error' => $ex->getMessage()], 500);
        }
    }
}
