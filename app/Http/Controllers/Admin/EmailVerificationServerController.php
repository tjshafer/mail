<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailVerificationServerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\EmailVerificationServer())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\EmailVerificationServer())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        // exlude customer seding servers
        $request->merge(['no_customer' => true]);

        $servers = \App\Model\EmailVerificationServer::search($request);

        return view('admin.email_verification_servers.index', [
            'servers' => $servers,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\EmailVerificationServer())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\EmailVerificationServer())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        // exlude customer seding servers
        $request->merge(['no_customer' => true]);

        $servers = \App\Model\EmailVerificationServer::search($request)->paginate($request->per_page);

        return view('admin.email_verification_servers._list', [
            'servers' => $servers,
        ]);
    }

    /**
     * Select sending server type.
     */
    public function select(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('admin.email_verification_servers.select');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = new \App\Model\EmailVerificationServer();
        $server->status = \App\Model\EmailVerificationServer::STATUS_ACTIVE;
        $server->uid = '0';
        $server->fill($request->old());

        // authorize
        if (! $request->user()->admin->can('create', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        $options = [];
        if (! empty($request->old()['options'])) {
            $options = $request->old()['options'];
        }

        return view('admin.email_verification_servers.create', [
            'server' => $server,
            'options' => $options,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $server = new \App\Model\EmailVerificationServer();

        // authorize
        if (! $request->user()->admin->can('create', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $server->fill($request->all());

            $this->validate($request, $server->rules());

            // Save current user info
            $server->admin_id = $request->user()->admin->id;
            $server->status = \App\Model\EmailVerificationServer::STATUS_ACTIVE;
            $server->options = json_encode($request->options);

            if ($server->save()) {
                $request->session()->flash('alert-success', trans('messages.email_verification_server.created'));

                return redirect()->action([\App\Http\Controllers\Admin\EmailVerificationServerController::class, 'index']);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\EmailVerificationServer::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        $options = $server->getOptions();
        if (! empty($request->old()['options'])) {
            $options = $request->old()['options'];
        }

        return view('admin.email_verification_servers.edit', [
            'server' => $server,
            'options' => $options,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $server = \App\Model\EmailVerificationServer::findByUid($id);

        // authorize
        if (! $request->user()->admin->can('update', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $server->fill($request->all());

            $this->validate($request, $server->rules());

            // Save current user info
            $server->options = json_encode($request->options);

            if ($server->save()) {
                $request->session()->flash('alert-success', trans('messages.email_verification_server.updated'));

                return redirect()->action([\App\Http\Controllers\Admin\EmailVerificationServerController::class, 'index']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = \App\Model\EmailVerificationServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('delete', $item)) {
                $item->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.email_verification_servers.deleted');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function disable(Request $request): void
    {
        $items = \App\Model\EmailVerificationServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('disable', $item)) {
                $item->disable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.email_verification_servers.disabled');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function enable(Request $request): void
    {
        $items = \App\Model\EmailVerificationServer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('enable', $item)) {
                $item->enable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.email_verification_servers.enabled');
    }

    /**
     * Email verification server display options form.
     */
    public function options(Request $request, $uid = null): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($uid) {
            $server = \App\Model\EmailVerificationServer::findByUid($uid);
        } else {
            $server = new \App\Model\EmailVerificationServer($request->all());
            $options = $server->getOptions();
        }

        return view('admin.email_verification_servers._options', [
            'server' => $server,
            'options' => $options,
        ]);
    }
}
