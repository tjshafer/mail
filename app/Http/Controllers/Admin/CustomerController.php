<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Plan;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (\Gate::denies('read', new \App\Model\Customer())) {
            return $this->notAuthorized();
        }

        // If admin can view all customer
        if (! $request->user()->admin->can('readAll', new \App\Model\Customer())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        $customers = \App\Model\Customer::search($request);

        return view('admin.customers.index', [
            'customers' => $customers,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (\Gate::denies('read', new \App\Model\Customer())) {
            return $this->notAuthorized();
        }

        // If admin can view all customer
        if (! $request->user()->admin->can('readAll', new \App\Model\Customer())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        $customers = \App\Model\Customer::search($request)->paginate($request->per_page);

        return view('admin.customers._list', [
            'customers' => $customers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = new \App\Model\Customer();
        $customer->status = 'active';
        $customer->uid = '0';

        if (! empty($request->old())) {
            $customer->fill($request->old());
        }

        // User info
        $customer->user = new \App\Model\User();
        $customer->user->fill($request->old());

        // authorize
        if (\Gate::denies('create', $customer)) {
            return $this->notAuthorized();
        }

        return view('admin.customers.create', [
            'customer' => $customer,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $customer = new \App\Model\Customer();
        $contact = new \App\Model\Contact();

        // authorize
        if (\Gate::denies('create', $customer)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $user = new \App\Model\User();
            $user->fill($request->all());
            $user->activated = true;

            $this->validate($request, $user->rules());

            // Update password
            if (! empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            // Save current user info
            $customer->admin_id = $request->user()->admin->id;
            $customer->fill($request->all());
            $customer->status = 'active';

            if ($customer->save()) {
                $user->customer_id = $customer->id;
                $user->save();
                // Upload and save image
                if ($request->hasFile('image')) {
                    if ($request->file('image')->isValid()) {
                        // Remove old images
                        $user->uploadProfileImage($request->file('image'));
                    }
                }

                // Remove image
                if ($request->_remove_image == 'true') {
                    $user->removeProfileImage();
                }

                $request->session()->flash('alert-success', trans('messages.customer.created'));

                return redirect()->action([\App\Http\Controllers\Admin\CustomerController::class, 'index']);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = \App\Model\Customer::findByUid($id);
        event(new  \App\Events\UserUpdated($customer));

        // authorize
        if (\Gate::denies('update', $customer)) {
            return $this->notAuthorized();
        }

        if (! empty($request->old())) {
            $customer->fill($request->old());
            // User info
            $customer->user->fill($request->old());
        }

        return view('admin.customers.edit', [
            'customer' => $customer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $customer = \App\Model\Customer::findByUid($id);

        // authorize
        if (\Gate::denies('update', $customer)) {
            return $this->notAuthorized();
        }

        // Prenvent save from demo mod
        if ($this->isDemoMode()) {
            return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
        }

        // save posted data
        if ($request->isMethod('patch')) {
            // Prenvent save from demo mod
            if ($this->isDemoMode()) {
                return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
            }

            $user = $customer->user;
            $user->fill($request->all());

            $this->validate($request, $user->rules());

            // Update password
            if (! empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            // Save current user info
            $customer->fill($request->all());
            $customer->save();

            // Upload and save image
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    // Remove old images
                    $user->uploadProfileImage($request->file('image'));
                }
            }

            // Remove image
            if ($request->_remove_image == 'true') {
                $user->removeProfileImage();
            }

            if ($customer->save()) {
                $request->session()->flash('alert-success', trans('messages.customer.updated'));

                return redirect()->action([\App\Http\Controllers\Admin\CustomerController::class, 'index']);
            }
        }
    }

    /**
     * Enable item.
     */
    public function enable(Request $request): void
    {
        $items = \App\Model\Customer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if (\Gate::allows('update', $item)) {
                $item->enable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.customers.disabled');
    }

    /**
     * Disable item.
     */
    public function disable(Request $request): void
    {
        $items = \App\Model\Customer::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if (\Gate::allows('update', $item)) {
                $item->disable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.customers.disabled');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(Request $request): void
    {
        $customers = \App\Model\Customer::whereIn('uid', explode(',', $request->uids));

        foreach ($customers->get() as $customer) {
            // authorize
            if (\Gate::denies('delete', $customer)) {
                return;
            }
        }

        foreach ($customers->get() as $customer) {
            // Delete Customer account but KEEP user account if it is associated with an Admin
            $customer->deleteAccount();
        }

        // Redirect to my lists page
        echo trans('messages.customers.deleted');
    }

    /**
     * Switch user.
     */
    public function loginAs(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        $customer = \App\Model\Customer::findByUid($request->uid);

        // authorize
        if (\Gate::denies('loginAs', $customer)) {
            return $this->notAuthorized();
        }

        $orig_id = $request->user()->uid;
        \Auth::login($customer->user);
        \Session::put('orig_customer_id', $orig_id);

        return redirect()->action([\App\Http\Controllers\HomeController::class, 'index']);
    }

    /**
     * Log in back user.
     */
    public function loginBack(Request $request): \Illuminate\Http\RedirectResponse
    {
        $id = \Session::pull('orig_customer_id');
        $orig_user = \App\Model\Customer::findByUid($id);

        \Auth::login($orig_user);

        return redirect()->action([\App\Http\Controllers\Admin\UserController::class, 'index']);
    }

    /**
     * Select2 customer.
     */
    public function select2(Request $request): void
    {
        echo \App\Model\Customer::select2($request);
    }

    /**
     * User's subscriptions.
     *
     * @param  int  $id
     */
    public function subscriptions(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = \App\Model\Customer::findByUid($uid);

        // authorize
        if (\Gate::denies('read', $customer)) {
            return $this->notAuthorized();
        }

        return view('admin.customers.subscriptions', [
            'customer' => $customer,
        ]);
    }

    /**
     * Customers growth chart content.
     */
    public function growthChart(Request $request): \Illuminate\Http\Response|string|bool
    {
        // authorize
        if (\Gate::denies('read', new \App\Model\Customer())) {
            return $this->notAuthorized();
        }

        $result = [
            'columns' => [],
            'data' => [],
            'bar_names' => [trans('messages.customers_growth')],
        ];

        // columns
        for ($i = 4; $i >= 0; $i--) {
            $result['columns'][] = \Illuminate\Support\Carbon::now()->subMonthsNoOverflow($i)->format('m/Y');
        }

        // datas
        foreach ($result['bar_names'] as $bar) {
            $data = [];
            for ($i = 4; $i >= 0; $i--) {
                $data[] = \App\Model\Customer::customersCountByTime(
                    \Illuminate\Support\Carbon::now()->subMonthsNoOverflow($i)->startOfMonth(),
                    \Illuminate\Support\Carbon::now()->subMonthsNoOverflow($i)->endOfMonth(),
                    $request->user()->admin
                );
            }

            $result['data'][] = [
                'name' => $bar,
                'type' => 'bar',
                'data' => $data,
                'itemStyle' => [
                    'normal' => [
                        'label' => [
                            'show' => true,
                            'textStyle' => [
                                'fontWeight' => 500,
                            ],
                        ],
                    ],
                ],
            ];
        }

        return json_encode($result);
    }

    /**
     * Update customer contact information.
     **/
    public function contact(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Get current user
        $customer = \App\Model\Customer::findByUid($uid);

        // authorize
        if (\Gate::denies('update', $customer)) {
            return $this->notAuthorized();
        }

        if (is_object($customer->contact)) {
            $contact = $customer->contact;
        } else {
            $contact = new \App\Model\Contact([
                'first_name' => $request->user()->first_name,
                'last_name' => $request->user()->last_name,
                'email' => $request->user()->email,
            ]);
        }

        // Create new company if null
        if (! is_object($contact)) {
            $contact = new \App\Model\Contact();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $this->validate($request, \App\Model\Contact::$rules);

            $contact->fill($request->all());

            // Save current user info
            if ($contact->save()) {
                if (is_object($contact)) {
                    $customer->contact_id = $contact->id;
                    $customer->save();
                }
                $request->session()->flash('alert-success', trans('messages.customer_contact.updated'));
            }
        }

        return view('admin.customers.contact', [
            'customer' => $customer,
            'contact' => $contact->fill($request->old()),
        ]);
    }

    /**
     * Customer's sub-account list.
     **/
    public function subAccount(Request $request, $uid): \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Get current user
        $customer = \App\Model\Customer::findByUid($uid);

        // authorize
        if (\Gate::denies('viewSubAccount', $customer)) {
            return redirect()->action([\App\Http\Controllers\Admin\CustomerController::class, 'edit'], $customer->uid);
        }

        return view('admin.customers.sub_account', [
            'customer' => $customer,
        ]);
    }

    /**
     * Assign plan to customer.
     */
    public function assignPlan(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $customer = \App\Model\Customer::findByUid($uid);
        $plans = Plan::active()->get();

        // authorize
        if (\Gate::denies('assignPlan', $customer)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $plan = Plan::findByUid($request->plan_uid);

            $customer->assignPlan($plan);

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.customer.plan.assigned', [
                    'plan' => $plan->name,
                    'customer' => $customer->user->displayName(),
                ]),
            ], 201);
        }

        return view('admin.customers.assign_plan', [
            'customer' => $customer,
            'plans' => $plans,
        ]);
    }
}
