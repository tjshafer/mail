<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Setting;
use App\Model\Template;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\Template())) {
            return $this->notAuthorized();
        }

        return view('admin.templates.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\Template())) {
            return $this->notAuthorized();
        }

        $templates = Template::shared()
            ->categoryUid($request->category_uid)
            ->search($request->keyword)
            ->orderBy($request->sort_order, $request->sort_direction)
            ->paginate($request->per_page);

        return view('admin.templates._list', [
            'templates' => $templates,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $user = $request->user();
        $template = new \App\Model\Template();

        // authorize
        if (! $request->user()->admin->can('create', Template::class)) {
            return $this->notAuthorized();
        }

        // Get old post values
        if (null !== $request->old()) {
            $template->fill($request->old());
        }

        return view('admin.templates.create', [
            'template' => $template,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $user = $request->user();
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        // Get old post values
        if (null !== $request->old()) {
            $template->fill($request->old());
        }

        return view('admin.templates.edit', [
            'template' => $template,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Generate info
        $user = $request->user();
        $template = Template::findByUid($request->uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('patch')) {
            // Save template
            $template->fill($request->all());

            $rules = [
                'name' => 'required',
                'content' => 'required',
            ];

            // make validator
            $validator = \Validator::make($request->all(), $rules);

            // redirect if fails
            if ($validator->fails()) {
                // faled
                return response()->json($validator->errors(), 400);
            }

            $template->save();

            // success
            return response()->json([
                'status' => 'success',
                'message' => trans('messages.template.updated'),
            ], 201);
        }
    }

    /**
     * Upload template.
     */
    public function uploadTemplate(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (! $request->user()->admin->can('create', Template::class)) {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            $asAdmin = true;
            $template = Template::uploadSystemTemplate($request, $asAdmin);

            if (! empty(Setting::get('storage.s3'))) {
                App::make('xstore')->store($template);
            }

            $request->session()->flash('alert-success', trans('messages.template.uploaded'));

            return redirect()->action([\App\Http\Controllers\Admin\TemplateController::class, 'index']);
        }

        return view('admin.templates.upload');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if (isSiteDemo()) {
            return response()->json([
                'status' => 'notice',
                'message' => trans('messages.operation_not_allowed_in_demo'),
            ], 403);
        }

        $templates = Template::whereIn('uid', explode(',', $request->uids));
        $total = $templates->count();
        $deleted = 0;
        foreach ($templates->get() as $template) {
            // authorize
            if ($request->user()->admin->can('delete', $template)) {
                $template->deleteAndCleanup();
                $deleted += 1;
            }
        }

        echo trans('messages.templates.deleted', ['deleted' => $deleted, 'total' => $total]);
    }

    /**
     * Preview template.
     */
    public function preview(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $template = Template::findByUid($id);

        // authorize
        if (! $request->user()->admin->can('preview', $template)) {
            return $this->notAuthorized();
        }

        return view('admin.templates.preview', [
            'template' => $template,
        ]);
    }

    /**
     * Custom sort items.
     */
    public function sort(Request $request): void
    {
        echo trans('messages._deleted_');
    }

    /**
     * Copy template.
     *
     * @return \Illuminate\Http\Response
     */
    public function copy(Request $request)
    {
        $template = Template::findByUid($request->uid);

        if ($request->isMethod('post')) {
            // authorize
            if (! $request->user()->admin->can('copy', $template)) {
                return $this->notAuthorized();
            }

            $template->copy($request->name, null, $request->user()->admin);

            echo trans('messages.template.copied');

            return;
        }

        return view('admin.templates.copy', [
            'template' => $template,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function builderEdit(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $user = $request->user();
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            $rules = [
                'content' => 'required',
            ];

            $this->validate($request, $rules);

            $template->content = $request->content;
            $template->save();

            return response()->json([
                'status' => 'success',
            ]);
        }

        return view('admin.templates.builder.edit', [
            'template' => $template,
            'templates' => $template->getBuilderAdminTemplates(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function builderEditContent(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $user = $request->user();
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        return view('admin.templates.builder.content', [
            'content' => $template->content,
        ]);
    }

    /**
     * Upload asset to builder.
     *
     * @param  int  $id
     */
    public function uploadTemplateAssets(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        if ($request->assetType == 'upload') {
            $assetUrl = $template->uploadAsset($request->file('file'));
        } elseif ($request->assetType == 'url') {
            $assetUrl = $template->uploadAssetFromUrl($request->url);
        } elseif ($request->assetType == 'base64') {
            $assetUrl = $template->uploadAssetFromBase64($request->url_base64);
        }

        return response()->json([
            'url' => $assetUrl,
        ]);
    }

    /**
     * Create template / temlate selection.
     */
    public function builderCreate(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $template = new Template();
        $template->name = trans('messages.untitled_template');

        // authorize
        if (! $request->user()->admin->can('create', Template::class)) {
            return $this->notAuthorized();
        }

        // Gallery
        $templates = Template::where('customer_id', '=', null);

        // validate and save posted data
        if ($request->isMethod('post')) {
            $currentTemplate = Template::findByUid($request->template);

            // create from template
            $template = $currentTemplate->copy([
                'name' => $request->name,
                'admin_id' => $request->user()->admin->id,
            ]);

            // save
            $template->save();

            return redirect()->action([\App\Http\Controllers\Admin\TemplateController::class, 'builderEdit'], $template->uid);
        }

        return view('admin.templates.builder.create', [
            'template' => $template,
            'templates' => $templates,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function builderTemplates(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (! $request->user()->admin->can('create', Template::class)) {
            return $this->notAuthorized();
        }

        // category
        $category = \App\Model\TemplateCategory::findByUid($request->category_uid);

        // sort, pagination
        $templates = $category->templates()->search($request->keyword)
            ->orderBy($request->sort_order, $request->sort_direction)
            ->paginate($request->per_page);

        return view('admin.templates.builder.templates', [
            'templates' => $templates,
        ]);
    }

    /**
     * Change template from exist template.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function builderChangeTemplate(Request $request, $uid, $change_uid)
    {
        // Generate info
        $template = Template::findByUid($uid);
        $changeTemplate = Template::findByUid($change_uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        $template->changeTemplate($changeTemplate);
    }

    /**
     * Update template thumb.
     */
    public function updateThumb(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        if ($request->isMethod('post')) {
            // make validator
            $validator = \Validator::make($request->all(), [
                'file' => 'required',
            ]);

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('templates.updateThumb', [
                    'template' => $template,
                    'errors' => $validator->errors(),
                ], 400);
            }

            // update thumb
            $template->uploadThumbnail($request->file);

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.template.thumb.uploaded'),
            ], 201);
        }

        return view('admin.templates.updateThumb', [
            'template' => $template,
        ]);
    }

    /**
     * Update template thumb url.
     */
    public function updateThumbUrl(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        if ($request->isMethod('post')) {
            // make validator
            $validator = \Validator::make($request->all(), [
                'url' => 'required|url',
            ]);

            // redirect if fails
            if ($validator->fails()) {
                return response()->view('templates.updateThumbUrl', [
                    'template' => $template,
                    'errors' => $validator->errors(),
                ], 400);
            }

            // update thumb
            $template->uploadThumbnailUrl($request->url);

            return response()->json([
                'status' => 'success',
                'message' => trans('messages.template.thumb.uploaded'),
            ], 201);
        }

        return view('admin.templates.updateThumbUrl', [
            'template' => $template,
        ]);
    }

    /**
     * Template categories.
     */
    public function categories(Request $request, $uid): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $template = Template::findByUid($uid);

        // authorize
        if (! $request->user()->admin->can('update', $template)) {
            return $this->notAuthorized();
        }

        if ($request->isMethod('post')) {
            foreach ($request->categories as $key => $value) {
                $category = \App\Model\TemplateCategory::findByUid($key);
                if ($value == 'true') {
                    $template->addCategory($category);
                } else {
                    $template->removeCategory($category);
                }
            }
        }

        return view('admin.templates.categories', [
            'template' => $template,
        ]);
    }
}
