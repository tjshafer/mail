<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (\Gate::denies('read', new \App\Model\AdminGroup())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\AdminGroup())) {
            $request->merge(['creator_id' => $request->user()->id]);
        }

        $groups = \App\Model\AdminGroup::search($request);

        return view('admin.admin_groups.index', [
            'groups' => $groups,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (\Gate::denies('read', new \App\Model\AdminGroup())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\AdminGroup())) {
            $request->merge(['creator_id' => $request->user()->id]);
        }

        $groups = \App\Model\AdminGroup::search($request)->paginate($request->per_page);

        return view('admin.admin_groups._list', [
            'groups' => $groups,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $user = $request->user();

        $group = new \App\Model\AdminGroup([
            'backend_access' => false,
            'frontend_access' => true,
        ]);
        $group->fill($request->old());

        // authorize
        if (\Gate::denies('create', $group)) {
            return $this->notAuthorized();
        }

        // For permissions
        if (isset($request->old()['permissions'])) {
            $group->permissions = json_encode($request->old()['permissions']);
        }
        $permissions = $group->getPermissions();

        return view('admin.admin_groups.create', [
            'group' => $group,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Generate info
        $user = $request->user();
        $group = new \App\Model\AdminGroup([]);

        // authorize
        if (\Gate::denies('create', $group)) {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            $this->validate($request, \App\Model\AdminGroup::rules());

            $group->fill($request->all());
            $group->permissions = json_encode($request->permissions);
            $group->creator_id = $user->id;
            $group->save();

            $request->session()->flash('alert-success', trans('messages.admin_group.created'));

            return redirect()->action([\App\Http\Controllers\Admin\AdminGroupController::class, 'index']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // Generate info
        $user = $request->user();
        $group = \App\Model\AdminGroup::find($id);
        $group->fill($request->old());

        // authorize
        if (\Gate::denies('update', $group)) {
            return $this->notAuthorized();
        }

        // For permissions
        if (isset($request->old()['permissions'])) {
            $group->permissions = json_encode($request->old()['permissions']);
        }
        $permissions = $group->getPermissions();

        return view('admin.admin_groups.edit', [
            'group' => $group,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Generate info
        $user = $request->user();

        $group = \App\Model\AdminGroup::find($id);

        // authorize
        if (\Gate::denies('update', $group)) {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('patch')) {
            $this->validate($request, \App\Model\AdminGroup::rules());

            $group->fill($request->all());
            $group->permissions = json_encode($request->permissions);
            $group->save();

            $request->session()->flash('alert-success', trans('messages.admin_group.updated'));

            return redirect()->action([\App\Http\Controllers\Admin\AdminGroupController::class, 'edit'], $group->id);
        }
    }

    /**
     * Custom sort items.
     */
    public function sort(Request $request): void
    {
        echo trans('messages._deleted_');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(Request $request): void
    {
        $lists = \App\Model\AdminGroup::whereIn('id', explode(',', $request->ids));

        foreach ($lists->get() as $item) {
            // authorize
            if (\Gate::denies('delete', $item)) {
                return;
            }
        }

        foreach ($lists->get() as $item) {
            $item->delete();
        }

        // Redirect to my lists page
        echo trans('messages.admin_groups.deleted');
    }
}
