<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Tool;
use App\Model\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (\Gate::denies('list', Language::class)) {
            return $this->notAuthorized();
        }

        return view('admin.languages.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (\Gate::denies('list', Language::class)) {
            return $this->notAuthorized();
        }

        $languages = Language::search($request->keyword)
            ->orderBy($request->sort_order, $request->sort_direction)
            ->paginate($request->per_page);

        return view('admin.languages._list', [
            'languages' => $languages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $language = Language::newDefaultLanguage();

        // authorize
        if (\Gate::denies('create', $language)) {
            return $this->notAuthorized();
        }

        return view('admin.languages.create', [
            'language' => $language,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        // authorize
        if (\Gate::denies('create', Language::class)) {
            return $this->notAuthorized();
        }

        [$language, $validator] = Language::createFromRequest($request);

        if ($validator !== true) {
            return response()->view('admin.languages.create', [
                'language' => $language,
                'errors' => $validator->errors(),
            ], 400);
        }

        $request->session()->flash('alert-success', trans('messages.language.created'));

        return redirect()->action([\App\Http\Controllers\Admin\LanguageController::class, 'index']);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $language = Language::findByUid($id);

        // authorize
        if (\Gate::denies('update', $language)) {
            return $this->notAuthorized();
        }

        return view('admin.languages.edit', [
            'language' => $language,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        $language = Language::findByUid($id);

        // authorize
        if (\Gate::denies('update', $language)) {
            return $this->notAuthorized();
        }

        $validator = $language->updateFromRequest($request);

        if ($validator !== true) {
            return response()->view('admin.languages.edit', [
                'language' => $language,
                'errors' => $validator->errors(),
            ], 400);
        }

        $request->session()->flash('alert-success', trans('messages.language.created'));

        return redirect()->action([\App\Http\Controllers\Admin\LanguageController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $languages = Language::whereIn('uid', explode(',', $request->uids));

        foreach ($languages->get() as $language) {
            // authorize
            if (\Gate::allows('delete', $language)) {
                $language->deleteAndCleanup();
            }
        }

        echo trans('messages.languages.deleted');
    }

    /**
     * Translate.
     *
     * @return \Illuminate\Http\Response
     */
    public function translate(Request $request, int $id)
    {
        $language = Language::findByUid($id);
        $currentFile = $language->findFileById($request->file_id);

        // Prenvent save from demo mod
        if ($this->isDemoMode()) {
            return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
        }

        // authorize
        if (\Gate::denies('translate', $language)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            [$currentFile, $validator] = $language->translateFile($request->file_id, $request->content);

            if ($validator->fails()) {
                return response()->view('admin.languages.translate', [
                    'language' => $language,
                    'currentFile' => $currentFile,
                    'content' => $request->content,
                    'errors' => $validator->errors(),
                ], 400);
            }

            $request->session()->flash('alert-success', trans('messages.language.updated'));

            return redirect()->action([\App\Http\Controllers\Admin\LanguageController::class, 'translate'], ['id' => $language->uid, 'file_id' => $currentFile['id']]);
        }

        return view('admin.languages.translate', [
            'language' => $language,
            'currentFile' => $currentFile,
            'content' => Language::fileToYaml($currentFile['path']),
        ]);
    }

    /**
     * Disable language.
     *
     * @param  int  $id
     */
    public function disable(Request $request): void
    {
        $items = Language::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if (\Gate::allows('disable', $item)) {
                $item->disable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.languages.disabled');
    }

    /**
     * Disable language.
     *
     * @param  int  $id
     */
    public function enable(Request $request): void
    {
        $items = Language::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if (\Gate::allows('enable', $item)) {
                $item->enable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.languages.enabled');
    }

    /**
     * Download language package.
     */
    public function download(Request $request, int $id): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $language = Language::findByUid($id);
        $tmpzip = storage_path('tmp/language-'.$language->code.'.zip');

        Tool::zip($language->languageDir(), $tmpzip);

        return response()->download($tmpzip)->deleteFileAfterSend(true);
    }

    /**
     * Upload language package.
     */
    public function upload(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $language = Language::findByUid($id);

        // validate and save posted data
        if ($request->isMethod('post')) {
            $validator = $language->upload($request);

            if ($validator->fails()) {
                return response()->view('admin.languages.upload', [
                    'language' => $language,
                    'errors' => $validator->errors(),
                ], 400);
            }

            $request->session()->flash('alert-success', trans('messages.language.uploaded'));

            return redirect()->action([\App\Http\Controllers\Admin\LanguageController::class, 'index']);
        }

        return view('admin.languages.upload', [
            'language' => $language,
        ]);
    }

    /**
     * Delete confirm message.
     */
    public function deleteConfirm(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $languages = Language::whereIn('uid', explode(',', $request->uids));

        return view('admin.languages.delete_confirm', [
            'languages' => $languages,
        ]);
    }
}
