<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeedbackLoopHandlerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (\Gate::denies('read', new \App\Model\FeedbackLoopHandler())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\FeedbackLoopHandler())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        $items = \App\Model\FeedbackLoopHandler::search($request);

        return view('admin.feedback_loop_handlers.index', [
            'items' => $items,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (\Gate::denies('read', new \App\Model\FeedbackLoopHandler())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\FeedbackLoopHandler())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        $items = \App\Model\FeedbackLoopHandler::search($request)->paginate($request->per_page);

        return view('admin.feedback_loop_handlers._list', [
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = new \App\Model\FeedbackLoopHandler();
        $server->status = 'active';
        $server->uid = '0';
        $server->fill($request->old());

        // authorize
        if (\Gate::denies('create', $server)) {
            return $this->notAuthorized();
        }

        return view('admin.feedback_loop_handlers.create', [
            'server' => $server,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $server = new \App\Model\FeedbackLoopHandler();

        // authorize
        if (\Gate::denies('create', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $this->validate($request, \App\Model\FeedbackLoopHandler::rules());

            // Save current user info
            $server->fill($request->all());
            $server->admin_id = $request->user()->admin->id;
            $server->status = 'active';

            if ($server->save()) {
                $request->session()->flash('alert-success', trans('messages.feedback_loop_handler.created'));

                return redirect()->action([\App\Http\Controllers\Admin\FeedbackLoopHandlerController::class, 'index']);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): void
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\FeedbackLoopHandler::findByUid($id);

        // authorize
        if (\Gate::denies('update', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        return view('admin.feedback_loop_handlers.edit', [
            'server' => $server,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $server = \App\Model\FeedbackLoopHandler::findByUid($id);

        // authorize
        if (\Gate::denies('update', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $this->validate($request, \App\Model\FeedbackLoopHandler::rules());

            // Save current user info
            $server->fill($request->all());

            if ($server->save()) {
                $request->session()->flash('alert-success', trans('messages.feedback_loop_handler.updated'));

                return redirect()->action([\App\Http\Controllers\Admin\FeedbackLoopHandlerController::class, 'index']);
            }
        }
    }

    /**
     * Custom sort items.
     */
    public function sort(Request $request): void
    {
        echo trans('messages._deleted_');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = \App\Model\FeedbackLoopHandler::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if (\Gate::denies('delete', $item)) {
                return;
            }
        }

        foreach ($items->get() as $item) {
            $item->delete();
        }

        // Result message
        echo trans('messages.feedback_loop_handlers.deleted');
    }

    /**
     * Test feedback loop handler.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request, $uid)
    {
        // Get current user
        $current_user = $request->user();

        // Fill new server info
        if ($uid) {
            $server = \App\Model\FeedbackLoopHandler::findByUid($uid);
        } else {
            $server = new \App\Model\FeedbackLoopHandler();
        }

        $server->fill($request->all());

        // authorize
        if (\Gate::denies('test', $server)) {
            return $this->notAuthorized();
        }

        try {
            $server->test();
            echo json_encode([
                'status' => 'success', // or success
                'message' => trans('messages.feedback_loop_handler.test_success'),
            ]);
        } catch (\Exception $e) {
            echo json_encode([
                'status' => 'error', // or success
                'message' => $e->getMessage(),
            ]);
        }
    }
}
