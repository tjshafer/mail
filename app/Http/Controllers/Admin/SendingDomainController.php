<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendingDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\SendingDomain())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\SendingDomain())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        // exlude customer seding domain
        $request->merge(['no_customer' => true]);

        $items = \App\Model\SendingDomain::search($request);

        return view('admin.sending_domains.index', [
            'items' => $items,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\SendingDomain())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\SendingDomain())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        // exlude customer seding domain
        $request->merge(['no_customer' => true]);

        $items = \App\Model\SendingDomain::search($request)->paginate($request->per_page);

        return view('admin.sending_domains._list', [
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = new \App\Model\SendingDomain([
            'signing_enabled' => true,
        ]);
        $server->status = 'active';
        $server->uid = '0';
        $server->fill($request->old());

        // authorize
        if (! $request->user()->admin->can('create', $server)) {
            return $this->notAuthorized();
        }

        return view('admin.sending_domains.create', [
            'server' => $server,
            'readonly' => '0',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get current user
        $current_user = $request->user();
        $server = new \App\Model\SendingDomain();

        // authorize
        if (! $request->user()->admin->can('create', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('post')) {
            $this->validate($request, \App\Model\SendingDomain::rules());

            // Save current user info
            $server->fill($request->all());
            $server->admin_id = $request->user()->admin->id;
            $server->status = 'active';

            if ($server->save()) {
                $server->syncToMta();
                $request->session()->flash('alert-success', trans('messages.sending_domain.created'));

                return redirect()->action([\App\Http\Controllers\Admin\SendingDomainController::class, 'show'], $server->uid);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\SendingDomain::findByUid($id);

        return view('admin.sending_domains.show', [
            'server' => $server,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, int $id): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->admin->can('update', $server)) {
            return $this->notAuthorized();
        }

        $server->fill($request->old());

        return view('admin.sending_domains.edit', [
            'server' => $server,
            'readonly' => 'readonly',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        // Get current user
        $current_user = $request->user();
        $server = \App\Model\SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->admin->can('update', $server)) {
            return $this->notAuthorized();
        }

        // save posted data
        if ($request->isMethod('patch')) {
            $this->validate($request, \App\Model\SendingDomain::rules());

            // Save current user info
            $server->fill($request->all());

            if ($server->save()) {
                $request->session()->flash('alert-success', trans('messages.sending_domain.updated'));

                return redirect()->action([\App\Http\Controllers\Admin\SendingDomainController::class, 'show'], $server->uid);
            }
        }
    }

    /**
     * Custom sort items.
     */
    public function sort(Request $request): void
    {
        echo trans('messages._deleted_');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = \App\Model\SendingDomain::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('delete', $item)) {
                $item->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.sending_domains.deleted');
    }

    /**
     * Verify sending domain.
     */
    public function verify(int $id): void
    {
        $server = \App\Model\SendingDomain::findByUid($id);
        $server->verify();
    }

    /**
     * sending domain's records.
     */
    public function records(int $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $server = \App\Model\SendingDomain::findByUid($id);

        return view('admin.sending_domains._records', [
            'server' => $server,
        ]);
    }

    /**
     * update VerificationTxtName.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateVerificationTxtName(int $id, Request $request)
    {
        $server = \App\Model\SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->admin->can('update', $server)) {
            return $this->notAuthorized();
        }

        if (! $server->setVerificationTxtName($request->value)) {
            return response(trans('messages.sending_domain.verification_hostname.not_valid'), 404)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     * update VerificationTxtName.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateDkimSelector(int $id, Request $request)
    {
        $server = \App\Model\SendingDomain::findByUid($id);

        // authorize
        if (! $request->user()->admin->can('update', $server)) {
            return $this->notAuthorized();
        }

        if (! $server->setDkimSelector($request->value)) {
            return response(trans('messages.sending_domain.dkim_selector.not_valid'), 404)
                ->header('Content-Type', 'text/plain');
        }
    }
}
