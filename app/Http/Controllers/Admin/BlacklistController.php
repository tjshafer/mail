<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Acelle\Jobs\ImportBlacklistJob;
use App\Model\Blacklist;
use App\Model\JobMonitor;
use Exception;
use Illuminate\Http\Request;

class BlacklistController extends Controller
{
    /**
     * Search items.
     */
    public function search($request): \App\Model\collect
    {
        $request->merge(['admin_id' => $request->user()->admin->id]);
        $blacklists = \App\Model\Blacklist::search($request);

        return $blacklists;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $admin = $request->user()->admin;

        if (! $admin->can('read', new \App\Model\Blacklist())) {
            return $this->notAuthorized();
        }

        $blacklists = $this->search($request);

        // Get current job

        return view('admin.blacklists.index', [
            'blacklists' => $blacklists,

        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\Blacklist())) {
            return $this->notAuthorized();
        }

        $blacklists = $this->search($request)->paginate($request->per_page);

        return view('admin.blacklists._list', [
            'blacklists' => $blacklists,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        if ($request->select_tool == 'all_items') {
            $blacklists = $this->search($request);
        } else {
            $blacklists = \App\Model\Blacklist::whereIn('id', explode(',', $request->uids));
        }

        foreach ($blacklists->get() as $blacklist) {
            // authorize
            if ($request->user()->admin->can('delete', $blacklist)) {
                // Log
                $blacklist->delist();
                $blacklist->delete();
            }
        }

        // Redirect to my lists page
        echo trans('messages.blacklists.deleted');
    }

    /**
     * Start import process.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $admin = $request->user()->admin;
        $job = $admin->importBlacklistJobs()->first();

        if ($job) {
            return view('admin.blacklists.import', [
                'currentJobUid' => $job->uid,
                'cancelUrl' => action([\App\Http\Controllers\Admin\BlacklistController::class, 'cancelImport'], ['job_uid' => $job->uid]),
                'progressCheckUrl' => action([\App\Http\Controllers\Admin\BlacklistController::class, 'importProgress'], ['job_uid' => $job->uid]),
            ]);
        } else {
            return view('admin.blacklists.import', [
                //
            ]);
        }
    }

    public function startImport(Request $request)
    {
        $admin = $request->user()->admin;

        // authorize
        if (! $admin->can('import', new \App\Model\Blacklist())) {
            return $this->notAuthorized();
        }

        if ($request->hasFile('file')) {
            $filepath = Blacklist::upload($request->file('file'));

            // Start system job
            $job = $admin->dispatchWithMonitor(new ImportBlacklistJob($filepath, $customer = null));

            return redirect()->action([\App\Http\Controllers\Admin\BlacklistController::class, 'import']);
        } else {
            throw new Exception('no upload file');
        }
    }

    /**
     * Check import proccessing.
     */
    public function importProgress(Request $request): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $admin = $request->user()->admin;

        // authorize
        if (! $admin->can('read', new \App\Model\Blacklist())) {
            return $this->notAuthorized();
        }

        $job = JobMonitor::findByUid($request->job_uid);

        throw_if(is_null($job), new Exception(sprintf('Blacklist import job #%s does not exist', $request->job_uid)));

        $progress = $job->getJsonData();
        $progress['status'] = $job->status;
        $progress['error'] = $job->error;

        // Get progress updated by the import process and status of the final job monitor
        return response()->json($progress);
    }

    /**
     * Cancel importing job.
     */
    public function cancelImport(Request $request): \Illuminate\Http\JsonResponse
    {
        $job = JobMonitor::findByUid($request->job_uid);
        throw_if(is_null($job), new Exception(sprintf('Verification job #%s does not exist', $request->job_uid)));

        $job->cancel();

        return response()->json();
    }

    /**
     * Reason.
     */
    public function reason(Request $request, $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $blacklist = \App\Model\Blacklist::find($id);

        return view('admin.blacklists.reason', [
            'blacklist' => $blacklist,
        ]);
    }
}
