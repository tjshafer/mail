<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Docs for api v1 - Admin.
     */
    public function doc(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('docs.api.v1', ['view' => 'backend']);
    }
}
