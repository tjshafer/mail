<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Automation2;
use App\Model\Notification;
use App\Model\Subscriber;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        parent::__construct();

        // Trigger admin monitoring events when admin is logged in
        event(new  \App\Events\AdminLoggedIn());
    }

    /**
     * Show the application admin dashboard.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $notifications = Notification::top();

        return view('admin.dashboard', [
            'notifications' => $notifications,
            'subscribersCount' => Subscriber::count(),
            'automationsCount' => Automation2::count(),
        ]);
    }
}
