<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Acelle\Library\Facades\Billing;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class PaymentController extends Controller
{
    /**
     * Display all paymentt.
     */
    public function index(Request $request, MessageBag $message_bag): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('admin.payments.index', [
            'gateways' => Billing::getGateways(),
            'enabledGateways' => Billing::getEnabledPaymentGateways(),
        ]);
    }

    /**
     * Enable payment.
     */
    public function enable(Request $request, int $name): \Illuminate\Http\RedirectResponse
    {
        // enable gateway
        Billing::enablePaymentGateway($name);

        $request->session()->flash('alert-success', trans('messages.payment_gateway.updated'));

        return redirect()->action([\App\Http\Controllers\Admin\PaymentController::class, 'index']);
    }

    /**
     * Disable payment.
     */
    public function disable(Request $request, int $name): \Illuminate\Http\RedirectResponse
    {
        // disable gateway
        Billing::disablePaymentGateway($name);

        $request->session()->flash('alert-success', trans('messages.payment_gateway.updated'));

        return redirect()->action([\App\Http\Controllers\Admin\PaymentController::class, 'index']);
    }
}
