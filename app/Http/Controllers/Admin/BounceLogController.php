<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BounceLogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($request->user()->admin->getPermission('report_bounce_log') == 'no') {
            return $this->notAuthorized();
        }

        $items = \App\Model\BounceLog::getAll();

        return view('admin.bounce_logs.index', [
            'items' => $items,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($request->user()->admin->getPermission('report_bounce_log') == 'no') {
            return $this->notAuthorized();
        }

        $items = \App\Model\BounceLog::search($request)->paginate($request->per_page);

        return view('admin.bounce_logs._list', [
            'items' => $items,
        ]);
    }
}
