<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Plugin;
use Exception;
use Illuminate\Http\Request;

class PluginController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\Plugin())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\Plugin())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        // exlude customer seding plugins
        $request->merge(['no_customer' => true]);

        $plugins = \App\Model\Plugin::search($request);

        return view('admin.plugins.index', [
            'plugins' => $plugins,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! $request->user()->admin->can('read', new \App\Model\Plugin())) {
            return $this->notAuthorized();
        }

        // If admin can view all sending domains
        if (! $request->user()->admin->can('readAll', new \App\Model\Plugin())) {
            $request->merge(['admin_id' => $request->user()->admin->id]);
        }

        // exlude customer seding plugins
        $request->merge(['no_customer' => true]);

        $plugins = \App\Model\Plugin::search($request)->paginate($request->per_page);

        $settingUrls = [];
        $blacklist = [];
        foreach ($plugins as $plugin) {
            // Generate setting buttons
            try {
                $composerJson = $plugin->getComposerJson($plugin->name);
                if (array_key_exists('extra', $composerJson) && array_key_exists('setting-route', $composerJson['extra'])) {
                    $url = action($composerJson['extra']['setting-route']);
                    $settingUrls[$plugin->name] = $url;
                } else {
                    throw new Exception('extra/setting-route not found');
                }
            } catch (Exception $ex) {
                $blacklist[$plugin->name] = 'Something went wrong with the plugin: '.$ex->getMessage();
            }
        }

        return view('admin.plugins._list', [
            'plugins' => $plugins,
            'settingUrls' => $settingUrls,
            'blacklist' => $blacklist,
        ]);
    }

    /**
     * Install/Upgrage plugins.
     */
    public function install(Request $request): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // authorize
        if (! $request->user()->admin->can('install', Plugin::class)) {
            return $this->notAuthorized();
        }

        // do install
        if ($request->isMethod('post')) {
            // Upload
            $pluginName = Plugin::upload($request);

            // Install Plugin
            Plugin::installFromDir($pluginName);

            return response()->json(['url' => action([\App\Http\Controllers\Admin\PluginController::class, 'index'])]);
        }

        return view('admin.plugins.install');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete(Request $request): void
    {
        $items = \App\Model\Plugin::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('delete', $item)) {
                $item->deleteAndCleanup();
            }
        }

        // Redirect to my lists page
        echo trans('messages.plugins.deleted');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function disable(Request $request): void
    {
        $items = \App\Model\Plugin::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('disable', $item)) {
                $item->disable();
            }
        }

        // Redirect to my lists page
        echo trans('messages.plugins.disabled');
    }

    /**
     * Disable sending server.
     *
     * @param  int  $id
     */
    public function enable(Request $request): void
    {
        $items = \App\Model\Plugin::whereIn('uid', explode(',', $request->uids));

        foreach ($items->get() as $item) {
            // authorize
            if ($request->user()->admin->can('enable', $item)) {
                $item->activate();
            }
        }

        // Redirect to my lists page
        echo trans('messages.plugins.enabled');
    }

    /**
     * Email verification server display options form.
     */
    public function options(Request $request, $uid = null): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($uid) {
            $plugin = \App\Model\Plugin::findByUid($uid);
        } else {
            $plugin = new \App\Model\Plugin($request->all());
            $options = $plugin->getOptions();
        }

        return view('admin.plugins._options', [
            'server' => $plugin,
            'options' => $options,
        ]);
    }
}
