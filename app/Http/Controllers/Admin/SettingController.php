<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Library\ExtendedSwiftMessage;
use App\Model\Setting;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display and update all settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_general') == 'yes') {
            return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'general']);
        //} elseif ($request->user()->admin->getPermission('setting_sending') == 'yes') {
        //   return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'sending']);
        } elseif ($request->user()->admin->getPermission('setting_system_urls') == 'yes') {
            return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'urls']);
        } elseif ($request->user()->admin->getPermission('setting_background_job') == 'yes') {
            return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'cronjob']);
        }
    }

    /**
     * General settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function general(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_general') != 'yes') {
            return $this->notAuthorized();
        }

        // Setting::updateAll();
        $settings = Setting::getAll();
        if (null !== $request->old()) {
            foreach ($request->old() as $name => $value) {
                if (isset($settings[$name])) {
                    $settings[$name]['value'] = $value;
                }
            }
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            // Prenvent save from demo mod
            if ($this->isDemoMode()) {
                return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
            }

            $rules = [
                'site_name' => 'required',
                'site_keyword' => 'required',
                'site_online' => 'required',
                'site_offline_message' => 'required',
                'site_description' => 'required',
                'frontend_scheme' => 'required',
                'backend_scheme' => 'required',
            ];
            $this->validate($request, $rules);

            // Save settings
            foreach ($request->all() as $name => $value) {
                if ($name != '_token' && isset($settings[$name])) {
                    // Upload and save image
                    if ($name == 'site_logo_small' || $name == 'site_logo_big' || $name == 'site_favicon') {
                        if ($request->hasFile($name) && $request->file($name)->isValid()) {
                            Setting::uploadFile($request->file($name), $name, false);
                        }
                    } else {
                        if ($settings[$name]['cat'] == 'general' && $request->user()->admin->getPermission('setting_general') == 'yes') {
                            Setting::set($name, $value);
                        }
                    }
                }
            }

            // Redirect to my lists page
            $request->session()->flash('alert-success', trans('messages.setting.updated'));

            return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'general']);
        }

        return view('admin.settings.general', [
            'settings' => $settings,
        ]);
    }

    /**
     * Sending settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function sending(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_sending') != 'yes') {
            return $this->notAuthorized();
        }

        // Setting::updateAll();
        $settings = Setting::getAll();
        if (null !== $request->old()) {
            foreach ($request->old() as $name => $value) {
                if (isset($settings[$name])) {
                    $settings[$name]['value'] = $value;
                }
            }
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            // Prenvent save from demo mod
            if ($this->isDemoMode()) {
                return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
            }

            $rules = [
                'sending_campaigns_at_once' => 'required',
                'sending_change_server_time' => 'required',
                'sending_emails_per_minute' => 'required',
                'sending_pause' => 'required',
                'sending_at_once' => 'required',
                'sending_subscribers_at_once' => 'required',
            ];
            $this->validate($request, $rules);

            // Save settings
            foreach ($request->all() as $name => $value) {
                if ($name != '_token' && isset($settings[$name])) {
                    if ($settings[$name]['cat'] == 'sending' && $request->user()->admin->getPermission('setting_sending') == 'yes') {
                        Setting::set($name, $value);
                    }
                }
            }

            // Redirect to my lists page
            $request->session()->flash('alert-success', trans('messages.setting.updated'));

            return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'sending']);
        }

        return view('admin.settings.sending', [
            'settings' => $settings,
        ]);
    }

    /**
     * Url settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function urls(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_system_urls') != 'yes') {
            return $this->notAuthorized();
        }

        $settings = Setting::getAll();

        // Check URL
        $current = url('/');
        $cached = config('app.url');

        if (! is_null($request->input('debug'))) {
            echo "Current: {$current} vs. Cached: {$cached}";

            return;
        }

        return view('admin.settings.urls', [
            'settings' => $settings,
            'matched' => ($cached == $current),
            'current' => $current,
            'cached' => $cached,
        ]);
    }

    /**
     * Cronjob list.
     *
     * @return \Illuminate\Http\Response
     */
    public function cronjob(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_background_job') != 'yes') {
            return $this->notAuthorized();
        }

        // Re-generate remote job url
        if ($request->re_generate_remote_job_url) {
            $remote_job_token = Str::random(60);
            Setting::set('remote_job_token', $remote_job_token);
            echo action([\App\Http\Controllers\Controller::class, 'remoteJob'], ['remote_job_token' => $remote_job_token]);

            return;
        }

        $respone =  \App\Library\Tool::cronjobUpdateController($request, $this);
        if ($respone == 'done' || $respone['valid'] == true) {
            $next = action([\App\Http\Controllers\Admin\SettingController::class, 'cronjob']).'#result_box';

            return redirect()->away($next);
        }

        return view('admin.settings.cronjob', $respone);
    }

    /**
     * Mailer settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function mailer(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_general') != 'yes') {
            return $this->notAuthorized();
        }

        // SMTP
        $env = [
            'MAIL_MAILER' => Setting::get('mailer.mailer') ?: Setting::get('mailer.driver'), // Laravel 5.8 compatibility
            'MAIL_HOST' => Setting::get('mailer.host'),
            'MAIL_PORT' => Setting::get('mailer.port'),
            'MAIL_USERNAME' => Setting::get('mailer.username'),
            'MAIL_PASSWORD' => Setting::get('mailer.password'),
            'MAIL_ENCRYPTION' => Setting::get('mailer.encryption'),
            'MAIL_FROM_ADDRESS' => Setting::get('mailer.from.address'),
            'MAIL_FROM_NAME' => Setting::get('mailer.from.name'),
            'sendmail_path' => Setting::get('mailer.sendmail_path') ?: '/usr/sbin/sendmail',
        ];

        if (null !== $request->old() && isset($request->old()['env'])) {
            foreach ($request->old()['env'] as $name => $value) {
                $env[$name] = $value;
            }
        }

        $env_rules = [
            'smtp' => [
                'env.MAIL_MAILER' => 'required',
                'env.MAIL_HOST' => 'required',
                'env.MAIL_PORT' => 'required',
                'env.MAIL_USERNAME' => 'required',
                'env.MAIL_PASSWORD' => 'required',
                'env.MAIL_FROM_ADDRESS' => 'required|email',
                'env.MAIL_FROM_NAME' => 'required',
            ],
            'sendmail' => [
                'env.MAIL_FROM_ADDRESS' => 'required|email',
                'env.MAIL_FROM_NAME' => 'required',
                'env.sendmail_path' => 'required',
            ],
        ];

        // validate and save posted data
        if ($request->isMethod('post')) {
            // Prenvent save from demo mod
            if ($this->isDemoMode()) {
                return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
            }

            $env = $request->env;

            $this->validate($request, $env_rules[$env['MAIL_MAILER']]);

            // Check SMTP connection
            $site_info = $request->all();
            if ($env['MAIL_MAILER'] == 'smtp') {
                $rules = [];
                $messages = [];
                try {
                    $transport = new \Swift_SmtpTransport($env['MAIL_HOST'], $env['MAIL_PORT'], $env['MAIL_ENCRYPTION']);
                    $transport->setUsername($env['MAIL_USERNAME']);
                    $transport->setPassword($env['MAIL_PASSWORD']);
                    // @todo: it is not recommended to allow self-signed
                    $transport->setStreamOptions(['ssl' => ['allow_self_signed' => true, 'verify_peer' => false, 'verify_peer_name' => false]]);
                    $mailer = new \Swift_Mailer($transport);
                    $mailer->getTransport()->start();
                } catch (\Swift_TransportException $e) {
                    $rules['smtp_valid'] = 'required';
                    $messages['required'] = $e->getMessage();
                } catch (\Exception $e) {
                    $rules['smtp_valid'] = 'required';
                    $messages['required'] = $e->getMessage();
                }
                $this->validate($request, $rules, $messages);
            }

            // update env
            foreach ($env as $key => $value) {
                if (empty($value)) {
                    $value = 'null';
                }
                if ($key == 'MAIL_PASSWORD') {
                    Setting::setEnv($key, quoteDotEnvValue($value));
                } else {
                    Setting::setEnv($key, $value);
                }
            }

            // update settings table
            Setting::set('mailer.mailer', $env['MAIL_MAILER']);
            Setting::set('mailer.host', $env['MAIL_HOST']);
            Setting::set('mailer.port', $env['MAIL_PORT']);
            Setting::set('mailer.encryption', $env['MAIL_ENCRYPTION']);
            Setting::set('mailer.username', $env['MAIL_USERNAME']);
            Setting::set('mailer.password', $env['MAIL_PASSWORD']);
            Setting::set('mailer.from.name', $env['MAIL_FROM_NAME']);
            Setting::set('mailer.from.address', $env['MAIL_FROM_ADDRESS']);
            Setting::set('mailer.sendmail_path', $env['sendmail_path']);

            // Redirect to my lists page
            $next = action([\App\Http\Controllers\Admin\SettingController::class, 'mailer']);
            $request->session()->flash('alert-success', trans('messages.setting.updated'));

            return redirect()->away($next);
        }

        return view('admin.settings.mailer', [
            'env_rules' => $env_rules['smtp'],
            'env' => $env,
        ]);
    }

    /**
     * Mailer settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function mailerTest(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_general') != 'yes') {
            return $this->notAuthorized();
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            // Prenvent save from demo mod
            if ($this->isDemoMode()) {
                return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
            }

            // build the message
            $message = new ExtendedSwiftMessage();
            $message->setEncoder(new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'));
            $message->setContentType('text/html; charset=utf-8');

            $message->setSubject($request->input('subject'));
            $message->setTo($request->input('to_email'));
            $message->addPart($request->input('content'), 'text/html');

            $mailer = App::make('xmailer');
            $result = $mailer->sendWithDefaultFromAddress($message);

            $statusCode = $result['status'] == 'sent' ? 200 : 400;

            return response()->json($result, $statusCode);
        }

        return view('admin.settings.mailerTest');
    }

    /**
     * Update all urls.
     */
    public function updateUrls(Request $request): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        // capture the current url, write to .env
        reset_app_url(true); // force update

        // @todo cannot use the artisan_config_cache() helper, cannot even call Artisan::call('config:cache') directly
        // \Artisan::call('config:cache');
        if (file_exists(base_path('bootstrap/cache/config.php'))) {
            unlink(base_path('bootstrap/cache/config.php'));
        }

        if ($request->user()->admin->getPermission('setting_system_urls') != 'yes') {
            return $this->notAuthorized();
        }

        Setting::set('url_unsubscribe', action([\App\Http\Controllers\CampaignController::class, 'unsubscribe'], ['message_id' => 'MESSAGE_ID', 'subscriber' => 'SUBSCRIBER']));
        Setting::set('url_open_track', action([\App\Http\Controllers\CampaignController::class, 'open'], ['message_id' => 'MESSAGE_ID']));
        Setting::set('url_click_track', action([\App\Http\Controllers\CampaignController::class, 'click'], ['message_id' => 'MESSAGE_ID', 'url' => 'URL']));
        Setting::set('url_delivery_handler', action([\App\Http\Controllers\DeliveryController::class, 'notify'], ['stype' => '']));
        Setting::set(
            'url_update_profile',
            action([\App\Http\Controllers\PageController::class, 'profileUpdateForm'], [
                'list_uid' => 'LIST_UID',
                'uid' => 'SUBSCRIBER_UID',
                'code' => 'SECURE_CODE', ])
        );
        Setting::set('url_web_view', action([\App\Http\Controllers\CampaignController::class, 'webView'], ['message_id' => 'MESSAGE_ID']));

        // Redirect to my lists page
        $request->session()->flash('alert-success', trans('messages.setting.updated'));

        return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'urls']);
    }

    /**
     * View system logs.
     */
    public function logs(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $path = base_path('artisan');
        $lines = 300;

        $error_logs = '';
        $file = file($path);
        for ($i = max(0, count($file) - $lines); $i < count($file); $i++) {
            $error_logs .= $file[$i];
        }

        return view('admin.settings.logs', [
            'error_logs' => $error_logs,
        ]);
    }

    /**
     * View system logs.
     */
    public function download_log(Request $request): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $path = storage_path('logs/'.$request->file);

        return response()->download($path);
    }

    /**
     * Advanced settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function advanced(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_general') != 'yes') {
            return $this->notAuthorized();
        }

        // Setting::updateAll();
        $settings = Setting::getAll();
        if (null !== $request->old()) {
            foreach ($request->old() as $name => $value) {
                if (isset($settings[$name])) {
                    $settings[$name]['value'] = $value;
                }
            }
        }

        // validate and save posted data
        if ($request->isMethod('post')) {
            // Prenvent save from demo mod
            if ($this->isDemoMode()) {
                return view('somethingWentWrong', ['message' => trans('messages.operation_not_allowed_in_demo')]);
            }

            $rules = [];
            $this->validate($request, $rules);

            // Save settings
            foreach ($request->all() as $name => $value) {
                if ($name != '_token' && isset($settings[$name])) {
                    // Upload and save image
                    if ($name == 'site_logo_small' || $name == 'site_logo_big') {
                        if ($request->hasFile($name) && $request->file($name)->isValid()) {
                            Setting::uploadSiteLogo($request->file($name), $name);
                        }
                    } else {
                        if ($settings[$name]['cat'] == 'advanced' && $request->user()->admin->getPermission('setting_general') == 'yes') {
                            Setting::set($name, $value);
                        }
                    }
                }
            }

            // Redirect to my lists page
            $request->session()->flash('alert-success', trans('messages.setting.updated'));

            return redirect()->action([\App\Http\Controllers\Admin\SettingController::class, 'advanced']);
        }

        return view('admin.settings.advanced', [
            'settings' => $settings,
        ]);
    }

    /**
     * Payment gateway settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        if ($request->user()->admin->getPermission('setting_general') != 'yes') {
            return $this->notAuthorized();
        }

        // Setting::updateAll();
        $settings = Setting::getAll();

        if ($this->isDemoMode()) {
            return $this->notAuthorized();
        }

        $rules = [
            'end_period_last_days' => 'required',
            'renew_free_plan' => 'required',
            'recurring_charge_before_days' => 'required',
        ];
        $this->validate($request, $rules);

        // Save settings
        Setting::set('end_period_last_days', $request->end_period_last_days);
        Setting::set('renew_free_plan', $request->renew_free_plan);
        Setting::set('recurring_charge_before_days', $request->recurring_charge_before_days);

        // Redirect to my lists page
        $request->session()->flash('alert-success', trans('messages.setting.updated'));

        return redirect()->action([\App\Http\Controllers\Admin\PaymentController::class, 'index']);
    }
}
