<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeedbackLogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($request->user()->admin->getPermission('report_feedback_log') == 'no') {
            return $this->notAuthorized();
        }

        $items = \App\Model\FeedbackLog::getAll();

        return view('admin.feedback_logs.index', [
            'items' => $items,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function listing(Request $request): \Illuminate\Http\Response|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($request->user()->admin->getPermission('report_feedback_log') == 'no') {
            return $this->notAuthorized();
        }

        $items = \App\Model\FeedbackLog::search($request)->paginate($request->per_page);

        return view('admin.feedback_logs._list', [
            'items' => $items,
        ]);
    }
}
