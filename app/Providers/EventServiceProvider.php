<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
         \App\Events\CampaignUpdated::class => [
             \App\Listeners\CampaignUpdatedListener::class,
        ],
         \App\Events\MailListUpdated::class => [
             \App\Listeners\MailListUpdatedListener::class,
        ],
         \App\Events\UserUpdated::class => [
             \App\Listeners\UserUpdatedListener::class,
        ],
         \App\Events\CronJobExecuted::class => [
             \App\Listeners\CronJobExecutedListener::class,
        ],
         \App\Events\AdminLoggedIn::class => [
             \App\Listeners\AdminLoggedInListener::class,
        ],
         \App\Events\MailListSubscription::class => [
            /* Use subscriber instead */
            // 'Acelle\Listeners\SendListNotificationToOwner',
            // 'Acelle\Listeners\SendListNotificationToSubscriber',
            // 'Acelle\Listeners\TriggerAutomation',
        ],
         \App\Events\MailListUnsubscription::class => [
            /* Use subscriber instead */
            // 'Acelle\Listeners\SendListNotificationToOwner',
            // 'Acelle\Listeners\SendListNotificationToSubscriber',
            // 'Acelle\Listeners\TriggerAutomation',
        ],
         \App\Events\MailListImported::class => [
             \App\Listeners\TriggerAutomationForImportedContacts::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
         \App\Listeners\TriggerAutomation::class,
         \App\Listeners\SendListNotificationToOwner::class,
         \App\Listeners\SendListNotificationToSubscriber::class,
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        parent::boot();
    }
}
