<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Acelle\Library\BillingManager;
use App\Library\Facades\Hook;
use App\Library\HookManager;
use App\Model\Notification;
use App\Model\Plugin;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Teak default settings (PHP, Laravel, etc.)
        $this->changeDefaultSettings();

        // Add custom validation rules
        // @deprecated
        $this->addCustomValidationRules();

        // Just finish if the application is not set up
        if (! isInitiated()) {
            return;
        }

        // Load application's plugins
        // Disabled plugin may also register hooks
        $this->loadPlugins();
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(HookManager::class, fn($app) => new HookManager());

        $this->app->singleton(BillingManager::class, fn($app) => new BillingManager());

        Hook::register('add_translation_file', fn() => [
            'id' => 'acelle_messages',
            'plugin_name' => 'Acelle/Core',
            'file_title' => 'Messages',
            'translation_folder' => base_path('resources/lang'),
            'file_name' => 'messages.php',
            'default' => 'default',
            'type' => 'default',
        ]);

        Hook::register('add_translation_file', fn() => [
            'id' => 'acelle_auth',
            'plugin_name' => 'Acelle/Core',
            'file_title' => 'Auth',
            'translation_folder' => base_path('resources/lang'),
            'file_name' => 'auth.php',
            'default' => 'default',
            'type' => 'default',
        ]);

        Hook::register('add_translation_file', fn() => [
            'id' => 'acelle_pagination',
            'plugin_name' => 'Acelle/Core',
            'file_title' => 'Pagination',
            'translation_folder' => base_path('resources/lang'),
            'file_name' => 'pagination.php',
            'default' => 'default',
            'type' => 'default',
        ]);

        Hook::register('add_translation_file', fn() => [
            'id' => 'acelle_passwords',
            'plugin_name' => 'Acelle/Core',
            'file_title' => 'Passwords',
            'translation_folder' => base_path('resources/lang'),
            'file_name' => 'passwords.php',
            'default' => 'default',
            'type' => 'default',
        ]);

        Hook::register('add_translation_file', fn() => [
            'id' => 'acelle_builder',
            'plugin_name' => 'Acelle/Core',
            'file_title' => 'Builder',
            'translation_folder' => base_path('resources/lang'),
            'file_name' => 'builder.php',
            'default' => 'default',
            'type' => 'default',
        ]);

        Hook::register('add_translation_file', fn() => [
            'id' => 'acelle_validation',
            'plugin_name' => 'Acelle/Core',
            'file_title' => 'Validation',
            'translation_folder' => base_path('resources/lang'),
            'file_name' => 'validation.php',
            'default' => 'default',
            'type' => 'default',
        ]);
    }

    private function loadPlugins(): void
    {
        try {
            Plugin::autoload();
            Notification::cleanupDuplicateNotifications('Plugin Error');
        } catch (\Exception $ex) {
            // Just in case
            Notification::warning([
                'message' => 'Cannot load Acelle plugins. Error: '.htmlspecialchars($ex->getMessage()),
                'title' => 'Plugin Error',
            ]);
        }
    }

    // @deprecated
    private function addCustomValidationRules(): void
    {
        // extend substring validator
        Validator::extend('substring', function ($attribute, $value, $parameters, $validator) {
            $tag = $parameters[0];
            if (!Str::of($value)->contains($tag)) {
                return false;
            }

            return true;
        });
        Validator::replacer('substring', fn($message, $attribute, $rule, $parameters) => str_replace(':tag', $parameters[0], $message));

        // License validator
        Validator::extend('license', fn($attribute, $value, $parameters, $validator) => $value == '' || true);

        // License error validator
        Validator::extend('license_error', fn($attribute, $value, $parameters, $validator) => false);
        Validator::replacer('license_error', fn($message, $attribute, $rule, $parameters) => str_replace(':error', $parameters[0], $message));
    }

    private function changeDefaultSettings(): void
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', 1000000000);

        // Laravel 5.5 to 5.6 compatibility
        Blade::withoutDoubleEncoding();

        // Check if HTTPS (including proxy case)
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') == 0) {
            $isSecure = true;
        } elseif (! empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') == 0 || ! empty($_SERVER['HTTP_X_FORWARDED_SSL']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_SSL'], 'on') == 0) {
            $isSecure = true;
        }

        if ($isSecure) {
            URL::forceScheme('https');
        }

        // HTTP or HTTPS
        // parse_url will return either 'http' or 'https'
        //$scheme = parse_url(config('app.url'), PHP_URL_SCHEME);
        //if (!empty($scheme)) {
        //    URL::forceScheme($scheme);
        //}

        // Fix Laravel 5.4 error
        // [Illuminate\Database\QueryException]
        // SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes
        Schema::defaultStringLength(191);

        if (! \App::runningInConsole()) {
            // This is just a trick for getting Controller name in view
            // See https://stackoverflow.com/questions/29549660/get-laravel-5-controller-name-in-view
            // @todo: fix this anti-pattern
            app('view')->composer('*', function ($view) {
                $route = app('request')->route();
                if (is_null($route)) {
                    return;
                }

                $action = app('request')->route()->getAction();

                if (! array_key_exists('controller', $action)) {
                    return;
                }

                $controller = class_basename($action['controller']);
                [$controller, $action] = explode('@', $controller);
                $view->with(compact('controller', 'action'));
            });
        }
    }
}
