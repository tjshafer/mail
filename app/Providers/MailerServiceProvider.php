<?php

namespace App\Providers;

use App\Model\SendingServer;
use App\Model\SendingServerSendmail;
use App\Model\SendingServerSmtp;
use App\Model\Setting;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class MailerServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        $this->app->bind('xmailer', function ($app) {
            $driver = Setting::get('mailer.driver') ?? config('mail.driver');

            $mailer = match ($driver) {
                SendingServer::TYPE_SMTP => SendingServerSmtp::instantiateFromSettings([
                    'host' => Setting::get('mailer.host') ?? config('mail.host'),
                    'smtp_port' => Setting::get('mailer.port') ?? config('mail.port'),
                    'smtp_protocol' => Setting::get('mailer.encryption') ?? config('mail.encryption'),
                    'smtp_username' => Setting::get('mailer.username') ?? config('mail.username'),
                    'smtp_password' => Setting::get('mailer.password') ?? config('mail.password'),
                    'from_name' => Setting::get('mailer.from.name') ?? config('mail.from.name'),
                    'from_address' => Setting::get('mailer.from.address') ?? config('mail.from.address'),
                ]),
                SendingServer::TYPE_SENDMAIL => SendingServerSendmail::instantiateFromSettings([
                    'sendmail_path' => Setting::get('mailer.sendmail_path') ?? config('mail.sendmail'),
                    'from_name' => Setting::get('mailer.from.name') ?? config('mail.from.name'),
                    'from_address' => Setting::get('mailer.from.address') ?? config('mail.from.address'),
                ]),
                default => throw new \Exception("Mail driver '{$driver}' not found by Acelle", 1),
            };

            return $mailer;
        });
    }

    /**
     * Get the services provided by the provider.
     */
    public function provides(): array
    {
        return ['xmailer'];
    }
}
