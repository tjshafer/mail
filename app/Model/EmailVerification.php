<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailVerification extends Model
{
    const RESULT_DELIVERABLE = 'deliverable';

    const RESULT_UNDELIVERABLE = 'undeliverable';

    const RESULT_UNKNOWN = 'unknown';

    const RESULT_RISKY = 'risky';

    const RESULT_UNVERIFIED = 'unverified';

    /**
     * Check if the email address is deliverable.
     */
    public function isDeliverable(): bool
    {
        return $this->result == self::RESULT_DELIVERABLE;
    }

    /**
     * Check if the email address is undeliverable.
     */
    public function isUndeliverable(): bool
    {
        return $this->result == self::RESULT_UNDELIVERABLE;
    }

    /**
     * Check if the email address is risky.
     */
    public function isRisky(): bool
    {
        return $this->result == self::RESULT_RISKY;
    }

    /**
     * Check if the email address is unknown.
     */
    public function isUnknown(): bool
    {
        return $this->result == self::RESULT_UNKNOWN;
    }

    /**
     * Email verification result types select options.
     */
    public static function resultSelectOptions(): array
    {
        return [
            ['value' => self::RESULT_DELIVERABLE, 'text' => trans('messages.email_verification_result_'.self::RESULT_DELIVERABLE)],
            ['value' => self::RESULT_UNDELIVERABLE, 'text' => trans('messages.email_verification_result_'.self::RESULT_UNDELIVERABLE)],
            ['value' => self::RESULT_UNKNOWN, 'text' => trans('messages.email_verification_result_'.self::RESULT_UNKNOWN)],
            ['value' => self::RESULT_RISKY, 'text' => trans('messages.email_verification_result_'.self::RESULT_RISKY)],
            ['value' => self::RESULT_UNVERIFIED, 'text' => trans('messages.email_verification_result_'.self::RESULT_UNVERIFIED)],
        ];
    }
}
