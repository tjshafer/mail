<?php

/**
 * SendingServer class.
 *
 * An abstract class for different types of sending servers
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use Acelle\Library\ExtendedSwiftMessage;
use App\Library\Facades\Hook;
use Acelle\Library\IdentityStore;
use Acelle\Library\Log as MailLog;
use Acelle\Library\QuotaTrackerFile;
use Acelle\Library\StringHelper;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;

class SendingServer extends Model
{
    const DELIVERY_STATUS_SENT = 'sent';

    const DELIVERY_STATUS_FAILED = 'failed';

    const STATUS_ACTIVE = 'active';

    const STATUS_INACTIVE = 'inactive';

    // TYPE
    const TYPE_AMAZON_API = 'amazon-api';

    const TYPE_AMAZON_SMTP = 'amazon-smtp';

    const TYPE_SENDGRID_API = 'sendgrid-api';

    const TYPE_SENDGRID_SMTP = 'sendgrid-smtp';

    const TYPE_MAILGUN_API = 'mailgun-api';

    const TYPE_MAILGUN_SMTP = 'mailgun-smtp';

    const TYPE_ELASTICEMAIL_API = 'elasticemail-api';

    const TYPE_ELASTICEMAIL_SMTP = 'elasticemail-smtp';

    const TYPE_SPARKPOST_API = 'sparkpost-api';

    const TYPE_SPARKPOST_SMTP = 'sparkpost-smtp';

    const TYPE_SENDMAIL = 'sendmail';

    const TYPE_SMTP = 'smtp';

    protected $quotaTracker;

    protected $subAccount;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     * @note important! consider updating the $fillable variable, it will affect some other methods
     */
    protected $fillable = [
        'name', 'type', 'host', 'aws_access_key_id', 'aws_secret_access_key', 'aws_region', 'domain', 'api_key', 'api_secret_key', 'smtp_username',
        'smtp_password', 'smtp_port', 'smtp_protocol', 'quota_value', 'sendmail_path', 'quota_base', 'quota_unit',
        'bounce_handler_id', 'feedback_loop_handler_id', 'status', 'default_from_email',
    ];

    // Supported server types
    public static $serverMapping = [
        self::TYPE_AMAZON_API => 'SendingServerAmazonApi',
        self::TYPE_AMAZON_SMTP => 'SendingServerAmazonSmtp',
        self::TYPE_SMTP => 'SendingServerSmtp',
        self::TYPE_SENDMAIL => 'SendingServerSendmail',
        self::TYPE_MAILGUN_API => 'SendingServerMailgunApi',
        self::TYPE_MAILGUN_SMTP => 'SendingServerMailgunSmtp',
        self::TYPE_SENDGRID_API => 'SendingServerSendGridApi',
        self::TYPE_SENDGRID_SMTP => 'SendingServerSendGridSmtp',
        self::TYPE_ELASTICEMAIL_API => 'SendingServerElasticEmailApi',
        self::TYPE_ELASTICEMAIL_SMTP => 'SendingServerElasticEmailSmtp',
        self::TYPE_SPARKPOST_API => 'SendingServerSparkPostApi',
        self::TYPE_SPARKPOST_SMTP => 'SendingServerSparkPostSmtp',
    ];

    /**
     * Tracking logs.
     *
     * @return collection
     */
    public function trackingLogs(): \Illuminate\Database\Query\Builder
    {
        return $this->hasMany( \App\Model\TrackingLog::class, 'sending_server_id')->oldest();
    }

    /**
     * Plans.
     *
     * @return collection
     */
    public function plans(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany( \App\Model\Plan::class, 'plans_sending_servers');
    }

    /**
     * Plans.
     *
     * @return collection
     */
    public function plansSendingServers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\PlansSendingServer::class, 'sending_server_id');
    }

    /**
     * Get the bounce handler.
     */
    public function bounceHandler(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\BounceHandler::class);
    }

    public function sendingDomains(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\SendingDomain::class, 'sending_server_id');
    }

    /**
     * Senders.
     *
     * @return collection
     */
    public function senders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Sender::class, 'sending_server_id');
    }

    /**
     * Map a server to its class type and retrive an instance from the database.
     *
     *
     * @param campaign
     * @return mixed
     */
    public static function mapServerType($server)
    {
        if (array_key_exists($server->type, self::$serverMapping)) {
            // Old sending server types
            $class_name = ' \App\Model\\'.self::$serverMapping[$server->type];
        } else {
            $extendedTypes = Hook::execute('register_sending_server');
            foreach ($extendedTypes as $meta) {
                if ($meta['type'] == $server->type) {
                    $class_name = $meta['class'];
                }
            }
        }

        if (! isset($class_name)) {
            throw new Exception('Unknown sending server type '.$server->type);
        }

        if ($server->id) {
            $instance = $class_name::find($server->id);
        } else {
            $instance = new $class_name(['type' => $server->type]);
        }

        $instance->fill($server->getAttributes());

        return $instance;
    }

    /**
     * Get all items.
     *
     * @return collect
     */
    public function getVerp($recipient)
    {
        if (is_object($this->bounceHandler)) {
            $validator = \Validator::make(
                ['email' => $this->bounceHandler->username],
                ['email' => 'required|email']
            );

            if ($validator->passes()) {
                // @todo disable VERP as it is not supported by all mailbox
                // return str_replace('@', '+'.str_replace('@', '=', $recipient).'@', $this->bounceHandler->username);
                return $this->bounceHandler->username;
            } else {
                // @todo raise an error here, hold off the entire campaign
                return $this->bounceHandler->email;
            }
        }

    }

    /**
     * Get all items.
     */
    public static function getAll(): \collect
    {
        return self::where('status', '=', 'active');
    }

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    public function admin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Admin::class);
    }

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $user = $request->user();
        $query = self::select('sending_servers.*');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('sending_servers.name', 'like', '%'.$keyword.'%')
                        ->orWhere('sending_servers.type', 'like', '%'.$keyword.'%')
                        ->orWhere('sending_servers.host', 'like', '%'.$keyword.'%');
                });
            }
        }

        // filters
        $filters = $request->filters;
        if (! empty($filters)) {
            if (! empty($filters['type'])) {
                $query = $query->where('sending_servers.type', '=', $filters['type']);
            }
        }

        // Other filter
        if (! empty($request->customer_id)) {
            $query = $query->where('sending_servers.customer_id', '=', $request->customer_id);
        }

        if (! empty($request->admin_id)) {
            $query = $query->where('sending_servers.admin_id', '=', $request->admin_id);
        }

        // remove customer sending servers
        if (! empty($request->no_customer)) {
            $query = $query->whereNull('customer_id');
        }

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;
        });
    }

    /**
     * Type of server.
     */
    public static function types(): array
    {
        return [
            self::TYPE_AMAZON_SMTP => [
                'cols' => [
                    'host' => 'required',
                    'aws_access_key_id' => 'required',
                    'aws_secret_access_key' => 'required',
                    'aws_region' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_AMAZON_API => [
                'cols' => [
                    'aws_access_key_id' => 'required',
                    'aws_secret_access_key' => 'required',
                    'aws_region' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SENDGRID_SMTP => [
                'cols' => [
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SENDGRID_API => [
                'cols' => [
                    'api_key' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_MAILGUN_API => [
                'cols' => [
                    'api_key' => 'required',
                    'domain' => 'required',
                    'host' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_MAILGUN_SMTP => [
                'cols' => [
                    'domain' => 'required',
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_ELASTICEMAIL_API => [
                'cols' => [
                    'api_key' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_ELASTICEMAIL_SMTP => [
                'cols' => [
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SPARKPOST_API => [
                'cols' => [
                    'host' => 'required',
                    'api_key' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SPARKPOST_SMTP => [
                'cols' => [
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => '',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SMTP => [
                'cols' => [
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => '',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                    'bounce_handler_id' => '',
                    'feedback_loop_handler_id' => '',
                ],
            ],
            self::TYPE_SENDMAIL => [
                'cols' => [
                    'sendmail_path' => 'required',
                ],
                'settings' => [
                    'name' => 'required',
                    'default_from_email' => 'email',
                    'bounce_handler_id' => '',
                    'feedback_loop_handler_id' => '',
                ],
            ],
        ];
    }

    /**
     * Type of server.
     */
    public static function frontendTypes(): array
    {
        return [
            self::TYPE_AMAZON_SMTP => [
                'cols' => [
                    'name' => 'required',
                    'host' => 'required',
                    'aws_access_key_id' => 'required',
                    'aws_secret_access_key' => 'required',
                    'aws_region' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_AMAZON_API => [
                'cols' => [
                    'name' => 'required',
                    'aws_access_key_id' => 'required',
                    'aws_secret_access_key' => 'required',
                    'aws_region' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SENDGRID_SMTP => [
                'cols' => [
                    'name' => 'required',
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SENDGRID_API => [
                'cols' => [
                    'name' => 'required',
                    'api_key' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_MAILGUN_API => [
                'cols' => [
                    'name' => 'required',
                    'api_key' => 'required',
                    'domain' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_MAILGUN_SMTP => [
                'cols' => [
                    'name' => 'required',
                    'domain' => 'required',
                    'api_key' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_ELASTICEMAIL_API => [
                'cols' => [
                    'name' => 'required',
                    'api_key' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_ELASTICEMAIL_SMTP => [
                'cols' => [
                    'name' => 'required',
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SPARKPOST_API => [
                'cols' => [
                    'name' => 'required',
                    'host' => 'required',
                    'api_key' => 'required',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SPARKPOST_SMTP => [
                'cols' => [
                    'name' => 'required',
                    'api_key' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => '',
                    'default_from_email' => 'email',
                ],
            ],
            self::TYPE_SMTP => [
                'cols' => [
                    'name' => 'required',
                    'host' => 'required',
                    'smtp_username' => 'required',
                    'smtp_password' => 'required',
                    'smtp_port' => 'required',
                    'smtp_protocol' => '',
                    'default_from_email' => 'email',
                    'bounce_handler_id' => '',
                    'feedback_loop_handler_id' => '',
                ],
            ],
            self::TYPE_SENDMAIL => [
                'cols' => [
                    'name' => 'required',
                    'sendmail_path' => 'required',
                    'default_from_email' => 'email',
                    'bounce_handler_id' => '',
                    'feedback_loop_handler_id' => '',
                ],
            ],
        ];
    }

    /**
     * Get select options.
     */
    public static function getSelectOptions(): array
    {
        $query = self::getAll();
        $options = $query->oldest('name')->get()->map(fn($item) => ['value' => $item->uid, 'text' => $item->name]);

        return $options;
    }

    /**
     * Get sparkpost select options.
     */
    public static function getSparkpostHostnameSelectOptions(): array
    {
        $options = [
            ['text' => trans('messages.choose'), 'value' => ''],
            ['text' => 'SparkPost Global', 'value' => 'api.sparkpost.com'],
            ['text' => 'SparkPost EU', 'value' => 'api.eu.sparkpost.com'],
        ];

        return $options;
    }

    /**
     * Get sending server's quota.
     */
    public function getSendingQuota(): string
    {
        return $this->quota_value;
    }

    /**
     * Get sending server's sending quota.
     */
    public function getSendingQuotaUsage(): string
    {
        $tracker = $this->getQuotaTracker();

        return $tracker->getUsage();
    }

    /**
     * Get rules.
     */
    public static function rules($type): string
    {
        $rules = self::types()[$type]['cols'];
        $rules['quota_value'] = 'required|numeric';
        $rules['quota_base'] = 'required|numeric';
        $rules['quota_unit'] = 'required';

        return $rules;
    }

    /**
     * Get rules.
     */
    public static function frontendRules($type): string
    {
        $rules = self::frontendTypes()[$type]['cols'];
        $rules['quota_value'] = 'required|numeric';
        $rules['quota_base'] = 'required|numeric';
        $rules['quota_unit'] = 'required';

        return $rules;
    }

    /**
     * Get rules.
     */
    public function getFrontendRules(): string
    {
        $rules = self::frontendTypes()[$this->type]['cols'];

        return $rules;
    }

    /**
     * Get rules.
     */
    public function getRules(): string
    {
        $rules = self::types()[$this->type]['cols'];

        return $rules;
    }

    public function getCustomValidationError(): array
    {
        return [];
    }

    /**
     * Test connection.
     */
    public function validConnection($request): object
    {
        $validator = \Validator::make($request->all(), $this->getRules(), $this->getCustomValidationError());

        // test amazon api connection
        $validator->after(function ($validator) {
            try {
                $this->test();
            } catch (\Exception $e) {
                $validator->errors()->add('connection', $e->getMessage());
            }
        });

        return $validator;
    }

    /**
     * Get configuration rules.
     */
    public function getConfigRules(): string
    {
        $rules = self::types()[$this->type]['settings'];

        return $rules;
    }

    /**
     * Quota display.
     *
     * @return string
     */
    public function displayQuota()
    {
        if ($this->quota_value == -1) {
            return trans('messages.unlimited');
        }

        return $this->quota_value.'/'.$this->quota_base.' '.trans('messages.'. \App\Library\Tool::getPluralPrase($this->quota_unit, $this->quota_base));
    }

    /**
     * Quota display.
     *
     * @return string
     */
    public function displayQuotaHtml()
    {
        if ($this->quota_value == -1) {
            return trans('messages.unlimited');
        }

        return '<b>'.$this->quota_value.'</b>/<b>'.$this->quota_base.' '.trans('messages.'. \App\Library\Tool::getPluralPrase($this->quota_unit, $this->quota_base)).'</b>';
    }

    /**
     * Select options for aws region.
     */
    public static function awsRegionSelectOptions(): array
    {
        return [
            ['value' => '', 'text' => trans('messages.choose')],
            ['value' => 'us-east-1', 'text' => 'US East (N. Virginia)', 'host' => 'email-smtp.us-east-1.amazonaws.com'],
            ['value' => 'us-east-2', 'text' => 'US East (Ohio)', 'host' => 'email-smtp.us-east-2.amazonaws.com'],
            ['value' => 'us-west-1', 'text' => 'US West (N. California)', 'host' => 'email-smtp.us-west-1.amazonaws.com'],
            ['value' => 'us-west-2', 'text' => 'US West (Oregon)', 'host' => 'email-smtp.us-west-2.amazonaws.com'],
            ['value' => 'ap-south-1', 'text' => 'Asia Pacific (Mumbai)', 'host' => 'email-smtp.ap-south-1.amazonaws.com'],
            ['value' => 'ap-southeast-1', 'text' => 'Asia Pacific (Singapore)', 'host' => 'email-smtp.ap-southeast-1.amazonaws.com'],
            ['value' => 'ap-southeast-2', 'text' => 'Asia Pacific (Sydney)', 'host' => 'email-smtp.ap-southeast-2.amazonaws.com'],
            ['value' => 'ap-northeast-1', 'text' => 'Asia Pacific (Tokyo)', 'host' => 'email-smtp.ap-northeast-1.amazonaws.com'],
            ['value' => 'ap-northeast-2', 'text' => 'Asia Pacific (Seoul)', 'host' => 'email-smtp.ap-northeast-2.amazonaws.com'],
            ['value' => 'ca-central-1', 'text' => 'Canada (Central)', 'host' => 'email-smtp.ca-central-1.amazonaws.com'],
            ['value' => 'eu-central-1', 'text' => 'Europe (Frankfurt)', 'host' => 'email-smtp.eu-central-1.amazonaws.com'],
            ['value' => 'eu-west-1', 'text' => 'EU (Ireland)', 'host' => 'email-smtp.eu-west-1.amazonaws.com'],
            ['value' => 'eu-west-2', 'text' => 'Europe (London)', 'host' => 'email-smtp.eu-west-2.amazonaws.com'],
            ['value' => 'eu-west-3', 'text' => 'Europe (Paris)', 'host' => 'email-smtp.eu-west-3.amazonaws.com'],
            ['value' => 'eu-north-1', 'text' => 'Europe (Stockholm)', 'host' => 'email-smtp.eu-north-1.amazonaws.com'],
            ['value' => 'me-south-1', 'text' => 'Middle East (Bahrain)', 'host' => 'email-smtp.me-south-1.amazonaws.com'],
            ['value' => 'sa-east-1', 'text' => 'South America (São Paulo)', 'host' => 'email-smtp.sa-east-1.amazonaws.com'],
        ];
    }

    /**
     * Select options for aws region.
     */
    public static function mailgunRegionSelectOptions(): array
    {
        return [
            ['value' => '', 'text' => trans('messages.choose')],
            ['value' => 'https://api.mailgun.net', 'text' => 'US/Global Server'],
            ['value' => 'https://api.eu.mailgun.net', 'text' => 'EU Server'],
        ];
    }

    /**
     * Disable sending server.
     */
    public function disable(): void
    {
        $this->status = 'inactive';
        $this->save();
    }

    /**
     * Enable sending server.
     */
    public function enable(): void
    {
        $this->status = 'active';
        $this->save();
    }

    /**
     * Get sending server's QuotaTracker.
     */
    public function getQuotaTracker(): ? \App\Library\QuotaTrackerFile
    {
        if (! $this->quotaTracker) {
            $this->initQuotaTracker();
        }

        return $this->quotaTracker;
    }

    /**
     * Initialize the quota tracker.
     */
    public function initQuotaTracker(): void
    {
        $this->quotaTracker = new QuotaTrackerFile($this->getSendingQuotaLockFile(), ['start' => $this->created_at->timestamp, 'max' => -1], [$this->getQuotaIntervalString() => $this->getSendingQuota()]);
        $this->quotaTracker->cleanupSeries();
        // @note: in case of multi-process, the following command must be issued manually
        //     $this->renewQuotaTracker();
    }

    /**
     * Clean up the quota tracking files to prevent it from growing too large.
     */
    public function cleanupQuotaTracker(): void
    {
        // @todo: hard-coded for 1 month
        $this->getQuotaTracker()->cleanupSeries(null, '1 month');
    }

    /**
     * Get sending quota lock file.
     *
     * @return string file path
     */
    public function getSendingQuotaLockFile(): string
    {
        return storage_path("app/server/quota/{$this->uid}");
    }

    /**
     * Get quota starting time.
     */
    public function getQuotaIntervalString(): string
    {
        return "{$this->quota_base} {$this->quota_unit}";
    }

    /**
     * Get quota starting time.
     */
    public function getQuotaStartingTime(): array
    {
        return "{$this->getQuotaIntervalString()} ago";
    }

    /**
     * Increment quota usage.
     */
    public function countUsage(Carbon $timePoint = null)
    {
        return $this->getQuotaTracker($timePoint)->add();
    }

    /**
     * Check if user has used up all quota allocated.
     */
    public function overQuota(): bool
    {
        return ! $this->getQuotaTracker()->check();
    }

    /**
     * Check if sending server supports custom ReturnPath header (used for bounced/feedback handling).
     */
    public function allowCustomReturnPath(): bool
    {
        return  $this->type == 'smtp' || $this->type == 'sendmail' || $this->type == 'php-mail';
    }

    /**
     * Get all active items.
     */
    public static function scopeActive($query): \collect
    {
        return $query->where('status', '=', self::STATUS_ACTIVE);
    }

    /**
     * Get all active system items.
     */
    public static function scopeSystem($query): \collect
    {
        return $query->active()->whereNull('customer_id');
    }

    /**
     * Add customer action log.
     */
    public function log($name, $customer, $add_datas = []): void
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        $data = [...$data, ...$add_datas];

        Log::create([
            'customer_id' => $customer->id,
            'type' => 'sending_server',
            'name' => $name,
            'data' => json_encode($data),
        ]);
    }

    /**
     * Send a test email for the sending server.
     */
    public function sendTestEmail($params): bool
    {
        /*
         * Required keys include
         *     + from_email
         *     + to_email
         *     + subject
         *     + plain
         */
        MailLog::info(sprintf('Sending test email to %s for sending server `%s`', $params['to_email'], $this->name));
        $message = new ExtendedSwiftMessage();
        $msgId = StringHelper::generateMessageId(StringHelper::getDomainFromEmail($params['from_email']));
        $message->setId($msgId);
        $message->getHeaders()->addTextHeader('X-Acelle-Message-Id', $msgId); // this header is required for SendGrid API sending server
        $message->setContentType('text/plain; charset=utf-8');
        $message->setSubject($params['subject']);
        $message->setFrom($params['from_email']);
        $message->setTo($params['to_email']);
        $message->setReplyTo($params['from_email']);
        // $message->setEncoder(\Swift_Encoding::get8bitEncoding());
        $message->setEncoder(new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'));
        $message->addPart($params['plain'], 'text/plain');
        $result = $this->send($message);

        if (array_key_exists('error', $result)) {
            throw new \Exception($result['error']);
        }

        return true;
    }

    /**
     * Check if the sending server is ElasticEmailAPI or ElasticEmailSmtp.
     */
    public function isElasticEmailServer(): bool
    {
        return $this->type == 'elasticemail-api' || $this->type == 'elasticemail-smtp';
    }

    /**
     * Get all sub-account supported sending server types.
     */
    public static function getSubAccountTypes(): array
    {
        return [
            'sendgrid-api',
            'sendgrid-smtp',
        ];
    }

    public function setSubAccount($subAccount): void
    {
        $this->subAccount = $subAccount;
    }

    /**
     * Get sending server select2 select options.
     */
    public static function select2($request): string|bool
    {
        $data = ['items' => [], 'more' => true];

        $query = self::getAll();
        if (isset($request->q)) {
            $keyword = $request->q;
            $query = $query->where(function ($q) use ($keyword) {
                $q->orwhere('sending_servers.name', 'like', '%'.$keyword.'%');
            });
        }

        // plan
        if ($request->plan_uid) {
            $plan = \App\Model\Plan::findByUid($request->plan_uid);
            $existIds = $plan->plansSendingServers()->pluck('sending_server_id')->toArray();
        }

        foreach ($query->limit(20)->get() as $server) {
            if ($request->plan_uid && in_array($server->id, $existIds)) {
                $data['items'][] = [
                    'id' => $server->uid,
                    'text' => $server->name.' ('.trans('messages.sending_server.added').')'.'|||'.trans('messages.'.$server->type),
                    'disabled' => true,
                ];
            } else {
                $data['items'][] = ['id' => $server->uid, 'text' => $server->name.'|||'.trans('messages.'.$server->type)];
            }
        }

        return json_encode($data);
    }

    /**
     * Get sending server select2 select options.
     */
    public static function adminSelect2($request): string|bool
    {
        $data = ['items' => [], 'more' => true];

        $query = self::getAll()->whereNull('customer_id');
        if (isset($request->q)) {
            $keyword = $request->q;
            $query = $query->where(function ($q) use ($keyword) {
                $q->orwhere('sending_servers.name', 'like', '%'.$keyword.'%');
            });
        }

        // plan
        if ($request->plan_uid) {
            $plan = \App\Model\Plan::findByUid($request->plan_uid);
            $existIds = $plan->plansSendingServers()->pluck('sending_server_id')->toArray();
        }

        foreach ($query->limit(20)->get() as $server) {
            if ($request->plan_uid && in_array($server->id, $existIds)) {
                $data['items'][] = [
                    'id' => $server->uid,
                    'text' => $server->name.' ('.trans('messages.sending_server.added').')'.'|||'.trans('messages.'.$server->type),
                    'disabled' => true,
                ];
            } else {
                $type = ($server->mapType()->isExtended()) ? $server->mapType()->getTypeName() : trans('messages.'.$server->type);
                $data['items'][] = ['id' => $server->uid, 'text' => $server->name.'|||'.$type];
            }
        }

        return json_encode($data);
    }

    /**
     * Get sending limit types.
     */
    public static function sendingLimitValues(): array
    {
        return [
            'unlimited' => [
                'quota_value' => -1,
                'quota_base' => -1,
                'quota_unit' => 'day',
            ],
            '100_per_minute' => [
                'quota_value' => 100,
                'quota_base' => 1,
                'quota_unit' => 'minute',
            ],
            '1000_per_hour' => [
                'quota_value' => 1000,
                'quota_base' => 1,
                'quota_unit' => 'hour',
            ],
            '10000_per_day' => [
                'quota_value' => 10000,
                'quota_base' => 1,
                'quota_unit' => 'day',
            ],
        ];
    }

    /**
     * Get sending limit select options.
     */
    public function getSendingLimitSelectOptions(): array
    {
        $options = [];
        $current = trans('messages.sending_servers.sending_limit.phrase', [
            'quota_value' =>  \App\Library\Tool::format_number($this->quota_value),
            'quota_base' =>  \App\Library\Tool::format_number($this->quota_base),
            'quota_unit' => $this->quota_unit,
        ]);
        if ($this->quota_value == -1) {
            $current = trans('messages.sending_server.quota.unlimited');
        }

        $exist = false;
        foreach (self::sendingLimitValues() as $key => $data) {
            $wording = trans('messages.sending_servers.sending_limit.phrase', [
                'quota_value' =>  \App\Library\Tool::format_number($data['quota_value']),
                'quota_base' =>  \App\Library\Tool::format_number($data['quota_base']),
                'quota_unit' => $data['quota_unit'],
            ]);

            if ($data['quota_value'] == -1) {
                $wording = trans('messages.sending_server.quota.unlimited');
            }

            $options[] = ['text' => $wording, 'value' => $key];

            if ($wording == $current) {
                $exist = true;
                $this->setOption('sending_limit', $key);
            }
        }

        // exist
        if (! $exist) {
            $options[] = ['text' => $current, 'value' => 'current'];
            $this->setOption('sending_limit', 'current');
        }

        // Custom
        $options[] = ['text' => trans('messages.sending_servers.quota.custom'), 'value' => 'custom'];

        return $options;
    }

    /**
     * Default options.
     */
    public static function defaultOptions(): array
    {
        return [
            'domains' => [],
            'emails' => [],
            'allow_unverified_from_email' => 'no',
            'allow_verify_domain_remotely' => 'no',
            'allow_verify_email_remotely' => 'no',
            'allow_verify_domain_remotely' => 'no',
            'allow_verify_email_remotely' => 'no',
        ];
    }

    /**
     * Get options.
     */
    public function getOptions(): array
    {
        $savedOptions = isset($this->options) ? json_decode($this->options, true) : [];

        return [...self::defaultOptions(), ...$savedOptions];
    }

    /**
     * Get option.
     */
    public function getOption($name): array
    {
        $options = $this->getOptions();

        $value = $options[$name] ?? null;

        // default value
        if (! $value) {
            // default verification email
            if ($name == 'custom_verification_email') {
                $value = trans('messages.sending_server.default_email_verification.content');
            }

            // default verification email
            if ($name == 'custom_verification_email_subject') {
                $value = trans('messages.sending_server.default_email_verification.subject');
            }
        }

        return $value;
    }

    /**
     * Get options.
     */
    public function setOptions($options): void
    {
        $savingOptions = $this->getOptions();
        foreach ($options as $key => $option) {
            $savingOptions[$key] = $option;
        }

        $this->options = json_encode($savingOptions);
    }

    /**
     * Get options.
     */
    public function setOption($name, $value): void
    {
        if (! isset($this->options)) {
            $options = [];
        } else {
            $options = json_decode($this->options, true);
        }

        $options[$name] = $value;

        $this->options = json_encode($options);
    }

    /**
     * Get Mailgun domains info.
     */
    public function getMailgunDomainInfo(): array
    {
        return [
            [
                'domain' => 'acellemail.com',
                'created_at' => \Illuminate\Support\Carbon::now()->subDay(2),
            ],
            [
                'domain' => 'bolero.vn',
                'created_at' => \Illuminate\Support\Carbon::now()->subDay(13),
            ],
        ];
    }

    /**
     * Get local identity info.
     */
    public function getLocalIdentityInfo(): array
    {
        return [
            [
                'type' => 'domain',
                'name' => 'acellemail.com',
                'created_at' => \Illuminate\Support\Carbon::now()->subDay(2),
            ],
            [
                'type' => 'domain',
                'name' => 'bolero.vn',
                'created_at' => \Illuminate\Support\Carbon::now()->subDay(13),
            ],
            [
                'type' => 'email',
                'name' => 'system@acellemail.com',
                'created_at' => \Illuminate\Support\Carbon::now()->subDay(13),
            ],
        ];
    }

    /**
     * Add domain.
     */
    public function addIdentity($domain): void
    {
        $identityStore = $this->getIdentityStore();
        $identityStore->add([$domain => ['VerificationStatus' => true]]);
        $this->setOption('identities', $identityStore->get());
        $this->save();
    }

    /**
     * Add domain.
     */
    public function removeIdentity($identity): void
    {
        $identityStore = $this->getIdentityStore();
        $identityStore->remove($identity);
        $this->setOption('identities', $identityStore->get());
        $this->save();
    }

    /**
     * Check if domain is enabled.
     */
    public function isDomainEnabled($domain): bool
    {
        $domains = $this->getDomains();

        return in_array($domain, $domains);
    }

    /**
     * Check if emails is enabled.
     */
    public function isEmailEnabled($email): bool
    {
        $emails = $this->getEmails();

        return in_array($email, $emails);
    }

    /**
     * Check if domain is enabled.
     */
    public function isIdentityEnabled($type, $value): bool
    {
        $values = $this->getOption($type.'s');

        return in_array($values, $value);
    }

    /**
     * Allow user to verify his/her own sending domain against Acelle Mail.
     */
    public function allowVerifyingOwnDomains(): bool
    {
        $options = json_decode($this->options, true);

        if (is_null($options)) {
            return false;
        }

        return array_key_exists('allow_verify_domain_against_acelle', $options) && $options['allow_verify_domain_against_acelle'] == 'yes';
    }

    /**
     * Allow user to verify his/her own sending domain against Acelle Mail.
     */
    public function allowVerifyingOwnEmails(): bool
    {
        $options = json_decode($this->options, true);

        if (is_null($options)) {
            return false;
        }

        return array_key_exists('allow_verify_email_against_acelle', $options) && $options['allow_verify_email_against_acelle'] == 'yes';
    }

    /**
     * Allow user to verify his/her own emails against AWS.
     */
    public function allowVerifyingOwnDomainsRemotely(): bool
    {
        $options = json_decode($this->options, true);

        if (is_null($options)) {
            return false;
        }

        return array_key_exists('allow_verify_domain_remotely', $options) && $options['allow_verify_domain_remotely'] == 'yes';
    }

    /**
     * Allow user to verify his/her own emails against AWS.
     */
    public function allowVerifyingOwnEmailsRemotely(): bool
    {
        $options = json_decode($this->options, true);

        if (is_null($options)) {
            return false;
        }

        return array_key_exists('allow_verify_email_remotely', $options) && $options['allow_verify_email_remotely'] == 'yes';
    }

    /**
     * Allow user send from unverified FROM email address.
     */
    public function allowUnverifiedFromEmailAddress(): bool
    {
        $options = json_decode($this->options, true);

        if (is_null($options)) {
            return false;
        }

        return array_key_exists('allow_unverified_from_email', $options) && $options['allow_unverified_from_email'] == 'yes';
    }

    /**
     * Check the sending server settings, make sure it does work.
     */
    public function test(): bool
    {
        return true;
    }

    /**
     * Get all verified identities.
     */
    public function verifiedIdentitiesDroplist($keyword = null): array
    {
        $droplist = [];
        $topList = [];
        $bottomList = [];

        if (! $keyword) {
            $keyword = '###';
        }

        foreach ($this->getVerifiedIdentities() as $item) {
            // check if email
            if (extract_email($item) !== null) {
                $email = extract_email($item);
                if (str_starts_with(strtolower($email), $keyword)) {
                    $topList[] = [
                        'text' => extract_name($item),
                        'value' => $email,
                        'desc' => str_replace($keyword, '<span class="text-semibold text-primary"><strong>'.$keyword.'</strong></span>', $email),
                    ];
                } else {
                    $bottomList[] = [
                        'text' => extract_name($item),
                        'value' => $email,
                        'desc' => $email,
                    ];
                }
            } else { // domains are alse
                $dKey = explode('@', $keyword);
                $dKey = $dKey[1] ?? null;
                // if ( (!isset($dKey) || $dKey == '') || ($dKey && strpos(strtolower($item), $dKey) === 0 )) {
                $topList[] = [
                    'text' => '****@'.str_replace($dKey, '<span class="text-semibold text-primary"><strong>'.$dKey.'</strong></span>', $item),
                    'subfix' => $item,
                    'desc' => null,
                ];
                // }
            }
        }

        $droplist = [...$topList, ...$bottomList];

        return $droplist;
    }

    /**
     * Delete sending server.
     */
    public function doDelete(): void
    {
        $plans = $this->plans;

        // delete
        $this->delete();

        // check all plans status
        foreach ($plans as $plan) {
            $plan->checkStatus();
        }
    }

    public function updateIdentitiesList($selected): void
    {
        // For now, it is for Amazon only
        $options = $this->getOptions();
        if (! array_key_exists('identities', $options)) {
            return;
        }

        $selectedEmails = array_key_exists('emails', $selected) ? $selected['emails'] : [];
        $selectedDomains = array_key_exists('domains', $selected) ? $selected['domains'] : [];
        $identityStore = new IdentityStore($options['identities']);
        $identityStore->select([...$selectedEmails, ...$selectedDomains]);

        $options['identities'] = $identityStore->get();
        $this->setOptions($options);
    }

    public function getVerifiedIdentities(): array
    {
        // by default, only return SELECTED | VERIRIED | NON-PRIVATE identities
        $filtered = $this->getIdentityStore()->get(['Selected' => true, 'UserId' => null, 'VerificationStatus' => 'Success']);

        return array_keys($filtered);
    }

    public function getIdentityStore(): IdentityStore
    {
        $options = $this->getOptions();
        $identityStore = new IdentityStore(array_key_exists('identities', $options) ? $options['identities'] : []);

        return $identityStore;
    }

    public function mapType()
    {
        return self::mapServerType($this);
    }

    /**
     * Check an identity (email or domain) if it is verified against AWS.
     */
    public function verifyIdentity($identity): bool
    {
        $this->syncIdentities();

        return array_key_exists($identity, $this->getIdentityStore()->get(['VerificationStatus' => IdentityStore::VERIFICATION_STATUS_SUCCESS]));
    }

    public function sendWithDefaultFromAddress($message, $params = [])
    {
        if (empty($this->from_name)) {
            $message->setFrom([$this->from_name => $this->from_address]);
        } else {
            $message->setFrom($this->from_address);
        }

        return  $this->send($message, $params);
    }

    public function setDefaultFromEmailAddress(): void
    {
        if (! empty($this->default_from_email)) {
            return;
        }

        $identityStore = $this->getIdentityStore();
        $names = array_keys($identityStore->get(['VerificationStatus' => 'Success']));

        $emails = array_values(array_filter($names, fn($name) => checkEmail($name)));
        $domains = array_values(array_filter($names, fn($name) => ! checkEmail($name)));

        $default = null;

        if (! empty($domains)) {
            $default = 'noreply@'.$domains[0];
        } elseif (! empty($emails)) {
            $default = $emails[0];
        }

        if (! is_null($default)) {
            $this->default_from_email = $default;
            $this->save();
        }
    }

    public function getSpfHost()
    {
        return null;
    }

    public function isExtended(): bool
    {
        return false;
    }

    public function getIconUrl()
    {
        return null;
    }

    public function getDefaultName()
    {
        return null;
    }

    public static function createFromRequest($request): array
    {
        $server = new self();
        $server->fill($request->all());
        $server = $server->mapType();

        // validation
        $validator = $server->validConnection($request); //\Validator::make($request->all(), $server->getRules());

        if ($validator->fails()) {
            return [$validator, $server]; // IMPORTANT, $server instance (not saved) is required by parent controller
        }

        $server->admin_id = $request->user()->admin->id;
        $server->status = self::STATUS_ACTIVE;

        // default name
        if (! $server->name) {
            $server->name = $server->getDefaultName() ?: trans('messages.'.$server->type);
        }

        // default sever quota
        if (! $server->quota_value) {
            $server->quota_value = 1000;
            $server->quota_base = 1;
            $server->quota_unit = 'hour';
            $options = ['sending_limit' => '1000_per_hour'];
            $server->options = json_encode($options);
        }

        // bounce / feedback hanlder nullable
        if (empty($request->bounce_handler_id)) {
            $server->bounce_handler_id = null;
        }
        if (empty($request->feedback_loop_handler_id)) {
            $server->feedback_loop_handler_id = null;
        }

        $server->save();

        return [$validator, $server];
    }

    public function getTypeName()
    {
        return trans('messages.'.$this->type);
    }
}
