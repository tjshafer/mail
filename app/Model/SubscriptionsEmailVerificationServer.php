<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubscriptionsEmailVerificationServer extends Model
{
    public function emailVerificationServer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\EmailVerificationServer::class, 'server_id');
    }
}
