<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    // statuses
    const STATUS_NEW = 'new';               // unpaid

    const STATUS_PAID = 'paid';

    // type
    const TYPE_RENEW_SUBSCRIPTION = 'renew_subscription';

    const TYPE_NEW_SUBSCRIPTION = 'new_subscription';

    const TYPE_CHANGE_PLAN = 'change_plan';

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    public function scopeNew($query): void
    {
        $query->whereIn('status', [
            self::STATUS_NEW,
        ]);
    }

    public function scopeUnpaid($query): void
    {
        $query->whereIn('status', [
            self::STATUS_NEW,
        ]);
    }

    public function scopeChangePlan($query): void
    {
        $query->where('type', self::TYPE_CHANGE_PLAN);
    }

    public function scopeRenew($query): void
    {
        $query->where('type', self::TYPE_RENEW_SUBSCRIPTION);
    }

    public function scopeNewSubscription($query): void
    {
        $query->whereIn('type', [
            self::TYPE_NEW_SUBSCRIPTION,
        ]);
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $item->uid = uniqid();
        });
    }

    /**
     * Invoice currency.
     */
    public function currency(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Currency::class);
    }

    /**
     * Invoice customer.
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    /**
     * Invoice items.
     */
    public function invoiceItems(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\InvoiceItem::class);
    }

    /**
     * Transactions.
     */
    public function transactions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Transaction::class);
    }

    /**
     * Get pending transaction.
     */
    public function getPendingTransaction()
    {
        return $this->transactions()
            ->where('status', \App\Model\Transaction::STATUS_PENDING)
            ->latest()
            ->first();
    }

    /**
     * Last transaction.
     */
    public function lastTransaction()
    {
        return $this->transactions()
            ->latest()
            ->first();
    }

    /**
     * Last transaction is failed.
     */
    public function lastTransactionIsFailed()
    {
        if ($this->lastTransaction()) {
            return $this->lastTransaction()->isFailed();
        } else {
            return false;
        }
    }

    /**
     * Set as pending.
     */
    public function setPending(): void
    {
        $this->status = self::STATUS_PENDING;
        $this->save();
    }

    /**
     * Set as paid.
     */
    public function setPaid(): void
    {
        $this->status = self::STATUS_PAID;
        $this->save();
    }

    public function getTax(): int|float
    {
        $total = 0;

        foreach ($this->invoiceItems as $item) {
            $total += $item->getTax();
        }

        return $total;
    }

    public function total(): int|float
    {
        $total = 0;

        foreach ($this->invoiceItems as $item) {
            $total += $item->total();
        }

        return $total;
    }

    /**
     * formatted Total.
     */
    public function formattedTotal(): string
    {
        return format_price($this->total(), $this->currency->format);
    }

    /**
     * Get metadata.
     *
     * @var object | collect
     */
    public function getMetadata($name = null)
    {
        if (! $this['metadata']) {
            return json_decode('{}', true);
        }

        $data = json_decode($this['metadata'], true);

        if ($name != null) {
            if (isset($data[$name])) {
                return $data[$name];
            } else {
                return null;
            }
        } else {
            return $data;
        }
    }

    /**
     * Get metadata.
     *
     * @var object | collect
     */
    public function updateMetadata($data): void
    {
        $metadata = (object) [...(array) $this->getMetadata(), ...$data];
        $this['metadata'] = json_encode($metadata);

        $this->save();
    }

    // /**
    //  * Get type.
    //  *
    //  * @return void
    //  */
    // public function getType()
    // {
    //     return $this->invoiceItems()->first()->item_type;
    // }
    /**
     * Check new.
     */
    public function isNew(): bool
    {
        return $this->status == self::STATUS_NEW;
    }

    /**
     * set status as new.
     */
    public function setNew(): void
    {
        $this->status = self::STATUS_NEW;
        $this->save();
    }

    /**
     * Approve invoice.
     */
    public function approve(): void
    {
        // for only new invoice
        if (! $this->isNew() || ! $this->getPendingTransaction()) {
            throw new \Exception("Trying to approve an invoice that is not NEW or does not have a pending transaction (Invoice ID: {$this->id}, status: {$this->status}");
        }

        // fulfill invoice
        $this->fulfill();
    }

    /**
     * Reject invoice.
     */
    public function reject($error): void
    {
        // for only new invoice
        if (! $this->isNew() || ! $this->getPendingTransaction()) {
            throw new \Exception("Trying to approve an invoice that is not NEW or does not have a pending transaction (Invoice ID: {$this->id}, status: {$this->status}");
        }

        // fulfill invoice
        $this->payFailed($error);
    }

    /**
     * Pay invoice.
     */
    public function fulfill(): void
    {
        // set status as paid
        $this->setPaid();

        // set transaction as success
        // Important: according to current design, the rule is: one invoice only has one pending transaction
        $this->getPendingTransaction()->setSuccess();

        // invoice after pay actions
        $this->process();
    }

    /**
     * Pay invoice failed.
     */
    public function payFailed($error): void
    {
        $this->getPendingTransaction()->setFailed(trans('messages.payment.cannot_charge', [
            'id' => $this->uid,
            'error' => $error,
            'service' => $this->getPendingTransaction()->method,
        ]));
    }

    /**
     * Process invoice.
     */
    public function process(): void
    {
        $data = $this->getMetadata();
        $subscription = Subscription::findByUid($data['subscription_uid']);

        switch ($this->type) {
            case self::TYPE_NEW_SUBSCRIPTION:
                $subscription->activate();
                break;
            case self::TYPE_RENEW_SUBSCRIPTION:
                $subscription->renew();
                break;
            case self::TYPE_CHANGE_PLAN:
                $newPlan = \App\Model\Plan::findByUid($data['new_plan_uid']);
                $subscription->changePlan($newPlan);
                break;
            default:
                throw new \Exception('Invoice type is not valid: '.$this->type);
        }
    }

    /**
     * Check paid.
     */
    public function isPaid(): bool
    {
        return $this->status == self::STATUS_PAID;
    }

    /**
     * Check done.
     */
    public function isDone(): bool
    {
        return $this->status == self::STATUS_DONE;
    }

    /**
     * Check rejected.
     */
    public function isRejected(): bool
    {
        return $this->status == self::STATUS_REJECTED;
    }

    /**
     * Get billing info.
     */
    public function getBillingInfo(): array
    {
        switch ($this->type) {
            case self::TYPE_RENEW_SUBSCRIPTION:
                $subscription = Subscription::findByUid($this->getMetadata()['subscription_uid']);
                $chargeInfo = trans('messages.bill.charge_before', [
                    'date' =>  \App\Library\Tool::formatDate($subscription->current_period_ends_at),
                ]);
                break;
            case self::TYPE_NEW_SUBSCRIPTION:
                $chargeInfo = trans('messages.bill.charge_now');
                break;
            case self::TYPE_CHANGE_PLAN:
                $chargeInfo = trans('messages.bill.charge_now');
                break;
            default:
                $chargeInfo = '';
        }

        return  [
            'title' => $this->title,
            'description' => $this->description,
            'bill' => $this->invoiceItems()->get()->map(fn($item) => [
                'title' => $item->title,
                'description' => $item->description,
                'price' => format_price($item->amount, $item->invoice->currency->format),
                'tax' => format_price($item->getTax(), $item->invoice->currency->format),
                'discount' => format_price($item->discount, $item->invoice->currency->format),
            ]),
            'charge_info' => $chargeInfo,
            'total' => format_price($this->total(), $this->currency->format),
            'pending' => $this->getPendingTransaction(),
        ];
    }

    /**
     * Add transactions.
     */
    public function addLog($type, $data, $transaction_id = null): \App\Model\SubscriptionLog
    {
        $log = new SubscriptionLog();
        $log->subscription_id = $this->id;
        $log->type = $type;
        $log->transaction_id = $transaction_id;
        $log->save();

        if (isset($data)) {
            $log->updateData($data);
        }

        return $log;
    }

    /**
     * Check is renew subscription invoice.
     */
    public function isRenewSubscriptionInvoice(): bool
    {
        return $this->type == self::TYPE_RENEW_SUBSCRIPTION;
    }

    /**
     * Check is change plan invoice.
     */
    public function isChangePlanInvoice(): bool
    {
        return $this->type == self::TYPE_CHANGE_PLAN;
    }

    /**
     * Add transaction.
     */
    public function createPendingTransaction($gateway): \App\Model\Transaction
    {
        if ($this->getPendingTransaction()) {
            throw new \Exception('Invoice already has a pending transaction!');
        }

        // @todo: dung transactions()->new....
        $transaction = new Transaction();
        $transaction->invoice_id = $this->id;
        $transaction->status = Transaction::STATUS_PENDING;
        $transaction->allow_manual_review = $gateway->allowManualReviewingOfTransaction();

        // This information is needed for verifying a transaction status later on
        $transaction->method = $gateway->getType();

        $transaction->save();

        return $transaction;
    }

    public function isUnpaid(): bool
    {
        return in_array($this->status, [
            self::STATUS_NEW,
        ]);
    }

    /**
     * Checkout.
     */
    public function checkout($gateway, $payCallback): void
    {
        $invoice = $this;
        // \DB::transaction(function() use ($gateway, $invoice) {
        $invoice->createPendingTransaction($gateway);

        try {
            $result = $payCallback($invoice);

            if ($result->isDone()) {
                // Stripe, PayPal, Braintree for example
                $invoice->fulfill();
            } elseif ($result->isFailed()) {
                // Stripe, PayPal, Braintree for example
                $invoice->payFailed($result->error);
            } elseif ($result->isStillPending()) {
                // Coin, offline shouls return this status
                // Wait more, check again later....
                // Coinpayment, offline
            } elseif ($result->isVerificationNotNeeded()) {
                // IMPORTANT: this special status is used for checking (pending) transaction status only
                //          **** SERVICES SHOULD NOT RETURN THIS STATUS IN CHECKOUT method ****
                // Do nothing, just wait for the service to finish it itself (Stripe)
                // Service should not return this status, it is used for verification only
            }
        } catch (\Exception $e) {
            // pay failed
            $invoice->payFailed($e->getMessage());
        }
    }
}
