<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id', 'item_type', 'amount', 'title', 'description', 'vat',
    ];

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Invoice.
     */
    public function invoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Invoice::class);
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $item->uid = uniqid();
        });
    }

    /**
     * Tax amount.
     */
    public function getTax(): int|float
    {
        return $this->subTotal() * ($this->vat / 100);
    }

    public function subTotal(): int|float
    {
        return $this->amount - $this->discount;
    }

    /**
     * Total amount.
     *
     * @return void
     */
    public function total()
    {
        return $this->subTotal() + $this->getTax();
    }
}
