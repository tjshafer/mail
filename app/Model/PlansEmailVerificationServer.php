<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlansEmailVerificationServer extends Model
{
    // Plan status
    const STATUS_INACTIVE = 'inactive';

    const STATUS_ACTIVE = 'active';

    public function emailVerificationServer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\EmailVerificationServer::class, 'server_id');
    }
}
