<?php

/**
 * SendingServerSendmail class.
 *
 * Abstract class for Sendmail sending server
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use Acelle\Library\Log as MailLog;

class SendingServerSendmail extends SendingServer
{
    protected $table = 'sending_servers';

    /**
     * Send the provided message.
     *
     *
     * @param message
     * @return bool
     */
    public function send($message, $params = [])
    {
        try {
            $transport = new \Swift_SendmailTransport($this->sendmail_path.' -bs');

            // Create the Mailer using your created Transport
            $mailer = new \Swift_Mailer($transport);

            // Actually send
            $sent = $mailer->send($message);

            if ($sent) {
                MailLog::info('Sent!');

                return [
                    'status' => self::DELIVERY_STATUS_SENT,
                ];
            } else {
                MailLog::warning('Sending failed');

                return [
                    'status' => self::DELIVERY_STATUS_FAILED,
                    'error' => 'Unknown SMTP error',
                ];
            }
        } catch (\Exception $e) {
            MailLog::warning('Sending failed');
            MailLog::warning($e->getMessage());

            return [
                'status' => self::DELIVERY_STATUS_FAILED,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Check the sending server settings, make sure it does work.
     */
    public function test(): bool
    {
        if (! file_exists($this->sendmail_path)) {
            throw new \Exception("File {$this->sendmail_path} does not exists");
        }

        if (! is_executable($this->sendmail_path)) {
            throw new \Exception("File {$this->sendmail_path} is not executable");
        }

        return true;
    }

    public function allowVerifyingOwnEmailsRemotely(): bool
    {
        return false;
    }

    public function allowVerifyingOwnDomainsRemotely(): bool
    {
        return false;
    }

    public function syncIdentities(): void
    {
        // just do nothing
    }

    public static function instantiateFromSettings($settings = []): \App\Model\SendingServerSendmail
    {
        $properties = ['sendmail_path', 'from_name', 'from_address'];
        $required = ['sendmail_path', 'from_address'];

        $server = new self();

        // Validate
        foreach ($properties as $property) {
            if ((! array_key_exists($property, $settings) || empty($settings[$property])) && in_array($property, $required)) {
                throw new \Exception("Cannot instantiate Sendmail mailer, '{$property}' property is missing");
            }

            $server->{$property} = $settings[$property];
        }

        return $server;
    }

    public function setupBeforeSend($fromEmailAddress): void
    {
        //
    }
}
