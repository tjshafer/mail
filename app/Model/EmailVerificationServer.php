<?php

namespace App\Model;

use Acelle\Library\Log as MailLog;
use Acelle\Library\QuotaTrackerFile;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use JsonPath\JsonObject;

class EmailVerificationServer extends Model
{
    // set the table name
    protected $table = 'email_verification_servers';

    protected $quotaTracker;

    const STATUS_ACTIVE = 'active';

    const STATUS_INACTIVE = 'inactive';

    const WAIT = 30;

    protected $fillable = ['type', 'name'];

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    public function admin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Admin::class);
    }

    /**
     * Verify email address.
     *
     * @return mixed
     */
    public function verify($email): array
    {
        // enforce the verification speed limit
        $logged = false;
        while ($this->isOverQuota()) {
            if (! $logged) {
                $logged = true;
                $options = $this->getOptions();
                $limit = "[{$options['limit_value']} / {$options['limit_base']} {$options['limit_unit']}]";
                MailLog::warning(sprintf('Verification server `%s` (%s) is exceeding speed limit of %s, waiting...', $this->name, $this->id, $limit));
            }
            sleep(self::WAIT);
        }

        // retrieve the service settings
        $config = $this->getConfig();
        $options = $this->getOptions();
        $client = new Client(['verify' => false]);

        // build the request URI
        $uri = $config['uri'];
        $uri = str_replace('{EMAIL}', $email, $uri);
        $uri = array_key_exists('api_key', $options) ? str_replace('{API_KEY}', $options['api_key'], $uri) : $uri;
        $uri = array_key_exists('api_secret_key', $options) ? str_replace('{API_SECRET_KEY}', $options['api_secret_key'], $uri) : $uri;
        $uri = array_key_exists('username', $options) ? str_replace('{USERNAME}', $options['username'], $uri) : $uri;
        $uri = array_key_exists('password', $options) ? str_replace('{PASSWORD}', $options['password'], $uri) : $uri;

        // build the request URI
        if ($config['request_type'] == 'POST') {
            $postdata = $config['post_data'];
            $postdata = str_replace('{EMAIL}', $email, $postdata);
            foreach ($config['fields'] as $field) {
                if (array_key_exists($field, $options)) {
                    $postdata = str_replace('{'.strtoupper($field).'}', $options[$field], $postdata);
                }
            }

            $headers = $config['post_headers'];
            foreach ($headers as $header => $value) {
                foreach ($config['fields'] as $field) {
                    if (array_key_exists($field, $options)) {
                        $value = str_replace('{'.strtoupper($field).'}', $options[$field], $value);
                        $headers[$header] = $value;
                    }
                }
            }

            // make POST request
            $response = $client->request(
                $config['request_type'],
                $uri,
                ['headers' => $headers, 'body' => $postdata, 'verify' => false]
            );
        } else { // GET request
            // actually request to the service
            $response = $client->request($config['request_type'], $uri, ['verify' => false]);
        }

        // fetch the result
        $raw = (string) $response->getBody();

        // workaround for https://emaillistvalidation.com/
        if ($raw == 'error_credit') {
            throw new Exception('No verification credits available for service '.$this->type);
        }

        if (array_key_exists('response_type', $config) && $config['response_type'] == 'plain') {
            if (! array_key_exists($raw, $config['result_map'])) {
                throw new Exception('Error verifying email address with service `'.$this->type.'`. Response from service: '.$raw);
            }
            $mapped = $config['result_map'][$raw];
            $this->countUsage();

            return [$mapped, $raw];
        }
        // end workaround for https://emaillistvalidation.com/

        $jsonObject = new JsonObject($raw);

        try {
            $result = $jsonObject->get($config['result_xpath']);

            if (empty($result)) {
                throw new Exception('Empty response after parse XPATH');
            }

            $result = $result[0];
        } catch (\Throwable $ex) {
            $message = sprintf("[Verification Server #%s, %s] Cannot parse result with XPATH: `%s`\n", $this->id, $this->type, $config['result_xpath']);
            $message .= sprintf("Raw:\n%s\n", $raw);
            $message .= sprintf('Error: %s', $ex->getMessage());

            throw new Exception($message);
        }

        // map the result value to those of Acelle Mail
        $result = is_bool($result) ? json_encode($result) : $result;
        if (! array_key_exists($result, $config['result_map'])) {
            throw new \Exception('Unexpected result from verification service: '.$raw);
        }
        $mapped = $config['result_map'][$result];
        $this->countUsage();

        return [$mapped, $raw];
    }

    /**
     * Find the configuration settings for a given verification service.
     *
     * @return mixed
     */
    public function getConfig()
    {
        $configs = config('verification.services');
        foreach ($configs as $config) {
            if ($config['id'] == $this->type) {
                return $config;
            }
        }

        throw new \Exception('Cannot find settings for verification service '.$this->type);
    }

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $user = $request->user();
        $query = self::select('email_verification_servers.*');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('email_verification_servers.name', 'like', '%'.$keyword.'%')
                        ->orWhere('email_verification_servers.type', 'like', '%'.$keyword.'%')
                        ->orWhere('email_verification_servers.host', 'like', '%'.$keyword.'%');
                });
            }
        }

        // filters
        $filters = $request->filters;
        if (! empty($filters)) {
            if (! empty($filters['type'])) {
                $query = $query->where('email_verification_servers.type', '=', $filters['type']);
            }
        }

        // Other filter
        if (! empty($request->customer_id)) {
            $query = $query->where('email_verification_servers.customer_id', '=', $request->customer_id);
        }

        if (! empty($request->admin_id)) {
            $query = $query->where('email_verification_servers.admin_id', '=', $request->admin_id);
        }

        // remove customer sending servers
        if (! empty($request->no_customer)) {
            $query = $query->whereNull('customer_id');
        }

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * Get server type select options.
     */
    public static function typeSelectOptions(): array
    {
        $services = config('verification.services');
        $options = [];
        foreach ($services as $service) {
            $options[] = ['value' => $service['id'], 'text' => $service['name']];
        }

        return $options;
    }

    /**
     * Get campaign validation rules.
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'type' => 'required',
            'options.limit_value' => 'required',
            'options.limit_base' => 'required',
            'options.limit_unit' => 'required',
        ];

        if ($this->type) {
            foreach ($this->getConfig()['fields'] as $field) {
                $rules['options.'.$field] = 'required';
            }
        }

        return $rules;
    }

    /**
     * Frequency time unit options.
     */
    public static function quotaTimeUnitOptions(): array
    {
        return [
            ['value' => 'minute', 'text' => trans('messages.minute')],
            ['value' => 'hour', 'text' => trans('messages.hour')],
            ['value' => 'day', 'text' => trans('messages.day')],
        ];
    }

    /**
     * Server status select options.
     */
    public static function statusSelectOptions(): array
    {
        return [
            ['value' => self::STATUS_ACTIVE, 'text' => trans('messages.email_verification_server_status_'.self::STATUS_ACTIVE)],
            ['value' => self::STATUS_INACTIVE, 'text' => trans('messages.email_verification_server_status_'.self::STATUS_INACTIVE)],
        ];
    }

    /**
     * Get all items.
     */
    public static function getAll(): \collect
    {
        return self::where('status', '=', 'active');
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;
        });
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Get all options.
     */
    public function getOptions(): object
    {
        return ! isset($this->options) ? [] : json_decode($this->options, true);
    }

    /**
     * Disable verification server.
     */
    public function disable(): void
    {
        $this->status = self::STATUS_INACTIVE;
        $this->save();
    }

    /**
     * Enable verification server.
     */
    public function enable(): void
    {
        $this->status = self::STATUS_ACTIVE;
        $this->save();
    }

    /**
     * Get all active items.
     */
    public static function getAllActive(): \collect
    {
        return self::where('status', '=', SendingServer::STATUS_ACTIVE);
    }

    /**
     * Get all active system items.
     */
    public static function getAllAdminActive(): \collect
    {
        return self::getAllActive()->whereNotNull('admin_id');
    }

    /**
     * Add customer action log.
     */
    public function log($name, $customer, $add_datas = []): void
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        $data = [...$data, ...$add_datas];

        Log::create([
            'customer_id' => $customer->id,
            'type' => 'email_verification_server',
            'name' => $name,
            'data' => json_encode($data),
        ]);
    }

    /**
     * Get sending server's QuotaTracker.
     */
    public function getQuotaTracker(): ? \App\Library\QuotaTrackerFile
    {
        if (! $this->quotaTracker) {
            $this->initQuotaTracker();
        }

        return $this->quotaTracker;
    }

    /**
     * Get credits used.
     *
     * @return int credit used
     */
    public function getCreditUsage(): int
    {
        return $this->getQuotaTracker()->getUsage();
    }

    /**
     * Get speed limit string.
     *
     * @return string speed limit
     */
    public function getSpeedLimitString(): string
    {
        $options = $this->getOptions();

        return "{$options['limit_value']} / {$options['limit_base']} {$options['limit_unit']}";
    }

    /**
     * Initialize the quota tracker.
     */
    public function initQuotaTracker(): void
    {
        $options = $this->getOptions();
        $base = "{$options['limit_base']} {$options['limit_unit']}";
        $limit = [
            "{$base}" => $options['limit_value'],
        ];

        $this->quotaTracker = new QuotaTrackerFile($this->getQuotaLockFile(), ['start' => $this->created_at->timestamp, 'max' => -1], $limit);
        $this->quotaTracker->cleanupSeries();
    }

    /**
     * Get sending quota lock file.
     *
     * @return string file path
     */
    public function getQuotaLockFile(): string
    {
        return storage_path("app/server/quota/{$this->uid}");
    }

    /**
     * Increment quota usage.
     *
     * @return mixed
     */
    public function countUsage(Carbon $timePoint = null)
    {
        return $this->getQuotaTracker($timePoint)->add();
    }

    /**
     * Check if user has used up all quota allocated.
     */
    public function isOverQuota(): bool
    {
        return ! $this->getQuotaTracker()->check();
    }

    /**
     * Get service type name.
     *
     * @return string
     */
    public function getTypeName()
    {
        try {
            $service = $this->getConfig();

            return $service['name'];
        } catch (\Exception $ex) {
            return 'Error: Config missing!';
        }
    }
}
