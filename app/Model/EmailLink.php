<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailLink extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'link',
    ];

    /**
     * Association with mailList through mail_list_id column.
     */
    public function email(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Email::class);
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating automation.
        static::creating(function ($item) {
            // Create new uid
            $item->uid = uniqid();
        });
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }
}
