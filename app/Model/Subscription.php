<?php

namespace App\Model;

use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    const STATUS_NEW = 'new';

    const STATUS_ACTIVE = 'active';

    const STATUS_ENDED = 'ended';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $casts = [
        'trial_ends_at' => 'datetime',
        'ends_at' => 'datetime',
        'current_period_ends_at' => 'datetime',
        'started_at' => 'datetime',
        'last_period_ends_at' => 'datetime',
    ];

    /**
     * Indicates if the plan change should be prorated.
     *
     * @var bool
     */
    protected $prorate = true;

    /**
     * The date on which the billing cycle should be anchored.
     *
     * @var string|null
     */
    protected $billingCycleAnchor = null;

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $item->uid = uniqid();
        });
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function plan(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        // @todo dependency injection
        return $this->belongsTo( \App\Model\Plan::class);
    }

    /**
     * Get the user that owns the subscription.
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        // @todo dependency injection
        return $this->belongsTo( \App\Model\Customer::class);
    }

    /**
     * Get related invoices.
     */
    public function invoices(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        $id = $this->id;
        $type = self::class;

        return Invoice::whereIn('id', function ($query) use ($id, $type) {
            $query->select('invoice_id')
            ->from(with(new InvoiceItem)->getTable())
            ->where('item_type', $type)
            ->where('item_id', $id);
        });
    }

    /**
     * Subscription only has one new invoice.
     */
    public function getUnpaidInvoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->invoices()
            ->unpaid()
            ->first();
    }

    public function scopeActive($query): void
    {
        $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeEnded($query): void
    {
        $query->where('status', self::STATUS_ENDED);
    }

    /**
     * Get last invoice.
     */
    public function getItsOnlyUnpaidInitInvoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        if (! $this->isNew()) {
            throw new \Exception('Method getItsOnlyUnpaidInitInvoice() only use for NEW subscription');
        }

        $query = $this->invoices()
            ->newSubscription()
            ->unpaid();

        // more than on invoice
        if ($query->count() != 1) {
            throw new \Exception('New Subscription must have only one unpaid TYPE_NEW_SUBSCRIPTION invoice!');
        }

        return $query->first();
    }

    /**
     * Get renew invoice.
     */
    public function getItsOnlyUnpaidChangePlanInvoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->invoices()
            ->changePlan()
            ->unpaid()
            ->first();
    }

    /**
     * Get change plan invoice.
     */
    public function getItsOnlyUnpaidRenewInvoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->invoices()
            ->renew()
            ->unpaid()
            ->first();
    }

    /**
     * Create init invoice.
     */
    public function createInitInvoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        //
        if ($this->getUnpaidInvoice()) {
            throw new \Exception(trans('messages.error.has_waiting_invoices'));
        }

        // create invoice
        $invoice = new Invoice();
        $invoice->status = Invoice::STATUS_NEW;
        $invoice->type = Invoice::TYPE_NEW_SUBSCRIPTION;
        $invoice->title = trans('messages.invoice.init_subscription');
        $invoice->description = trans('messages.invoice.init_subscription.desc', [
            'plan' => $this->plan->name,
            'date' =>  \App\Library\Tool::formatDate($this->getPeriodEndsAt(\Illuminate\Support\Carbon::now())),
        ]);
        $invoice->customer_id = $this->customer->id;
        $invoice->currency_id = $this->plan->currency_id;
        $invoice->save();

        // data
        $invoice->updateMetadata([
            'subscription_uid' => $this->uid,
        ]);

        // add item
        $invoiceItem = $invoice->invoiceItems()->create([
            'item_id' => $this->id,
            'item_type' => $this::class,
            'amount' => $this->plan->price,
            'vat' => $this->plan->vat,
            'title' => $this->plan->name,
            'description' => view('plans._bill_desc', ['plan' => $this->plan]),
        ]);

        return $invoice;
    }

    /**
     * Create renew invoice.
     */
    public function createRenewInvoice(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        //
        if ($this->getUnpaidInvoice()) {
            throw new \Exception(trans('messages.error.has_waiting_invoices'));
        }

        // create invoice
        $invoice = new Invoice();
        $invoice->status = Invoice::STATUS_NEW;
        $invoice->type = Invoice::TYPE_RENEW_SUBSCRIPTION;
        $invoice->title = trans('messages.invoice.renew_subscription');
        $invoice->description = trans('messages.renew_subscription.desc', [
            'plan' => $this->plan->name,
            'date' =>  \App\Library\Tool::formatDate($this->nextPeriod()),
        ]);
        $invoice->customer_id = $this->customer->id;
        $invoice->currency_id = $this->plan->currency_id;
        $invoice->save();

        // data
        $invoice->updateMetadata([
            'subscription_uid' => $this->uid,
        ]);

        // add item
        $invoiceItem = $invoice->invoiceItems()->create([
            'item_id' => $this->id,
            'item_type' => $this::class,
            'amount' => $this->plan->price,
            'vat' => $this->plan->vat,
            'title' => $this->plan->name,
            'description' => view('plans._bill_desc', ['plan' => $this->plan]),
        ]);

        return $invoice;
    }

    /**
     * Create change plan invoice.
     */
    public function createChangePlanInvoice($newPlan): \App\Model\Invoice
    {
        //
        if ($this->getUnpaidInvoice()) {
            $this->getUnpaidInvoice()->delete();
        }

        // calculate change plan amout ends at
        $metadata = $this->calcChangePlan($newPlan);

        // create invoice
        $invoice = new Invoice();
        $invoice->status = Invoice::STATUS_NEW;
        $invoice->type = Invoice::TYPE_CHANGE_PLAN;
        $invoice->title = trans('messages.invoice.change_plan');
        $invoice->description = trans('messages.change_plan.desc', [
            'plan' => $this->plan->name,
            'newPlan' => $newPlan->name,
            'date' =>  \App\Library\Tool::formatDate(\Illuminate\Support\Carbon::parse($metadata['endsAt'])),
        ]);
        $invoice->customer_id = $this->customer->id;
        $invoice->currency_id = $this->plan->currency_id;
        $invoice->save();

        // save data
        $invoice->updateMetadata([
            'subscription_uid' => $this->uid,
            'new_plan_uid' => $newPlan->uid,
        ]);

        // add item
        $invoiceItem = $invoice->invoiceItems()->create([
            'item_id' => $this->id,
            'item_type' => $this::class,
            'amount' => $metadata['amount'],
            'title' => $this->plan->name,
            'vat' => $this->plan->vat,
            'description' => view('plans._bill_desc', ['plan' => $this->plan]),
        ]);

        return $invoice;
    }

    /**
     * Set subscription as ended.
     */
    public function setEnded(): void
    {
        // then set the sub end
        $this->status = self::STATUS_ENDED;
        $this->ends_at = \Illuminate\Support\Carbon::now();
        $this->save();
    }

    /**
     * Get lastest bill information
     *
     * @return void
     */
    public function getUpcomingBillingInfo()
    {
        if (! $this->getUnpaidInvoice()) {
            return null;
        }

        return $this->getUnpaidInvoice()->getBillingInfo();
    }

    /**
     * Get period by start date.
     *
     * @param  date  $date
     */
    public function getPeriodEndsAt($startDate): \date
    {
        // dose not support recurring, update ends at column
        $interval = $this->plan->frequency_unit;
        $intervalCount = $this->plan->frequency_amount;

        switch ($interval) {
            case 'month':
                $endsAt = $startDate->addMonthsNoOverflow($intervalCount);
                break;
            case 'day':
                $endsAt = $startDate->addDay($intervalCount);
                // no break
            case 'week':
                $endsAt = $startDate->addWeek($intervalCount);
                break;
            case 'year':
                $endsAt = $startDate->addYearsNoOverflow($intervalCount);
                break;
            default:
                $endsAt = null;
        }

        return $endsAt;
    }

    // 1 End luôn subscription nếu đã hết hạn
    // 2 Sinh ra RENEW invoice
    // 3 Xử lý thanh toán ==> #todo tach ra thành processInvoices()
    public function check(): void
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                // nothing to check
                break;
            case self::STATUS_ACTIVE:
                // check expired
                if ($this->isExpired()) {
                    $this->cancelNow();

                    return;
                }

                // check expiring
                if ($this->isExpiring() && $this->canRenewPlan() && ! $this->cancelled()) {
                    $pendingInvoice = $this->getUnpaidInvoice();

                    // create renew invoice if no pending invoice
                    if (! $pendingInvoice) {
                        $pendingInvoice = $this->createRenewInvoice();
                    }

                    // check if invoice is change plan -> do nothing
                    if ($pendingInvoice->isChangePlanInvoice()) {
                        return;
                    }
                }
                break;
            case self::STATUS_ENDED:
                // nothing to check
                break;
        }
    }

    public function processRenewInvoice(): void
    {
        if (! $this->isActive()) {
            return;
        }

        $invoice = $this->getItsOnlyUnpaidRenewInvoice();

        if (! $invoice) {
            return;
        }

        // not reach due date
        if (! $this->reachDueDate()) {
            return;
        }

        // check if customer can auto charge
        if (! $this->customer->preferredPaymentGatewayCanAutoCharge()) {
            return;
        }

        // auto charge
        $this->customer->getPreferredPaymentGateway()->autoCharge($invoice);
    }

    public function getDueDate()
    {
        return $this->current_period_ends_at->subDays( \App\Model\Setting::get('recurring_charge_before_days'));
    }

    /**
     * reach due date.
     */
    public function reachDueDate(): bool
    {
        return \Illuminate\Support\Carbon::now()->greaterThanOrEqualTo(
            $this->getDueDate()
        );
    }

    /**
     * Change plan.
     */
    public function changePlan($newPlan): void
    {
        // calculate change plan amout ends at
        $metadata = $this->calcChangePlan($newPlan);

        // new plan
        $this->plan_id = $newPlan->id;

        // new end period
        $this->current_period_ends_at = $metadata['endsAt'];

        // update ends at if it exist
        if ($this->ends_at != null) {
            $this->ends_at = $this->current_period_ends_at;
        }

        $this->save();

        // logs
        $this->addLog(SubscriptionLog::TYPE_PLAN_CHANGED, [
            'old_plan' => $this->plan->name,
            'plan' => $newPlan->name,
        ]);
    }

    /**
     * Check subscription status.
     *
     * @param  int  $subscriptionId
     * @return date
     */
    public static function checkAll(): void
    {
        $subscriptions = self::whereNull('ends_at')->orWhere('ends_at', '>=', \Illuminate\Support\Carbon::now())->get();
        foreach ($subscriptions as $subscription) {
            $subscription->check();
        }
    }

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function subscriptionLogs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        // @todo dependency injection
        return $this->hasMany( \App\Model\SubscriptionLog::class);
    }

    /**
     * Get all transactions from invoices.
     */
    public function transactions()
    {
        return \App\Model\Transaction::whereIn('invoice_id', $this->invoices()->select('id'))
            ->latest();
    }

    /**
     * Determine if the subscription is recurring and not on trial.
     */
    public function isRecurring(): bool
    {
        return ! $this->cancelled();
    }

    /**
     * Determine if the subscription is active.
     */
    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Determine if the subscription is active.
     */
    public function isNew(): bool
    {
        return $this->status == self::STATUS_NEW;
    }

    /**
     * Determine if the subscription is no longer active.
     */
    public function cancelled(): bool
    {
        return ! is_null($this->ends_at);
    }

    /**
     * Determine if the subscription is ended.
     */
    public function isEnded(): bool
    {
        return $this->status == self::STATUS_ENDED;
    }

    /**
     * Determine if the subscription is pending.
     */
    public function activate(): void
    {
        if (! $this->isNew()) {
            throw new \Exception('Only new subscription can be activated, double check your code to make sure you call activate() on a new subscription');
        }

        $this->current_period_ends_at = $this->getPeriodEndsAt(Carbon::now());
        $this->ends_at = null;
        $this->status = self::STATUS_ACTIVE;
        $this->started_at = \Illuminate\Support\Carbon::now();
        $this->save();

        // add log
        $this->addLog(SubscriptionLog::TYPE_SUBSCRIBED, [
            'plan' => $this->plan->name,
            'price' => $this->plan->getFormattedPrice(),
        ]);
    }

    /**
     * Next one period to subscription.
     *
     * @param  Gateway  $gateway
     */
    public function nextPeriod(): \App\Model\date
    {
        return $this->getPeriodEndsAt($this->current_period_ends_at);
    }

    /**
     * Next one period to subscription.
     *
     * @param  Gateway  $gateway
     */
    public function periodStartAt(): bool
    {
        $startAt = $this->current_period_ends_at;
        $interval = $this->plan->frequency_unit;
        $intervalCount = $this->plan->frequency_amount;

        switch ($interval) {
            case 'month':
                $startAt = $startAt->subMonthsNoOverflow($intervalCount);
                break;
            case 'day':
                $startAt = $startAt->subDay($intervalCount);
                // no break
            case 'week':
                $startAt = $startAt->subWeek($intervalCount);
                break;
            case 'year':
                $startAt = $startAt->subYearsNoOverflow($intervalCount);
                break;
            default:
                $startAt = null;
        }

        return $startAt;
    }

    /**
     * Check if subscription is expired.
     *
     * @param  int  $subscriptionId
     * @return date
     */
    public function isExpired(): bool
    {
        return isset($this->ends_at) && \Illuminate\Support\Carbon::now()->endOfDay() > $this->ends_at;
    }

    /**
     * Subscription transactions.
     */
    public function getLogs(): array
    {
        return $this->subscriptionLogs()->latest()->get();
    }

    /**
     * Subscription transactions.
     */
    public function addLog($type, $data, $transaction_id = null): \App\Model\SubscriptionLog
    {
        $log = new SubscriptionLog();
        $log->subscription_id = $this->id;
        $log->type = $type;
        $log->transaction_id = $transaction_id;
        $log->save();

        if (isset($data)) {
            $log->updateData($data);
        }

        return $log;
    }

    /**
     * Cancel subscription. Set ends at to the end of period.
     */
    public function cancel(): void
    {
        $this->ends_at = $this->current_period_ends_at;
        $this->save();

        // delete pending invoice
        if ($this->getUnpaidInvoice()) {
            $this->getUnpaidInvoice()->delete();
        }
    }

    /**
     * Cancel subscription. Set ends at to the end of period.
     */
    public function resume(): void
    {
        if ($this->isEnded()) {
            throw new Exception('Subscription is ended. Can not change ended subscription state!');
        }

        if (! $this->cancelled()) {
            throw new Exception('Subscription is not cancelled. No need to resume again.');
        }

        $this->ends_at = null;
        $this->save();
    }

    /**
     * Cancel subscription. Set ends at to the end of period.
     */
    public function cancelNow(): void
    {
        if ($this->isEnded()) {
            throw new Exception('Subscription is already ended');
        }

        // set status = ended
        $this->setEnded();

        // cancel all pending invoices (new)
        $this->invoices()->new()->delete();

        // add log
        $this->addLog(SubscriptionLog::TYPE_CANCELLED_NOW, [
            'plan' => $this->plan->name,
            'price' => $this->plan->getFormattedPrice(),
        ]);
    }

    /**
     * Renew subscription
     */
    public function renew(): void
    {
        // set new current period
        $this->current_period_ends_at = $this->getPeriodEndsAt($this->current_period_ends_at);

        // ends at
        if ($this->ends_at != null) {
            $this->ends_at = $this->current_period_ends_at;
        }

        $this->save();

        // logs
        $this->addLog(SubscriptionLog::TYPE_RENEWED, [
            'plan' => $this->plan->name,
            'price' => $this->plan->getFormattedPrice(),
        ]);
    }

    public function isExpiring(): bool
    {
        // check if recurring accur
        if (\Illuminate\Support\Carbon::now()->greaterThanOrEqualTo($this->current_period_ends_at->subDays(Setting::get('end_period_last_days')))) {
            return true;
        }

        return false;
    }

    /**
     * Check if can renew free plan. amount > 0 or == 0 && renew_free_plan setting = true
     */
    public function canRenewPlan(): bool
    {
        return $this->plan->price > 0 ||
            (Setting::isYes('renew_free_plan') && $this->plan->price == 0);
    }

    /**
     * user want to change plan.
     *
     * @return bollean
     */
    public function calcChangePlan($plan): array
    {
        if (($this->plan->frequency_unit != $plan->frequency_unit) ||
            ($this->plan->frequency_amount != $plan->frequency_amount) ||
            ($this->plan->currency->code != $plan->currency->code)
        ) {
            throw new \Exception(trans('messages.can_not_change_to_diff_currency_period_plan'));
        }

        // new ends at
        $newEndsAt = $this->current_period_ends_at;

        // amout per day of current plan
        $currentAmount = $this->plan->price;
        $periodDays = $this->current_period_ends_at->diffInDays($this->periodStartAt()->startOfDay());
        $remainDays = $this->current_period_ends_at->diffInDays(\Illuminate\Support\Carbon::now()->startOfDay());
        $currentPerDayAmount = ($currentAmount / $periodDays);
        $newAmount = ($plan->price / $periodDays) * $remainDays;
        $remainAmount = $currentPerDayAmount * $remainDays;

        $amount = $newAmount - $remainAmount;

        // if amount < 0
        if ($amount < 0) {
            $days = (int) ceil(-($amount / $currentPerDayAmount));
            $amount = 0;
            $newEndsAt->addDays($days);

            // if free plan
            if ($plan->price == 0) {
                $newEndsAt = $this->current_period_ends_at;
            }
        }

        return [
            'amount' => round($amount, 2),
            'endsAt' => $newEndsAt,
        ];
    }

    public function abortNew(): void
    {
        if (! $this->isNew()) {
            throw new \Exception('This subscription is not NEW. Can not abortNew!');
        }

        $this->getItsOnlyUnpaidInitInvoice()->delete();

        // if subscription is new -> cancel now subscription.
        // Make sure a new subscription must have a pending invoice
        $this->cancelNow();
    }
}
