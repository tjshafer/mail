<?php

namespace App\Model;

use Illuminate\Bus\Batch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Bus;

class JobMonitor extends Model
{
    use HasFactory;

    const STATUS_QUEUED = 'queued';

    const STATUS_RUNNING = 'running';

    const STATUS_DONE = 'done';

    const STATUS_FAILED = 'failed';

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($me) {
            $me->uid = uniqid();
        });
    }

    public static function makeInstance($subject, $jobType): \App\Model\JobMonitor
    {
        $monitor = new self();
        $monitor->status = self::STATUS_QUEUED;
        $monitor->subject_name = $subject::class;
        $monitor->subject_id = $subject->id;
        $monitor->job_type = $jobType;

        // Return
        return $monitor;
    }

    public function scopeByJobType($query, $jobType)
    {
        return $query->where('job_type', $jobType);
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    public function getBatch()
    {
        if (is_null($this->batch_id)) {
            return;
        }

        return Bus::findBatch($this->batch_id);
    }

    public function job(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Job::class);
    }

    public function getJob()
    {
        return $this->job;
    }

    public function hasJob(): bool
    {
        return ! is_null($this->job_id);
    }

    public function hasBatch(): bool
    {
        return ! is_null($this->batch_id);
    }

    public function setFailed($exception): void
    {
        $this->status = self::STATUS_FAILED;
        $errorMsg = 'Error executing job. '.$exception->getMessage();
        $this->error = $errorMsg;
        $this->save();
    }

    public function setRunning(): void
    {
        $this->status = self::STATUS_RUNNING;
        $this->save();
    }

    public function setDone(): void
    {
        $this->status = self::STATUS_DONE;
        $this->save();
    }

    public function setQueued(): void
    {
        $this->status = self::STATUS_QUEUED;
        $this->save();
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [self::STATUS_RUNNING, self::STATUS_QUEUED]);
    }

    public function getJsonData()
    {
        // Also convert null or empty string to an empty array ([])
        return json_decode($this->data, true) ?: [];
    }

    public function setJsonData($data): void
    {
        $this->data = json_encode($data);
        $this->save();
    }

    public function updateJsonData($data): void
    {
        $json = $this->getJsonData();
        $json = [...$json, ...$data];
        $this->setJsonData($json);
    }

    public function cancelWithoutDeleteBatch(): void
    {
        $this->cancelBatch();
    }

    public function cancel(): void
    {
        $this->cancelJob(); // if any
        $this->cancelBatch(); // if any

        // For now, do not store cancelled job
        // So, just delete the record
        $this->delete();
    }

    private function cancelJob(): void
    {
        // Get the job record in the `jobs` database table
        $job = $this->getJob();

        // Remove it from queue, if any
        if (! is_null($job)) {
            $job->delete();
        }
    }

    private function cancelBatch(): void
    {
        // Then get the batch
        // This is not the batch record in job_batches model
        // So we can just cancel it to have its remaining jobs perish!
        // It will be pruned with queue:prune-batches command
        $batch = $this->getBatch();
        if (! is_null($batch)) {
            $batch->cancel();
        }
    }
}
