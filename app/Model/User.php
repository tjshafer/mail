<?php

namespace App\Model;

use Illuminate\Support\Str;
use App\Library\ExtendedSwiftMessage;
use App\Library\Log as MailLog;
use App\Library\Tool;
use App\Notifications\ResetPassword;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\URL;

class User extends Authenticatable
{
    use Notifiable;

    /*******
     * Important: Storage location: User vs Customer
     * One customer (account) may have one or more users
     *
     * User has its own location for storing:
     * + Lock files
     * + Assets (which is used by File Manager)
     *
     * Customer has a common place to store account's assets (available to all users of the same account)
     * + Templates
     * + Attachments (account-wide, not individual user)
     *
     */
    const BASE_DIR = 'app/users';

    const ASSETS_DIR = 'home/files';

    const ASSETS_THUMB_DIR = 'home/thumbs';

    const PROFILE_IMAGE_PATH = 'home/avatar';

    const PROFILE_THUMB_PATH = 'home/avatar-thumb';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'first_name', 'last_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    public function admin(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne( \App\Model\Admin::class);
    }

    public function createUserDirectories(): void
    {
        $paths = [
            $this->getBasePath(),
            $this->getAssetsPath(),
            $this->getThumbsPath(),
        ];

        foreach ($paths as $path) {
            if (! File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
        }
    }

    /**
     * Get authenticate from file.
     */
    public static function getAuthenticateFromFile(): array
    {
        $path = base_path('.authenticate');

        if (file_exists($path)) {
            $content = \File::get($path);
            $lines = explode("\n", $content);
            if (count($lines) > 1) {
                $demo = session()->get('demo');
                if (! isset($demo) || $demo == 'backend') {
                    return ['email' => $lines[0], 'password' => $lines[1]];
                } else {
                    return ['email' => $lines[2], 'password' => $lines[3]];
                }
            }
        }

        return ['email' => '', 'password' => ''];
    }

    /**
     * Send regitration activation email.
     */
    public function sendActivationMail($name = null): void
    {
        $layout = \App\Model\Layout::where('alias', 'registration_confirmation_email')->first();
        $token = $this->getToken();

        $layout->content = str_replace('{ACTIVATION_URL}', join_url(config('app.url'), action([\App\Http\Controllers\UserController::class, 'activate'], ['token' => $token], false)), $layout->content);
        $layout->content = str_replace('{CUSTOMER_NAME}', $name, $layout->content);

        $name = is_null($name) ? trans('messages.to_email_name') : $name;

        // build the message
        $message = new ExtendedSwiftMessage();
        $message->setEncoder(new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'));
        $message->setContentType('text/html; charset=utf-8');

        $message->setSubject($layout->subject);
        $message->setTo([$this->email => $name]);
        $message->setReplyTo(Setting::get('mail.reply_to'));
        $message->addPart($layout->content, 'text/html');

        $mailer = App::make('xmailer');
        $result = $mailer->sendWithDefaultFromAddress($message);

        if (array_key_exists('error', $result)) {
            throw new \Exception($result['error']);
        }

        MailLog::info('Sent activation email to '.$this->email);
    }

    /**
     * The rules for validation.
     *
     * @var array
     */
    public function rules(): array
    {
        $rules = [
            'email' => 'required|email|unique:users,email,'.$this->id.',id',
            'first_name' => 'required',
            'last_name' => 'required',
            'timezone' => 'required',
            'language_id' => 'required',
        ];

        if (isset($this->id)) {
            $rules['password'] = 'nullable|confirmed|min:5';
        } else {
            $rules['password'] = 'required|confirmed|min:5';
        }

        return $rules;
    }

    /**
     * The rules for validation.
     *
     * @var array
     */
    public function registerRules(): array
    {
        $rules = [
            'email' => 'required|email|unique:users,email,'.$this->id.',id',
            'first_name' => 'required',
            'last_name' => 'required',
            'timezone' => 'required',
            'language_id' => 'required',
        ];

        if (isset($this->id)) {
            $rules['password'] = 'min:5';
        } else {
            $rules['password'] = 'required|min:5';
        }

        return $rules;
    }

    /**
     * The rules for validation via api.
     *
     * @var array
     */
    public function apiRules(): array
    {
        return [
            'email' => 'required|email|unique:users,email,'.$this->id.',id',
            'first_name' => 'required',
            'last_name' => 'required',
            'timezone' => 'required',
            'language_id' => 'required',
            'password' => 'required|min:5',
        ];
    }

    /**
     * The rules for validation via api.
     *
     * @var array
     */
    public function apiUpdateRules($request): array
    {
        $arr = [];

        if (isset($request->email)) {
            $arr['email'] = 'required|email|unique:users,email,'.$this->id.',id';
        }
        if (isset($request->first_name)) {
            $arr['first_name'] = 'required';
        }
        if (isset($request->last_name)) {
            $arr['last_name'] = 'required';
        }
        if (isset($request->timezone)) {
            $arr['timezone'] = 'required';
        }
        if (isset($request->language_id)) {
            $arr['language_id'] = 'required';
        }
        if (isset($request->password)) {
            $arr['password'] = 'min:5';
        }

        return $arr;
    }

    /**
     * Display customer name: first_name last_name.
     *
     * @var string
     */
    public function displayName(): string
    {
        return htmlspecialchars(trim($this->first_name.' '.$this->last_name));
    }

    /**
     * User activation.
     */
    public function userActivation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne( \App\Model\userActivation::class);
    }

    /**
     * Create activation token for user.
     */
    public function getToken(): string
    {
        $token = \App\Model\UserActivation::getToken();

        $userActivation = $this->userActivation;

        if (! is_object($userActivation)) {
            $userActivation = new \App\Model\UserActivation();
            $userActivation->user_id = $this->id;
        }

        $userActivation->token = $token;
        $userActivation->save();

        return $token;
    }

    /**
     * Set user is activated.
     */
    public function setActivated(): void
    {
        $this->activated = true;
        $this->save();
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;

            // Add api token
            $item->api_token = Str::random(60);
        });
    }

    public static function findByUid($uid)
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Check if user has admin account.
     */
    public function isAdmin(): bool
    {
        return is_object($this->admin);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     */
    public function sendPasswordResetNotification($token): void
    {
        // $this->notify(new ResetPassword($token, url('password/reset', $token)));

        $resetPasswordUrl = url('password/reset', $token);
        $htmlContent = '<p>Please click the link below to reset your password:<br><a href="'.$resetPasswordUrl.'">'.$resetPasswordUrl.'</a>';

        // build the message
        $message = new ExtendedSwiftMessage();
        $message->setEncoder(new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'));
        $message->setContentType('text/html; charset=utf-8');

        $message->setSubject('Password Reset');
        $message->setTo($this->email);
        $message->setReplyTo(Setting::get('mail.reply_to'));
        $message->addPart($htmlContent, 'text/html');

        $mailer = App::make('xmailer');
        $result = $mailer->sendWithDefaultFromAddress($message);

        if (array_key_exists('error', $result)) {
            throw new \Exception($result['error']);
        }
    }

    public function getLockPath($path): string
    {
        $base = $this->getBasePath('locks');

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    public function getAssetsPath($path = null): string
    {
        $base = $this->getBasePath(self::ASSETS_DIR);

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    public function getThumbsPath($path = null): string
    {
        $base = $this->getBasePath(self::ASSETS_THUMB_DIR);

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    public function getBasePath($path = null): string
    {
        $base = storage_path(join_paths(self::BASE_DIR, $this->uid));

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    /**
     * Generate one time token.
     */
    public function generateOneTimeToken(): void
    {
        $this->one_time_api_token = generateRandomString(32);
        $this->save();
    }

    /**
     * Clear one time token.
     */
    public function clearOneTimeToken(): void
    {
        $this->one_time_api_token = null;
        $this->save();
    }

    public function getAssetsSubUrl(): string
    {
        $appSubDirectory =  \App\Helpers\getAppSubdirectory();
        $appSubDirectory = (is_null($appSubDirectory)) ? '' : $appSubDirectory;

        // Returns a relative subpath like "/files/000000"
        $subpath = route('user_files', ['uid' => $this->uid], false);

        return join_paths($appSubDirectory, $subpath);
    }

    public function getThumbsSubUrl(): string
    {
        $appSubDirectory =  \App\Helpers\getAppSubdirectory();
        $appSubDirectory = (is_null($appSubDirectory)) ? '' : $appSubDirectory;

        // Returns a relative subpath like "/thumbs/000000"
        $subpath = route('user_thumbs', ['uid' => $this->uid], false);

        return join_paths($appSubDirectory, $subpath);
    }

    public function getFilemanagerConfig(): array
    {
        // Create user directories if not exists
        $this->createUserDirectories();

        // Base URL with port, but without subpath
        $baseUrl = parse_url(url('/'));

        // port may be null
        if (! array_key_exists('port', $baseUrl)) {
            $baseUrl['port'] = '';
        }

        $baseUrl = "{$baseUrl['scheme']}://{$baseUrl['host']}{$baseUrl['port']}";

        // Limitation
        // Since this method is called from within the dialog.php file, there is no way to know if whether or not it is an admin or customer session
        // So, there is no chance to apply quota here
        if ($this->customer) {
            // Limited by plan
            $maxSizeTotal = $this->customer->getOption('max_size_upload_total') > 0 ? $this->customer->getOption('max_size_upload_total') : 2048;
            $maxSizeUpload = $this->customer->getOption('max_file_size_upload') > 0 ? $this->customer->getOption('max_file_size_upload') : 2048;
        } else {
            // Virtually unlimited
            $maxSizeTotal = 2048;
            $maxSizeUpload = 2048;
        }

        $config = [
            'base_url' => $baseUrl,
            'upload_dir' => join_paths('/', $this->getAssetsSubUrl(), '/'),
            'thumb_dir' => join_paths('/', $this->getThumbsSubUrl(), '/'),
            'thumbs_upload_dir' => join_paths('/', $this->getThumbsSubUrl(), '/'),
            // relative path from filemanager folder to upload folder, WITH FINAL /
            'current_path' => join_paths('../../storage/', self::BASE_DIR, $this->uid, self::ASSETS_DIR, '/'),
            // relative path from filemanager folder to upload folder, WITH FINAL /
            'thumbs_base_path' => join_paths('../../storage/', self::BASE_DIR, $this->uid, self::ASSETS_THUMB_DIR, '/'),
            'MaxSizeTotal' => $maxSizeTotal,
            'MaxSizeUpload' => $maxSizeUpload,
        ];

        return $config;
    }

    public function getProfileImageUrl()
    {
        $path = $this->getProfileImagePath();
        if (file_exists($path)) {
            return  \App\Helpers\generatePublicPath($path);
        } else {
            return URL::asset('assets/images/placeholder.jpg');
        }
    }

    public function getProfileThumbUrl()
    {
        $path = $this->getProfileThumbPath();
        if (file_exists($path)) {
            return  \App\Helpers\generatePublicPath($path);
        } else {
            return URL::asset('assets/images/placeholder.jpg');
        }
    }

    public function getProfileImagePath(): string
    {
        return $this->getBasePath(self::PROFILE_IMAGE_PATH);
    }

    public function getProfileThumbPath(): string
    {
        return $this->getBasePath(self::PROFILE_THUMB_PATH);
    }

    /**
     * Upload and resize avatar.
     *
     * @var void
     */
    public function uploadProfileImage(UploadedFile $file)
    {
        // Full path: /storage/app/users/000000/home/avatar
        $path = $this->getProfileImagePath();

        // File name: avatar
        $filename = basename($path);

        // The base dir: /storage/app/users/000000/home/
        $dirname = dirname($path);

        // save to server at /storage/app/users/000000/home/avatar
        $file->move($dirname, $filename);

        // create thumbnails and replace the original image with the the small-sized thumbnail
        $img = \Image::make($path);
        $img->fit(120, 120)->save($path);

        return $path;
    }

    public function removeProfileImage(): void
    {
        $path = $this->getProfileImagePath();
        if (file_exists($path)) {
            File::delete($path);
        }

        $thumb = $this->getProfileThumbPath();
        if (file_exists($thumb)) {
            File::delete($thumb);
        }
    }

    public function deleteAndCleanup(): void
    {
        // User's storage location
        // For example: storage/app/users/000000/
        $path = $this->getBasePath();
        if (file_exists($path)) {
            Tool::xdelete($path);
        }

        $this->delete();
    }
}
