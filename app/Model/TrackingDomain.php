<?php

/**
 * SendingDomain class.
 *
 * Model class for sending domains
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class TrackingDomain extends Model
{
    const STATUS_VERIFIED = 'verified';

    const STATUS_UNVERIFIED = 'unverified';

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;

            // Default status = inactive (until domain verified)
            $item->status = self::STATUS_UNVERIFIED;
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'scheme',
    ];

    /**
     * Get validation rules.
     */
    public function rules(): array
    {
        return [
            'name' => 'required|regex:/^([a-z0-9A-Z]+(-[a-z0-9A-Z]+)*\.)+[a-zA-Z]{2,}$/',
        ];
    }

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $user = $request->user();
        $query = self::select('tracking_domains.*');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('tracking_domains.name', 'like', '%'.$keyword.'%');
                });
            }
        }

        // filters
        $filters = $request->filters;

        // filter by status
        if (! empty($request->status)) {
            $query = $query->where('tracking_domains.status', '=', $request->status);
        }

        // by customer
        if (! empty($request->customer_id)) {
            $query = $query->where('tracking_domains.customer_id', '=', $request->customer_id);
        }

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request, $server = null): \collect
    {
        $query = self::filter($request, $server);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * get verified domains.
     */
    public static function scopeVerified($query): \collect
    {
        return $query->where('status', '=', self::STATUS_VERIFIED);
    }

    public function isVerified(): bool
    {
        return $this->status == self::STATUS_VERIFIED;
    }

    public function getFQDN($trailingDot = true): string
    {
        return $this->name.(($trailingDot) ? '.' : '');
    }

    public function getUrl(): string
    {
        return $this->scheme.'://'.$this->name;
    }

    public function getVerificationUrl(): string
    {
        return $this->getUrl().route('appkey', [], false);
    }

    public function setVerified(): void
    {
        $this->status = self::STATUS_VERIFIED;
    }

    public function verify($debug = false): bool
    {
        try {
            $client = new Client(['verify' => false]);
            $response = $client->request('GET', $this->getVerificationUrl());

            if ($response->getBody() == get_app_identity()) {
                $this->setVerified();
                $this->save();
            } else {
                if ($debug) {
                    echo "This app's identity: ".get_app_identity();
                    echo '<br>';
                    echo 'Retrieved identity: '.$response->getBody();
                    exit;
                }
                throw new \Exception('Verification failed');
            }

            return true;
        } catch (\Exception $ex) {
            if ($debug) {
                echo "This app's identity: ".get_app_identity();
                echo '<br>';
                echo $ex->getMessage();
                exit;
            }
            // loggging here
            return false;
        }
    }
}
