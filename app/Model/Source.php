<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    public static $itemsPerPage = 25;

    public $service;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sources';

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            $uid = uniqid();
            $item->uid = $uid;
        });
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Customer.
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    /**
     * Mail List.
     */
    public function mailList(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\MailList::class);
    }

    /**
     * Products.
     */
    public function products(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Product::class);
    }

    /**
     * Get data.
     *
     * @var object | collect
     */
    public function getData()
    {
        if (! $this['meta']) {
            return json_decode('{}', true);
        }

        return json_decode($this['meta'], true);
    }

    /**
     * Update data.
     *
     * @var object | collect
     */
    public function updateData($data): void
    {
        $data = (object) [...(array) $this->getData(), ...$data];
        $this['meta'] = json_encode($data);

        $this->save();
    }

    /**
     * Search.
     *
     * @var object | collect
     */
    public function scopeSearch($query, $request)
    {
        $query = $query->where('customer_id', '=', $request->user()->id);

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('sources.type', 'like', '%'.strtolower($keyword).'%');
                });
            }
        }

        return $query;
    }

    /**
     * Check if source connected.
     *
     * @var object | collect
     */
    public function connected(): bool
    {
        return isset($this->id);
    }

    /**
     * Products count.
     */
    public function productsCount(): object
    {
        return $this->products()->count();
    }

    /**
     * Get display name.
     */
    public function getName(): object
    {
        return match ($this->type) {
            'WooCommerce' => $this->getData()['data']['name'],
            default => trans('messages.source.'.$this->type),
        };
    }

    /**
     * Get class for exist source.
     */
    public function classMapping(): object
    {
        $class = '\ \App\\Model\\'.$this->type;

        return $class::find($this->id);
    }

    /**
     * Get source list.
     *
     * @return object
     */
    public function getList()
    {
        if ($this->mailList) {
            return $this->mailList;
        }

        // contact
        $contact = new \App\Model\Contact();
        $contact->address_1 = 'empty';
        $contact->country_id = $this->customer->country_id;
        $contact->save();

        // list
        $list = new \App\Model\MailList();
        $list->customer_id = $this->customer_id;
        $list->contact_id = $contact->id;
        $list->name = trans('messages.source.list.default_name', [
            'source' => $this->getName(),
        ]);
        $list->default_subject = $this->getName();
        $list->save();

        // list assign
        $this->mail_list_id = $list->id;
        $this->save();

        return $list;
    }
}
