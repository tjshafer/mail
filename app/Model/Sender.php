<?php

/**
 * Sender class.
 *
 * Model class for countries
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use Acelle\Library\ExtendedSwiftMessage;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log as LaravelLog;

class Sender extends Model
{
    // Statuses
    const STATUS_NEW = 'new'; // deprecated

    const STATUS_PENDING = 'pending';

    const STATUS_VERIFIED = 'verified';

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $user = $request->user();
        $query = $user->customer->allSenders()->select('senders.*');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('senders.email', 'like', '%'.$keyword.'%')
                        ->orwhere('senders.name', 'like', '%'.$keyword.'%');
                });
            }
        }

        // Other filter
        if (! empty($request->customer_id)) {
            $query = $query->where('senders.customer_id', '=', $request->customer_id);
        }

        return $query;
    }

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    public function sendingServer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\SendingServer::class);
    }

    public static function pending()
    {
        return self::where('status', self::STATUS_PENDING);
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The rules for validation.
     *
     * @var array
     */
    public function rules(): array
    {
        $rules = [
            'email' => 'required|email|unique:senders,email,'.$this->id.',id',
            'name' => 'required',
        ];

        return $rules;
    }

    /**
     * The rules for validation.
     *
     * @var array
     */
    public function editRules(): array
    {
        $rules = [
            'name' => 'required',
        ];

        return $rules;
    }

    /**
     * Set sender status to pending.
     *
     * @var void
     */
    public function setPending(): void
    {
        $this->status = self::STATUS_PENDING;
        $this->save();
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $item->uid = uniqid();
        });
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Check if sender is verified.
     */
    public function isVerified(): bool
    {
        return $this->status == self::STATUS_VERIFIED;
    }

    /**
     * Get domain name from email.
     */
    public function getDomain(): string
    {
        return explode('@', $this->email)[1];
    }

    /**
     * Get domain name from email.
     */
    public static function getAllVerified(): string
    {
        return self::where('status', '=', self::STATUS_VERIFIED);
    }

    /**
     * Verify sender.
     */
    public function updateVerificationStatus(): void
    {
        // only work for server of allowVerifyingOwnEmailsRemotely() == true
        $server = $this->sendingServer;
        if (! is_null($server)) {
            $verified = $server->mapType()->verifyIdentity($this->email);
            $this->status = $verified ? self::STATUS_VERIFIED : self::STATUS_PENDING;
            $this->save();
            LaravelLog::info(sprintf('Verify sender %s done, status: %s', $this->email, $this->status));
        } else {
            // verify by clicking on an Acelle link
            // so nothing to do here
        }
    }

    public function setVerified(): static
    {
        $this->status = self::STATUS_VERIFIED;
        $this->save();

        return $this;
    }

    public function toRFCEmailAddress()
    {
        return "{$this->name} <{$this->email}>";
    }

    public function isPending(): bool
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function verifyWith($server = null): void
    {
        // Check if it is already verify
        $this->updateVerificationStatus();

        if ($this->isVerified()) {
            return;
        }

        if (! is_null($server) && $server->allowVerifyingOwnEmailsRemotely()) {
            // this sender is bound with a specific sending server
            // set $this->sending_server_id = $server->id the correct way
            $this->sendingServer()->associate($server);
            $this->setPending();
            $this->save();

            $server->mapType()->sendVerificationEmail($this);
        } else {
            // If server does not support verification
            $this->sendVerificationEmail();
        }
    }

    public function sendVerificationEmail(): void
    {
        $template = Layout::where('alias', 'sender_verification_email')->first();
        $htmlContent = $template->content;

        $htmlContent = str_replace('{USER_NAME}', $this->name, $htmlContent);
        $htmlContent = str_replace('{USER_EMAIL}', $this->email, $htmlContent);
        $htmlContent = str_replace('{VERIFICATION_LINK}', $this->generateVerificationUrl(), $htmlContent);

        // build the message
        $message = new ExtendedSwiftMessage();
        $message->setEncoder(new \Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'));
        $message->setContentType('text/html; charset=utf-8');

        $message->setSubject($template->subject);
        $message->setTo($this->email);
        $message->setReplyTo(Setting::get('mail.reply_to'));
        $message->addPart($htmlContent, 'text/html');

        $mailer = App::make('xmailer');
        $result = $mailer->sendWithDefaultFromAddress($message);

        if (array_key_exists('error', $result)) {
            throw new \Exception($result['error']);
        }
    }

    public function generateVerificationToken(): string
    {
        $token = urlencode(encrypt($this->uid));

        return $token;
    }

    public function generateVerificationUrl(): string
    {
        return action([\App\Http\Controllers\SenderController::class, 'verify'], ['token' => $this->generateVerificationToken()]);
    }

    public function generateVerificationResultUrl(): string
    {
        return action([\App\Http\Controllers\SenderController::class, 'verifyResult'], ['uid' => $this->uid]);
    }

    public static function verifyToken($token)
    {
        $uid = null;

        try {
            $uid = decrypt(urldecode($token));
        } catch (\Exception $ex) {
            throw new \Exception('Invalid sender verification token', 1);
        }

        $sender = self::findByUid($uid);

        if (is_null($sender)) {
            throw new \Exception('Identity not found', 1);
        }

        return $sender->setVerified();
    }
}
