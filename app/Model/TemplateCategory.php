<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TemplateCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * The template that belong to the categories.
     */
    public function templates(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany( \App\Model\Template::class, 'templates_categories', 'category_id', 'template_id');
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating item.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;
        });
    }
}
