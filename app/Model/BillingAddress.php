<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name', 'email', 'country_id',
        'address', 'phone',
    ];

    /**
     * customer.
     */
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    /**
     * country.
     */
    public function country(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Country::class);
    }

    /**
     * Get default billing address.
     *
     * @var object
     */
    public function updateAll($request): array
    {
        // make validator
        $validator = \Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'country_id' => 'required',
            'address' => 'required',
        ]);

        // redirect if fails
        if ($validator->fails()) {
            return [$validator, $this];
        }

        $this->customer_id = $request->user()->customer->id;
        $this->fill($request->all());
        $this->save();

        return [$validator, $this];
    }

    /**
     * Copy from contact.
     *
     * @var object
     */
    public function copyFromContact(): void
    {
        if ($this->customer && $this->customer->contact) {
            $this->fill($this->customer->contact->toArray());
        }
    }
}
