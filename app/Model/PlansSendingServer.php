<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlansSendingServer extends Model
{
    public function sendingServer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\SendingServer::class);
    }

    /**
     * Show fitness.
     *
     * @var string
     */
    public function showFitness(): float
    {
        $sum = self::where('plan_id', '=', $this->plan_id)
            ->sum('fitness');

        return round(($this->fitness / $sum) * 100);
    }

    /**
     * Check if is primary.
     *
     * @var bool
     */
    public function isPrimary()
    {
        return $this->is_primary;
    }
}
