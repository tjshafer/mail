<?php

/**
 * Customer class.
 *
 * Model class for customer
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use Acelle\Jobs\ImportBlacklistJob;
use Acelle\Library\AutoBillingData;
use Acelle\Library\Facades\Billing;
use Acelle\Library\QuotaTrackerFile;
use App\Library\Traits\TrackJobs;
use Illuminate\Support\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use TrackJobs;

    protected $quotaTracker;

    // Plan status
    const STATUS_INACTIVE = 'inactive';

    const STATUS_ACTIVE = 'active';

    const BASE_DIR = 'app/customers'; // storage/customers/000000

    const ATTACHMENTS_DIR = 'home/attachments';  // storage/customers/000000/home/files

    const TEMPLATES_DIR = 'home/templates';  // storage/customers/000000/home/files

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'timezone', 'language_id', 'color_scheme', 'text_direction',
    ];

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function subscription()
    {
        return $this->hasOne( \App\Model\Subscription::class)
            ->where('status', '!=', Subscription::STATUS_ENDED)
            ->latest();
    }

    public function contact(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Contact::class);
    }

    public function subscriptions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Subscription::class);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne( \App\Model\User::class);
    }

    public function admin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Admin::class);
    }

    public function lists(): \Illuminate\Database\Query\Builder
    {
        return $this->hasMany( \App\Model\MailList::class)->latest();
    }

    public function templates(): \Illuminate\Database\Query\Builder
    {
        return $this->hasMany( \App\Model\Template::class)->latest();
    }

    public function language(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Language::class);
    }

    public function campaigns(): \Illuminate\Database\Query\Builder
    {
        return $this->hasMany( \App\Model\Campaign::class)->latest();
    }

    public function sentCampaigns(): \Illuminate\Database\Query\Builder
    {
        return $this->hasMany( \App\Model\Campaign::class)->where('status', '=', 'done')->latest();
    }

    public function subscribers(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough( \App\Model\Subscriber::class, \App\Model\MailList::class);
    }

    public function logs()
    {
        return $this->hasMany( \App\Model\Log::class)->latest();
    }

    public function trackingLogs(): \Illuminate\Database\Query\Builder
    {
        return $this->hasMany( \App\Model\TrackingLog::class)->oldest();
    }

    public function automation2s(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Automation2::class);
    }

    public function activeAutomation2s(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->hasMany( \App\Model\Automation2::class)->where('status', Automation2::STATUS_ACTIVE);
    }

    public function sendingDomains(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\SendingDomain::class);
    }

    public function activeSendingDomains()
    {
        return $this->sendingDomains()->where('status', '=', SendingDomain::STATUS_ACTIVE);
    }

    public function activeDkimSendingDomains()
    {
        return $this->activeSendingDomains()->where('dkim_verified', true);
    }

    public function emailVerificationServers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\EmailVerificationServer::class);
    }

    public function activeEmailVerificationServers()
    {
        return $this->emailVerificationServers()->where('status', '=', EmailVerificationServer::STATUS_ACTIVE);
    }

    public function blacklists(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Blacklist::class);
    }

    // Only direct senders children
    public function senders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Sender::class);
    }

    // tracking domain
    public function trackingDomains(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\TrackingDomain::class);
    }

    /**
     * Get user sources.
     */
    public function sources(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasmany( \App\Model\Source::class);
    }

    // billing addresses
    public function billingAddresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\BillingAddress::class);
    }

    // All senders, including ones that are not associated to a particular sending servers
    public function allSenders()
    {
        $plan = $this->activeSubscription()->plan;
        if ($plan->useSystemSendingServer()) {
            $server = $plan->primarySendingServer();

            if (is_null($server)) {
                throw new \Exception('No sending server available for plan #Customer::allSenders');
            }

            $result = $this->senders()->whereRaw(sprintf('(%s IS NULL OR %s = %s)', table('senders.sending_server_id'), table('senders.sending_server_id'), $server->id));
        } else {
            $result = $this->senders();
        }

        return $result;
    }

    public function getBasePath($path = null): string
    {
        $base = storage_path(join_paths(self::BASE_DIR, $this->uid)); // storage/app/customers/000000/

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    public function getTemplatesPath($path = null): string
    {
        $base = $this->getBasePath(self::TEMPLATES_DIR);

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    public function getAttachmentsPath($path = null): string
    {
        $base = $this->getBasePath(self::ATTACHMENTS_DIR);

        if (! \File::exists($base)) {
            \File::makeDirectory($base, 0777, true, true);
        }

        return join_paths($base, $path);
    }

    public function activeVerifiedSendingDomains()
    {
        $plan = $this->activeSubscription()->plan;

        if ($plan->useSystemSendingServer()) {
            $server = $plan->primarySendingServer();

            if (is_null($server)) {
                throw new \Exception('No sending server available for plan #Customer::activeVerifiedSendingDomains');
            }

            return $this->sendingDomains()
                ->where('status', '=', SendingDomain::STATUS_ACTIVE)
                ->where('domain_verified', '=', SendingDomain::VERIFIED)
                ->whereRaw(sprintf('(%s IS NULL or %s = %s)', table('sending_domains.sending_server_id'), table('sending_domains.sending_server_id'), $server->id));
        } else {
            return $this->sendingDomains()
                ->where('status', '=', SendingDomain::STATUS_ACTIVE)
                ->where('domain_verified', '=', SendingDomain::VERIFIED);
        }
    }

    /**
     * Get active subscription.
     */
    public function activeSubscription(): ?object
    {
        return (is_object($this->subscription) && $this->subscription->isActive()) ? $this->subscription : null;
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;
        });
    }

    /**
     * Get all items.
     */
    public static function getAll(): \collect
    {
        return self::select('customers.*');
    }

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $query = self::select('customers.*')
            ->leftJoin('users', 'users.customer_id', '=', 'customers.id');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('users.first_name', 'like', '%'.$keyword.'%')
                        ->orWhere('users.last_name', 'like', '%'.$keyword.'%')
                        ->orWhere('users.email', 'like', '%'.$keyword.'%');
                });
            }
        }

        // filters
        $filters = $request->filters;
        if (! empty($filters)) {
        }

        // Admin filter
        if (! empty($request->admin_id)) {
            $query = $query->where('customers.admin_id', '=', $request->admin_id);
        }

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * Subscribers count by time.
     */
    public static function subscribersCountByTime($begin, $end, $customer_id = null, $list_id = null): \number
    {
        $query = \App\Model\Subscriber::leftJoin('mail_lists', 'mail_lists.id', '=', 'subscribers.mail_list_id')
                                ->leftJoin('customers', 'customers.id', '=', 'mail_lists.customer_id');

        if (isset($list_id)) {
            $query = $query->where('subscribers.mail_list_id', '=', $list_id);
        }
        if (isset($customer_id)) {
            $query = $query->where('customers.id', '=', $customer_id);
        }

        $query = $query->where('subscribers.created_at', '>=', $begin)
                        ->where('subscribers.created_at', '<=', $end);

        return $query->count();
    }

    /**
     * Get max list quota.
     *
     * @return number
     */
    public function maxLists()
    {
        $count = $this->getOption('list_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Count customer lists.
     */
    public function listsCount(): \number
    {
        return $this->lists()->count();
    }

    /**
     * Calculate list usage.
     *
     * @return number
     */
    public function listsUsage(): int|float
    {
        $max = $this->maxLists();
        $count = $this->listsCount();

        if ($max == '∞') {
            return 0;
        }

        if ($max == 0) {
            return 0;
        }

        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Display calculate list usage.
     *
     * @return number
     */
    public function displayListsUsage()
    {
        if ($this->maxLists() == '∞') {
            return trans('messages.unlimited');
        }

        return $this->listsUsage().'%';
    }

    /**
     * Get campaigns quota.
     *
     * @return number
     */
    public function maxCampaigns()
    {
        $count = $this->getOption('campaign_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Count customer's campaigns.
     */
    public function campaignsCount(): \number
    {
        return $this->campaigns()->count();
    }

    /**
     * Calculate campaign usage.
     *
     * @return number
     */
    public function campaignsUsage(): int|float
    {
        $max = $this->maxCampaigns();
        $count = $this->campaignsCount();

        if ($max == '∞') {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Calculate campaign usage.
     *
     * @return number
     */
    public function displayCampaignsUsage()
    {
        if ($this->maxCampaigns() == '∞') {
            return trans('messages.unlimited');
        }

        return $this->campaignsUsage().'%';
    }

    /**
     * Get subscriber quota.
     *
     * @return number
     */
    public function maxSubscribers()
    {
        $count = $this->getOption('subscriber_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Count customer's subscribers.
     *
     * @return number
     */
    public function subscribersCount($cache = false)
    {
        if ($cache) {
            return $this->readCache('SubscriberCount');
        }

        // return distinctCount($this->subscribers(), 'subscribers.email', 'distinct');
        return $this->subscribers()->count();
    }

    /**
     * Calculate subscibers usage.
     *
     * @return number
     */
    public function subscribersUsage($cache = false): int|float
    {
        $max = $this->maxSubscribers();
        $count = $this->subscribersCount($cache);

        if ($max == '∞') {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Calculate subscibers usage.
     *
     * @return number
     */
    public function displaySubscribersUsage()
    {
        if ($this->maxSubscribers() == '∞') {
            return trans('messages.unlimited');
        }

        // @todo: avoid using cached value in a function
        //        cache value must be called directly from view only
        return $this->readCache('SubscriberUsage', 0).'%';
    }

    /**
     * Get customer's quota.
     *
     * @return string
     */
    public function maxQuota()
    {
        $quota = $this->getOption('sending_quota');
        if ($quota == '-1') {
            return '∞';
        } else {
            return $quota;
        }
    }

    /**
     * Check if customer has access to ALL sending servers.
     */
    public function allSendingServer(): bool
    {
        $check = $this->getOption('all_sending_servers');

        return $check == 'yes';
    }

    /**
     * Check if customer has used up all quota allocated.
     */
    public function overQuota(): bool
    {
        return ! $this->getQuotaTracker()->check();
    }

    /**
     * Increment quota usage.
     */
    public function countUsage(Carbon $timePoint = null)
    {
        return $this->getQuotaTracker($timePoint)->add();
    }

    /**
     * Get customer's sending quota rate.
     *
     * @return string
     */
    public function displaySendingQuotaUsage()
    {
        if ($this->getSendingQuota() == -1) {
            return trans('messages.unlimited');
        }
        // @todo use percentage helper here
        return $this->getSendingQuotaUsagePercentage().'%';
    }

    /**
     * Clean up the quota tracking files to prevent it from growing too large.
     */
    public function cleanupQuotaTracker(): void
    {
        $this->getQuotaTracker()->cleanupSeries();
    }

    /**
     * Get customer's color scheme.
     *
     * @return string
     */
    public function getColorScheme()
    {
        if (! empty($this->color_scheme)) {
            return $this->color_scheme;
        } else {
            return \App\Model\Setting::get('frontend_scheme');
        }
    }

    /**
     * Color array.
     */
    public static function colors($default): array
    {
        return [
            ['value' => '', 'text' => trans('messages.system_default')],
            ['value' => 'blue', 'text' => trans('messages.blue')],
            ['value' => 'green', 'text' => trans('messages.green')],
            ['value' => 'brown', 'text' => trans('messages.brown')],
            ['value' => 'pink', 'text' => trans('messages.pink')],
            ['value' => 'grey', 'text' => trans('messages.grey')],
            ['value' => 'white', 'text' => trans('messages.white')],
        ];
    }

    /**
     * Disable customer.
     */
    public function disable(): bool
    {
        $this->status = 'inactive';

        return $this->save();
    }

    /**
     * Enable customer.
     */
    public function enable(): bool
    {
        $this->status = 'active';

        return $this->save();
    }

    /**
     * Get customer's quota.
     */
    public function getSendingQuota(): string
    {
        // -1 indicate unlimited
        return $this->getOption('email_max');
    }

    /**
     * Get customer's sending quota.
     *
     * @return string
     */
    public function getSendingQuotaUsage()
    {
        try {
            $tracker = $this->getQuotaTracker();

            return $tracker->getUsage();
        } catch (Exception $ex) {
            throw new Exception('Error processing quota tracking file: '.$this->getSendingQuotaLockFile().' Error: '.$ex->getMessage());
        }
    }

    /**
     * Get customer's sending quota rate.
     */
    public function getSendingQuotaUsagePercentage(): int|float
    {
        $max = $this->getSendingQuota();
        $count = $this->getSendingQuotaUsage();

        if ($max == -1) {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Get the quota/limit object.
     */
    public function getQuotaHash(): array
    {
        $current = $this->getCurrentSubscription();

        // if (is_null($current->started_at)) {
        //     throw new \Exception('Application error: subscription started_at not set!');
        // }

        $quota = [
            'start' => (isset($current->started_at) ? $current->started_at->timestamp : null),
            'max' => $this->getOption('email_max'),
        ];

        return $quota;
    }

    /**
     * Initialize the quota tracker.
     */
    public function initQuotaTracker(): void
    {
        $this->quotaTracker = new QuotaTrackerFile($this->getSendingQuotaLockFile(), $this->getQuotaHash(), $this->getSendingLimits());
        $this->quotaTracker->cleanupSeries();
        // @note: in case of multi-process, the following command must be issued manually
        //     $this->renewQuotaTracker();
    }

    public function getSendingLimits()
    {
        $timeValue = $this->getOption('sending_quota_time');
        if ($timeValue == -1) {
            return []; // no limit
        }
        $timeUnit = $this->getOption('sending_quota_time_unit');
        $limit = $this->getOption('sending_quota');

        return ["{$timeValue} {$timeUnit}" => $limit];
    }

    /**
     * Get sending quota lock file.
     *
     * @return string file path
     */
    public function getSendingQuotaLockFile(): string
    {
        return storage_path("app/customer/quota/{$this->uid}");
    }

    /**
     * Get customer's QuotaTracker.
     */
    public function getQuotaTracker(): ? \App\Library\QuotaTrackerFile
    {
        if (! $this->quotaTracker) {
            $this->initQuotaTracker();
        }

        return $this->quotaTracker;
    }

    /**
     * Get customer timezone.
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }

    /**
     * Get customer language code.
     */
    public function getLanguageCode(): string
    {
        return is_object($this->language) ? $this->language->code : null;
    }

    /**
     * Get customer language code.
     *
     * @return string
     */
    public function getLanguageCodeFull()
    {
        $region_code = $this->language->region_code ? strtoupper($this->language->region_code) : strtoupper($this->language->code);

        return is_object($this->language) ? ($this->language->code.'-'.$region_code) : null;
    }

    /**
     * Get customer select2 select options.
     */
    public static function select2($request): string|bool
    {
        $data = ['items' => [], 'more' => true];

        $query = \App\Model\Customer::getAll()->leftJoin('users', 'users.customer_id', '=', 'customers.id');
        if (isset($request->q)) {
            $keyword = $request->q;
            $query = $query->where(function ($q) use ($keyword) {
                $q->orwhere('users.first_name', 'like', '%'.$keyword.'%')
                    ->orWhere('users.last_name', 'like', '%'.$keyword.'%')
                    ->orWhere('users.email', 'like', '%'.$keyword.'%');
            });
        }

        // Read all check
        if (! $request->user()->admin->can('readAll', new \App\Model\Customer())) {
            $query = $query->where('customers.admin_id', '=', $request->user()->admin->id);
        }

        foreach ($query->limit(20)->get() as $customer) {
            $data['items'][] = ['id' => $customer->uid, 'text' => $customer->displayNameEmailOption()];
        }

        return json_encode($data);
    }

    /**
     * Create/Update customer information.
     */
    public function createAccountAndUser($request): \App\Model\User
    {
        $user = new User();

        DB::transaction(function () use ($request, &$user) {
            // Customer
            $this->fill($request->all());
            $this->status = self::STATUS_ACTIVE;
            $this->save();

            // User
            $user = new User();
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->customer()->associate($this);
            $user->save();
        });

        // Important: return the newly created USER
        return $user;
    }

    public function sendingServers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\SendingServer::class);
    }

    public function subAccounts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\SubAccount::class);
    }

    /**
     * Customers count by time.
     */
    public static function customersCountByTime($begin, $end, $admin = null): \number
    {
        $query = \App\Model\Customer::select('customers.*');

        if (isset($admin) && ! $admin->can('readAll', new \App\Model\Customer())) {
            $query = $query->where('customers.admin_id', '=', $admin->id);
        }

        $query = $query->where('customers.created_at', '>=', $begin)
            ->where('customers.created_at', '<=', $end);

        return $query->count();
    }

    public function getSubscriberCountByStatus($status)
    {
        // @note: in this particular case, a simple count(distinct) query is much more efficient
        $query = $this->subscribers()->where('subscribers.status', $status)->distinct('subscribers.email');

        return $query->count();
    }

    /**
     * Update Campaign cached data.
     */
    public function updateCache($key = null): void
    {
        // cache indexes
        $index = [
            // @note: SubscriberCount must come first as its value shall be used by the others
            'SubscriberCount' => fn(&$customer) => $customer->subscribersCount(false),
            'SubscriberUsage' => fn(&$customer) => $customer->subscribersUsage(true),
            'SubscribedCount' => fn(&$customer) => $customer->getSubscriberCountByStatus(Subscriber::STATUS_SUBSCRIBED),
            'UnsubscribedCount' => fn(&$customer) => $customer->getSubscriberCountByStatus(Subscriber::STATUS_UNSUBSCRIBED),
            'UnconfirmedCount' => fn(&$customer) => $customer->getSubscriberCountByStatus(Subscriber::STATUS_UNCONFIRMED),
            'BlacklistedCount' => fn(&$customer) => $customer->getSubscriberCountByStatus(Subscriber::STATUS_BLACKLISTED),
            'SpamReportedCount' => fn(&$customer) => $customer->getSubscriberCountByStatus(Subscriber::STATUS_SPAM_REPORTED),
            'MailListSelectOptions' => fn(&$customer) => $customer->getMailListSelectOptions([], true),
            'Bounce/FeedbackRate' => fn(&$customer) => $customer->getBounceFeedbackRate(),

        ];

        // retrieve cached data
        $cache = json_decode($this->cache, true);
        if (is_null($cache)) {
            $cache = [];
        }

        if (is_null($key)) {
            // update all cache
            foreach ($index as $key => $callback) {
                $cache[$key] = $callback($this);
                if ($key == 'SubscriberCount') {
                    // SubscriberCount cache must always be updated as its value will be used for the others
                    $this->cache = json_encode($cache);
                    $this->save();
                }
            }
        } else {
            // update specific key
            $callback = $index[$key];
            $cache[$key] = $callback($this);
        }

        // write back to the DB
        $this->cache = json_encode($cache);
        $this->save();
    }

    /**
     * Retrieve Campaign cached data.
     *
     * @return mixed
     */
    public function readCache($key, $default = null)
    {
        $cache = json_decode($this->cache, true);
        if (is_null($cache)) {
            return $default;
        }
        if (array_key_exists($key, $cache)) {
            if (is_null($cache[$key])) {
                return $default;
            } else {
                return $cache[$key];
            }
        } else {
            return $default;
        }
    }

    public function getBounceFeedbackRate()
    {
        $delivery = $this->trackingLogs()->count();

        if ($delivery == 0) {
            return 0;
        }

        $bounce = DB::table('bounce_logs')->leftJoin('tracking_logs', 'tracking_logs.message_id', '=', 'bounce_logs.message_id')->count();
        $feedback = DB::table('feedback_logs')->leftJoin('tracking_logs', 'tracking_logs.message_id', '=', 'feedback_logs.message_id')->count();

        $percentage = ($feedback + $bounce) / $delivery;
    }

    /**
     * Sending servers count.
     *
     * @var int
     */
    public function sendingServersCount()
    {
        return $this->sendingServers()->count();
    }

    /**
     * Sending domains count.
     *
     * @var int
     */
    public function sendingDomainsCount()
    {
        return $this->sendingDomains()->count();
    }

    /**
     * Get max sending server count.
     *
     * @var int
     */
    public function maxSendingServers()
    {
        $count = $this->getOption('sending_servers_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Get max email verification server count.
     *
     * @var int
     */
    public function maxEmailVerificationServers()
    {
        $count = $this->getOption('email_verification_servers_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Calculate email verification server usage.
     *
     * @return number
     */
    public function emailVerificationServersUsage(): int|float
    {
        $max = $this->maxEmailVerificationServers();
        $count = $this->emailVerificationServersCount();

        if ($max == '∞') {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Calculate email verigfication servers usage.
     *
     * @return number
     */
    public function displayEmailVerificationServersUsage()
    {
        if ($this->maxEmailVerificationServers() == '∞') {
            return trans('messages.unlimited');
        }

        return $this->emailVerificationServersUsage().'%';
    }

    /**
     * Calculate sending servers usage.
     *
     * @return number
     */
    public function sendingServersUsage(): int|float
    {
        $max = $this->maxSendingServers();
        $count = $this->sendingServersCount();

        if ($max == '∞') {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Calculate sending servers usage.
     *
     * @return number
     */
    public function displaySendingServersUsage()
    {
        if ($this->maxSendingServers() == '∞') {
            return trans('messages.unlimited');
        }

        return $this->sendingServersUsage().'%';
    }

    /**
     * Get max sending server count.
     *
     * @var int
     */
    public function maxSendingDomains()
    {
        $count = $this->getOption('sending_domains_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Calculate subscibers usage.
     *
     * @return number
     */
    public function sendingDomainsUsage(): int|float
    {
        $max = $this->maxSendingDomains();
        $count = $this->sendingDomainsCount();

        if ($max == '∞') {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Calculate subscibers usage.
     *
     * @return number
     */
    public function displaySendingDomainsUsage()
    {
        if ($this->maxSendingDomains() == '∞') {
            return trans('messages.unlimited');
        }

        return $this->sendingDomainsUsage().'%';
    }

    /**
     * Count customer automations.
     *
     * @return number
     */
    public function automationsCount(): float|int
    {
        return $this->automation2sCount();
    }

    /**
     * Get max automation count.
     *
     * @var int
     */
    public function maxAutomations()
    {
        $count = $this->getOption('automation_max');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Calculate subscibers usage.
     *
     * @return number
     */
    public function automationsUsage(): int|float
    {
        $max = $this->maxAutomations();
        $count = $this->automation2sCount();

        if ($max == '∞') {
            return 0;
        }
        if ($max == 0) {
            return 0;
        }
        if ($count > $max) {
            return 100;
        }

        return round((($count / $max) * 100), 2);
    }

    /**
     * Calculate subscibers usage.
     *
     * @return number
     */
    public function displayAutomationsUsage()
    {
        if ($this->maxAutomations() == '∞') {
            return trans('messages.unlimited');
        }

        return $this->automationsUsage().'%';
    }

    /**
     * Check if customer has admin account.
     */
    public function hasAdminAccount(): bool
    {
        return is_object($this->user) && is_object($this->user->admin);
    }

    /**
     * Get all customer active sending servers.
     */
    public function activeSendingServers(): \collect
    {
        return $this->sendingServers()->where('status', '=', \App\Model\SendingServer::STATUS_ACTIVE);
    }

    /**
     * Check if customer is disabled.
     */
    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Check if customer is disabled.
     *
     * @return bool
     */
    public function currentPlanName()
    {
        return is_object($this->activeSubscription()) ?
            $this->activeSubscription()->plan->name :
            '';
    }

    /**
     * Get total file size usage.
     *
     * @return number
     */
    public function totalUploadSize(): int|float
    {
        return  \App\Library\Tool::getDirectorySize(base_path('public/source/'.$this->user->uid)) / 1048576;
    }

    /**
     * Get max upload size quota.
     *
     * @return number
     */
    public function maxTotalUploadSize()
    {
        $count = $this->getOption('max_size_upload_total');
        if ($count == -1) {
            return '∞';
        } else {
            return $count;
        }
    }

    /**
     * Calculate campaign usage.
     *
     * @return number
     */
    public function totalUploadSizeUsage(): int|float
    {
        if ($this->maxTotalUploadSize() == '∞') {
            return 0;
        }
        if ($this->maxTotalUploadSize() == 0) {
            return 100;
        }

        return round((($this->totalUploadSize() / $this->maxTotalUploadSize()) * 100), 2);
    }

    /**
     * Custom can for customer.
     */
    public function can($action, $item): bool
    {
        return $this->user->can($action, [$item, 'customer']);
    }

    /**
     * Get customer contact.
     */
    public function getContact(): object
    {
        if (is_object($this->contact)) {
            $contact = $this->contact;
        } else {
            $contact = new \App\Model\Contact([
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->user->email,
            ]);
        }

        return $contact;
    }

    /*
     * Custom name + email.
     *
     * @return string
     */
    public function displayNameEmailOption(): string
    {
        return $this->user->displayName().'|||'.$this->user->email;
    }

    /**
     * Email verification servers count.
     *
     * @var int
     */
    public function emailVerificationServersCount()
    {
        return $this->emailVerificationServers()->count();
    }

    /**
     * Get list of available email verification servers.
     *
     * @var bool
     */
    public function getEmailVerificationServers()
    {
        // Check the customer has permissions using email verification servers and has his own email verification servers
        if ($this->getOption('create_email_verification_servers') == 'yes') {
            return $this->activeEmailVerificationServers()->get()->map(fn($server) => $server);
        // If customer dont have permission creating sending servers
        } else {
            // Get server from the plan
            return  $this->activeSubscription()->plan->getEmailVerificationServers();
        }
    }

    /**
     * Get list of available sending domains.
     *
     * @var bool
     */
    public function getSendingDomains()
    {
        $result = [];

        // Check the customer has permissions using sending domains and has his own sending domains
        if ($this->getOption('create_sending_domains') == 'yes') {
            $result = $this->activeSendingDomains()->get()->map(fn($server) => $server);
        // If customer dont have permission creating sending domain
        } else {
            // Check if has email verification servers for current subscription
            $result = \App\Model\SendingDomain::getAllAdminActive()->get()->map(fn($server) => $server);
        }

        return $result;
    }

    /**
     * Get list of available sending domains options.
     *
     * @var bool
     */
    public function getSendingDomainOptions()
    {
        return $this->getSendingDomains()->map(fn($domain) => ['value' => $domain->uid, 'text' => $domain->name]);
    }

    /**
     * Get the list of available mail lists, used for populating select box.
     */
    public function getMailListSelectOptions($options = [], $cache = false): array
    {
        $query = \App\Model\MailList::getAll();
        $query->where('customer_id', '=', $this->id);

        // Other list
        if (isset($options['other_list_of'])) {
            $query->where('id', '!=', $options['other_list_of']);
        }

        $result = $query->oldest('name')->get()->map(fn($item) => ['id' => $item->id, 'value' => $item->uid, 'text' => $item->name.' ('.$item->subscribersCount($cache).' '.strtolower(trans('messages.subscribers')).')']);

        return $result;
    }

    /**
     * Get email verification servers select options.
     */
    public function emailVerificationServerSelectOptions(): array
    {
        $servers = $this->getEmailVerificationServers();
        $options = [];
        foreach ($servers as $server) {
            $options[] = ['text' => $server->name, 'value' => $server->uid];
        }

        return $options;
    }

    /**
     * Get customer's sending servers type.
     */
    public function getSendingServertypes(): array
    {
        $allTypes = \App\Model\SendingServer::types();
        $types = [];

        foreach ($allTypes as $type => $server) {
            if ($this->isAllowCreateSendingServerType($type)) {
                $types[$type] = $server;
            }
        }

        if (! Setting::isYes('delivery.sendmail')) {
            unset($types['sendmail']);
        }

        if (! Setting::isYes('delivery.phpmail')) {
            unset($types['php-mail']);
        }

        return $types;
    }

    /**
     * Check customer can create sending servers type.
     */
    public function isAllowCreateSendingServerType($type): bool
    {
        $customerTypes = $this->getOption('sending_server_types');
        if ($this->getOption('all_sending_server_types') == 'yes' ||
            (isset($customerTypes[$type]) && $customerTypes[$type] == 'yes')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Add email to customer's blacklist.
     */
    public function addEmaillToBlacklist($email): void
    {
        $email = trim(strtolower($email));

        if ( \App\Library\Tool::isValidEmail($email)) {
            $exist = $this->blacklists()->where('email', '=', $email)->count();
            if (! $exist) {
                $blacklist = new \App\Model\Blacklist();
                $blacklist->customer_id = $this->id;
                $blacklist->email = $email;
                $blacklist->save();
            }
        }
    }

    /**
     * Get all customer's and system domains.
     */
    public function getAllSystemAndOwnDomains(): \App\Model\collect
    {
        return \App\Model\SendingDomain::getAllActive();
    }

    /**
     * Get customer options.
     *
     * @return string
     */
    public function getOptions()
    {
        if (is_object($this->activeSubscription())) {
            // Find plan
            return $this->activeSubscription()->plan->getOptions();
        } else {
            return [];
        }
    }

    /**
     * Get customer option.
     */
    public function getOption($name): ?string
    {
        $options = $this->getOptions();
        // @todo: chỗ này cần raise 1 cái exception chứ ko phải là trả về rỗng (trả vể rỗng để "dìm" lỗi đi à?)
        return $options[$name] ?? null;
    }

    /**
     * Check if customer has api access.
     */
    public function canUseApi(): bool
    {
        return $this->getOption('api_access') == 'yes';
    }

    /**
     * Get all verified identities.
     */
    public function getVerifiedIdentities(): array
    {
        $list = [];

        // own emails
        $senders = $this->allSenders()->where('status', Sender::STATUS_VERIFIED)->get();
        foreach ($senders as $sender) {
            $list[] = $sender->name.' <'.$sender->email.'>';
        }

        // own domain
        $domains = $this->activeVerifiedSendingDomains()->get();
        foreach ($domains as $domain) {
            $list[] = $domain->name;
        }

        // plan sending server emails if system sending servers
        $plan = $this->activeSubscription()->plan;
        if ($plan->useSystemSendingServer()) {
            $list = [...$plan->getVerifiedIdentities(), ...$list];
        }

        return array_values(array_unique($list));
    }

    /**
     * Get all verified identities.
     */
    public function verifiedIdentitiesDroplist($keyword = null): array
    {
        $droplist = [];
        $topList = [];
        $bottomList = [];

        if (! $keyword) {
            $keyword = '###';
        }

        foreach ($this->getVerifiedIdentities() as $item) {
            // check if email
            if (extract_email($item) !== null) {
                $email = extract_email($item);
                if (str_starts_with(strtolower($email), $keyword)) {
                    $topList[] = [
                        'text' => extract_name($item),
                        'value' => $email,
                        'desc' => str_replace($keyword, '<span class="text-semibold text-primary"><strong>'.$keyword.'</strong></span>', $email),
                    ];
                } else {
                    $bottomList[] = [
                        'text' => extract_name($item),
                        'value' => $email,
                        'desc' => $email,
                    ];
                }
            } else { // domains are alse
                $dKey = explode('@', $keyword);
                $eKey = $dKey[0];
                $dKey = $dKey[1] ?? null;
                // if ( (!isset($dKey) || $dKey == '') || ($dKey && strpos(strtolower($item), $dKey) === 0 )) {
                if ($keyword == '###') {
                    $eKey = '****';
                }
                $topList[] = [
                    'text' => $eKey.'@'.str_replace($dKey, '<span class="text-semibold text-primary"><strong>'.$dKey.'</strong></span>', $item),
                    'subfix' => $item,
                    'desc' => null,
                ];
                // }
            }
        }

        $droplist = [...$topList, ...$bottomList];

        return $droplist;
    }

    public function getCurrentSubscription()
    {
        return $this->subscription;
    }

    /**
     * Count customer automation2s.
     */
    public function automation2sCount(): \number
    {
        return $this->automation2s()->count();
    }

    /**
     * Assign plan to customer.
     */
    public function assignPlan($plan)
    {
        $subscription = $this->subscription;
        // create tmp subscription
        if (! $subscription) {
            $subscription = new Subscription();
            $subscription->customer_id = $this->id;
        }

        // cancel all pending invoices (new)
        $subscription->invoices()->new()->delete();

        // reset value
        $subscription->plan_id = $plan->id;
        $subscription->current_period_ends_at = null;
        $subscription->ends_at = null;
        $subscription->started_at = null;
        $subscription->status = Subscription::STATUS_NEW;
        $subscription->save();

        // create init invoice
        $subscription->createInitInvoice();

        return $subscription;
    }

    /**
     * Check customer if has notice.
     */
    public function hasSubscriptionNotice(): bool
    {
        $subscription = $this->subscription;

        if (! $subscription) {
            return false;
        }

        if ($subscription->getUnpaidInvoice()) {
            return true;
        }

        return false;
    }

    public function isSubscriptionActive()
    {
        $subscription = $this->activeSubscription();
        if (is_null($subscription)) {
            return false;
        }

        return $subscription->isActive();
    }

    public function getLockPath($path)
    {
        return $this->user->getLockPath($path);
    }

    public function allowUnverifiedFromEmailAddress()
    {
        $plan = $this->subscription->plan;

        if (! $plan) {
            return false;
        }

        if ($plan->useSystemSendingServer() && is_object($plan->primarySendingServer())) {
            return $plan->primarySendingServer()->allowUnverifiedFromEmailAddress();
        }

        return true;
    }

    /**
     * Get current period ends at.
     */
    public function getCurrentPeriodEndsAt()
    {
        return (is_object($this->subscription) && $this->subscription->isActive()) ? $this->current_period_ends_at : null;
    }

    /**
     * Get list of available tracking domain options.
     *
     * @var bool
     */
    public function getVerifiedTrackingDomainOptions()
    {
        return $this->trackingDomains()->verified()->get()->map(fn($domain) => ['value' => $domain->uid, 'text' => $domain->name]);
    }

    // Get the current time in Customer timezone
    public function getCurrentTime()
    {
        $dt = $this->parseDateTime(null);

        return $dt;
    }

    public function parseDateTime($datetime, $fallback = false)
    {
        // IMPORTANT: datetime string must NOT contain timezone information
        try {
            $dt = Carbon::parse($datetime, $this->timezone);
            $dt = $dt->timezone($this->timezone);
        } catch (\Exception $ex) {
            // parse special chars, for example ('sadfh&#($783943') ==> exception
            if ($fallback) {
                $dt = $this->parseDateTime('1900-01-01');
            } else {
                throw $ex;
            }
        }

        return $dt;
    }

    /**
     * Get customer product source.
     *
     * @var collect
     */
    public function newProductSource($type): object
    {
        $class = '\ \App\\Model\\'.$type;
        $source = new $class();
        $source->customer_id = $this->id;
        $source->type = $type;

        return $source;
    }

    /**
     * Get customer product source.
     *
     * @var collect
     */
    public function findProductSource($type)
    {
        $source = $this->sources()
            ->where('type', '=', $type)
            ->first();

        if (! $source) {
            $source = new Source();
            $source->customer_id = $this->id;
            $source->type = $type;
        }

        return $source;
    }

    /**
     * Get source select options.
     */
    public function getSelectOptions($type = null): object
    {
        $query = $this->sources();

        if ($type) {
            $query = $query->where('type', '=', $type);
        }

        return $query->get()->map(fn($source) => ['text' => $source->getData()['data']['name'], 'value' => $source->uid]);
    }

    /**
     * Get payment method.
     *
     * @var object | collect
     */
    public function getPreferredPaymentGateway()
    {
        if (! $this['payment_method']) {
            return null;
        }

        // For example, the user preferred method is "paddle", but paddle is no longer available
        // Then simply return null
        // So, in the web UI, this user does not have a preferred payment method
        $meta = json_decode($this['payment_method'], true);

        if (! array_key_exists('method', $meta)) {
            throw new Exception("The 'method' key is required for 'preferred payment' data");
        }

        $type = $meta['method'];

        if (Billing::isGatewayTypeRegistered($type)) {
            return Billing::getGateway($type);
        } else {
            // payment method was previously stored but it is dropped in the current version
            return null;
        }
    }

    /**
     * Update payment method.
     *
     * @var object | collect
     */
    public function updatePaymentMethod($data = []): void
    {
        // $paymentMethod = $this->getPaymentMethod();

        // if (!isset($paymentMethod)) {
        //     $paymentMethod = [];
        // }

        // $data = (object) [...(array) $paymentMethod, ...$data];
        $this['payment_method'] = json_encode($data);

        $this->save();
    }

    /**
     * Update payment method.
     *
     * @var object | collect
     */
    public function updatePaymentMethodData($data = []): void
    {
        $paymentMethod = $this->getPaymentMethod();

        if (! isset($paymentMethod)) {
            $paymentMethod = [];
        }

        $data = (object) [...(array) $paymentMethod, ...$data];
        $this['payment_method'] = json_encode($data);

        $this->save();
    }

    /**
     * Remove payment method.
     *
     * @var object | collect
     */
    public function removePaymentMethod(): void
    {
        $this->payment_method = null;
        $this->save();
    }

    /**
     * Get default billing address.
     *
     * @var object
     */
    public function newBillingAddress(): \App\Model\BillingAddress
    {
        $address = new \App\Model\BillingAddress();
        $address->customer_id = $this->id;

        return $address;
    }

    /**
     * Get default billing address.
     *
     * @var object
     */
    public function getDefaultBillingAddress()
    {
        return $this->billingAddresses()->first();
    }

    public function createAbandonedEmailAutomation($store)
    {
        $auto = $this->automation2s()->create([
            'name' => 'Abandoned Cart Notification - Auto',
            'mail_list_id' => $store->mail_list_id,
            'status' => 'inactive',
        ]);

        $email = $auto->emails()->create();
        $auto->data = json_encode([
            [
                'title' => 'Abandoned Cart Reminder',
                'id' => 'trigger',
                'type' => 'ElementTrigger',
                'child' => '1000000001',
                'options' => [
                    'key' => 'woo-abandoned-cart',
                    'type' => 'woo-abandoned-cart',
                    'source_uid' => '6076f03620408',
                    'wait' => '24_hour',
                    'init' => 'true',
                ],
            ], [
                'title' => 'Hey, you have an item left in cart',
                'id' => '1000000001',
                'type' => 'ElementAction',
                'child' => null,
                'options' => [
                    'init' => 'true',
                    'email_uid' => $email->uid,
                ],
            ],
        ]);

        $email->save();
        $auto->save();

        return $email;
    }

    public function getAbandonedEmailAutomation($store)
    {
        $auto = $this->automation2s()->where('mail_list_id', '=', $store->mail_list_id)->first();
        if (! $auto) {
            $auto = $this->createAbandonedEmailAutomation($store);
        }

        return $auto;
    }

    /**
     * Get auto billing data.
     *
     * @var object
     */
    public function getAutoBillingData(): ? \App\Library\AutoBillingData
    {
        if ($this->auto_billing_data == null) {
            return null;
        }

        return AutoBillingData::fromJson($this->auto_billing_data);
    }

    /**
     * Get auto billing data.
     *
     * @var object
     */
    public function setAutoBillingData(AutoBillingData $autoBillingData): void
    {
        $this->auto_billing_data = $autoBillingData->toJson();
        $this->save();
    }

    // Do not call delete() directly on Customer
    // This method helps delete Customer account but KEEP
    // the associated User account if it is also associated with an Admin
    public function deleteAccount(): void
    {
        DB::transaction(function () {
            $user = $this->user;
            $this->delete();
            if (! $user->admin()->exists()) {
                $user->deleteAndCleanup();
            }
        });
    }

    public function copyTemplateAs(Template $template, $name): \App\Model\Template
    {
        $copy = $template->copy([
            'name' => $name,
            'customer_id' => $this->id,
        ]);

        return $copy;
    }

    /**
     * Get builder templates.
     *
     * @return mixed
     */
    public function getBuilderTemplates(): array
    {
        $result = [];

        // Gallery
        $templates = \App\Model\Template::where('customer_id', '=', null)
            ->orWhere('customer_id', '=', $this->id)
            ->oldest('customer_id')
            ->get();

        foreach ($templates as $template) {
            $result[] = [
                'name' => $template->name,
                'url' => action([\App\Http\Controllers\CampaignController::class, 'templateChangeTemplate'], ['uid' => $this->uid, 'template_uid' => $template->uid]),
                'thumbnail' => $template->getThumbUrl(),
            ];
        }

        return $result;
    }

    public function preferredPaymentGatewayCanAutoCharge(): bool
    {
        // check auto billing data
        if ($this->getPreferredPaymentGateway() == null) {
            return false;
        }

        // support auto billing
        if (! $this->getPreferredPaymentGateway()->supportsAutoBilling()) {
            return false;
        }

        // no autobilling data
        if ($this->getAutoBillingData() == null) {
            return false;
        }

        // check payment
        if ($this->getPreferredPaymentGateway()->getType() != $this->getAutoBillingData()->getGateway()->getType()) {
            return false;
        }

        return true;
    }

    public function importBlacklistJobs()
    {
        return $this->jobMonitors()->latest('job_monitors.id')->where('job_type', ImportBlacklistJob::class);
    }
}
