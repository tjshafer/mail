<?php

/**
 * Field class.
 *
 * Model class for List's custom fields
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mail_list_id', 'type', 'label', 'tag', 'default_value', 'visible', 'required',
    ];

    /**
     * The rules for validation.
     *
     * @var array
     */
    public static $fields_rules = [

    ];

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function mailList(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\MailList::class);
    }

    public function fieldOptions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\FieldOption::class);
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;
        });
    }

    /**
     * Format string to field tag.
     *
     * @var string
     */
    public static function formatTag($string): string
    {
        return strtoupper(preg_replace('/[^0-9a-zA-Z_]/m', '', $string));
    }

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Get select options.
     */
    public function getSelectOptions(): array
    {
        $options = $this->fieldOptions->map(fn($item) => ['value' => $item->value, 'text' => $item->label]);

        return $options;
    }

    /**
     * Get control name.
     *
     * @return array
     */
    public static function getControlNameByType($type)
    {
        if ($type == 'date') {
            return 'date';
        } elseif ($type == 'datetime') {
            return 'datetime';
        }

        return 'text';
    }
}
