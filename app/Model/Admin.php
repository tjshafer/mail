<?php

namespace App\Model;

use App\Jobs\ImportBlacklistJob;
use App\Library\Traits\TrackJobs;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use TrackJobs;

    const STATUS_ACTIVE = 'active';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'timezone', 'language_id', 'color_scheme', 'text_direction',
    ];

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function contact(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Contact::class);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\User::class);
    }

    public function adminGroup(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\AdminGroup::class);
    }

    public function customers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Customer::class);
    }

    public function templates(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Template::class);
    }

    public function language(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Language::class);
    }

    public function creator(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\User::class, 'creator_id');
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            $item->uid = uniqid();
        });
    }

    /**
     * Check if admin has customer account.
     */
    public function hasCustomerAccount(): bool
    {
        return is_object($this->user) && is_object($this->user->customer);
    }

    /**
     * Get all items.
     */
    public static function getAll(): \collect
    {
        return self::select('*');
    }

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $query = self::select('admins.*')
                        ->join('users', 'users.id', '=', 'admins.user_id')
                        ->leftJoin('admin_groups', 'admin_groups.id', '=', 'admins.admin_group_id');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('users.first_name', 'like', '%'.$keyword.'%')
                        ->orWhere('admin_groups.name', 'like', '%'.$keyword.'%')
                        ->orWhere('users.last_name', 'like', '%'.$keyword.'%');
                });
            }
        }

        // filters
        $filters = $request->filters;
        if (! empty($filters)) {
            if (! empty($filters['admin_group_id'])) {
                $query = $query->where('admins.admin_group_id', '=', $filters['admin_group_id']);
            }
        }

        if (! empty($request->creator_id)) {
            $query = $query->where('admins.creator_id', '=', $request->creator_id);
        }

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * Get admin setting.
     */
    public function getOption($name): string
    {
        return $this->adminGroup->getOption($name);
    }

    /**
     * Get admin permission.
     */
    public function getPermission($name): string
    {
        return $this->adminGroup->getPermission($name);
    }

    /**
     * Get user's color scheme.
     *
     * @return string
     */
    public function getColorScheme()
    {
        if (! empty($this->color_scheme)) {
            return $this->color_scheme;
        } else {
            return \App\Model\Setting::get('backend_scheme');
        }
    }

    /**
     * Color array.
     */
    public static function colors($default): array
    {
        return [
            ['value' => '', 'text' => trans('messages.system_default')],
            ['value' => 'blue', 'text' => trans('messages.blue')],
            ['value' => 'green', 'text' => trans('messages.green')],
            ['value' => 'brown', 'text' => trans('messages.brown')],
            ['value' => 'pink', 'text' => trans('messages.pink')],
            ['value' => 'grey', 'text' => trans('messages.grey')],
            ['value' => 'white', 'text' => trans('messages.white')],
        ];
    }

    /**
     * Disable admin.
     */
    public function disable(): bool
    {
        $this->status = 'inactive';

        return $this->save();
    }

    /**
     * Enable admin.
     */
    public function enable(): bool
    {
        $this->status = 'active';

        return $this->save();
    }

    /**
     * Get recent resellers.
     */
    public function getAllCustomers(): \collect
    {
        $query = \App\Model\Customer::getAll();

        if (! $this->user->can('readAll', new \App\Model\Customer())) {
            $query = $query->where('customers.admin_id', '=', $this->id);
        }

        return $query;
    }

    /**
     * Get recent resellers.
     */
    public function recentCustomers(): \collect
    {
        return $this->getAllCustomers()->latest()->limit(5)->get();
    }

    /**
     * Get all admin's subcriptions.
     */
    public function getAllSubscriptions(): \collect
    {
        if ($this->user->can('readAll', new \App\Model\Customer())) {
            $query = Subscription::select('subscriptions.*')->leftJoin('customers', 'customers.id', '=', 'subscriptions.customer_id');
        } else {
            $query = Subscription::select('subscriptions.*')
                ->join('customers', 'customers.id', '=', 'subscriptions.customer_id')
                ->where('customers.admin_id', '=', $this->id);
            /* ERROR
            $query = $query->where(function ($q) {
                $q->orwhere('customers.admin_id', '=', $this->id)
                    ->orWhere('subscriptions.admin_id', '=', $this->id);
            });
            */
        }

        return $query;
    }

    /**
     * Get subscription notification count.
     *
     * @return collect
     */
    public function subscriptionNotificationCount()
    {
        $query = $this->getAllSubscriptions()
            ->where('subscriptions.ends_at', '>=', \Illuminate\Support\Carbon::now()->endOfDay())
            ->count();

        return $query == 0 ? '' : $query;
    }

    /**
     * Get recent subscriptions.
     */
    public function recentSubscriptions($number = 5): \collect
    {
        $query = $this->getAllSubscriptions()
            ->whereNull('ends_at')->orWhere('ends_at', '>=', \Illuminate\Support\Carbon::now())
            ->latest('subscriptions.created_at')->limit($number);

        return $query->get();
    }

    /**
     * Get admin language code.
     */
    public function getLanguageCode(): string
    {
        return is_object($this->language) ? $this->language->code : null;
    }

    /**
     * Get customer language code.
     *
     * @return string
     */
    public function getLanguageCodeFull()
    {
        $region_code = $this->language->region_code ? strtoupper($this->language->region_code) : strtoupper($this->language->code);

        return is_object($this->language) ? ($this->language->code.'-'.$region_code) : null;
    }

    /**
     * Get admin logs of their customers.
     */
    public function getLogs(): string
    {
        $query = \App\Model\Log::select('logs.*')->join('customers', 'customers.id', '=', 'logs.customer_id')
            ->leftJoin('admins', 'admins.id', '=', 'customers.admin_id');

        if (! $this->user->can('readAll', new \App\Model\Customer())) {
            $query = $query->where('admins.id', '=', $this->id);
        }

        return $query;
    }

    /**
     * Create customer account.
     */
    public function createCustomerAccount(): \App\Model\Customer
    {
        $customer = new \App\Model\Customer();
        $customer->admin_id = $this->id;
        $customer->language_id = $this->language_id;
        // [moved] $customer->first_name = $this->first_name;
        // [moved] $customer->last_name = $this->last_name;
        $customer->timezone = $this->timezone;
        $customer->status = $this->status;
        $customer->save();

        return $customer;
    }

    /**
     * Check if customer is disabled.
     */
    public function isActive(): bool
    {
        return $this->status == Customer::STATUS_ACTIVE;
    }

    /**
     * Custom can for admin.
     *
     * @return bool
     */
    public function can($action, $item = null)
    {
        if ($item) {
            return $this->user->can($action, [$item, 'admin']);
        } else {
            return $this->user->can($action, ['admin']);
        }
    }

    /**
     * Destroy admin.
     */
    public function deleteAccount(): void
    {
        // unset all customers
        $this->customers()->update(['admin_id' => null]);

        // Delete admin and user
        $user = $this->user;

        $this->delete();

        if (! $user->customer()->exists()) {
            $user->deleteAndCleanup();
        }
    }

    /**
     * Get all subscription count by plan.
     */
    public function getAllSubscriptionsByPlan($plan): int
    {
        return $this->getAllSubscriptions()->where('subscriptions.plan_id', '=', $plan->id);
    }

    /**
     * Get all plans.
     */
    public function getAllPlans(): int
    {
        return \App\Model\Plan::active();
    }

    /**
     * Get all admin.
     */
    public function getAllAdmins(): int
    {
        $query = \App\Model\Admin::getAll()
            ->where('admins.status', '=', \App\Model\Admin::STATUS_ACTIVE);

        if (! $this->can('readAll', new \App\Model\Admin())) {
            $query = $query->where('admins.creator_id', '=', $this->user_id);
        }

        return $query;
    }

    /**
     * Get all admin.
     */
    public function getAllAdminGroups(): int
    {
        $query = \App\Model\AdminGroup::getAll();

        if (! $this->can('readAll', new \App\Model\AdminGroup())) {
            $query = $query->where('admin_groups.creator_id', '=', $this->user_id);
        }

        return $query;
    }

    /**
     * Get all sending servers.
     */
    public function getAllSendingServers(): int
    {
        $query = \App\Model\SendingServer::getAll();

        if (! $this->can('readAll', new \App\Model\SendingServer())) {
            $query = $query->where('sending_servers.admin_id', '=', $this->id);
        }

        // remove customer sending servers
        $query = $query->whereNull('customer_id');

        return $query;
    }

    /**
     * Get all sending servers.
     */
    public function getAllSendingDomains(): int
    {
        $query = \App\Model\SendingDomain::getAll();

        if (! $this->can('readAll', new \App\Model\SendingDomain())) {
            $query = $query->where('sending_domains.admin_id', '=', $this->id);
        }

        // remove customer sending servers
        $query = $query->whereNull('customer_id');

        return $query;
    }

    /**
     * Get all campaigns.
     */
    public function getAllCampaigns(): \collect
    {
        $query = \App\Model\Campaign::getAll();

        if (! $this->can('readAll', new \App\Model\Customer())) {
            $query = $query->leftJoin('customers', 'customers.id', '=', 'campaigns.customer_id')
                ->where('customers.admin_id', '=', $this->id);
        }

        return $query;
    }

    /**
     * Get all lists.
     */
    public function getAllLists(): \collect
    {
        $query = \App\Model\MailList::getAll();

        if (! $this->can('readAll', new \App\Model\Customer())) {
            $query = $query->leftJoin('customers', 'customers.id', '=', 'mail_lists.customer_id')
                ->where('customers.admin_id', '=', $this->id);
        }

        return $query;
    }

    /**
     * Get sub-account sending servers.
     */
    public function getSubaccountSendingServers(): int
    {
        $query = $this->getAllSendingServers();

        $query = $query->whereIn('type', \App\Model\SendingServer::getSubAccountTypes());

        return $query;
    }

    /**
     * Get sub-account sending servers options.
     */
    public function getSubaccountSendingServersSelectOptions(): array
    {
        $options = [];

        foreach ($this->getSubaccountSendingServers()->get() as $server) {
            $options[] = ['value' => $server->uid, 'text' => $server->name];
        }

        return $options;
    }

    /**
     * Get system notification.
     */
    public function notifications(): int
    {
        return Notification::latest();
    }

    public function importBlacklistJobs()
    {
        return $this->jobMonitors()->latest('job_monitors.id')->where('job_type', ImportBlacklistJob::class);
    }
}
