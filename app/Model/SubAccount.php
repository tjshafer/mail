<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubAccount extends Model
{
    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating resource.
        static::creating(function ($account) {
            // Create new uid
            $uid = uniqid();
            $account->uid = $uid;
        });
    }

    /**
     * Find resource by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function sendingServer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\SendingServer::class);
    }

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( \App\Model\Customer::class);
    }

    public function subscriptions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Subscription::class, 'sub_account_id', 'id');
    }

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $user = $request->user();
        $query = self::select('sub_accounts.*');
        $query = $query->leftJoin('sending_servers', 'sending_servers.id', '=', 'sub_accounts.sending_server_id');
        $query = $query->leftJoin('customers', 'customers.id', '=', 'sub_accounts.customer_id');

        // Keyword
        if (! empty(trim($request->keyword))) {
            foreach (explode(' ', trim($request->keyword)) as $keyword) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->orwhere('sub_accounts.username', 'like', '%'.$keyword.'%')
                        ->orwhere('sending_servers.username', 'like', '%'.$keyword.'%')
                        ->orwhere('customers.username', 'like', '%'.$keyword.'%');
                });
            }
        }

        if (! empty($request->admin_id)) {
            $query = $query->where('sending_servers.admin_id', '=', $request->admin_id);
        }

        // filters
        $filters = $request->filters;
        if (! empty($filters)) {
            if (! empty($filters['type'])) {
                $query = $query->where('sending_servers.type', '=', $filters['type']);
            }
        }

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        if (! empty($request->sort_order)) {
            $query = $query->orderBy($request->sort_order, $request->sort_direction);
        }

        return $query;
    }

    /**
     * Get secured api key.
     *
     * @return collect
     */
    public function getSecuredApiKey(): string
    {
        return substr($this->api_key, 0, 4).'...'.substr($this->api_key, -4);
    }
}
