<?php

/**
 * Layout class.
 *
 * Model class for layouts
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (http://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://acellemail.com
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'alias', 'content', 'subject',
    ];

    /**
     * Items per page.
     *
     * @var array
     */
    public static $itemsPerPage = 25;

    /**
     * Find item by uid.
     */
    public static function findByUid($uid): object
    {
        return self::where('uid', '=', $uid)->first();
    }

    /**
     * Associations.
     *
     * @var object | collect
     */
    public function pages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany( \App\Model\Page::class);
    }

    /**
     * Bootstrap any application services.
     */
    public static function boot(): void
    {
        parent::boot();

        // Create uid when creating list.
        static::creating(function ($item) {
            // Create new uid
            $uid = uniqid();
            $item->uid = $uid;
        });
    }

    public function tags(): array
    {
        $tags = match ($this->alias) {
            'sign_up_form' => [
                ['name' => '{FIELDS}', 'required' => true],
                ['name' => '{SUBSCRIBE_BUTTON}', 'required' => true],
            ],
            'sign_up_thankyou_page' => [
            ],
            'sign_up_confirmation_email' => [
                ['name' => '{SUBSCRIBE_CONFIRM_URL}', 'required' => true],
            ],
            'sign_up_confirmation_thankyou' => [
            ],
            'sign_up_welcome_email' => [
                ['name' => '{UNSUBSCRIBE_URL}', 'required' => true],
            ],
            'unsubscribe_form' => [
                ['name' => '{EMAIL_FIELD}', 'required' => true],
                ['name' => '{UNSUBSCRIBE_BUTTON}', 'required' => true],
            ],
            'sign_up_confirmation_thankyou' => [
            ],
            'unsubscribe_success_page' => [
            ],
            'unsubscribe_goodbye_email' => [
            ],
            'profile_update_email_sent' => [
            ],
            'profile_update_email' => [
                ['name' => '{UPDATE_PROFILE_URL}', 'required' => true],
            ],
            'profile_update_form' => [
                ['name' => '{FIELDS}', 'required' => true],
                ['name' => '{UPDATE_PROFILE_BUTTON}', 'required' => true],
                ['name' => '{UNSUBSCRIBE_URL}', 'required' => true],
            ],
            'profile_update_success_page' => [
            ],
            default => [],
        };

        $tags = [...$tags, ...[
            ['name' => '{LIST_NAME}', 'required' => false],
            ['name' => '{CONTACT_NAME}', 'required' => false],
            ['name' => '{CONTACT_STATE}', 'required' => false],
            ['name' => '{CONTACT_ADDRESS_1}', 'required' => false],
            ['name' => '{CONTACT_ADDRESS_2}', 'required' => false],
            ['name' => '{CONTACT_CITY}', 'required' => false],
            ['name' => '{CONTACT_ZIP}', 'required' => false],
            ['name' => '{CONTACT_COUNTRY}', 'required' => false],
            ['name' => '{CONTACT_PHONE}', 'required' => false],
            ['name' => '{CONTACT_EMAIL}', 'required' => false],
            ['name' => '{CONTACT_URL}', 'required' => false],
        ]];

        return $tags;
    }

    /**
     * Get all items.
     */
    public static function getAll(): \collect
    {
        return self::select('*');
    }

    /**
     * Filter items.
     */
    public static function filter($request): \collect
    {
        $user = $request->user();
        $query = self::select('layouts.*');

        return $query;
    }

    /**
     * Search items.
     */
    public static function search($request): \collect
    {
        $query = self::filter($request);

        $query = $query->orderBy($request->sort_order, $request->sort_direction);

        return $query;
    }
}
