<?php

namespace App\Policies;

use App\Model\FeedbackLoopHandler;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FeedbackLoopHandlerPolicy
{
    use HandlesAuthorization;

    public function read(User $user, FeedbackLoopHandler $item): bool
    {
        $ability = $user->admin->getPermission('fbl_handler_read');
        $can = $ability == 'all'
                || ($ability == 'own' && $user->admin->id == $item->admin_id);

        return $can;
    }

    public function readAll(User $user, FeedbackLoopHandler $item): bool
    {
        $can = $user->admin->getPermission('fbl_handler_read') == 'all';

        return $can;
    }

    public function create(User $user, FeedbackLoopHandler $item): bool
    {
        $can = $user->admin->getPermission('fbl_handler_create') == 'yes';

        return $can;
    }

    public function update(User $user, FeedbackLoopHandler $item): bool
    {
        $ability = $user->admin->getPermission('fbl_handler_update');
        $can = $ability == 'all'
                || ($ability == 'own' && $user->admin->id == $item->admin_id);

        return $can;
    }

    public function delete(User $user, FeedbackLoopHandler $item): bool
    {
        $ability = $user->admin->getPermission('fbl_handler_delete');
        $can = $ability == 'all'
                || ($ability == 'own' && $user->admin->id == $item->admin_id);

        return $can;
    }

    public function test(User $user, FeedbackLoopHandler $item): bool
    {
        return $this->update($user, $item) || ! isset($item->id);
    }
}
