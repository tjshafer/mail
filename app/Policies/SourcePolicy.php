<?php

namespace App\Policies;

use App\Model\Source;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SourcePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any sources.
     *
     * @return mixed
     */
    public function viewAny(User $user): void
    {
        //
    }

    /**
     * Determine whether the user can view the source.
     *
     * @param   \App\Source  $source
     * @return mixed
     */
    public function view(User $user, Source $source): void
    {
        //
    }

    /**
     * Determine whether the user can create sources.
     *
     * @return mixed
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the source.
     *
     * @param   \App\Source  $source
     * @return mixed
     */
    public function update(User $user, Source $source): void
    {
        //
    }

    /**
     * Determine whether the user can delete the source.
     *
     * @param   \App\Source  $source
     * @return mixed
     */
    public function delete(User $user, Source $source): bool
    {
        return $user->customer->id == $source->customer_id;
    }

    /**
     * Determine whether the user can restore the source.
     *
     * @param   \App\Source  $source
     * @return mixed
     */
    public function restore(User $user, Source $source): void
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the source.
     *
     * @param   \App\Source  $source
     * @return mixed
     */
    public function forceDelete(User $user, Source $source): void
    {
        //
    }
}
