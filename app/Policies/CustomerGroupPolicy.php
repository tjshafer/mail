<?php

namespace App\Policies;

use App\Model\CustomerGroup;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerGroupPolicy
{
    use HandlesAuthorization;

    public function read(User $user, CustomerGroup $item): bool
    {
        $can = $user->admin->getPermission('customer_group_read') != 'no';

        return $can;
    }

    public function read_all(User $user, CustomerGroup $item): bool
    {
        $can = $user->admin->getPermission('customer_group_read') == 'all';

        return $can;
    }

    public function create(User $user, CustomerGroup $item): bool
    {
        $can = $user->admin->getPermission('customer_group_create') == 'yes';

        return $can;
    }

    public function update(User $user, CustomerGroup $item): bool
    {
        $ability = $user->admin->getPermission('customer_group_update');
        $can = $ability == 'all'
                || ($ability == 'own' && $user->admin->id == $item->admin_id);

        return $can;
    }

    public function sort(User $user, CustomerGroup $item): bool
    {
        $ability = $user->admin->getPermission('customer_group_update');
        $can = $ability == 'all'
                || ($ability == 'own' && $user->admin->id == $item->admin_id);

        return $can;
    }

    public function delete(User $user, CustomerGroup $item): bool
    {
        $ability = $user->admin->getPermission('customer_group_delete');
        $can = $ability == 'all'
                || ($ability == 'own' && $user->admin->id == $item->admin_id);

        return $can;
    }
}
