<?php

namespace App\Policies;

use App\Model\TrackingDomain;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TrackingDomainPolicy
{
    use HandlesAuthorization;

    public function read(User $user, TrackingDomain $item, $role): bool
    {
        return true;
    }

    public function create(User $user, TrackingDomain $item, $role): bool
    {
        return true;
    }

    public function update(User $user, TrackingDomain $item, $role): bool
    {
        return $user->customer->id == $item->customer_id;
    }

    public function delete(User $user, TrackingDomain $item, $role): bool
    {
        return $user->customer->id == $item->customer_id;
    }
}
