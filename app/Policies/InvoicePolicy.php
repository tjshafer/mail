<?php

namespace App\Policies;

use App\Model\Invoice;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoicePolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Invoice $invoice, $role)
    {
        $can = match ($role) {
            'admin' => $invoice->isNew(),
            'customer' => $invoice->isNew() && $invoice->customer_id == $user->customer->id,
            default => $can,
        };

        return $can;
    }
}
