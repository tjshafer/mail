<?php

namespace App\Policies;

use App\Model\Template;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TemplatePolicy
{
    use HandlesAuthorization;

    public function read(User $user, Template $item, $role)
    {
        $can = match ($role) {
            'admin' => $user->admin->getPermission('template_read') != 'no',
            'customer' => $user->customer->id == $item->customer_id || ! isset($item->customer_id),
            default => $can,
        };

        return $can;
    }

    public function create(User $user, $role)
    {
        $can = match ($role) {
            'admin' => $user->admin->getPermission('template_create') == 'yes',
            'customer' => true,
            default => $can,
        };

        return $can;
    }

    public function view(User $user, Template $item, $role)
    {
        switch ($role) {
            case 'admin':
                $ability = $user->admin->getPermission('template_read');
                $can = $ability == 'all'
                    || ($ability == 'own' && $user->admin->id == $item->admin_id);
                break;
            case 'customer':
                $can = $user->customer->id == $item->customer_id || ! isset($item->customer_id);
                break;
        }

        return $can;
    }

    public function update(User $user, Template $item, $role)
    {
        switch ($role) {
            case 'admin':
                $ability = $user->admin->getPermission('template_update');
                $can = $ability == 'all'
                    || ($ability == 'own' && $user->admin->id == $item->admin_id);
                break;
            case 'customer':
                $can = $user->customer->id == $item->customer_id;
                break;
        }

        return $can;
    }

    public function image(User $user, Template $item, $role)
    {
        switch ($role) {
            case 'admin':
                $ability = $user->admin->getPermission('template_read');
                $can = $ability == 'all'
                    || ($ability == 'own' && $user->admin->id == $item->admin_id);
                break;
            case 'customer':
                $can = $user->customer->id == $item->customer_id || ! isset($item->customer_id);
                break;
        }

        return $can;
    }

    public function delete(User $user, Template $item, $role)
    {
        switch ($role) {
            case 'admin':
                $ability = $user->admin->getPermission('template_delete');
                $can = $ability == 'all'
                    || ($ability == 'own');
                break;
            case 'customer':
                $can = $user->customer->id == $item->customer_id;
                break;
        }

        return $can;
    }

    public function preview(User $user, Template $item, $role)
    {
        switch ($role) {
            case 'admin':
                $ability = $user->admin->getPermission('template_read');
                $can = $ability == 'all'
                    || ($ability == 'own' && $user->admin->id == $item->admin_id);
                break;
            case 'customer':
                $can = $user->customer->id == $item->customer_id || ! isset($item->customer_id);
                break;
        }

        return $can;
    }

    public function copy(User $user, Template $item, $role)
    {
        switch ($role) {
            case 'admin':
                $ability = $user->admin->getPermission('template_update');
                $can = $ability == 'all'
                    || ($ability == 'own' && $user->admin->id == $item->admin_id);
                break;
            case 'customer':
                $can = $user->customer->id == $item->customer_id || ! isset($item->customer_id);
                break;
        }

        return $can;
    }
}
