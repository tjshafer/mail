<?php

namespace App\Policies;

use App\Model\Subscription;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubscriptionPolicy
{
    use HandlesAuthorization;

    public function approve(User $user, Subscription $subscription, $role)
    {
        $can = match ($role) {
            'admin' => $subscription->getUnpaidInvoice() &&
                $subscription->getUnpaidInvoice()->getPendingTransaction() &&
                $subscription->getUnpaidInvoice()->getPendingTransaction()->allowManualReview(),
            'customer' => false,
            default => $can,
        };

        return $can;
    }

    public function readAll(User $user, Subscription $subscription, $role)
    {
        $can = match ($role) {
            'admin' => $user->admin->getPermission('subscription_read') == 'all',
            'customer' => false,
            default => $can,
        };

        return $can;
    }

    public function read(User $user, Subscription $subscription, $role)
    {
        $can = match ($role) {
            'admin' => $user->admin->getPermission('subscription_read') != 'no',
            'customer' => ! $subscription->id || $user->customer->id == $subscription->customer_id,
            default => $can,
        };

        return $can;
    }

    public function cancel(User $user, Subscription $subscription, $role): bool
    {
        $can = $subscription->isActive() && $subscription->isRecurring();
        switch ($role) {
            case 'admin':
                break;
            case 'customer':
                break;
        }

        return $can;
    }

    public function resume(User $user, Subscription $subscription, $role): bool
    {
        $can = $subscription->isActive() && $subscription->cancelled() && ! $subscription->isEnded();
        switch ($role) {
            case 'admin':
                break;
            case 'customer':
                break;
        }

        return $can;
    }

    public function cancelNow(User $user, Subscription $subscription, $role): bool
    {
        $can = ! $subscription->isEnded();

        switch ($role) {
            case 'admin':
                break;
            case 'customer':
                break;
        }

        return $can;
    }

    public function invoices(User $user, Subscription $subscription, $role): bool
    {
        $can = isset($subscription);

        switch ($role) {
            case 'admin':
                break;
            case 'customer':
                break;
        }

        return $can;
    }

    public function changePlan(User $user, Subscription $subscription, $role)
    {
        $can = match ($role) {
            'admin' => false,
            'customer' => $subscription->isActive(),
            default => $can,
        };

        return $can;
    }

    public function rejectPending(User $user, Subscription $subscription, $role)
    {
        $can = match ($role) {
            'admin' => $subscription->getUnpaidInvoice() &&
                $subscription->getUnpaidInvoice()->getPendingTransaction() &&
                $subscription->getUnpaidInvoice()->getPendingTransaction()->allowManualReview(),
            'customer' => false,
            default => $can,
        };

        return $can;
    }

    public function delete(User $user, Subscription $subscription, $role): bool
    {
        $can = $subscription->isEnded();

        switch ($role) {
            case 'admin':
                break;
            case 'customer':
                $can = false;
                break;
        }

        return $can;
    }
}
