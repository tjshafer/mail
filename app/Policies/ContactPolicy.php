<?php

namespace App\Policies;

use App\Model\Contact;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Contact $item): bool
    {
        return ! isset($item->id) || $user->contact_id == $item->id;
    }
}
