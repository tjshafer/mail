<?php

use Illuminate\Support\Str;

/**
 * Globally available helper methods.
 *
 * LICENSE: This product includes software developed at
 * the App Co., Ltd. (http://Appmail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@Appmail.com>
 * @author     L. Pham <l.pham@Appmail.com>
 * @copyright  App Co., Ltd
 * @license    App Co., Ltd
 *
 * @version    1.0
 *
 * @link       http://Appmail.com
 */

/**
 * Get full table name by adding the DB prefix.
 *
 * @param string table name
 * @return string fulle table name with prefix
 */
function table($name): string
{
    return \DB::getTablePrefix().$name;
}

/**
 * Quote a value with astrophe to inject to an SQL statement.
 *
 * @param string original value
 * @return string quoted value
 *
 * @todo: use MySQL escape function to correctly escape string with astrophe
 */
function quote($value): string
{
    return "'$value'";
}

/**
 * Quote a value with astrophe to inject to an SQL statement.
 *
 * @param string original value
 * @return string quoted value
 *
 * @todo: use MySQL escape function to correctly escape string with astrophe
 */
function db_quote($value): string
{
    return \DB::connection()->getPdo()->quote($value);
}

/**
 * Break an array into smaller batches (arrays).
 *
 * @param array original array
 * @param int batch size
 * @param bool whether or not to skip the first header line
 * @param callable function
 */
function each_batch($array, $batchSize, $skipHeader, $callback)
{
    $batch = [];
    foreach ($array as $i => $value) {
        // skip the header
        if ($i == 0 && $skipHeader) {
            continue;
        }

        if ($i % $batchSize == 0) {
            $callback($batch);
            $batch = [];
        }
        $batch[] = $value;
    }

    // the last callback
    if (count($batch) > 0) {
        $callback($batch);
    }
}

/**
 * Join filesystem path strings.
 *
 * @param * parts of the path
 * @return string a full path
 */
function join_paths(): string
{
    $paths = [];
    foreach (func_get_args() as $arg) {
        throw_if(preg_match('/http:\/\//i', $arg), new \Exception('Path contains http://! Use `join_url` instead. Error for '.implode('/', func_get_args())));

        if ($arg !== '') {
            $paths[] = $arg;
        }
    }

    return preg_replace('#/+#', '/', implode('/', $paths));
}

/**
 * Join URL parts.
 *
 * @param * parts of the URL. Note that the first part should be something like http:// or http://host.name
 * @return string a full URL
 */
function join_url(): string
{
    $paths = [];
    foreach (func_get_args() as $arg) {
        if (! empty($arg)) {
            $paths[] = $arg;
        }
    }

    return preg_replace('#(?<=[^:])/+#', '/', implode('/', $paths));
}

/**
 * Get unique array based on user defined condition.
 *
 * @param array original array
 * @return array unique array
 */
function array_unique_by($array, $callback): array
{
    $result = [];
    foreach ($array as $value) {
        $key = $callback($value);
        $result[$key] = $value;
    }

    return array_values($result);
}

/**
 * Get UTC offset of a particular time zone.
 *
 * @param string timezone
 * @return string UTC offset (+02:00 for example)
 */
function utc_offset($timezone): string
{
    $offset = \Illuminate\Support\Carbon::now($timezone)->offsetHours - \Illuminate\Support\Carbon::now('UTC')->offsetHours;

    return sprintf("%+'03d:00", $offset);
}

/**
 * Check if exec() function is available.
 */
function exec_enabled(): bool
{
    try {
        // make a small test
        exec('ls');

        return function_exists('exec') && ! in_array('exec', array_map('trim', explode(', ', ini_get('disable_functions'))));
    } catch (\Exception $ex) {
        return false;
    }
}

function reset_app_url($force = false)
{ // replace if already exists
    // update .env file, set app_url to current host url

    // get .env file path
    $path = base_path('.env');
    $raw = preg_split('/[\r\n]+/', file_get_contents($path));

    // read from .env, load into $settings as [ key1 => value1, key2 => value2, etc. ]
    $settings = [];
    foreach ($raw as $e) {
        preg_match('/^(?<key>[A-Z0-9_]+)=(?<value>.*)/', $e, $matched);

        if (array_key_exists('key', $matched) && array_key_exists('value', $matched)) {
            $settings[$matched['key']] = $matched['value'];
        }
    }

    // add APP_URL setting if not exists
    if (! array_key_exists('APP_URL', $settings)) {
        $settings['APP_URL'] = url('/');
    } elseif ($force) {
        $settings['APP_URL'] = url('/');
    }

    // Write back to .env file
    $file = fopen($path, 'w');
    foreach ($settings as $key => $value) {
        fwrite($file, "{$key}=$value\n");
    }
    fclose($file);
}

/**
 * Run artisan migrate.
 */
function artisan_migrate(): bool
{
    \Artisan::call('migrate', ['--force' => true]);
}

/**
 * Check if site is in demo mod.
 */
function isSiteDemo(): bool
{
    return config('app.demo');
}

/**
 * Get language code.
 */
function language_code(): string
{
    // Get default language code from setting
    $default_language = \App\Model\Language::find( \App\Model\Setting::get('default_language'));

    if (isset($_COOKIE['last_language_code'])) {
        $language_code = $_COOKIE['last_language_code'];
    } elseif (is_object($default_language)) {
        $language_code = $default_language->code;
    } else {
        $language_code = 'en';
    }

    return $language_code;
}

/**
 * Get language code.
 */
function language(): string
{
    return \App\Model\Language::where('code', '=', language_code())->first();
}

/**
 * Format a number as percentage.
 */
function number_to_percentage($number, $precision = 2): string
{
    if (! is_numeric($number)) {
        return $number;
    }

    return sprintf("%.{$precision}f%%", $number * 100);
}

/**
 * Format a number with delimiter.
 */
function number_with_delimiter($number, $precision = 0, $seperator = ','): string
{
    if (! is_numeric($number)) {
        return $number;
    }

    return number_format($number, $precision, '.', $seperator);
}

/**
 * Function to convert IP address to IP number (IPv6).
 */
function Dot2LongIPv6($IPaddr): string
{
    $int = inet_pton($IPaddr);
    $bits = 15;
    $ipv6long = 0;
    while ($bits >= 0) {
        $bin = sprintf('%08b', (ord($int[$bits])));
        if ($ipv6long) {
            $ipv6long = $bin.$ipv6long;
        } else {
            $ipv6long = $bin;
        }
        $bits--;
    }
    $ipv6long = gmp_strval(gmp_init($ipv6long, 2), 10);

    return $ipv6long;
}

/**
 * Paginate a Laravel collection, used for service only as
 * it DOES NOT return a true Pagination object (for rendering to view).
 *
 * @input Laravel Query Builder
 * @input callback function
 *
 * @return collection result set
 */
function paginate($builder, $callback, $params = []): \collection
{
    $default = [
        'limit' => 10000,
    ];

    $params = [...$default, ...$params];

    // in some cases, use the pre-computed count rather than counting against the builder
    if (! array_key_exists('count', $params)) {
        $params['count'] = $builder->count();
    }

    // count the number of pages needed
    $pages = ceil($params['count'] / $params['limit']);

    for ($page = 1; $page <= $pages; $page += 1) {
        $offset = $params['limit'] * ($page - 1);

        $builder = $builder->skip($offset)->take($params['limit']);
        $callback($builder, $page);
    }
}

/**
 * Overwrite the Laravel's Builder#paginate, accept a $total parameter specifying the total number of records.
 *
 * @param  array  $columns
 * @param  string  $pageName
 * @param  int|null  $page
 */
function optimized_paginate($builder, int $perPage = 15, $columns = null, $pageName = null, $page = null, $total = null): \Illuminate\Contracts\Pagination\LengthAwarePaginator
{
    $pageName = $pageName ?: 'page';
    $page = $page ?: \Illuminate\Pagination\Paginator::resolveCurrentPage($pageName);
    $columns = $columns ?: ['*'];
    $total = is_null($total) ? $builder->getCountForPagination() : $total;
    // in case $total == 0
    $results = $total ? $builder->forPage($page, $perPage)->get($columns) : collect([]);

    return new \Illuminate\Pagination\LengthAwarePaginator($results, $total, $perPage, $page, [
        'path' => \Illuminate\Pagination\Paginator::resolveCurrentPath(),
        'pageName' => $pageName,
    ]);
}

/**
 * Distinct count helper for performance.
 */
function distinctCount($builder, $column = null, $method = 'group'): int
{
    $q = clone $builder;
    /*
     * There are 2 options to COUNT DISTINCT
     *   1. Use DISTINCT
     *   2. Use GROUP BY
     * Normally GROUP BY yields better performance (for example: 500,000 records, DISTINCT -> 7 seconds, GROUP BY -> 1.9 seconds)
     **/

    if (is_null($column)) {
        // just count it
    } elseif ($method == 'group') {
        $q->groupBy($column)->select($column);
    } elseif ($method == 'distinct') {
        $q->select($column)->distinct();
    }

    // Result
    $count = \DB::table(\DB::raw("({$q->toSql()}) as sub"))
        ->addBinding($q->getBindings()) // you need to get underlying Query Builder
        ->count();

    return $count;
}

/**
 * Measure execution time of a script.
 *
 * @return float execution time
 */
function measure($callback, $tests = 5): float
{
    $results = [];

    for ($i = 0; $i <= $tests; $i += 1) {
        $start = microtime(true);
        $callback();
        $time = microtime(true) - $start;
        $results[] = $time;
    }
    $agv = array_sum($results) / count($results);
    echo "$agv\n";

    return $agv;
}

/**
 * Check if function is enabled.
 */
function func_enabled($name): bool
{
    try {
        $disabled = explode(',', ini_get('disable_functions'));

        return ! in_array($name, $disabled);
    } catch (\Exception $ex) {
        return false;
    }
}

/**
 * Get the current application version.
 *
 * @return string version
 */
function app_version(): string
{
    return trim(file_get_contents(base_path('VERSION')));
}

/**
 * Extract email from a string
 * For example: get abc@mail.com from "My Name <abc@mail.com>".
 *
 * @return string version
 */
function extract_email($str): string
{
    preg_match("/(?<email>[-0-9a-zA-Z\.+_]+@[-0-9a-zA-Z\.+_]+\.[a-zA-Z]+)/", $str, $matched);
    if (array_key_exists('email', $matched)) {
        return $matched['email'];
    } else {
        return '';
    }
}

/**
 * Extract name from a string
 * For example: get abc@mail.com from "My Name <abc@mail.com>".
 *
 * @return string version
 */
function extract_name($str): string
{
    $parts = explode('<', $str);
    if (count($parts) > 1) {
        return trim($parts[0]);
    }
    $parts = explode('@', extract_email($str));

    return $parts[0];
}

/**
 * Extract domain from an email
 * For example: get mail.com from "My Name <abc@mail.com>".
 *
 * @return string version
 */
function extract_domain($email): string
{
    $email = extract_email($email);
    $domain = substr(strrchr($email, '@'), 1);

    return $domain;
}

/**
 * Doublequote a string.
 */
function doublequote($str): string
{
    return sprintf('"%s"', preg_replace('/^"+|"+$/', '', $str));
}

function jsonGet($array, $path)
{
    $jsonObject = new \JsonPath\JsonObject($array);
    $result = $jsonObject->get($path)[0];

    return $result;
}

/**
 * Format price.
 *
 * @param string
 */
function format_number($number): string
{
    if (is_numeric($number) && floor($number) != $number) {
        return number_format($number, 2, trans('messages.dec_point'), trans('messages.thousands_sep'));
    } elseif (is_numeric($number)) {
        return number_format($number, 0, trans('messages.dec_point'), trans('messages.thousands_sep'));
    } else {
        return $number;
    }
}
/**
 * Format price.
 *
 * @param string
 */
function format_price($price, $format = '{PRICE}'): string
{
    return str_replace('{PRICE}', format_number($price), $format);
}

/**
 * Check if the app is initiated.
 */
function isInitiated(): bool
{
    return file_exists(storage_path('app/installed'));
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2).' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2).' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2).' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes.' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes.' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}

/**
 * Get random item from array.
 */
function rand_item($arr): object
{
    return $arr[array_rand($arr)];
}

/**
 * Return date string.
 */
function toDateString($datetime): object
{
    return  \App\Library\Tool::dateTime($datetime)->format('Y-m-d');
}

/**
 * Return time string.
 */
function toTimeString($datetime): object
{
    return  \App\Library\Tool::dateTime($datetime)->format('h:i A');
}

/**
 * Check if string is email.
 */
function checkEmail($email): object
{
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}

function tinyDocTypeTransform($content)
{
    if (stripos(strtolower($content), '<!doctype') === false) {
        $doctype = '<!DoCtYPe html>';
    } else {
        $doctype = '';
    }

    return $doctype.$content;
}

function demo_auth()
{
    $auth = \App\Model\User::getAuthenticateFromFile();

    return [
        'email' => $auth['email'] ?? '',
        'password' => $auth['password'] ? $auth['password'] : '',
    ];
}

function get_app_identity()
{
    return md5(config('app.key'));
}

function quoteDotEnvValue($value)
{
    $containsSharp = (Str::of($value)->contains('#'));

    if ($containsSharp) {
        $value = str_replace('"', '\"', $value);
        $value = '"'.$value.'"';
    }

    return $value;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

/**
 * Strip Tags Only
 *
 * Just like strip_tags, but only removes the HTML tags specified and not all of
 * them.
 *
 * @param  string  $text The text to strip the tags from.
 * @param  string|array  $allowedTags This can either be one tag (eg. 'p') or an
 *     array, (eg. ['p','br','h1']).
 * @return string The text with the mentioned tags stripped.
 *
 * @author Aalaap Ghag <aalaap@gmail.com>
 */
function strip_tags_only(string $text, string|array $allowedTags = []): string
{
    if (! is_array($allowedTags)) {
        $allowedTags = [
            $allowedTags,
        ];
    }

    array_map(
        function ($allowedTag) use (&$text) {
            $regEx = '#<'.$allowedTag.'.*?>(.*?)</'.$allowedTag.'>#is';
            $text = preg_replace($regEx, '', $text);
        },
        $allowedTags
    );

    return $text;
}

/**
 * Get controller action name
 **/
function controllerAction()
{
    // GET FROM SCREEN OPTION
    $controller = explode('\\', request()->route()->getAction()['controller']);

    return $controller[count($controller) - 1];
}

/**
 * Get controller name
 **/
function controllerName()
{
    $controllerAction = controllerAction();

    return explode('@', $controllerAction)[0];
}

/**
 * Get action name
 **/
function actionName()
{
    $controllerAction = controllerAction();

    return explode('@', $controllerAction)[1];
}

/*
 *  Iterate through a Eloquent $query using cursor paginate
 *  The $orderBy parameter is critically required for a cursor pagination
 */
function cursorIterate($query, $orderBy, $size, $callback)
{
    $cursor = null;
    $page = 1;
    do {
        // The 4th parameter contains the offset cursor
        $list = $query->orderBy($orderBy)->cursorPaginate($size, ['*'], 'cursor', $cursor);
        $callback($list->items(), $page);
        $cursor = $list->nextCursor();
        $page += 1;
    } while ($list->hasMorePages());
}
