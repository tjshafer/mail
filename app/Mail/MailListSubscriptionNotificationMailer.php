<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailListSubscriptionNotificationMailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(protected $subscriber)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject($this->subject)
                    ->from(config('mail.from')['address'], config('mail.from')['name'])
                    ->markdown('emails.mail_list_subscription_notification_email')
                    ->with(['subscriber' => $this->subscriber]);
    }
}
