<?php

namespace App\Mail;

use App\Model\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscriptionDoneMailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        /**
         * The subscription instance.
         */
        protected Subscription $subscription
    )
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject(trans('messages.subscription_done.email_subject'))
            ->view('account.subscription.email.subscription_done_'. \App\Model\Setting::get('system.payment_gateway'))
            ->with([
                'customerName' => $this->subscription->user->displayName(),
                'planName' => $this->subscription->plan->name,
                'link' => action([\App\Http\Controllers\AccountSubscriptionController::class, 'index']),
            ]);
    }
}
