<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class AdminLoggedIn extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(protected $admin = null)
    {
    }

    /**
     * Get the channels the event should be broadcast on.
     */
    public function broadcastOn(): array
    {
        return [];
    }
}
