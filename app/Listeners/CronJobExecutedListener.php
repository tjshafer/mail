<?php

namespace App\Listeners;

use Acelle\Events\CronJobExecuted;
use App\Model\Setting;

class CronJobExecutedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(CronJobExecuted $event): void
    {
        Setting::set('cronjob_last_execution', \Illuminate\Support\Carbon::now()->timestamp);
    }
}
