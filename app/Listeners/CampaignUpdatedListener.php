<?php

namespace App\Listeners;

use Acelle\Events\CampaignUpdated;
use Acelle\Jobs\UpdateCampaignJob;

class CampaignUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(CampaignUpdated $event): void
    {
        if ($event->delayed) {
            dispatch(new UpdateCampaignJob($event->campaign));
        } else {
            // @deprecated
            $event->campaign->updateCache();
        }
    }
}
