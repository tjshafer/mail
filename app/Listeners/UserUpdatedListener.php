<?php

namespace App\Listeners;

use Acelle\Events\UserUpdated;

class UserUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(UserUpdated $event): void
    {
        dispatch(new  \App\Jobs\UpdateUserJob($event->customer));
    }
}
