<?php

namespace App\Listeners;

use Acelle\Events\MailListImported;
use App\Model\Automation2;
use App\Model\Setting;

class TriggerAutomationForImportedContacts
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(MailListImported $event): void
    {
        $trigger = Setting::isYes('automation.trigger_imported_contacts');

        $automations = $event->list->automations;
        foreach ($automations as $auto) {
            if ($auto->getTriggerType() != Automation2::TRIGGER_TYPE_WELCOME_NEW_SUBSCRIBER) {
                continue;
            }

            if (! $trigger) {
                $auto->logger()->warning('Do not trigger automation for imported contacts');

                continue;
            }

            if (! $auto->isActive()) {
                $auto->logger()->warning('Automation is INACTIVE');

                continue;
            }

            $auto->triggerImportedContacts();
        }
    }
}
