<?php

namespace App\Listeners;

use Acelle\Events\MailListSubscription;
use Acelle\Events\MailListUnsubscription;
use Acelle\Mail\MailListSubscriptionNotificationMailer;
use App\Model\Setting;
use Illuminate\Support\Facades\Mail;

class SendListNotificationToOwner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handleMailListSubscription(MailListSubscription $event): void
    {
        $subscriber = $event->subscriber;
        $user = $subscriber->mailList->customer->user;

        if (Setting::isYes('send_notification_email_for_list_subscription')) {
            // Send notification
            Mail::to(
                json_decode(json_encode(['email' => $user->email, 'name' => $user->displayName()]))
            )->send(
                new MailListSubscriptionNotificationMailer($subscriber)
            );
        }
    }

    /**
     * Handle the event.
     *
     * @param  MailListSubscription  $event
     */
    public function handleMailListUnsubscription(MailListUnsubscription $event): void
    {
        $subscriber = $event->subscriber;
        $list = $subscriber->mailList;

        if (Setting::isYes('send_notification_email_for_list_subscription')) {
            $list->sendUnsubscriptionNotificationEmailToListOwner($subscriber);
        }
    }

    /**
     * Handle the event.
     *
     * @param  AdminLoggedIn  $event
     */
    public function subscribe($events): void
    {
        $events->listen(
             \App\Events\MailListSubscription::class,
            [SendListNotificationToOwner::class, 'handleMailListSubscription']
        );

        $events->listen(
             \App\Events\MailListUnsubscription::class,
            [SendListNotificationToOwner::class, 'handleMailListUnsubscription']
        );
    }
}
