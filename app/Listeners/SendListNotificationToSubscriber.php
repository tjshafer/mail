<?php

namespace App\Listeners;

use Acelle\Events\MailListSubscription;
use Acelle\Events\MailListUnsubscription;

class SendListNotificationToSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handleMailListSubscription(MailListSubscription $event): void
    {
        $subscriber = $event->subscriber;
        $list = $subscriber->mailList;

        if ($list->send_welcome_email) {
            $list->sendSubscriptionWelcomeEmail($subscriber);
        }
    }

    /**
     * Handle the event.
     *
     * @param  MailListSubscription  $event
     */
    public function handleMailListUnsubscription(MailListUnsubscription $event): void
    {
        $subscriber = $event->subscriber;
        $list = $subscriber->mailList;

        if ($list->unsubscribe_notification) {
            $list->sendUnsubscriptionNotificationEmail($subscriber);
        }
    }

    /**
     * Handle the event.
     *
     * @param  AdminLoggedIn  $event
     */
    public function subscribe($events): void
    {
        $events->listen(
             \App\Events\MailListSubscription::class,
            [SendListNotificationToSubscriber::class, 'handleMailListSubscription']
        );

        $events->listen(
             \App\Events\MailListUnsubscription::class,
            [SendListNotificationToSubscriber::class, 'handleMailListUnsubscription']
        );
    }
}
