<?php

namespace App\Listeners;

use Acelle\Events\MailListUpdated;

class MailListUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(MailListUpdated $event): void
    {
        dispatch(new  \App\Jobs\UpdateMailListJob($event->mailList));
    }
}
