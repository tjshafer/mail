<?php

namespace App\Listeners;

use Acelle\Events\MailListSubscription;
use Acelle\Events\MailListUnsubscription;
use Acelle\Library\Log as MailLog;
use App\Model\Automation2;

class TriggerAutomation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handleMailListSubscription(MailListSubscription $event): void
    {
        MailLog::info('[MailListSubscription::TriggerAutomation] triggered');
        $automations = $event->subscriber->mailList->automations;
        $automations = $automations->filter(fn($auto, $key) => $auto->isActive() && (
            $auto->getTriggerType() == Automation2::TRIGGER_TYPE_WELCOME_NEW_SUBSCRIBER
        ));

        foreach ($automations as $auto) {
            if (is_null($auto->getAutoTriggerFor($event->subscriber))) {
                $auto->initTrigger($event->subscriber);
            }
        }
    }

    /**
     * Handle the event.
     *
     * @param  MailListSubscription  $event
     */
    public function handleMailListUnsubscription(MailListUnsubscription $event): void
    {
        MailLog::info('[MailListUnsubscription::TriggerAutomation] triggered');
        $automations = $event->subscriber->mailList->automations;
        $automations = $automations->filter(fn($auto, $key) => $auto->isActive() && (
            $auto->getTriggerType() == Automation2::TRIGGER_TYPE_SAY_GOODBYE_TO_SUBSCRIBER
        ));

        foreach ($automations as $auto) {
            if (is_null($auto->getAutoTriggerFor($event->subscriber))) {
                $forceTriggerUnsubscribedContact = true;
                $auto->initTrigger($event->subscriber, $forceTriggerUnsubscribedContact);
            }
        }
    }

    // Subscribe to many events
    public function subscribe($events): void
    {
        $events->listen(
             \App\Events\MailListSubscription::class,
            [TriggerAutomation::class, 'handleMailListSubscription']
        );

        $events->listen(
             \App\Events\MailListUnsubscription::class,
            [TriggerAutomation::class, 'handleMailListUnsubscription']
        );
    }
}
