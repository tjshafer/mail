<?php

namespace App\Framework;

use Illuminate\Http\Request as BaseRequest;

class LaravelRequest extends BaseRequest
{
    /**
     * Create a new Illuminate HTTP request from server variables.
     */
    public static function capture($uri = null): static
    {
        static::enableHttpMethodParameterOverride();

        return static::createFromBase(SymfonyRequest::createFromGlobals($uri));
    }
}
