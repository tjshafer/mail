<?php

use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Acelle\User::class, fn(Faker\Generator $faker) => [
    'name' => $faker->name(),
    'email' => $faker->safeEmail(),
    'password' => bcrypt(Str::random(10)),
    'remember_token' => Str::random(10),
]);
