<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->integer('automation2_id')->unsigned();
            $table->string('subject');
            $table->string('from');
            $table->string('from_name');
            $table->string('reply_to');
            $table->text('content');

            $table->timestamps();

            $table->foreign('automation2_id')->references('id')->on('automation2s')->onDelete('cascade');
        });
    }
};
