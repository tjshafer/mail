<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_group_sending_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sending_server_id');
            $table->string('customer_group_id');
            $table->integer('fitness');

            $table->timestamps();
        });
    }
};
