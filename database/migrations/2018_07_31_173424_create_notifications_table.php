<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        try {
            Schema::create('notifications', function (Blueprint $table) {
                $table->increments('id');
                $table->uuid('uid');
                $table->text('type');
                $table->text('title');
                $table->text('message');
                $table->text('level');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('customer_id')->unsigned()->nullable();

                $table->timestamps();

                $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
                $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            });
        } catch (\Exception $ex) {
            //
        }
    }
};
