<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        \App\Model\Setting::set('recurring_charge_before_days', 2);
        \App\Model\Setting::set('renew_free_plan', 'yes');
        \App\Model\Setting::set('end_period_last_days', 10);
    }
};
