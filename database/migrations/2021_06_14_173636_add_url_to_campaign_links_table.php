<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('campaign_links', function (Blueprint $table) {
            $table->text('url');
        });

        try {
            Schema::table('campaign_links', function (Blueprint $table) {
                $table->dropForeign(['link_id']);
            });
        } catch (\Exception $ex) {
            // ignore
        }

        try {
            Schema::table('campaign_links', function (Blueprint $table) {
                $table->dropColumn(['link_id']);
            });
        } catch (\Exception $ex) {
            // ignore
        }

        Schema::dropIfExists('links');
    }
};
