<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('field_options', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->integer('field_id')->unsigned();
            $table->string('label');
            $table->string('value');

            $table->timestamps();

            // foreign
            $table->foreign('field_id')->references('id')->on('fields')->onDelete('cascade');
        });
    }
};
