<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('segment_conditions', function (Blueprint $table) {
            $table->integer('field_id')->unsigned()->nullable()->change();
        });
    }
};
