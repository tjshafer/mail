<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->integer('default_mail_list_id')->unsigned()->nullable();

            $table->foreign('default_mail_list_id')->references('id')->on('mail_lists')->onDelete('cascade');
        });
    }
};
