<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('sending_servers', function (Blueprint $table) {
            $table->text('api_secret_key')->nullable();
        });
    }
};
