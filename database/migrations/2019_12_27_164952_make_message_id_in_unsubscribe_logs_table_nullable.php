<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('unsubscribe_logs', function (Blueprint $table) {
            $table->dropForeign(['message_id']);
        });

        Schema::table('unsubscribe_logs', function (Blueprint $table) {
            $table->string('message_id')->nullable()->change();
        });

        Schema::table('unsubscribe_logs', function (Blueprint $table) {
            $table->foreign('message_id')->references('message_id')->on('tracking_logs')->onDelete('cascade');
        });
    }
};
