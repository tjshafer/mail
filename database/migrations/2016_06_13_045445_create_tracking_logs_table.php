<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tracking_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('runtime_message_id')->unique()->nullable();
            $table->string('message_id')->unique()->nullable();
            $table->integer('customer_id')->unsigned(); // deliberate redundant for quick retrieving
            $table->integer('sending_server_id')->unsigned();
            $table->integer('campaign_id')->unsigned(); // deliberate redundant for quick retrieving
            $table->integer('subscriber_id')->unsigned(); // deliberate redundant for quick retrieving
            $table->string('status');
            $table->string('error')->nullable();

            $table->timestamps();
        });
    }
};
