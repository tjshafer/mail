<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Just return if running in console (when $request object is unknown)
        if (App::runningInConsole()) {
            return;
        }

        reset_app_url();
    }
};
