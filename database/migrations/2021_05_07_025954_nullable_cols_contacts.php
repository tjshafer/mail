<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('address_1')->nullable()->change();
            $table->integer('country_id')->unsigned()->nullable()->change();
            $table->string('state')->nullable()->change();
            $table->string('zip')->nullable()->change();
        });
    }
};
