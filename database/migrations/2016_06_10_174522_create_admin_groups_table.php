<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admin_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('options');
            $table->text('permissions');
            $table->integer('custom_order')->default(0);
            $table->integer('creator_id')->unsigned()->nullable();

            $table->timestamps();

            // foreign
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');
        });
    }
};
