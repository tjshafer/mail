<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        try {
            Schema::table('campaigns', function (Blueprint $table) {
                $table->integer('tracking_domain_id')->unsigned()->nullable();
            });
        } catch (\Exception $ex) {
            //
        }
    }
};
