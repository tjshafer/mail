<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('emails', function (Blueprint $table) {
            $table->integer('customer_id')->unsigned()->default(0);
        });

        \DB::statement(sprintf('UPDATE %s e SET customer_id = (SELECT customer_id FROM %s a WHERE a.id = e.automation2_id)', table('emails'), table('automation2s')));

        Schema::table('emails', function (Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }
};
