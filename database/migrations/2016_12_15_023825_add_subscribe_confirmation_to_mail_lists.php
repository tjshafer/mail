<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('mail_lists', function (Blueprint $table) {
            $table->boolean('subscribe_confirmation')->default(true);
        });
    }
};
