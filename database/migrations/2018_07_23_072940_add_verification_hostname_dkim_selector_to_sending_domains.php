<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('sending_domains', function (Blueprint $table) {
            $table->string('verification_hostname')->default('acellemail');
            $table->string('dkim_selector')->default('mailer');
        });
    }
};
