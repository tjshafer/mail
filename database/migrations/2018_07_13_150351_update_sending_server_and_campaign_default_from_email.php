<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $servers = \App\Model\SendingServer::whereRaw("default_from_email = '' or default_from_email is null")->get();
        foreach ($servers as $server) {
            $server->default_from_email = 'default@localhost.localdomain';
            $server->save();
        }

        $campaigns = \App\Model\Campaign::whereRaw('use_default_sending_server_from_email is null')->get();
        foreach ($campaigns as $campaign) {
            $campaign->use_default_sending_server_from_email = false;
            $campaign->save();
        }
    }
};
