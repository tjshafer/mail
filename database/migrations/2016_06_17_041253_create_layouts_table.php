<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('layouts', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->string('alias');
            $table->string('group_name');
            $table->longtext('content');
            $table->string('type');

            $table->timestamps();
        });
    }
};
