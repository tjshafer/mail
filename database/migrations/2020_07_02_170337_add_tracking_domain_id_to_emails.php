<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('emails', function (Blueprint $table) {
            $table->bigInteger('tracking_domain_id')->unsigned()->nullable();
            $table->foreign('tracking_domain_id')->references('id')->on('tracking_domains')->onDelete('set null');
        });
    }
};
