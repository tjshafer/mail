<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
         \App\Library\Facades\Billing::enablePaymentGateway('offline');
    }
};
