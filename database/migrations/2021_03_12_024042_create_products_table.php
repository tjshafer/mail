<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->integer('customer_id')->unsigned();
            $table->integer('source_id')->unsigned();
            $table->string('title')->nullable();
            $table->mediumText('description')->nullable();
            $table->longtext('content')->nullable();
            $table->double('price', 16, 2)->nullable();
            $table->timestamps();

            $table->string('source_item_id')->nullable();
            $table->longText('meta')->nullable();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('source_id')->references('id')->on('sources')->onDelete('cascade');
        });
    }
};
