<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $customers = \App\Model\Customer::all();
        foreach ($customers as $customer) {
            $customer->quota = null;
            $customer->save();
        }
    }
};
