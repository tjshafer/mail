<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->integer('mail_list_id')->unsigned();
            $table->string('email');
            $table->string('status');
            $table->string('from');
            $table->string('ip');

            $table->timestamps();

            // foreign
            $table->foreign('mail_list_id')->references('id')->on('mail_lists')->onDelete('cascade');
        });
    }
};
