<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('sending_domains', function (Blueprint $table) {
            $table->text('verification_hostname')->default(null)->nullable()->change();
            $table->text('dkim_selector')->default(null)->nullable()->change();
        });
    }
};
