<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tracking_logs', function (Blueprint $table) {
            $table->dropForeign(['campaign_id']);

            $table->integer('email_id')->unsigned()->nullable();
            $table->foreign('email_id')->references('id')->on('emails')->onDelete('cascade');
        });

        Schema::table('tracking_logs', function (Blueprint $table) {
            $table->integer('campaign_id')->unsigned()->nullable()->change();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
        });
    }
};
