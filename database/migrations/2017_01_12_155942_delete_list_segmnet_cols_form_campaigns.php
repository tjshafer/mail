<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign(['mail_list_id']);
            $table->dropColumn('mail_list_id');

            $table->dropForeign(['segment_id']);
            $table->dropColumn('segment_id');
        });
    }
};
