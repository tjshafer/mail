<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plans_sending_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sending_server_id')->unsigned();
            $table->integer('plan_id')->unsigned();
            $table->integer('fitness');

            $table->timestamps();

            $table->foreign('sending_server_id', table('pss_sending_server_id_fk'))->references('id')->on('sending_servers')->onDelete('cascade');
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('cascade');
        });
    }
};
