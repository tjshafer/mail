<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plans_email_verification_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id')->unsigned();
            $table->integer('plan_id')->unsigned();

            $table->timestamps();

            $table->foreign('server_id', table('pevs_server_id_fk'))->references('id')->on('email_verification_servers')->onDelete('cascade');
            $table->foreign('plan_id', table('pevs_plan_id_fk'))->references('id')->on('plans')->onDelete('cascade');
        });
    }
};
