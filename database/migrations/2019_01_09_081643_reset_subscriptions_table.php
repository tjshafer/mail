<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('subscriptions', function ($table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->string('user_id');
            $table->string('plan_id');
            $table->string('status');
            $table->timestamp('current_period_ends_at')->nullable();
            $table->timestampTz('trial_ends_at')->nullable();
            $table->timestampTz('ends_at')->nullable();

            $table->timestamps();
        });
    }
};
