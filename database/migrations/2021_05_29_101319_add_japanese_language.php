<?php

use App\Model\Language;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (! Language::where('code', 'ja')->exists()) {
            $japanese = new Language();
            $japanese->name = '日本語 (Japanese)';
            $japanese->code = 'ja';
            $japanese->region_code = 'ja';
            $japanese->status = Language::STATUS_ACTIVE;
            $japanese->save();
        }
    }
};
