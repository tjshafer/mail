<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->text('template_source')->change();
            $table->text('status')->change();
        });

        Schema::table('campaigns', function (Blueprint $table) {
            $table->text('preheader')->nullable();
        });
    }
};
