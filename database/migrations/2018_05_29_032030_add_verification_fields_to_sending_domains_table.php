<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        try {
            Schema::table('sending_domains', function (Blueprint $table) {
                $table->text('verification_token')->nullable();
                $table->boolean('domain_verified')->default(false);
                $table->boolean('dkim_verified')->default(false);
                $table->boolean('spf_verified')->default(false);
            });
        } catch (\Exception $ex) {
            //
        }

        try {
            $domains = \App\Model\SendingDomain::whereNull('verification_token')->get();
            foreach ($domains as $domain) {
                $domain->generateVerificationToken();
                $domain->save();
            }
        } catch (\Exception $ex) {
            //
        }
    }
};
