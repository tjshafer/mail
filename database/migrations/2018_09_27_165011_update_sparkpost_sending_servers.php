<?php

use App\Model\SendingServer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $sparkies = SendingServer::where('type', 'sparkpost-api')->whereNull('host')->get();
        foreach ($sparkies as $sparky) {
            $sparky->host = 'api.sparkpost.com';
            $sparky->save();
        }
    }
};
