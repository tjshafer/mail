<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('subscription_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uid');
            $table->integer('subscription_id')->unsigned();
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->string('type');
            $table->text('data');

            $table->timestamps();

            $table->foreign('subscription_id', table('sl_subscription_id_fk'))->references('id')->on('subscriptions')->onDelete('cascade');
        });
    }
};
