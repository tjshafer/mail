<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement('ALTER TABLE '.\DB::getTablePrefix().'subscriptions CHANGE `ends_at` `ends_at` datetime');
        DB::statement('ALTER TABLE '.\DB::getTablePrefix().'subscriptions CHANGE `current_period_ends_at` `current_period_ends_at` datetime');

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('timezone');
        });
    }
};
