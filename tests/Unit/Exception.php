<?php

namespace Tests\Unit;

use Tests\TestCase;

class Exception extends TestCase
{
    /**
     * A basic test example.
     */
    public function testExample(): void
    {
        $this->expectException(\ErrorException::class);
        preg_replace('/'.preg_quote($this->generateRandomString(100000), '/').'/', '', '');
    }

    public function generateRandomString($length = 10): string
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }
}
